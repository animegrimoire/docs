# Animegrimoire DMCA Information

Animegrimoire respects the intellectual property rights of others. It is therefore, our policy to expeditiously process, investigate, and address notices of alleged infringement, and to take appropriate action under the DMCA and other relevant intellectual property laws.


If you are a copyright owner or an agent thereof and you believe that any content hosted on our servers infringes your copyrights, then you may submit a notification pursuant to the Digital Millennium Copyright Act (DMCA) by providing our Designated Copyright Agent with the following information in writing to dmca (at) animegrimoire (dot) moe.


1. A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.
2. Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit Animegrimoire to locate the material. 
3. Providing URLs in the body of an email is the only way to locate those files.
4. URLs in every DMCA notice have to be no more than 10.
5. Information reasonably sufficient to permit Animegrimoire to contact the complaining party, such as an address, telephone number, and, if available, an electronic mail address at which the complaining party may be contacted.
6. A statement that the complaining party has a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law.
7. A statement that the information in the notification is accurate, and under penalty of perjury, that the complaining party is authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.A scanned physical document that proves you are or you are representing the copyright owner.
8. A valid email address username@copyrightcompany will help you to prove you're representing or you're the legitimate copyright owner. We will not process requests from free mailboxes (Gmail, Hotmail, Yahoo, AOL, etc)
9. All received emails have to be in plain text, no formatting or html, etc.
10. All mails should be sent to dmca (at) animegrimoire (dot) moe or else they wont be processed. (Example: mailing our (I)SPs means we wont process your requests)
11. We only delete the actual file(s) from our servers, any cache between our users and our hardware will expire after we remove the file from our servers.
12. Personal or attorneys notices will not be processed. They have to come from companies or dmca/eipred agents.

##### Please note that under Section 512(f) of the DMCA, any person who knowingly materially misrepresents that material or activity is infringing may be subject to liability. Consult your legal counsel or See 17 U.S.C. Section 512(c)(3) to confirm these requirements.
