# The Grimoire Archive

```
Archive
├── 2004
│   └── Fall
│       └── Kannazuki no Miko
│           ├── [animegrimoire] Kannazuki no Miko - 01 [576p][F82AD461].mp4
│           ├── [animegrimoire] Kannazuki no Miko - 02 [576p][0A6D0D7C].mp4
│           ├── [animegrimoire] Kannazuki no Miko - 03 [576p][8B5D225B].mp4
│           ├── [animegrimoire] Kannazuki no Miko - 04 [576p][B4764ACA].mp4
│           ├── [animegrimoire] Kannazuki no Miko - 05 [576p][6CAC225F].mp4
│           ├── [animegrimoire] Kannazuki no Miko - 06 [576p][0389DDF6].mp4
│           ├── [animegrimoire] Kannazuki no Miko - 07 [576p][C9590B81].mp4
│           ├── [animegrimoire] Kannazuki no Miko - 08 [576p][DE1A1797].mp4
│           ├── [animegrimoire] Kannazuki no Miko - 09 [576p][D4B1B0AF].mp4
│           ├── [animegrimoire] Kannazuki no Miko - 10 [576p][C075855E].mp4
│           ├── [animegrimoire] Kannazuki no Miko - 11 [576p][DBC55D1D].mp4
│           └── [animegrimoire] Kannazuki no Miko - 12 [576p][624069C9].mp4
├── 2005
│   └── Summer
│       └── Kamichu!
│           ├── [animegrimoire] Kamichu! - 01 [BD720p][229CD4A9].mp4
│           ├── [animegrimoire] Kamichu! - 02 [BD720p][E2B67988].mp4
│           ├── [animegrimoire] Kamichu! - 03 [BD720p][AADB6CF0].mp4
│           ├── [animegrimoire] Kamichu! - 04 [BD720p][02B663B2].mp4
│           ├── [animegrimoire] Kamichu! - 05 [BD720p][725BCF58].mp4
│           ├── [animegrimoire] Kamichu! - 06 [BD720p][E5EB6B93].mp4
│           ├── [animegrimoire] Kamichu! - 07 [BD720p][0876B9C5].mp4
│           ├── [animegrimoire] Kamichu! - 08 [BD720p][064CC31D].mp4
│           ├── [animegrimoire] Kamichu! - 09 [BD720p][095BECB3].mp4
│           ├── [animegrimoire] Kamichu! - 10 [BD720p][3AE02A36].mp4
│           ├── [animegrimoire] Kamichu! - 11 [BD720p][70081AF0].mp4
│           ├── [animegrimoire] Kamichu! - 12 [BD720p][F1CC8CC5].mp4
│           ├── [animegrimoire] Kamichu! - 13 [BD720p][E0A58473].mp4
│           ├── [animegrimoire] Kamichu! - 14 [BD720p][EB9CCB94].mp4
│           ├── [animegrimoire] Kamichu! - 15 [BD720p][B493E03E].mp4
│           └── [animegrimoire] Kamichu! - 16 [BD720p][B91802C7].mp4
├── 2006
│   └── Spring
│       └── Suzumiya Haruhi no Yuuutsu
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 01 [BD720P][3495B0AE].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 02 [BD720P][7E80EFB8].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 03 [BD720P][06E37A16].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 04 [BD720P][E9818621].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 05 [BD720P][1CF8BA8D].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 06 [BD720P][C0914F94].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 07 [BD720P][9A6B7D15].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 08 [BD720P][451E9F42].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 09 [BD720P][D5C32FBD].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 10 [BD720P][B9E5929E].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 11 [BD720P][5B3E8597].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 12 [BD720P][FC93302E].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 13 [BD720P][1EEC48F3].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 14 [BD720P][81B4A115].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 15 [BD720P][5780CE39].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 16 [BD720P][6D5AA755].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 17 [BD720P][25300C16].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 18 [BD720P][993F409B].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 19 [BD720P][64D1DA64].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 20 [BD720P][4103C56D].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 21 [BD720P][5BB1A41A].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 22 [BD720P][F07F4836].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 23 [BD720P][9F0CE26A].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 24 [BD720P][7A57A477].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 25 [BD720P][42669439].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 26 [BD720P][00F00FDF].mp4
│           ├── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 27 [BD720P][45872D4A].mp4
│           └── [animegrimoire] Suzumiya Haruhi no Yuuutsu - 28 [BD720P][6BEBEF66].mp4
├── 2007
│   ├── Spring
│   │   └── Lucky Star
│   │       ├── [animegrimoire] Lucky Star - 01 [BD720p][743FD949].mp4
│   │       ├── [animegrimoire] Lucky Star - 02 [BD720p][3A1513D5].mp4
│   │       ├── [animegrimoire] Lucky Star - 03 [BD720p][9BBC77B1].mp4
│   │       ├── [animegrimoire] Lucky Star - 04 [BD720p][EFADA332].mp4
│   │       ├── [animegrimoire] Lucky Star - 05 [BD720p][4FE44A41].mp4
│   │       ├── [animegrimoire] Lucky Star - 06 [BD720p][1532CF54].mp4
│   │       ├── [animegrimoire] Lucky Star - 07 [BD720p][40870ED6].mp4
│   │       ├── [animegrimoire] Lucky Star - 08 [BD720p][17581858].mp4
│   │       ├── [animegrimoire] Lucky Star - 09 [BD720p][AE8E0E17].mp4
│   │       ├── [animegrimoire] Lucky Star - 10 [BD720p][A4B3A5AB].mp4
│   │       ├── [animegrimoire] Lucky Star - 11 [BD720p][82FBBC3B].mp4
│   │       ├── [animegrimoire] Lucky Star - 12 [BD720p][C9AFF4A9].mp4
│   │       ├── [animegrimoire] Lucky Star - 13 [BD720p][7D9FBD99].mp4
│   │       ├── [animegrimoire] Lucky Star - 14 [BD720p][F0D985D7].mp4
│   │       ├── [animegrimoire] Lucky Star - 15 [BD720p][FADBC76C].mp4
│   │       ├── [animegrimoire] Lucky Star - 16 [BD720p][46EAB491].mp4
│   │       ├── [animegrimoire] Lucky Star - 17 [BD720p][E7A6375A].mp4
│   │       ├── [animegrimoire] Lucky Star - 18 [BD720p][26BB4F70].mp4
│   │       ├── [animegrimoire] Lucky Star - 19 [BD720p][FC3832F2].mp4
│   │       ├── [animegrimoire] Lucky Star - 20 [BD720p][CFD15D6B].mp4
│   │       ├── [animegrimoire] Lucky Star - 21 [BD720p][34892FF0].mp4
│   │       ├── [animegrimoire] Lucky Star - 22 [BD720p][3CC451E2].mp4
│   │       ├── [animegrimoire] Lucky Star - 23 [BD720p][30EAC2D5].mp4
│   │       └── [animegrimoire] Lucky Star - 24 [BD720p][2099C5FB].mp4
│   ├── Summer
│   │   └── Moetan
│   │       ├── [animegrimoire] Moetan - 01 [BD720p][C0C86B39].mp4
│   │       ├── [animegrimoire] Moetan - 02 [BD720p][EE35CBA9].mp4
│   │       ├── [animegrimoire] Moetan - 03 [BD720p][E4BAC290].mp4
│   │       ├── [animegrimoire] Moetan - 04 [BD720p][8B6247A4].mp4
│   │       ├── [animegrimoire] Moetan - 05.5 [BD720p][C1E42157].mp4
│   │       ├── [animegrimoire] Moetan - 05 [BD720p][CD4577AD].mp4
│   │       ├── [animegrimoire] Moetan - 06 [BD720p][0C06CAF1].mp4
│   │       ├── [animegrimoire] Moetan - 07 [BD720p][C23FB48E].mp4
│   │       ├── [animegrimoire] Moetan - 08 [BD720p][A508946C].mp4
│   │       ├── [animegrimoire] Moetan - 09 [BD720p][AEED1A67].mp4
│   │       ├── [animegrimoire] Moetan - 10 [BD720p][C23F3961].mp4
│   │       ├── [animegrimoire] Moetan - 11 [BD720p][14E0787F].mp4
│   │       ├── [animegrimoire] Moetan - 12 [BD720p][78DFE6A9].mp4
│   │       └── [animegrimoire] Moetan - 13 [BD720p][C941E126].mp4
│   └── Winter
│       └── Gakuen Utopia Manabi Straight!
│           ├── [animegrimoire] Gakuen Utopia Manabi Straight! - 01 [BD576p][4D17DF03].mp4
│           ├── [animegrimoire] Gakuen Utopia Manabi Straight! - 02 [BD576p][D58AC06A].mp4
│           ├── [animegrimoire] Gakuen Utopia Manabi Straight! - 03 [BD576p][21A85B14].mp4
│           ├── [animegrimoire] Gakuen Utopia Manabi Straight! - 04 [BD576p][A5049A49].mp4
│           ├── [animegrimoire] Gakuen Utopia Manabi Straight! - 05 [BD576p][2495F35F].mp4
│           ├── [animegrimoire] Gakuen Utopia Manabi Straight! - 06 [BD576p][CCB0B01F].mp4
│           ├── [animegrimoire] Gakuen Utopia Manabi Straight! - 07 [BD576p][7E3D1E2E].mp4
│           ├── [animegrimoire] Gakuen Utopia Manabi Straight! - 08 [BD576p][8ECD9F7E].mp4
│           ├── [animegrimoire] Gakuen Utopia Manabi Straight! - 09 [BD576p][563F1CB3].mp4
│           ├── [animegrimoire] Gakuen Utopia Manabi Straight! - 10 [BD576p][1CE15948].mp4
│           ├── [animegrimoire] Gakuen Utopia Manabi Straight! - 11 [BD576p][7AC821C9].mp4
│           ├── [animegrimoire] Gakuen Utopia Manabi Straight! - 12 [BD576p][11E7AC87].mp4
│           └── [animegrimoire] Gakuen Utopia Manabi Straight! - 13 [BD576p][690D9904].mp4
├── 2008
│   ├── Fall
│   │   └── Kannagi
│   │       ├── [animegrimoire] Kannagi - 01 [BD720p][B182A790].mp4
│   │       ├── [animegrimoire] Kannagi - 02 [BD720p][72B663C8].mp4
│   │       ├── [animegrimoire] Kannagi - 03 [BD720p][AA2F1E4D].mp4
│   │       ├── [animegrimoire] Kannagi - 04 [BD720p][E7C2450E].mp4
│   │       ├── [animegrimoire] Kannagi - 05 [BD720p][9CC11CED].mp4
│   │       ├── [animegrimoire] Kannagi - 06 [BD720p][BE135F2B].mp4
│   │       ├── [animegrimoire] Kannagi - 07 [BD720p][23C4B844].mp4
│   │       ├── [animegrimoire] Kannagi - 08 [BD720p][4AB554E1].mp4
│   │       ├── [animegrimoire] Kannagi - 09 [BD720p][1FB1EE0E].mp4
│   │       ├── [animegrimoire] Kannagi - 10 [BD720p][476A88D7].mp4
│   │       ├── [animegrimoire] Kannagi - 11 [BD720p][EBE349D4].mp4
│   │       ├── [animegrimoire] Kannagi - 12 [BD720p][D9D489FF].mp4
│   │       ├── [animegrimoire] Kannagi - 13 [BD720p][DCF45900].mp4
│   │       └── [animegrimoire] Kannagi - 14 [BD720p][5A53682F].mp4
│   └── Spring
│       └── Candy Boy
│           ├── [animegrimoire] Candy Boy - 00 [576p][62FAA706].mp4
│           ├── [animegrimoire] Candy Boy - 01 [576p][8726FAC5].mp4
│           ├── [animegrimoire] Candy Boy - 02 [576p][F1C0C17D].mp4
│           ├── [animegrimoire] Candy Boy - 02.5 (EX1) [576p][20F3C793].mp4
│           ├── [animegrimoire] Candy Boy - 03 [576p][56917E4E].mp4
│           ├── [animegrimoire] Candy Boy - 04 [576p][6B435BE8].mp4
│           ├── [animegrimoire] Candy Boy - 05 [576p][D0EE8938].mp4
│           ├── [animegrimoire] Candy Boy - 06 [576p][FA4F75CD].mp4
│           ├── [animegrimoire] Candy Boy - 07 [576p][BADE69FB].mp4
│           └── [animegrimoire] Candy Boy - 08 (EX2) [576p][2A365A08].mp4
├── 2009
│   ├── Fall
│   │   ├── Kiddy Girl-and
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 01 [BD720p][45112039].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 02 [BD720p][8541B59C].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 03 [BD720p][5C185D3C].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 04 [BD720p][87075F0A].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 05 [BD720p][68EDB1A1].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 06 [BD720p][34EA79BE].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 07 [BD720p][C6B46986].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 08 [BD720p][E506DAEF].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 09 [BD720p][4A618638].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 10 [BD720p][60C82A14].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 11 [BD720p][BBEEE836].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 12 [BD720p][6BC64D65].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 13 [BD720p][C03B614B].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 14 [BD720p][54E5313C].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 15 [BD720p][381C8A6B].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 16 [BD720p][FF971BF6].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 17 [BD720p][5F4256A7].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 18 [BD720p][84987825].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 19 [BD720p][226EC0BB].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 20 [BD720p][D5CD14E6].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 21 [BD720p][FCBE9EF3].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 22 [BD720p][29CB7ABA].mp4
│   │   │   ├── [animegrimoire] Kiddy Girl-and - 23 [BD720p][100BB3EE].mp4
│   │   │   └── [animegrimoire] Kiddy Girl-and - 24 [BD720p][81E11167].mp4
│   │   └── Sasameki Koto
│   │       ├── [animegrimoire] Sasameki Koto - 01 [BD720p][85096889].mp4
│   │       ├── [animegrimoire] Sasameki Koto - 02 [BD720p][0E18826C].mp4
│   │       ├── [animegrimoire] Sasameki Koto - 03 [BD720p][EE7B3014].mp4
│   │       ├── [animegrimoire] Sasameki Koto - 04 [BD720p][EF13086C].mp4
│   │       ├── [animegrimoire] Sasameki Koto - 05 [BD720p][0C5EA5CD].mp4
│   │       ├── [animegrimoire] Sasameki Koto - 06 [BD720p][B949779D].mp4
│   │       ├── [animegrimoire] Sasameki Koto - 07 [BD720p][BA6CAEE1].mp4
│   │       ├── [animegrimoire] Sasameki Koto - 08 [BD720p][8BD53B63].mp4
│   │       ├── [animegrimoire] Sasameki Koto - 09 [BD720p][869351CD].mp4
│   │       ├── [animegrimoire] Sasameki Koto - 10 [BD720p][F47B5D8B].mp4
│   │       ├── [animegrimoire] Sasameki Koto - 11 [BD720p][A234BD76].mp4
│   │       ├── [animegrimoire] Sasameki Koto - 12 [BD720p][74EF63E2].mp4
│   │       └── [animegrimoire] Sasameki Koto - 13 [BD720p][814AA06C].mp4
│   ├── Spring
│   │   ├── Fullmetal Alchemist Brotherhood
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 01 [BD720p][76A20227].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 01 [BD720p][A982C6D5].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 02 [BD720p][4F1764FE].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 02 [BD720p][B4FCD5A8].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 03 [BD720p][00EB2099].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 04 [BD720p][C32A4F26].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 05 [BD720p][D91F740B].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 06 [BD720p][C30FC29D].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 07 [BD720p][68D79945].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 08 [BD720p][C0E49911].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 09 [BD720p][58D84B9A].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 10 [BD720p][D0F7A593].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 11 [BD720p][A88E2FA8].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 12 [BD720p][2ED96884].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 13 [BD720p][EEB17B6F].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 14 [BD720p][8C1686F1].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 15 [BD720p][C482FF52].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 16 [BD720p][86A359BE].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 17 [BD720p][3460F08A].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 18 [BD720p][20414538].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 19 [BD720p][3B72A196].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 20 [BD720p][A8BDA7C6].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 21 [BD720p][3EA9D47C].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 22 [BD720p][686E765A].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 23 [BD720p][ADC60648].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 24 [BD720p][DD709B3C].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 25 [BD720p][018BD22C].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 26 [BD720p][3B065EAA].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 27 [BD720p][9D999CA6].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 28 [BD720p][6C899802].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 29 [BD720p][547F392E].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 30 [BD720p][7DAAC7F1].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 31 [BD720p][D3A5F85F].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 32 [BD720p][EB5C58B6].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 33 [BD720p][EE6D582F].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 34 [BD720p][4FDD77DD].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 35 [BD720p][E4C3F09B].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 36 [BD720p][7EFF3195].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 37 [BD720p][132A93B1].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 38 [BD720p][4BA45631].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 39 [BD720p][95E73F67].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 40 [BD720p][8FF16645].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 41 [BD720p][7D2CCF9D].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 42 [BD720p][E47FE4AD].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 43 [BD720p][5D6BA2AE].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 44 [BD720p][6FD60F8C].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 45 [BD720p][C35EFA4B].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 46 [BD720p][D25D8DD2].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 47 [BD720p][43333449].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 48 [BD720p][30F36899].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 49 [BD720p][624FA7FC].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 50 [BD720p][36945246].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 51 [BD720p][6C185413].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 52 [BD720p][586C01B0].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 53 [BD720p][0613FAEE].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 54 [BD720p][D044164F].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 55 [BD720p][AD90505E].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 56 [BD720p][FC0A2AEB].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 57 [BD720p][C3F115DF].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 58 [BD720p][E6A1AED8].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 59 [BD720p][4D02555E].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 60 [BD720p][E3CE6DE0].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 61 [BD720p][95DA9F09].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 62 [BD720p][7EEA54F7].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 63 [BD720p][E71B6FFB].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 64 [BD720p][F6CABA53].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 65 [BD720p][9C3070D7].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 66 [BD720p][44D94FF9].mp4
│   │   │   ├── [animegrimoire] Fullmetal Alchemist Brotherhood - 67 [BD720p][FCBB319C].mp4
│   │   │   └── [animegrimoire] Fullmetal Alchemist Brotherhood - 68 [BD720p][2C8DD14E].mp4
│   │   └── K-on!
│   │       ├── [animegrimoire] K-On! - 01 [BD720p][35FA3508].mp4
│   │       ├── [animegrimoire] K-On! - 02 [BD720p][F7014E1A].mp4
│   │       ├── [animegrimoire] K-On! - 03 [BD720p][1D14A4F9].mp4
│   │       ├── [animegrimoire] K-On! - 04 [BD720p][00438625].mp4
│   │       ├── [animegrimoire] K-On! - 05 [BD720p][1A6EFA84].mp4
│   │       ├── [animegrimoire] K-On! - 06 [BD720p][7F2229E6].mp4
│   │       ├── [animegrimoire] K-On! - 07 [BD720p][767BFF15].mp4
│   │       ├── [animegrimoire] K-On! - 08 [BD720p][D27BC43A].mp4
│   │       ├── [animegrimoire] K-On! - 09 [BD720p][EEED0CBD].mp4
│   │       ├── [animegrimoire] K-On! - 10 [BD720p][417EBF57].mp4
│   │       ├── [animegrimoire] K-On! - 11 [BD720p][08EA6BA8].mp4
│   │       ├── [animegrimoire] K-On! - 12 [BD720p][3677C240].mp4
│   │       ├── [animegrimoire] K-On! - 13 [BD720p][C2845E19].mp4
│   │       └── [animegrimoire] K-On! - 14 [BD720p][E63480A3].mp4
│   ├── Summer
│   │   └── Bakemonogatari
│   │       ├── [animegrimoire] Bakemonogatari - 01 [BD720p][7C8F8183].mp4
│   │       ├── [animegrimoire] Bakemonogatari - 02 [BD720p][35673F81].mp4
│   │       ├── [animegrimoire] Bakemonogatari - 03 [BD720p][2B3D72C3].mp4
│   │       ├── [animegrimoire] Bakemonogatari - 04 [BD720p][AE9B6A35].mp4
│   │       ├── [animegrimoire] Bakemonogatari - 05 [BD720p][77322B2B].mp4
│   │       ├── [animegrimoire] Bakemonogatari - 06 [BD720p][62FB525F].mp4
│   │       ├── [animegrimoire] Bakemonogatari - 07 [BD720p][2DE8775F].mp4
│   │       ├── [animegrimoire] Bakemonogatari - 08 [BD720p][80350D43].mp4
│   │       ├── [animegrimoire] Bakemonogatari - 09 [BD720p][887949D1].mp4
│   │       ├── [animegrimoire] Bakemonogatari - 10 [BD720p][7B14873F].mp4
│   │       ├── [animegrimoire] Bakemonogatari - 11 [BD720p][864D4FDA].mp4
│   │       ├── [animegrimoire] Bakemonogatari - 12 [BD720p][BA3C80D1].mp4
│   │       ├── [animegrimoire] Bakemonogatari - 13 [BD720p][710EF492].mp4
│   │       ├── [animegrimoire] Bakemonogatari - 14 [BD720p][DD13CE47].mp4
│   │       └── [animegrimoire] Bakemonogatari - 15 [BD720p][C98ED9D6].mp4
│   └── Winter
│       ├── Sora wo Kakeru Shoujo
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 01 [BD720p][DAC265A0].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 02 [BD720p][D1EBC88B].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 03 [BD720p][9DADE176].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 04 [BD720p][7FC420DB].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 05 [BD720p][7CE2C3CE].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 06 [BD720p][23954265].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 07 [BD720p][456237BD].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 08 [BD720p][1C9308E0].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 09 [BD720p][70EB5C58].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 10 [BD720p][173DE461].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 11 [BD720p][EAFE17A8].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 12 [BD720p][440CAFA0].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 13 [BD720p][FE6EB301].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 14 [BD720p][709F22DF].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 15 [BD720p][C3BB55C4].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 16 [BD720p][D20CBD71].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 17 [BD720p][3065BDC1].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 18 [BD720p][5BDCFF0A].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 19 [BD720p][69B3DD73].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 20 [BD720p][BE7DEDA1].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 21 [BD720p][61D0C5CA].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 22 [BD720p][1C85EBDC].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 23 [BD720p][7D6AB011].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 24 [BD720p][0D0BBDE4].mp4
│       │   ├── [animegrimoire] Sora wo Kakeru Shoujo - 25 [BD720p][7143F573].mp4
│       │   └── [animegrimoire] Sora wo Kakeru Shoujo - 26 [BD720p][4ED089EB].mp4
│       └── Suzumiya Haruhi-chan no Yuuutsu
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 01 [BD720p][720FDE5E].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 02 [BD720p][F5C790D1].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 03 [BD720p][45CDC051].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 04 [BD720p][089909C9].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 05 [BD720p][9B5DA7A6].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 06 [BD720p][80EB4CE4].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 07 [BD720p][EA34616A].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 08 [BD720p][EA1C516E].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 09 [BD720p][857B12C8].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 10 [BD720p][F54F614C].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 11 [BD720p][CAD24F06].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 12 [BD720p][34FCA038].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 13 [BD720p][DFBB263C].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 14 [BD720p][CDFE5FB3].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 15 [BD720p][00337C7E].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 16 [BD720p][626E6010].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 17 [BD720p][9DF1311E].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 18 [BD720p][D1119E1F].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 19 [BD720p][EF2C602F].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 20 [BD720p][593FAFD0].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 21 [BD720p][A849E07B].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 22 [BD720p][DE3D9DF0].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 23 [BD720p][E2A529F3].mp4
│           ├── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 24 [BD720p][E670B06C].mp4
│           └── [animegrimoire] Suzumiya Haruhi-chan no Yuuutsu - 25 [BD720p][D1379292].mp4
├── 2010
│   ├── Fall
│   │   ├── MM!
│   │   │   ├── [animegrimoire] MM! - 01 [BD720p][B12FF384].mp4
│   │   │   ├── [animegrimoire] MM! - 02 [BD720p][724F074B].mp4
│   │   │   ├── [animegrimoire] MM! - 03 [BD720p][6916C70A].mp4
│   │   │   ├── [animegrimoire] MM! - 04 [BD720p][9746597F].mp4
│   │   │   ├── [animegrimoire] MM! - 05 [BD720p][51571FF8].mp4
│   │   │   ├── [animegrimoire] MM! - 06 [BD720p][E90BCDF9].mp4
│   │   │   ├── [animegrimoire] MM! - 07 [BD720p][6F1706E8].mp4
│   │   │   ├── [animegrimoire] MM! - 08 [BD720p][36369F26].mp4
│   │   │   ├── [animegrimoire] MM! - 09 [BD720p][B92A9375].mp4
│   │   │   ├── [animegrimoire] MM! - 10 [BD720p][CD0CD7E8].mp4
│   │   │   ├── [animegrimoire] MM! - 11 [BD720p][0EE8C6AE].mp4
│   │   │   └── [animegrimoire] MM! - 12 [BD720p][6D3B78A9].mp4
│   │   ├── Shinryaku! Ika Musume
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume - 01 [BD720p][AA1DCE31].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume - 02 [BD720p][C9A39FD5].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume - 03 [BD720p][5784F815].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume - 04 [BD720p][5D2B8C98].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume - 05 [BD720p][698E785C].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume - 06 [BD720p][43BDFEFD].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume - 07 [BD720p][11C5C5F2].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume - 08 [BD720p][8FC1C5E9].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume - 09 [BD720p][C97B9245].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume - 10 [BD720p][FF5A3AA5].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume - 11 [BD720p][00C2FB00].mp4
│   │   │   └── [animegrimoire] Shinryaku! Ika Musume - 12 [BD720p][EBC2A2A5].mp4
│   │   ├── Tantei Opera Milky Holmes
│   │   │   ├── [animegrimoire] Tantei Opera Milky Holmes - 01 [720p][672D4A00].mp4
│   │   │   ├── [animegrimoire] Tantei Opera Milky Holmes - 02 [720p][99C7609B].mp4
│   │   │   ├── [animegrimoire] Tantei Opera Milky Holmes - 03 [720p][7FC4307D].mp4
│   │   │   ├── [animegrimoire] Tantei Opera Milky Holmes - 04 [720p][6BB4A9B6].mp4
│   │   │   ├── [animegrimoire] Tantei Opera Milky Holmes - 05 [720p][9EEE153B].mp4
│   │   │   ├── [animegrimoire] Tantei Opera Milky Holmes - 06 [720p][CA2CE3FD].mp4
│   │   │   ├── [animegrimoire] Tantei Opera Milky Holmes - 07 [720p][5C1AD58A].mp4
│   │   │   ├── [animegrimoire] Tantei Opera Milky Holmes - 08 [720p][17023A2B].mp4
│   │   │   ├── [animegrimoire] Tantei Opera Milky Holmes - 09 [720p][545B5208].mp4
│   │   │   ├── [animegrimoire] Tantei Opera Milky Holmes - 10 [720p][BFBFF910].mp4
│   │   │   ├── [animegrimoire] Tantei Opera Milky Holmes - 11 [720p][C22BFE6F].mp4
│   │   │   └── [animegrimoire] Tantei Opera Milky Holmes - 12 [720p][71617DCF].mp4
│   │   └── Yosuga no Sora
│   │       ├── [animegrimoire] Yosuga no Sora - 01 [BD720p][CC94803A].mp4
│   │       ├── [animegrimoire] Yosuga no Sora - 02 [BD720p][6965D940].mp4
│   │       ├── [animegrimoire] Yosuga no Sora - 03 [BD720p][45718E07].mp4
│   │       ├── [animegrimoire] Yosuga no Sora - 04 [BD720p][F533E964].mp4
│   │       ├── [animegrimoire] Yosuga no Sora - 05 [BD720p][9B862A54].mp4
│   │       ├── [animegrimoire] Yosuga no Sora - 06 [BD720p][9AB4BF03].mp4
│   │       ├── [animegrimoire] Yosuga no Sora - 07 [BD720p][A296380B].mp4
│   │       ├── [animegrimoire] Yosuga no Sora - 08 [BD720p][00267C62].mp4
│   │       ├── [animegrimoire] Yosuga no Sora - 09 [BD720p][53B7EE5A].mp4
│   │       ├── [animegrimoire] Yosuga no Sora - 10 [BD720p][7ACF640F].mp4
│   │       ├── [animegrimoire] Yosuga no Sora - 11 [BD720p][9D9B824B].mp4
│   │       └── [animegrimoire] Yosuga no Sora - 12 [BD720p][34C8DB2D].mp4
│   ├── Spring
│   │   ├── K-on!!
│   │   │   ├── [animegrimoire] K-ON!! - 01 [BD720p][7AAA3ED8].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 02 [BD720p][C540707C].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 03 [BD720p][A1814CBB].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 04 [BD720p][63292287].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 05 [BD720p][7E914594].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 06 [BD720p][50B94617].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 07 [BD720p][69233275].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 08 [BD720p][703C9AE2].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 09 [BD720p][86E8808F].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 10 [BD720p][F11206A2].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 11 [BD720p][49E9546A].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 12 [BD720p][AFFED591].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 13 [BD720p][FAD179E1].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 14 [BD720p][DDD73E49].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 15 [BD720p][463EE1D9].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 16 [BD720p][7F72284E].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 17 [BD720p][A1AA2075].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 18 [BD720p][7F083D9D].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 19 [BD720p][E6EF41EC].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 20 [BD720p][626802BC].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 21 [BD720p][EC88B608].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 22 [BD720p][51CF8CF3].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 23 [BD720p][58E70A24].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 24 [BD720p][6697DFD5].mp4
│   │   │   ├── [animegrimoire] K-ON!! - 25 [BD720p][053E8A6B].mp4
│   │   │   └── [animegrimoire] K-ON!! - 26 [BD720p][11855C27].mp4
│   │   └── Working
│   │       ├── [animegrimoire] Working! - 01 [480p][B0B395DE].mp4
│   │       ├── [animegrimoire] Working! - 02 [480p][17F25200].mp4
│   │       ├── [animegrimoire] Working! - 03 [480p][64BD860C].mp4
│   │       ├── [animegrimoire] Working! - 04 [480p][5B2BC1DE].mp4
│   │       ├── [animegrimoire] Working! - 05 [480p][9E64307E].mp4
│   │       ├── [animegrimoire] Working! - 06 [480p][B2F0E6B0].mp4
│   │       ├── [animegrimoire] Working! - 07 [480p][0CE5B0BB].mp4
│   │       ├── [animegrimoire] Working! - 08 [480p][9DA67A15].mp4
│   │       ├── [animegrimoire] Working! - 09 [480p][39B526A5].mp4
│   │       ├── [animegrimoire] Working! - 10 [480p][E51B5011].mp4
│   │       ├── [animegrimoire] Working! - 11 [480p][D256C62F].mp4
│   │       ├── [animegrimoire] Working! - 12 [480p][5DB8500A].mp4
│   │       └── [animegrimoire] Working! - 13 [480p][EC07ECC9].mp4
│   ├── Summer
│   │   └── Seitokai Yakuindomo
│   │       ├── [animegrimoire] Seitokai Yakuindomo - 01 [720p][BB7D9068].mp4
│   │       ├── [animegrimoire] Seitokai Yakuindomo - 02 [720p][9992E24C].mp4
│   │       ├── [animegrimoire] Seitokai Yakuindomo - 03 [720p][CAE537E6].mp4
│   │       ├── [animegrimoire] Seitokai Yakuindomo - 04 [720p][55CF303A].mp4
│   │       ├── [animegrimoire] Seitokai Yakuindomo - 05 [720p][9D380D75].mp4
│   │       ├── [animegrimoire] Seitokai Yakuindomo - 06 [720p][E6B41B39].mp4
│   │       ├── [animegrimoire] Seitokai Yakuindomo - 07 [720p][47430ED0].mp4
│   │       ├── [animegrimoire] Seitokai Yakuindomo - 08 [720p][0ECC1FB7].mp4
│   │       ├── [animegrimoire] Seitokai Yakuindomo - 09 [720p][19D4E5F9].mp4
│   │       ├── [animegrimoire] Seitokai Yakuindomo - 10 [720p][67C47176].mp4
│   │       ├── [animegrimoire] Seitokai Yakuindomo - 11 [720p][33835CFD].mp4
│   │       ├── [animegrimoire] Seitokai Yakuindomo - 12 [720p][CA6F4B74].mp4
│   │       └── [animegrimoire] Seitokai Yakuindomo - 13 [720p][F2E2C304].mp4
│   └── Winter
│       └── Sora no Woto
│           ├── [animegrimoire] Sora no Woto - 01 [BD720p][5CAB3487].mp4
│           ├── [animegrimoire] Sora no Woto - 02 [BD720p][6DD71E0A].mp4
│           ├── [animegrimoire] Sora no Woto - 03 [BD720p][D163E716].mp4
│           ├── [animegrimoire] Sora no Woto - 04 [BD720p][66EAD0E3].mp4
│           ├── [animegrimoire] Sora no Woto - 05 [BD720p][30D5D207].mp4
│           ├── [animegrimoire] Sora no Woto - 06 [BD720p][2F1505FF].mp4
│           ├── [animegrimoire] Sora no Woto - 07.5 [BD720p][AB827B6A].mp4
│           ├── [animegrimoire] Sora no Woto - 07 [BD720p][D26876F5].mp4
│           ├── [animegrimoire] Sora no Woto - 08 [BD720p][CCDBF0D2].mp4
│           ├── [animegrimoire] Sora no Woto - 09 [BD720p][1C2EB435].mp4
│           ├── [animegrimoire] Sora no Woto - 10 [BD720p][5F5FCCD9].mp4
│           ├── [animegrimoire] Sora no Woto - 11 [BD720p][4E6D58E2].mp4
│           ├── [animegrimoire] Sora no Woto - 12 [BD720p][5DEBE64E].mp4
│           └── [animegrimoire] Sora no Woto - 13 [BD720p][E1306DF2].mp4
├── 2011
│   ├── Fall
│   │   ├── Shinryaku! Ika Musume S2
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume S2 - 01 [BD720p][789CB557].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume S2 - 02 [BD720p][96700C74].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume S2 - 03 [BD720p][D502DC85].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume S2 - 04 [BD720p][A2D12235].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume S2 - 05 [BD720p][1557C8EB].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume S2 - 06 [BD720p][EF9B3230].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume S2 - 07 [BD720p][25F50531].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume S2 - 08 [BD720p][B7BF632E].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume S2 - 09 [BD720p][7892E6A3].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume S2 - 10 [BD720p][384D0BE0].mp4
│   │   │   ├── [animegrimoire] Shinryaku! Ika Musume S2 - 11 [BD720p][58CFBEE6].mp4
│   │   │   └── [animegrimoire] Shinryaku! Ika Musume S2 - 12 [BD720p][68A4CEBD].mp4
│   │   └── Working S2
│   │       ├── [animegrimoire] Working!! - 01 [720p][7CC18323].mp4
│   │       ├── [animegrimoire] Working!! - 02 [720p][933828BE].mp4
│   │       ├── [animegrimoire] Working!! - 03 [720p][02A82E18].mp4
│   │       ├── [animegrimoire] Working!! - 04 [720p][80EC31EF].mp4
│   │       ├── [animegrimoire] Working!! - 05 [720p][1973077A].mp4
│   │       ├── [animegrimoire] Working!! - 06 [720p][7AA892C7].mp4
│   │       ├── [animegrimoire] Working!! - 07 [720p][1853ABBB].mp4
│   │       ├── [animegrimoire] Working!! - 08 [720p][F32A6BF9].mp4
│   │       ├── [animegrimoire] Working!! - 09 [720p][156C2A73].mp4
│   │       ├── [animegrimoire] Working!! - 10 [720p][5D5992FE].mp4
│   │       ├── [animegrimoire] Working!! - 11 [720p][B2D36DB9].mp4
│   │       ├── [animegrimoire] Working!! - 12 [720p][AF51427C].mp4
│   │       └── [animegrimoire] Working!! - 13 [720p][894A6CB2].mp4
│   ├── Spring
│   │   ├── Astarotte no Omocha!
│   │   │   ├── [animegrimoire] Astarotte no Omocha! - 01 [BD720p][0972384C].mp4
│   │   │   ├── [animegrimoire] Astarotte no Omocha! - 02 [BD720p][BE438DA9].mp4
│   │   │   ├── [animegrimoire] Astarotte no Omocha! - 03 [BD720p][02E0D0D8].mp4
│   │   │   ├── [animegrimoire] Astarotte no Omocha! - 04 [BD720p][E21DB875].mp4
│   │   │   ├── [animegrimoire] Astarotte no Omocha! - 05 [BD720p][2EA6E417].mp4
│   │   │   ├── [animegrimoire] Astarotte no Omocha! - 06 [BD720p][A58C07E8].mp4
│   │   │   ├── [animegrimoire] Astarotte no Omocha! - 07 [BD720p][F8D5DF94].mp4
│   │   │   ├── [animegrimoire] Astarotte no Omocha! - 08 [BD720p][4A6F7917].mp4
│   │   │   ├── [animegrimoire] Astarotte no Omocha! - 09 [BD720p][7F61FE2A].mp4
│   │   │   ├── [animegrimoire] Astarotte no Omocha! - 10 [BD720p][DAA257C6].mp4
│   │   │   ├── [animegrimoire] Astarotte no Omocha! - 11 [BD720p][E29520BE].mp4
│   │   │   └── [animegrimoire] Astarotte no Omocha! - 12 [BD720p][B9002CCF].mp4
│   │   ├── Hanasaku Iroha
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 01 [BD720p][EE596A6F].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 02 [BD720p][5253DF92].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 03 [BD720p][2E88E3C8].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 04 [BD720p][F0552F46].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 05 [BD720p][9331226F].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 06 [BD720p][926FF288].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 07 [BD720p][9309C3FC].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 08 [BD720p][CA50BA09].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 09 [BD720p][1D9B136E].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 10 [BD720p][90D22966].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 11 [BD720p][29899B2C].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 12 [BD720p][B0DE2E74].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 13 [BD720p][90984110].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 14 [BD720p][7F48AFC5].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 15 [BD720p][1833CB02].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 16 [BD720p][98C22596].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 17 [BD720p][0CBC2F6F].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 18 [BD720p][D0022E63].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 19 [BD720p][45623384].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 20 [BD720p][4BF96DDB].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 21 [BD720p][D1107F6A].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 22 [BD720p][4B986705].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 23 [BD720p][581A4EDD].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 24 [BD720p][569C1BBC].mp4
│   │   │   ├── [animegrimoire] Hanasaku Iroha - 25 [BD720p][AAE6776C].mp4
│   │   │   └── [animegrimoire] Hanasaku Iroha - 26 [BD720p][7ED8F882].mp4
│   │   └── Nichijou
│   │       ├── [animegrimoire] Nichijou - 00 [BD720p][B511F3E2].mp4
│   │       ├── [animegrimoire] Nichijou - 01 [BD720p][F9BAEE63].mp4
│   │       ├── [animegrimoire] Nichijou - 02 [BD720p][1584F0B2].mp4
│   │       ├── [animegrimoire] Nichijou - 03 [BD720p][821824F6].mp4
│   │       ├── [animegrimoire] Nichijou - 04 [BD720p][92DA4B6F].mp4
│   │       ├── [animegrimoire] Nichijou - 05 [BD720p][FD027724].mp4
│   │       ├── [animegrimoire] Nichijou - 06 [BD720p][230B5D4E].mp4
│   │       ├── [animegrimoire] Nichijou - 07 [BD720p][F23FE350].mp4
│   │       ├── [animegrimoire] Nichijou - 08 [BD720p][413A2372].mp4
│   │       ├── [animegrimoire] Nichijou - 09 [BD720p][50A74851].mp4
│   │       ├── [animegrimoire] Nichijou - 10 [BD720p][92233446].mp4
│   │       ├── [animegrimoire] Nichijou - 11 [BD720p][4C958254].mp4
│   │       ├── [animegrimoire] Nichijou - 12 [BD720p][B108373E].mp4
│   │       ├── [animegrimoire] Nichijou - 13 [BD720p][6C98BAD6].mp4
│   │       ├── [animegrimoire] Nichijou - 14 [BD720p][815AC718].mp4
│   │       ├── [animegrimoire] Nichijou - 15 [BD720p][49AAEDAD].mp4
│   │       ├── [animegrimoire] Nichijou - 16 [BD720p][FE1A5164].mp4
│   │       ├── [animegrimoire] Nichijou - 17 [BD720p][AA50DC6F].mp4
│   │       ├── [animegrimoire] Nichijou - 18 [BD720p][33592110].mp4
│   │       ├── [animegrimoire] Nichijou - 19 [BD720p][A68E551A].mp4
│   │       ├── [animegrimoire] Nichijou - 20 [BD720p][B8725965].mp4
│   │       ├── [animegrimoire] Nichijou - 21 [BD720p][7A4BD27A].mp4
│   │       ├── [animegrimoire] Nichijou - 22 [BD720p][1A64BD1F].mp4
│   │       ├── [animegrimoire] Nichijou - 23 [BD720p][6B1AACE2].mp4
│   │       ├── [animegrimoire] Nichijou - 24 [BD720p][1D1A82CA].mp4
│   │       ├── [animegrimoire] Nichijou - 25 [BD720p][045333E9].mp4
│   │       └── [animegrimoire] Nichijou - 26 [BD720p][36907698].mp4
│   ├── Summer
│   │   ├── Kamisama no Memochou
│   │   │   ├── [animegrimoire] Kamisama no Memochou - 01 [BD720p][45F35A45].mp4
│   │   │   ├── [animegrimoire] Kamisama no Memochou - 02 [BD720p][FE7C28F6].mp4
│   │   │   ├── [animegrimoire] Kamisama no Memochou - 03 [BD720p][E68F987E].mp4
│   │   │   ├── [animegrimoire] Kamisama no Memochou - 04 [BD720p][D5F3706F].mp4
│   │   │   ├── [animegrimoire] Kamisama no Memochou - 05 [BD720p][EC3C691E].mp4
│   │   │   ├── [animegrimoire] Kamisama no Memochou - 06 [BD720p][3FB261EF].mp4
│   │   │   ├── [animegrimoire] Kamisama no Memochou - 07 [BD720p][8AB67B1E].mp4
│   │   │   ├── [animegrimoire] Kamisama no Memochou - 08 [BD720p][DE63C6B0].mp4
│   │   │   ├── [animegrimoire] Kamisama no Memochou - 09 [BD720p][28D6A36A].mp4
│   │   │   ├── [animegrimoire] Kamisama no Memochou - 10 [BD720p][5D667C8E].mp4
│   │   │   ├── [animegrimoire] Kamisama no Memochou - 11 [BD720p][F3E5E5C8].mp4
│   │   │   └── [animegrimoire] Kamisama no Memochou - 12 [BD720p][E1E3562C].mp4
│   │   └── YuruYuri S1
│   │       ├── [animegrimoire] YuruYuri - 01 [720p][26EBD8DD].mp4
│   │       ├── [animegrimoire] YuruYuri - 02 [720p][1113ED1B].mp4
│   │       ├── [animegrimoire] YuruYuri - 03 [720p][F1DB9BEF].mp4
│   │       ├── [animegrimoire] YuruYuri - 04 [720p][5B1206A8].mp4
│   │       ├── [animegrimoire] YuruYuri - 05 [720p][B2DB8A80].mp4
│   │       ├── [animegrimoire] YuruYuri - 06 [720p][152012E8].mp4
│   │       ├── [animegrimoire] YuruYuri - 07 [720p][146582E1].mp4
│   │       ├── [animegrimoire] YuruYuri - 08 [720p][6733EC1A].mp4
│   │       ├── [animegrimoire] YuruYuri - 09 [720p][7C46F7F0].mp4
│   │       ├── [animegrimoire] YuruYuri - 10 [720p][F8D86B49].mp4
│   │       ├── [animegrimoire] YuruYuri - 11 [720p][2C8316C6].mp4
│   │       └── [animegrimoire] YuruYuri - 12 [720p][8DF04F41].mp4
│   └── Winter
│       └── Gosick
│           ├── [AnimeGrimoire] Gosick 01 [BD 720p] [6C9441B0].mp4
│           ├── [AnimeGrimoire] Gosick 02 [BD 720p] [7088F7C6].mp4
│           ├── [AnimeGrimoire] Gosick 03 [BD 720p] [9FC07FB1].mp4
│           ├── [AnimeGrimoire] Gosick 04 [BD 720p] [26C4CCA9].mp4
│           ├── [AnimeGrimoire] Gosick 05 [BD 720p] [94023DCF].mp4
│           ├── [AnimeGrimoire] Gosick 06 [BD 720p] [A8433180].mp4
│           ├── [AnimeGrimoire] Gosick 07 [BD 720p] [7C58E1C2].mp4
│           ├── [AnimeGrimoire] Gosick 08 [BD 720p] [A00880D7].mp4
│           ├── [AnimeGrimoire] Gosick 09 [BD 720p] [294C7C68].mp4
│           ├── [AnimeGrimoire] Gosick 10 [BD 720p] [B2B75288].mp4
│           ├── [AnimeGrimoire] Gosick 11 [BD 720p] [3CFB4408].mp4
│           ├── [AnimeGrimoire] Gosick 12 [BD 720p] [B201F96A].mp4
│           ├── [AnimeGrimoire] Gosick 13 [BD 720p] [BC1872F9].mp4
│           ├── [AnimeGrimoire] Gosick 14 [BD 720p] [F0E6078A].mp4
│           ├── [AnimeGrimoire] Gosick 15 [BD 720p] [31B793D6].mp4
│           ├── [AnimeGrimoire] Gosick 16 [BD 720p] [F2853EB5].mp4
│           ├── [AnimeGrimoire] Gosick 17 [BD 720p] [5196B560].mp4
│           ├── [AnimeGrimoire] Gosick 18 [BD 720p] [288D5F30].mp4
│           ├── [AnimeGrimoire] Gosick 19 [BD 720p] [9D605AB1].mp4
│           ├── [AnimeGrimoire] Gosick 20 [BD 720p] [EE31F980].mp4
│           ├── [AnimeGrimoire] Gosick 21 [BD 720p] [A8ABBF5F].mp4
│           ├── [AnimeGrimoire] Gosick 22 [BD 720p] [685A7782].mp4
│           ├── [AnimeGrimoire] Gosick 23 [BD 720p] [37464F44].mp4
│           └── [AnimeGrimoire] Gosick 24 [BD 720p] [3B3810EC].mp4
├── 2012
│   ├── Fall
│   │   ├── Chuunibyou demo Koi ga Shitai!
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! - 01 [BD720p][518FB222].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! - 02 [BD720p][AC7FE1B7].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! - 03 [BD720p][97C6D865].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! - 04 [BD720p][6E1D021F].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! - 05 [BD720p][C0751B64].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! - 06 [BD720p][81ABD592].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! - 07 [BD720p][3A963961].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! - 08 [BD720p][74ECE444].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! - 09 [BD720p][40830F49].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! - 10 [BD720p][2FD8A721].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! - 11 [BD720p][10000357].mp4
│   │   │   └── [animegrimoire] Chuunibyou demo Koi ga Shitai! - 12 [BD720p][8E72EAC4].mp4
│   │   ├── Chuunibyou demo Koi ga Shitai! Depth of Field
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Depth of Field - 1 [BD720p][42FA6224].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Depth of Field - 2 [BD720p][858600CD].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Depth of Field - 3 [BD720p][CF48C611].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Depth of Field - 4 [BD720p][4C9B6F2A].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Depth of Field - 5 [BD720p][DBC5A0CD].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Depth of Field - 6 [BD720p][EA106A1D].mp4
│   │   │   └── [animegrimoire] Chuunibyou demo Koi ga Shitai! Depth of Field - 7 [BD720p][57319CA8].mp4
│   │   ├── Chuunibyou demo Koi ga Shitai! Lite
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Lite - 01 [BD720p][6953E8A3].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Lite - 02 [BD720p][ED413593].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Lite - 03 [BD720p][AB68DFFA].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Lite - 04 [BD720p][D8751A84].mp4
│   │   │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Lite - 05 [BD720p][99C716AF].mp4
│   │   │   └── [animegrimoire] Chuunibyou demo Koi ga Shitai! Lite - 06 [BD720p][02EB7BF1].mp4
│   │   ├── From The New World
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 01 [BD 720p][3C50654D].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 02 [BD 720p][5A7DB96C].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 03 [BD 720p][51CF6178].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 04 [BD 720p][6EC0C63E].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 05 [BD 720p][E43D0B27].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 06 [BD 720p][47D56D80].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 07 [BD 720p][01B2C48E].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 08 [BD 720p][E65736B7].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 09 [BD 720p][411DDDCA].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 10 [BD 720p][E6361D43].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 11 [BD 720p][2BFD541C].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 12 [BD 720p][697E38D3].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 13 [BD 720p][4AB5C15F].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 14 [BD 720p][16BA950F].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 15 [BD 720p][3C58B750].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 16 [BD 720p][B4034B65].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 17 [BD 720p][03C8E365].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 18 [BD 720p][29D10D3D].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 19 [BD 720p][08A868D8].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 20 [BD 720p][85417513].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 21 [BD 720p][04CA1B11].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 22 [BD 720p][558BBD26].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 23 [BD 720p][E9B5444E].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 24 [BD 720p][20C3E886].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori 25 [BD 720p][5B3E2B0A].mp4
│   │   │   ├── [animegrimoire.org] Shinsekai Yori Creditless ED1 [BD 720p][2D14A2BE].mp4
│   │   │   └── [animegrimoire.org] Shinsekai Yori Creditless ED2 [BD 720p][06840EEB].mp4
│   │   ├── Girls und Panzer
│   │   │   ├── OVA 1-6
│   │   │   │   ├── [animegrimoire] Girls und Panzer - OVA 01[BD720p][B1C1560D].mp4
│   │   │   │   ├── [animegrimoire] Girls und Panzer - OVA 02[BD720p][BFDC3F18].mp4
│   │   │   │   ├── [animegrimoire] Girls und Panzer - OVA 03[BD720p][BEB94E1C].mp4
│   │   │   │   ├── [animegrimoire] Girls und Panzer - OVA 04[BD720p][90CC2E8A].mp4
│   │   │   │   ├── [animegrimoire] Girls und Panzer - OVA 05[BD720p][7745239C].mp4
│   │   │   │   └── [animegrimoire] Girls und Panzer - OVA 06[BD720p][F58D4E18].mp4
│   │   │   ├── OVA 7
│   │   │   │   └── [animegrimoire] Girls und Panzer - Kore ga Hontou no Anzio-sen Desu! [BD720p][9226EB40].mp4
│   │   │   └── Releases + Recap
│   │   │       ├── [animegrimoire] Girls und Panzer - 01 [BD720p][F08A6E45].mp4
│   │   │       ├── [animegrimoire] Girls und Panzer - 02 [BD720p][3F1A2C54].mp4
│   │   │       ├── [animegrimoire] Girls und Panzer - 03 [BD720p][DF629278].mp4
│   │   │       ├── [animegrimoire] Girls und Panzer - 04 [BD720p][E8FE927A].mp4
│   │   │       ├── [animegrimoire] Girls und Panzer - 05 [BD720p][C0FE6578].mp4
│   │   │       ├── [animegrimoire] Girls und Panzer - 06 [BD720p][60276DDC].mp4
│   │   │       ├── [animegrimoire] Girls und Panzer - 07 [BD720p][BE4BC124].mp4
│   │   │       ├── [animegrimoire] Girls und Panzer - 08 [BD720p][B43287CF].mp4
│   │   │       ├── [animegrimoire] Girls und Panzer - 09 [BD720p][1028018E].mp4
│   │   │       ├── [animegrimoire] Girls und Panzer - 10 [BD720p][7CBAB3A9].mp4
│   │   │       ├── [animegrimoire] Girls und Panzer - 11 [BD720p][83C08C08].mp4
│   │   │       ├── [animegrimoire] Girls und Panzer - 12 [BD720p][4FE5974C].mp4
│   │   │       ├── [animegrimoire] Girls und Panzer - Recap 01 [BD720p][1B70A567].mp4
│   │   │       └── [animegrimoire] Girls und Panzer - Recap 02 [BD720p][9C814690].mp4
│   │   ├── Little Buster
│   │   │   ├── [animegrimoire] Little Busters - 01 [BD720p][891138EA].mp4
│   │   │   ├── [animegrimoire] Little Busters - 02 [BD720p][8D324CB6].mp4
│   │   │   ├── [animegrimoire] Little Busters - 03 [BD720p][1ED1FC49].mp4
│   │   │   ├── [animegrimoire] Little Busters - 04 [BD720p][B487CCFE].mp4
│   │   │   ├── [animegrimoire] Little Busters - 05 [BD720p][A10121C9].mp4
│   │   │   ├── [animegrimoire] Little Busters - 06 [BD720p][5C17BC92].mp4
│   │   │   ├── [animegrimoire] Little Busters - 07 [BD720p][3E53326F].mp4
│   │   │   ├── [animegrimoire] Little Busters - 08 [BD720p][21A2B30E].mp4
│   │   │   ├── [animegrimoire] Little Busters - 09 [BD720p][1FC192DA].mp4
│   │   │   ├── [animegrimoire] Little Busters - 10 [BD720p][F3541909].mp4
│   │   │   ├── [animegrimoire] Little Busters - 11 [BD720p][A8D3CDF8].mp4
│   │   │   ├── [animegrimoire] Little Busters - 12 [BD720p][2BA93232].mp4
│   │   │   ├── [animegrimoire] Little Busters - 13 [BD720p][063C67BE].mp4
│   │   │   ├── [animegrimoire] Little Busters - 14 [BD720p][10343A30].mp4
│   │   │   ├── [animegrimoire] Little Busters - 15 [BD720p][EBFA6466].mp4
│   │   │   ├── [animegrimoire] Little Busters - 16 [BD720p][4DE95A82].mp4
│   │   │   ├── [animegrimoire] Little Busters - 17 [BD720p][0B6F5555].mp4
│   │   │   ├── [animegrimoire] Little Busters - 18 [BD720p][18F39FA7].mp4
│   │   │   ├── [animegrimoire] Little Busters - 19 [BD720p][87F0AEE1].mp4
│   │   │   ├── [animegrimoire] Little Busters - 20 [BD720p][1AAB224C].mp4
│   │   │   ├── [animegrimoire] Little Busters - 21 [BD720p][A5BD812C].mp4
│   │   │   ├── [animegrimoire] Little Busters - 22 [BD720p][B0BEE0DB].mp4
│   │   │   ├── [animegrimoire] Little Busters - 23 [BD720p][B945DCF0].mp4
│   │   │   ├── [animegrimoire] Little Busters - 24 [BD720p][73B50C94].mp4
│   │   │   ├── [animegrimoire] Little Busters - 25 [BD720p][3D4AAF52].mp4
│   │   │   ├── [animegrimoire] Little Busters - 26 [BD720p][437D18A2].mp4
│   │   │   └── [animegrimoire] Little Busters OVA [BD720p][EAF6CF21].mp4
│   │   ├── Onii-Ai
│   │   │   ├── [animegrimoire] Onii-chan Dakedo Ai Sae Areba Kankeinai Yo Ne 01 - OniAi [BD, 720p][BAE3E36C].mp4
│   │   │   ├── [animegrimoire] Onii-chan Dakedo Ai Sae Areba Kankeinai Yo Ne 02 - Loose [BD, 720p][9647DD89].mp4
│   │   │   ├── [animegrimoire] Onii-chan Dakedo Ai Sae Areba Kankeinai Yo Ne 03 - Braless [BD, 720p][B4DDD374].mp4
│   │   │   ├── [animegrimoire] Onii-chan Dakedo Ai Sae Areba Kankeinai Yo Ne 04 - In The Nude [BD, 720p][92ACC935].mp4
│   │   │   ├── [animegrimoire] Onii-chan Dakedo Ai Sae Areba Kankeinai Yo Ne 05 - Sexual Intercourse [BD, 720p][4786D0BE].mp4
│   │   │   ├── [animegrimoire] Onii-chan Dakedo Ai Sae Areba Kankeinai Yo Ne 06 - Mayo Chicken [BD, 720p][7015EE94].mp4
│   │   │   ├── [animegrimoire] Onii-chan Dakedo Ai Sae Areba Kankeinai Yo Ne 07 - Small Breasts [BD, 720p][D47B9D90].mp4
│   │   │   ├── [animegrimoire] Onii-chan Dakedo Ai Sae Areba Kankeinai Yo Ne 08 - Colorful [BD, 720p][980A4F82].mp4
│   │   │   ├── [animegrimoire] Onii-chan Dakedo Ai Sae Areba Kankeinai Yo Ne 09 - Meow Meow [BD, 720p][F1AE58D3].mp4
│   │   │   ├── [animegrimoire] Onii-chan Dakedo Ai Sae Areba Kankeinai Yo Ne 10 - Silver Ball [BD, 720p][4A58A01A].mp4
│   │   │   ├── [animegrimoire] Onii-chan Dakedo Ai Sae Areba Kankeinai Yo Ne 11 - Fleeting [BD, 720p][0DFCC244].mp4
│   │   │   └── [animegrimoire] Onii-chan Dakedo Ai Sae Areba Kankeinai Yo Ne 12 - It's Love [BD, 720p][5CE73A97].mp4
│   │   └── PSYCHO-PASS 3
│   │       ├── [animegrimoire] PSYCHO-PASS 3 - 01 [720p][BF60810F].mp4
│   │       ├── [animegrimoire] PSYCHO-PASS 3 - 02 [720p][EB9068D0].mp4
│   │       ├── [animegrimoire] PSYCHO-PASS 3 - 03 [720p][998572EF].mp4
│   │       ├── [animegrimoire] PSYCHO-PASS 3 - 04 [720p][23D36DE1].mp4
│   │       ├── [animegrimoire] PSYCHO-PASS 3 - 05 [720p][D7E44673].mp4
│   │       ├── [animegrimoire] PSYCHO-PASS 3 - 06 [720p][41FC9DED].mp4
│   │       ├── [animegrimoire] PSYCHO-PASS 3 - 07 [720p][45E7D378].mp4
│   │       └── [animegrimoire] PSYCHO-PASS 3 - 08 [720p][FD02801D].mp4
│   ├── Spring
│   │   ├── Haiyore! Nyaruko-san
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san - 01 [BD720p][15881C9B].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san - 02 [BD720p][91F58418].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san - 03 [BD720p][26062142].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san - 04 [BD720p][FB379786].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san - 05 [BD720p][5113369F].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san - 06 [BD720p][2720E49D].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san - 07 [BD720p][67EF35B3].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san - 08 [BD720p][22D984A7].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san - 09 [BD720p][6C8284AE].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san - 10 [BD720p][4081FD2A].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san - 11 [BD720p][9E5EABA0].mp4
│   │   │   └── [animegrimoire] Haiyore! Nyaruko-san - 12 [BD720p][A993020D].mp4
│   │   └── Hyouka
│   │       ├── [animegrimoire] Hyouka - 01 [720p][6166E9E5].mp4
│   │       ├── [animegrimoire] Hyouka - 02 [720p][B8C119B2].mp4
│   │       ├── [animegrimoire] Hyouka - 03 [720p][068E2C62].mp4
│   │       ├── [animegrimoire] Hyouka - 04 [720p][E9D79212].mp4
│   │       ├── [animegrimoire] Hyouka - 05 [720p][50661C68].mp4
│   │       ├── [animegrimoire] Hyouka - 06 [720p][DCA8A04D].mp4
│   │       ├── [animegrimoire] Hyouka - 07 [720p][DA0D871F].mp4
│   │       ├── [animegrimoire] Hyouka - 08 [720p][678EE819].mp4
│   │       ├── [animegrimoire] Hyouka - 09 [720p][EE2958B8].mp4
│   │       ├── [animegrimoire] Hyouka - 10 [720p][EECA9CEE].mp4
│   │       ├── [animegrimoire] Hyouka - 11.5 [720p] [02C28F0E].mp4
│   │       ├── [animegrimoire] Hyouka - 11 [720p][FE2DE6AB].mp4
│   │       ├── [animegrimoire] Hyouka - 12 [720p][92E135E0].mp4
│   │       ├── [animegrimoire] Hyouka - 13 [720p][FF75EE83].mp4
│   │       ├── [animegrimoire] Hyouka - 14 [720p][6558AF35].mp4
│   │       ├── [animegrimoire] Hyouka - 15 [720p][85236199].mp4
│   │       ├── [animegrimoire] Hyouka - 16 [720p][9201E557].mp4
│   │       ├── [animegrimoire] Hyouka - 17 [720p][AD31A122].mp4
│   │       ├── [animegrimoire] Hyouka - 18 [720p][0F93938D].mp4
│   │       ├── [animegrimoire] Hyouka - 19 [720p][512D7DAB].mp4
│   │       ├── [animegrimoire] Hyouka - 20 [720p][DCD67BC4].mp4
│   │       ├── [animegrimoire] Hyouka - 21 [720p][F1D621BB].mp4
│   │       └── [animegrimoire] Hyouka - 22 [720p][0D7AED47].mp4
│   ├── Summer
│   │   ├── Kono Naka ni Hitori, Imouto ga Iru!
│   │   │   ├── [animegrimoire] Kono Naka ni Hitori, Imouto ga Iru! - 01 [BD720p][0D0EB26F].mp4
│   │   │   ├── [animegrimoire] Kono Naka ni Hitori, Imouto ga Iru! - 02 [BD720p][8AAE8C8D].mp4
│   │   │   ├── [animegrimoire] Kono Naka ni Hitori, Imouto ga Iru! - 03 [BD720p][D5271248].mp4
│   │   │   ├── [animegrimoire] Kono Naka ni Hitori, Imouto ga Iru! - 04 [BD720p][DA4BD6C4].mp4
│   │   │   ├── [animegrimoire] Kono Naka ni Hitori, Imouto ga Iru! - 05 [BD720p][708873F3].mp4
│   │   │   ├── [animegrimoire] Kono Naka ni Hitori, Imouto ga Iru! - 06 [BD720p][3BD6ADCB].mp4
│   │   │   ├── [animegrimoire] Kono Naka ni Hitori, Imouto ga Iru! - 07 [BD720p][E10F32F3].mp4
│   │   │   ├── [animegrimoire] Kono Naka ni Hitori, Imouto ga Iru! - 08 [BD720p][2035813C].mp4
│   │   │   ├── [animegrimoire] Kono Naka ni Hitori, Imouto ga Iru! - 09 [BD720p][739EE9B8].mp4
│   │   │   ├── [animegrimoire] Kono Naka ni Hitori, Imouto ga Iru! - 10 [BD720p][13515CD0].mp4
│   │   │   ├── [animegrimoire] Kono Naka ni Hitori, Imouto ga Iru! - 11 [BD720p][C6E52184].mp4
│   │   │   ├── [animegrimoire] Kono Naka ni Hitori, Imouto ga Iru! - 12 [BD720p][E03C329B].mp4
│   │   │   └── [animegrimoire] Kono Naka ni Hitori, Imouto ga Iru! - 13 [BD720p][F4423A5B].mp4
│   │   ├── Mahou Shoujo Lyrical Nanoha The Movie 2nd A
│   │   │   └── [animegrimoire] Mahou Shoujo Lyrical Nanoha The Movie 2nd A - 00 [BD720p][F74AF304].mp4
│   │   ├── Tari Tari
│   │   │   ├── [animegrimoire] Tari Tari - 01 [BD720p][ED7A91B5].mp4
│   │   │   ├── [animegrimoire] Tari Tari - 02 [BD720p][781BF7C8].mp4
│   │   │   ├── [animegrimoire] Tari Tari - 03 [BD720p][11778A42].mp4
│   │   │   ├── [animegrimoire] Tari Tari - 04 [BD720p][852C9012].mp4
│   │   │   ├── [animegrimoire] Tari Tari - 05 [BD720p][695FC889].mp4
│   │   │   ├── [animegrimoire] Tari Tari - 06 [BD720p][7D9F0FDA].mp4
│   │   │   ├── [animegrimoire] Tari Tari - 07 [BD720p][B2B0B509].mp4
│   │   │   ├── [animegrimoire] Tari Tari - 08 [BD720p][FB2D5FC4].mp4
│   │   │   ├── [animegrimoire] Tari Tari - 09 [BD720p][3DDCC0D4].mp4
│   │   │   ├── [animegrimoire] Tari Tari - 10 [BD720p][D73ECB42].mp4
│   │   │   ├── [animegrimoire] Tari Tari - 11 [BD720p][E43ECEBD].mp4
│   │   │   ├── [animegrimoire] Tari Tari - 12 [BD720p][DF766477].mp4
│   │   │   └── [animegrimoire] Tari Tari - 13 [BD720p][22431F26].mp4
│   │   └── YuruYuri S2
│   │       ├── [animegrimoire] YuruYuri S2 - 01 [720p][C76A7336].mp4
│   │       ├── [animegrimoire] YuruYuri S2 - 02 [720p][C86A18BA].mp4
│   │       ├── [animegrimoire] YuruYuri S2 - 03 [720p][FF23415C].mp4
│   │       ├── [animegrimoire] YuruYuri S2 - 04 [720p][D3A5F07B].mp4
│   │       ├── [animegrimoire] YuruYuri S2 - 05 [720p][4D9222F2].mp4
│   │       ├── [animegrimoire] YuruYuri S2 - 06 [720p][334A0475].mp4
│   │       ├── [animegrimoire] YuruYuri S2 - 07 [720p][0FE4DC33].mp4
│   │       ├── [animegrimoire] YuruYuri S2 - 08 [720p][04548C4C].mp4
│   │       ├── [animegrimoire] YuruYuri S2 - 09 [720p][41993F76].mp4
│   │       ├── [animegrimoire] YuruYuri S2 - 10 [720p][CD6EF52A].mp4
│   │       ├── [animegrimoire] YuruYuri S2 - 11 [720p][A66804DF].mp4
│   │       └── [animegrimoire] YuruYuri S2 - 12 [720p][1410608E].mp4
│   └── Winter
│       ├── Ano Natsu de Matteru
│       │   ├── [animegrimoire] Ano Natsu de Matteru - 01 [BD720p][A12E5BF7].mp4
│       │   ├── [animegrimoire] Ano Natsu de Matteru - 02 [BD720p][626373B3].mp4
│       │   ├── [animegrimoire] Ano Natsu de Matteru - 03 [BD720p][89B2141B].mp4
│       │   ├── [animegrimoire] Ano Natsu de Matteru - 04 [BD720p][3C5C320A].mp4
│       │   ├── [animegrimoire] Ano Natsu de Matteru - 05 [BD720p][7CC989E8].mp4
│       │   ├── [animegrimoire] Ano Natsu de Matteru - 06 [BD720p][C6452344].mp4
│       │   ├── [animegrimoire] Ano Natsu de Matteru - 07 [BD720p][C6D0DFAB].mp4
│       │   ├── [animegrimoire] Ano Natsu de Matteru - 08 [BD720p][EE095A0A].mp4
│       │   ├── [animegrimoire] Ano Natsu de Matteru - 09 [BD720p][D01A9D83].mp4
│       │   ├── [animegrimoire] Ano Natsu de Matteru - 10 [BD720p][2584E031].mp4
│       │   ├── [animegrimoire] Ano Natsu de Matteru - 11 [BD720p][D9F8DE3B].mp4
│       │   ├── [animegrimoire] Ano Natsu de Matteru - 12 [BD720p][37AA5E75].mp4
│       │   └── [animegrimoire] Ano Natsu de Matteru - 13 [BD720p][F228F74E].mp4
│       └── Nisenmonogatari
│           ├── [animegrimoire] Nisemonogatari - 01 [BD720p][41770BCE].mp4
│           ├── [animegrimoire] Nisemonogatari - 02 [BD720p][C8893783].mp4
│           ├── [animegrimoire] Nisemonogatari - 03 [BD720p][4913B7C7].mp4
│           ├── [animegrimoire] Nisemonogatari - 04 [BD720p][7DED3396].mp4
│           ├── [animegrimoire] Nisemonogatari - 05 [BD720p][BEDE0B72].mp4
│           ├── [animegrimoire] Nisemonogatari - 06 [BD720p][C9A0618C].mp4
│           ├── [animegrimoire] Nisemonogatari - 07 [BD720p][BFA9B56D].mp4
│           ├── [animegrimoire] Nisemonogatari - 08 [BD720p][68E3A69A].mp4
│           ├── [animegrimoire] Nisemonogatari - 09 [BD720p][5DA431CB].mp4
│           ├── [animegrimoire] Nisemonogatari - 10 [BD720p][FF3924D2].mp4
│           └── [animegrimoire] Nisemonogatari - 11 [BD720p][7CA34D5C].mp4
├── 2013
│   ├── Fall
│   │   ├── Little Buster Refrain
│   │   │   ├── [animegrimoire] Little Busters Refrain - 01 [BD720p][1F5A3715].mp4
│   │   │   ├── [animegrimoire] Little Busters Refrain - 02 [BD720p][A0331F0F].mp4
│   │   │   ├── [animegrimoire] Little Busters Refrain - 03 [BD720p][3E344596].mp4
│   │   │   ├── [animegrimoire] Little Busters Refrain - 04 [BD720p][ED905DF2].mp4
│   │   │   ├── [animegrimoire] Little Busters Refrain - 05 [BD720p][3EFC815D].mp4
│   │   │   ├── [animegrimoire] Little Busters Refrain - 06 [BD720p][BD9A84E7].mp4
│   │   │   ├── [animegrimoire] Little Busters Refrain - 07 [BD720p][DAF89746].mp4
│   │   │   ├── [animegrimoire] Little Busters Refrain - 08 [BD720p][1EA1D032].mp4
│   │   │   ├── [animegrimoire] Little Busters Refrain - 09 [BD720p][586021EF].mp4
│   │   │   ├── [animegrimoire] Little Busters Refrain - 10 [BD720p][BA02CAE1].mp4
│   │   │   ├── [animegrimoire] Little Busters Refrain - 11 [BD720p][22DE543D].mp4
│   │   │   ├── [animegrimoire] Little Busters Refrain - 12 [BD720p][0FBEE35F].mp4
│   │   │   └── [animegrimoire] Little Busters Refrain - 13 [BD720p][76D345F0].mp4
│   │   ├── Log Horizon
│   │   │   ├── [animegrimoire] Log Horizon - 01 [BD720p][2272E1B4].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 02 [BD720p][19AF4357].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 03 [BD720p][73FE7616].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 04 [BD720p][676BDBBD].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 05 [BD720p][42F29514].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 06 [BD720p][420CC4A0].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 07 [BD720p][06F85946].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 08 [BD720p][B578D227].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 09 [BD720p][3917B56C].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 10 [BD720p][53B4226B].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 11 [BD720p][805EE975].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 12 [BD720p][AE21B054].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 13 [BD720p][9DBBFF4A].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 14 [BD720p][DFC8BE69].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 15 [BD720p][598D1AF1].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 16 [BD720p][878B507B].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 17 [BD720p][9E32E0F0].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 18 [BD720p][178F4C87].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 19 [BD720p][F11EA090].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 20 [BD720p][C6E20BDA].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 21 [BD720p][C32AACED].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 22 [BD720p][ACC1B1A8].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 23 [BD720p][3118336F].mp4
│   │   │   ├── [animegrimoire] Log Horizon - 24 [BD720p][CB4FA349].mp4
│   │   │   └── [animegrimoire] Log Horizon - 25 [BD720p][4E6B307C].mp4
│   │   ├── Nagi no Asukara
│   │   │   ├── [animegrimoire] Nagi no Asukara - 01 [BD720p][9619A66A].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 02 [BD720p][D80CD889].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 03 [BD720p][548306BA].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 04 [BD720p][CD6DB142].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 05 [BD720p][7E0C1F69].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 06 [BD720p][D673AED5].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 07 [BD720p][E57A129E].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 08 [BD720p][59FEF38A].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 09 [BD720p][4C06079A].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 10 [BD720p][9EB273F3].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 11 [BD720p][2FA65D01].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 12 [BD720p][B84E4B5E].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 13 [BD720p][D53BCB4B].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 14 [BD720p][8F1C428E].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 15 [BD720p][934033A5].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 16 [BD720p][6A2FA6E2].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 17 [BD720p][4BA0B955].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 18 [BD720p][ADA048AE].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 19 [BD720p][13AB1B90].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 20 [BD720p][404DEDD7].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 21 [BD720p][BEB219A5].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 22 [BD720p][C1CD604A].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 23 [BD720p][D950649D].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 24 [BD720p][8014DB6D].mp4
│   │   │   ├── [animegrimoire] Nagi no Asukara - 25 [BD720p][78A4B92F].mp4
│   │   │   └── [animegrimoire] Nagi no Asukara - 26 [BD720p][11D5DF57].mp4
│   │   └── White Album
│   │       ├── [animegrimoire] White Album 2 - 01 [BD720p][28F9E8FA].mp4
│   │       ├── [animegrimoire] White Album 2 - 02 [BD720p][DBFD78BD].mp4
│   │       ├── [animegrimoire] White Album 2 - 03 [BD720p][B3D0C0D3].mp4
│   │       ├── [animegrimoire] White Album 2 - 04 [BD720p][FB2B6F90].mp4
│   │       ├── [animegrimoire] White Album 2 - 05 [BD720p][7388CD92].mp4
│   │       ├── [animegrimoire] White Album 2 - 06 [BD720p][725516F4].mp4
│   │       ├── [animegrimoire] White Album 2 - 07 [BD720p][91CB75FB].mp4
│   │       ├── [animegrimoire] White Album 2 - 08 [BD720p][5BBD1FA1].mp4
│   │       ├── [animegrimoire] White Album 2 - 09 [BD720p][CAB8EE52].mp4
│   │       ├── [animegrimoire] White Album 2 - 10 [BD720p][920E16B3].mp4
│   │       ├── [animegrimoire] White Album 2 - 11 [BD720p][230BE18E].mp4
│   │       ├── [animegrimoire] White Album 2 - 12 [BD720p][FCCF3847].mp4
│   │       └── [animegrimoire] White Album 2 - 13 [BD720p][C461B22D].mp4
│   ├── Spring
│   │   ├── Aiura
│   │   │   ├── [animegrimoire] Aiura - 01 [BD720p][793BE962].mp4
│   │   │   ├── [animegrimoire] Aiura - 02 [BD720p][EF7364EC].mp4
│   │   │   ├── [animegrimoire] Aiura - 03 [BD720p][1C1278FB].mp4
│   │   │   ├── [animegrimoire] Aiura - 04 [BD720p][88B7A312].mp4
│   │   │   ├── [animegrimoire] Aiura - 05 [BD720p][9284D59A].mp4
│   │   │   ├── [animegrimoire] Aiura - 06 [BD720p][942A379B].mp4
│   │   │   ├── [animegrimoire] Aiura - 07 [BD720p][2A0893AF].mp4
│   │   │   ├── [animegrimoire] Aiura - 08 [BD720p][3EC2ABE1].mp4
│   │   │   ├── [animegrimoire] Aiura - 09 [BD720p][3144349F].mp4
│   │   │   ├── [animegrimoire] Aiura - 10 [BD720p][F36A3FFA].mp4
│   │   │   ├── [animegrimoire] Aiura - 11 [BD720p][03C8D381].mp4
│   │   │   └── [animegrimoire] Aiura - 12 [BD720p][F7A9A8D2].mp4
│   │   ├── Haiyore! Nyaruko-san W
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san W - 01 [BD720p][AC391E8D].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san W - 02 [BD720p][651B209E].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san W - 03 [BD720p][3EB8A206].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san W - 04 [BD720p][00B95233].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san W - 05 [BD720p][C280C4DB].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san W - 06 [BD720p][A75D32F1].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san W - 07 [BD720p][468EB4A7].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san W - 08 [BD720p][C86CD587].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san W - 09 [BD720p][AF7103EF].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san W - 10 [BD720p][483180A7].mp4
│   │   │   ├── [animegrimoire] Haiyore! Nyaruko-san W - 11 [BD720p][6D5B5452].mp4
│   │   │   └── [animegrimoire] Haiyore! Nyaruko-san W - 12 [BD720p][F5EB88E1].mp4
│   │   ├── Hataraku Maou sama
│   │   │   ├── [animegrimoire] Hataraku Maou-sama! - 01 [BD720p][AD08933E].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama! - 02 [BD720p][E452D269].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama! - 03 [BD720p][46470C6D].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama! - 04 [BD720p][28F6A853].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama! - 05 [BD720p][8C4ABA3D].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama! - 06 [BD720p][77087FF0].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama! - 07 [BD720p][D9A0CA16].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama! - 08 [BD720p][40CAD96B].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama! - 09 [BD720p][33ED5CFD].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama! - 10 [BD720p][CDFDD69F].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama! - 11 [BD720p][02C66BFA].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama! - 12 [BD720p][954503E1].mp4
│   │   │   └── [animegrimoire] Hataraku Maou-sama! - 13 [BD720p][5138D6AF].mp4
│   │   ├── Hentai Ouji
│   │   │   ├── [animegrimoire] Hentai Ouji to Warawanai Neko - 01 [BD720p][391EAD1E].mp4
│   │   │   ├── [animegrimoire] Hentai Ouji to Warawanai Neko - 02 [BD720p][5704775E].mp4
│   │   │   ├── [animegrimoire] Hentai Ouji to Warawanai Neko - 03 [BD720p][4E4545BF].mp4
│   │   │   ├── [animegrimoire] Hentai Ouji to Warawanai Neko - 04 [BD720p][7D06C0E3].mp4
│   │   │   ├── [animegrimoire] Hentai Ouji to Warawanai Neko - 05 [BD720p][263C1025].mp4
│   │   │   ├── [animegrimoire] Hentai Ouji to Warawanai Neko - 06 [BD720p][C8C710ED].mp4
│   │   │   ├── [animegrimoire] Hentai Ouji to Warawanai Neko - 07 [BD720p][B67D21A6].mp4
│   │   │   ├── [animegrimoire] Hentai Ouji to Warawanai Neko - 08 [BD720p][EE0025E8].mp4
│   │   │   ├── [animegrimoire] Hentai Ouji to Warawanai Neko - 09 [BD720p][2241F13D].mp4
│   │   │   ├── [animegrimoire] Hentai Ouji to Warawanai Neko - 10 [BD720p][01D61362].mp4
│   │   │   ├── [animegrimoire] Hentai Ouji to Warawanai Neko - 11 [BD720p][6E760746].mp4
│   │   │   └── [animegrimoire] Hentai Ouji to Warawanai Neko - 12 [BD720p][138B32C4].mp4
│   │   └── Yahari Ore no Seishun Love Comedy wa Machigatteiru
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Comedy wa Machigatteiru - 01 [BD720p][8E773EA8].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Comedy wa Machigatteiru - 02 [BD720p][3F866534].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Comedy wa Machigatteiru - 03 [BD720p][40D2A06C].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Comedy wa Machigatteiru - 04 [BD720p][67D0F1A8].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Comedy wa Machigatteiru - 05 [BD720p][24CBA131].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Comedy wa Machigatteiru - 06 [BD720p][D62DAFAF].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Comedy wa Machigatteiru - 07 [BD720p][C4E8EA48].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Comedy wa Machigatteiru - 08 [BD720p][73135E03].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Comedy wa Machigatteiru - 09 [BD720p][14DD71B0].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Comedy wa Machigatteiru - 10 [BD720p][6E8DDCFE].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Comedy wa Machigatteiru - 11 [BD720p][7FBA0DA1].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Comedy wa Machigatteiru - 12 [BD720p][E9B6EA93].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Comedy wa Machigatteiru - 13 [BD720p][16300092].mp4
│   │       └── [animegrimoire] Yahari Ore no Seishun Love Comedy wa Machigatteiru - OVA [BD720p][3F59031D].mp4
│   ├── Summer
│   │   ├── Futari wa Milky Holmes
│   │   │   ├── [animegrimoire] Futari wa Milky Holmes - 01 [BD720p][E9E33BBF].mp4
│   │   │   ├── [animegrimoire] Futari wa Milky Holmes - 02 [BD720p][E6D7ED3B].mp4
│   │   │   ├── [animegrimoire] Futari wa Milky Holmes - 03 [BD720p][507071B0].mp4
│   │   │   ├── [animegrimoire] Futari wa Milky Holmes - 04 [BD720p][3C38A4A8].mp4
│   │   │   ├── [animegrimoire] Futari wa Milky Holmes - 05 [BD720p][87A7D821].mp4
│   │   │   ├── [animegrimoire] Futari wa Milky Holmes - 06 [BD720p][29DE43B5].mp4
│   │   │   ├── [animegrimoire] Futari wa Milky Holmes - 07 [BD720p][A1441546].mp4
│   │   │   ├── [animegrimoire] Futari wa Milky Holmes - 08 [BD720p][A3FD26B7].mp4
│   │   │   ├── [animegrimoire] Futari wa Milky Holmes - 09 [BD720p][A4604C8C].mp4
│   │   │   ├── [animegrimoire] Futari wa Milky Holmes - 10 [BD720p][6AC47FAE].mp4
│   │   │   ├── [animegrimoire] Futari wa Milky Holmes - 11 [BD720p][B6E935C4].mp4
│   │   │   └── [animegrimoire] Futari wa Milky Holmes - 12 [BD720p][E80BA3C5].mp4
│   │   ├── Kiniro Mosaic
│   │   │   ├── [animegrimoire] Kiniro Mosaic - 01 [BD720p][7436B164].mp4
│   │   │   ├── [animegrimoire] Kiniro Mosaic - 02 [BD720p][6EF3EF47].mp4
│   │   │   ├── [animegrimoire] Kiniro Mosaic - 03 [BD720p][95FEFBE4].mp4
│   │   │   ├── [animegrimoire] Kiniro Mosaic - 04 [BD720p][F240C64B].mp4
│   │   │   ├── [animegrimoire] Kiniro Mosaic - 05 [BD720p][761D4620].mp4
│   │   │   ├── [animegrimoire] Kiniro Mosaic - 06 [BD720p][8B212A5B].mp4
│   │   │   ├── [animegrimoire] Kiniro Mosaic - 07 [BD720p][EB04CB47].mp4
│   │   │   ├── [animegrimoire] Kiniro Mosaic - 08 [BD720p][4FFF6463].mp4
│   │   │   ├── [animegrimoire] Kiniro Mosaic - 09 [BD720p][E22A5FF4].mp4
│   │   │   ├── [animegrimoire] Kiniro Mosaic - 10 [BD720p][9BFC7A17].mp4
│   │   │   ├── [animegrimoire] Kiniro Mosaic - 11 [BD720p][43B3E408].mp4
│   │   │   └── [animegrimoire] Kiniro Mosaic - 12 [BD720p][B8F0976E].mp4
│   │   ├── Prisma Illya 1
│   │   │   ├── [animegrimoire] Fate - kaleid Liner Prisma Illya - 01 [720p][3838C679].mp4
│   │   │   ├── [animegrimoire] Fate - kaleid Liner Prisma Illya - 02 [720p][0DBE097F].mp4
│   │   │   ├── [animegrimoire] Fate - kaleid Liner Prisma Illya - 03 [720p][B91141C1].mp4
│   │   │   ├── [animegrimoire] Fate - kaleid Liner Prisma Illya - 04 [720p][03B91B34].mp4
│   │   │   ├── [animegrimoire] Fate - kaleid Liner Prisma Illya - 05 [720p][6EE6D308].mp4
│   │   │   ├── [animegrimoire] Fate - kaleid Liner Prisma Illya - 06 [720p][F27632E4].mp4
│   │   │   ├── [animegrimoire] Fate - kaleid Liner Prisma Illya - 07 [720p][040AF796].mp4
│   │   │   ├── [animegrimoire] Fate - kaleid Liner Prisma Illya - 08 [720p][1614388D].mp4
│   │   │   ├── [animegrimoire] Fate - kaleid Liner Prisma Illya - 09 [720p][B08A1236].mp4
│   │   │   └── [animegrimoire] Fate - kaleid Liner Prisma Illya - 10 [720p][15C8C764].mp4
│   │   ├── Ryo Ku Bu SS
│   │   │   ├── [animegrimoire] Ro Kyu Bu! SS 01 [BD720p][32E4640A].mp4
│   │   │   ├── [animegrimoire] Ro Kyu Bu! SS 02 [BD720p][D9504507].mp4
│   │   │   ├── [animegrimoire] Ro Kyu Bu! SS 03 [BD720p][EA50AEA4].mp4
│   │   │   ├── [animegrimoire] Ro Kyu Bu! SS 04 [BD720p][E4CAE876].mp4
│   │   │   ├── [animegrimoire] Ro Kyu Bu! SS 05 [BD720p][BDC429A2].mp4
│   │   │   ├── [animegrimoire] Ro Kyu Bu! SS 06 [BD720p][067DCE66].mp4
│   │   │   ├── [animegrimoire] Ro Kyu Bu! SS 07 [BD720p][3649EDD6].mp4
│   │   │   ├── [animegrimoire] Ro Kyu Bu! SS 08 [BD720p][EAA58BB2].mp4
│   │   │   ├── [animegrimoire] Ro Kyu Bu! SS 09 [BD720p][06B6EE7F].mp4
│   │   │   ├── [animegrimoire] Ro Kyu Bu! SS 10 [BD720p][D7C7D3FD].mp4
│   │   │   ├── [animegrimoire] Ro Kyu Bu! SS 11 [BD720p][041BAB9E].mp4
│   │   │   └── [animegrimoire] Ro Kyu Bu! SS 12 [BD720p][DE49F190].mp4
│   │   └── Uchouten Kazoku
│   │       ├── [animegrimoire] Uchouten Kazoku 2 - 01 [BD720p][4511505C].mp4
│   │       ├── [animegrimoire] Uchouten Kazoku 2 - 02 [BD720p][9F58B42C].mp4
│   │       ├── [animegrimoire] Uchouten Kazoku 2 - 03 [BD720p][1A1112D5].mp4
│   │       ├── [animegrimoire] Uchouten Kazoku 2 - 04 [BD720p][AFE23673].mp4
│   │       ├── [animegrimoire] Uchouten Kazoku 2 - 05 [BD720p][5F8381DF].mp4
│   │       ├── [animegrimoire] Uchouten Kazoku 2 - 06 [BD720p][D9E47BD0].mp4
│   │       ├── [animegrimoire] Uchouten Kazoku 2 - 07 [BD720p][8E8CE34C].mp4
│   │       ├── [animegrimoire] Uchouten Kazoku 2 - 08 [BD720p][37CE7D2A].mp4
│   │       ├── [animegrimoire] Uchouten Kazoku 2 - 09 [BD720p][38599179].mp4
│   │       ├── [animegrimoire] Uchouten Kazoku 2 - 10 [BD720p][FE4F1386].mp4
│   │       ├── [animegrimoire] Uchouten Kazoku 2 - 11 [BD720p][582CD8AA].mp4
│   │       └── [animegrimoire] Uchouten Kazoku 2 - 12 [BD720p][EF57FFEC].mp4
│   └── Winter
│       └── Senran Kagura
│           ├── [animegrimoire] Senran Kagura - 01 [BD720p][C19CBCEC].mp4
│           ├── [animegrimoire] Senran Kagura - 02 [BD720p][595EA909].mp4
│           ├── [animegrimoire] Senran Kagura - 03 [BD720p][4A576531].mp4
│           ├── [animegrimoire] Senran Kagura - 04 [BD720p][E51FC854].mp4
│           ├── [animegrimoire] Senran Kagura - 07 [BD720p][D3E61588].mp4
│           ├── [animegrimoire] Senran Kagura - 08 [BD720p][D723A533].mp4
│           ├── [animegrimoire] Senran Kagura - 09 [BD720p][5127C392].mp4
│           ├── [animegrimoire] Senran Kagura - 10 [BD720p][0CA3325D].mp4
│           ├── [animegrimoire] Senran Kagura - 11 [BD720p][76017AAD].mp4
│           └── [animegrimoire] Senran Kagura - 12 [BD720p][9127B744].mp4
├── 2014
│   ├── Fall
│   │   ├── Fate Stay Night
│   │   │   └── [animegrimoire] Fate Stay Night - Unlimited Blade Works Movie - 00 [BD720p][CD3D0F5D].mp4
│   │   ├── Hitsugi no Chaika Avenging Battle
│   │   │   ├── [animegrimoire] Hitsugi no Chaika - OVA [BD720p][8D3B9A62].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Avenging Battle - 01 [BD720p][F9CC38E2].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Avenging Battle - 02 [BD720p][15CB427E].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Avenging Battle - 03 [BD720p][34143EAF].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Avenging Battle - 04 [BD720p][FEFA1515].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Avenging Battle - 05 [BD720p][BD2DE624].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Avenging Battle - 06 [BD720p][CA1C6E37].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Avenging Battle - 07 [BD720p][5B8449A8].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Avenging Battle - 08 [BD720p][AEBBFAC5].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Avenging Battle - 09 [BD720p][006510FA].mp4
│   │   │   └── [animegrimoire] Hitsugi no Chaika The Avenging Battle - 10 [BD720p][A91035E4].mp4
│   │   ├── Kokkuri
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - 01 [BD720p][8C969FE1].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - 02 [BD720p][C95C6877].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - 03 [BD720p][EF7EC5A6].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - 04 [BD720p][4BA5FF6E].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - 05 [BD720p][86875DAC].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - 06 [BD720p][18F7E0D6].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - 07 [BD720p][BF183552].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - 08 [BD720p][1EA9C7E7].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - 09 [BD720p][588C4FDA].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - 10 [BD720p][EEEB41A6].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - 11 [BD720p][E4F08546].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - 12 [BD720p][2A0AFCAC].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - Special 01 [BD720p][56F0C38A].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - Special 02 [BD720p][65466CDD].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - Special 03 [BD720p][29A1F04B].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - Special 04 [BD720p][E8D02FAD].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - Special 05 [BD720p][CB900187].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - Special 06 [BD720p][F3E8FB85].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - Special 07 [BD720p][D81ABA00].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - Special 08 [BD720p][065B30AD].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - Special 09 [BD720p][2A8B0BB4].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - Special 10 [BD720p][5BDDA80A].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - Special 11 [BD720p][9E7C1771].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - Special 12 [BD720p][37E3B914].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - Special 13 [BD720p][115C8F73].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - Special 14 [BD720p][7C446358].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - Special 15 [BD720p][1B43305D].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - Special 16 [BD720p][E4A968DA].mp4
│   │   │   ├── [animegrimoire] Gugure! Kokkuri-san - Special 17 [BD720p][A1B439AE].mp4
│   │   │   └── [animegrimoire] Gugure! Kokkuri-san - Special 18 [BD720p][AE0C799C].mp4
│   │   └── Log Horizon 2
│   │       ├── [animegrimoire] Log Horizon 2 - 01 [BD720p][CACB628E].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 02 [BD720p][03389F5C].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 03 [BD720p][5DC7ECF1].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 04 [BD720p][74B1E6FF].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 05 [BD720p][DA039919].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 06 [BD720p][C064E562].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 07 [BD720p][E2F65952].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 08 [BD720p][BECA2A1E].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 09 [BD720p][2C2CC9A0].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 10 [BD720p][321F1986].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 11 [BD720p][9A635AB8].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 12 [BD720p][F5B3296D].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 13 [BD720p][C9F5D646].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 14 [BD720p][D0AC0441].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 15 [BD720p][0CF7DB1E].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 16 [BD720p][DAB5A4BB].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 17 [BD720p][F78F50B3].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 18 [BD720p][52633C32].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 19 [BD720p][06ADB6D7].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 20 [BD720p][5B6149EE].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 21 [BD720p][88AEFE35].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 22 [BD720p][D86C996B].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 23 [BD720p][A443D53E].mp4
│   │       ├── [animegrimoire] Log Horizon 2 - 24 [BD720p][2185C1FD].mp4
│   │       └── [animegrimoire] Log Horizon 2 - 25 [BD720p][3E0E0288].mp4
│   ├── Spring
│   │   ├── Gochuumon wa Usagi desu ka
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka - 01 [BD720p][87D80FB4].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka - 02 [BD720p][F6EE364C].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka - 03 [BD720p][C62DBBA3].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka - 04 [BD720p][A3DFC360].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka - 05 [BD720p][F5195D05].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka - 06 [BD720p][19D3F208].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka - 07 [BD720p][4846FFEE].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka - 08 [BD720p][0446F453].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka - 09 [BD720p][62BC9C38].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka - 10 [BD720p][DA80D4AA].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka - 11 [BD720p][C0280929].mp4
│   │   │   └── [animegrimoire] Gochuumon wa Usagi desu Ka - 12 [BD720p][A4F8E016].mp4
│   │   ├── Gokukoku no Brynhildr
│   │   │   ├── [animegrimoire] Gokukoku no Brynhildr - 01 [BD720p][88203541].mp4
│   │   │   ├── [animegrimoire] Gokukoku no Brynhildr - 02 [BD720p][7A138A1A].mp4
│   │   │   ├── [animegrimoire] Gokukoku no Brynhildr - 03 [BD720p][AFCF26EC].mp4
│   │   │   ├── [animegrimoire] Gokukoku no Brynhildr - 04 [BD720p][E5182B5C].mp4
│   │   │   ├── [animegrimoire] Gokukoku no Brynhildr - 05 [BD720p][E2290021].mp4
│   │   │   ├── [animegrimoire] Gokukoku no Brynhildr - 06 [BD720p][3EAFC0B0].mp4
│   │   │   ├── [animegrimoire] Gokukoku no Brynhildr - 07 [BD720p][7AC94040].mp4
│   │   │   ├── [animegrimoire] Gokukoku no Brynhildr - 08 [BD720p][46637A38].mp4
│   │   │   ├── [animegrimoire] Gokukoku no Brynhildr - 09 [BD720p][0074F89B].mp4
│   │   │   ├── [animegrimoire] Gokukoku no Brynhildr - 10 [BD720p][CA464E26].mp4
│   │   │   ├── [animegrimoire] Gokukoku no Brynhildr - 11.5 [BD720p][F7C35126].mp4
│   │   │   ├── [animegrimoire] Gokukoku no Brynhildr - 11 [BD720p][6E6CAA47].mp4
│   │   │   ├── [animegrimoire] Gokukoku no Brynhildr - 12 [BD720p][87307A62].mp4
│   │   │   ├── [animegrimoire] Gokukoku no Brynhildr - 13 [BD720p][90A4551D].mp4
│   │   │   ├── [animegrimoire] Gokukoku no Brynhildr - NCED1 [BD720p][DC054CCF].mp4
│   │   │   ├── [animegrimoire] Gokukoku no Brynhildr - NCED2 [BD720p][18587154].mp4
│   │   │   ├── [animegrimoire] Gokukoku no Brynhildr - NCOP1 [BD720p][AFD86642].mp4
│   │   │   └── [animegrimoire] Gokukoku no Brynhildr - NCOP2 [BD720p][0FC7B61D].mp4
│   │   ├── Hitsugi no Chaika The Coffin Princess
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Coffin Princess - 01 [BD720p][F2E9A27D].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Coffin Princess - 02 [BD720p][50AD3275].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Coffin Princess - 03 [BD720p][DC8EF573].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Coffin Princess - 04 [BD720p][56A95DC9].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Coffin Princess - 05 [BD720p][270C760A].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Coffin Princess - 06 [BD720p][282B846F].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Coffin Princess - 07 [BD720p][D6CB20B2].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Coffin Princess - 08 [BD720p][D0DF1426].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Coffin Princess - 09 [BD720p][13126C4B].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Coffin Princess - 10 [BD720p][E70B9F4A].mp4
│   │   │   ├── [animegrimoire] Hitsugi no Chaika The Coffin Princess - 11 [BD720p][1AA97FA8].mp4
│   │   │   └── [animegrimoire] Hitsugi no Chaika The Coffin Princess - 12 [BD720p][6F5F1420].mp4
│   │   └── Mahouka Koukou no Rettousei
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 01 [BD720p][7F185523].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 02 [BD720p][87D00A76].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 03 [BD720p][BAC036CB].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 04 [BD720p][0C7A39A0].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 05 [BD720p][81A4F375].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 06 [BD720p][C4A5529D].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 07 [BD720p][5F24E4D3].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 08 [BD720p][DFE87F4C].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 09 [BD720p][6D8ABC59].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 10 [BD720p][602F02A3].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 11 [BD720p][60772FAC].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 12 [BD720p][AC83304F].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 13 [BD720p][AB73EB9D].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 14 [BD720p][DFC6EC1A].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 15 [BD720p][70763A0E].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 16 [BD720p][F5434991].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 17 [BD720p][DA0955A7].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 18 [BD720p][B80A4F30].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 19 [BD720p][88FD30B4].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 20 [BD720p][A1A5BC70].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 21 [BD720p][3CF3BA9E].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 22 [BD720p][7C7A767D].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 23 [BD720p][F4BDEBE7].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 24 [BD720p][AD086A7F].mp4
│   │       ├── [animegrimoire] Mahouka Koukou no Rettousei - 25 [BD720p][68AFBD2C].mp4
│   │       └── [animegrimoire] Mahouka Koukou no Rettousei - 26 [BD720p][469A03E1].mp4
│   ├── Summer
│   │   ├── Hanayamata
│   │   │   ├── [animegrimoire] Hanayamata - 01 [BD720p][F680B5AE].mp4
│   │   │   ├── [animegrimoire] Hanayamata - 02 [BD720p][824452C8].mp4
│   │   │   ├── [animegrimoire] Hanayamata - 03 [BD720p][F07C4114].mp4
│   │   │   ├── [animegrimoire] Hanayamata - 04 [BD720p][BECDED09].mp4
│   │   │   ├── [animegrimoire] Hanayamata - 05 [BD720p][41330F8D].mp4
│   │   │   ├── [animegrimoire] Hanayamata - 06 [BD720p][17D8AC78].mp4
│   │   │   ├── [animegrimoire] Hanayamata - 07 [BD720p][34D36ED9].mp4
│   │   │   ├── [animegrimoire] Hanayamata - 08 [BD720p][CFBB1AC5].mp4
│   │   │   ├── [animegrimoire] Hanayamata - 09 [BD720p][E3C7D2B9].mp4
│   │   │   ├── [animegrimoire] Hanayamata - 10 [BD720p][C024D119].mp4
│   │   │   ├── [animegrimoire] Hanayamata - 11 [BD720p][41BC8AF1].mp4
│   │   │   ├── [animegrimoire] Hanayamata - 12 [BD720p][ACB56409].mp4
│   │   │   └── [animegrimoire] Hanayamata - 12 NCED [BD720p][FF601318].mp4
│   │   ├── Locodol
│   │   │   ├── [animegrimoire] Futsuu no Joshikousei ga [Locodol] Yatte Mita. - 01 [BD720p][A7C2A0E5].mp4
│   │   │   ├── [animegrimoire] Futsuu no Joshikousei ga [Locodol] Yatte Mita. - 02 [BD720p][9A2328FB].mp4
│   │   │   ├── [animegrimoire] Futsuu no Joshikousei ga [Locodol] Yatte Mita. - 03 [BD720p][B6AD30A6].mp4
│   │   │   ├── [animegrimoire] Futsuu no Joshikousei ga [Locodol] Yatte Mita. - 04 [BD720p][39E7F428].mp4
│   │   │   ├── [animegrimoire] Futsuu no Joshikousei ga [Locodol] Yatte Mita. - 05 [BD720p][D1A6B06F].mp4
│   │   │   ├── [animegrimoire] Futsuu no Joshikousei ga [Locodol] Yatte Mita. - 06 [BD720p][F4B9FAC7].mp4
│   │   │   ├── [animegrimoire] Futsuu no Joshikousei ga [Locodol] Yatte Mita. - 07 [BD720p][570B80F3].mp4
│   │   │   ├── [animegrimoire] Futsuu no Joshikousei ga [Locodol] Yatte Mita. - 08 [BD720p][BE03F3A0].mp4
│   │   │   ├── [animegrimoire] Futsuu no Joshikousei ga [Locodol] Yatte Mita. - 09 [BD720p][BAC17A4A].mp4
│   │   │   ├── [animegrimoire] Futsuu no Joshikousei ga [Locodol] Yatte Mita. - 10 [BD720p][F8B0074E].mp4
│   │   │   ├── [animegrimoire] Futsuu no Joshikousei ga [Locodol] Yatte Mita. - 11 [BD720p][45ED20D1].mp4
│   │   │   ├── [animegrimoire] Futsuu no Joshikousei ga [Locodol] Yatte Mita. - 12 [BD720p][C5BC0076].mp4
│   │   │   ├── [animegrimoire] Futsuu no Joshikousei ga [Locodol] Yatte Mita. - 13 [BD720p][BF8762DB].mp4
│   │   │   ├── [animegrimoire] Locodol - 14 [720p][A73F5657].mp4
│   │   │   └── [animegrimoire] Locodol - 15 [720p][4A53F537].mp4
│   │   ├── Prisma Illya 2
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei! - 01 [BD720p][E192C0A1].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei! - 02 [BD720p][4842EC45].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei! - 03 [BD720p][FBAB9B17].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei! - 04 [BD720p][6139A729].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei! - 05 [BD720p][7F2EF15A].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei! - 06 [BD720p][7E332DD4].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei! - 07 [BD720p][0CFF3219].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei! - 08 [BD720p][B59AE2BF].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei! - 09 [BD720p][47233E40].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei! - 10 [BD720p][1593875A].mp4
│   │   │   └── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei! - 11 [BD720p][C42C4F69].mp4
│   │   └── Prisma Illya Special 2
│   │       ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei! Specials - 01 [BD720p][373FD40C].mp4
│   │       ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei! Specials - 02 [BD720p][44A5C266].mp4
│   │       ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei! Specials - 03 [BD720p][D115FBDD].mp4
│   │       ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei! Specials - 04 [BD720p][D1CB4687].mp4
│   │       └── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei! Specials - 05 [BD720p][3AEF15CA].mp4
│   └── Winter
│       ├── Chuunibyou demo Koi ga Shitai! Ren
│       │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Ren - 01 [BD720p][44CE46AB].mp4
│       │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Ren - 02 [BD720p][9B0FABC4].mp4
│       │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Ren - 03 [BD720p][5F4D9176].mp4
│       │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Ren - 04 [BD720p][DDDA3E3A].mp4
│       │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Ren - 05 [BD720p][63BD16CF].mp4
│       │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Ren - 06 [BD720p][CF070EF5].mp4
│       │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Ren - 07 [BD720p][FBC4CDFD].mp4
│       │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Ren - 08 [BD720p][70AAC4B3].mp4
│       │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Ren - 09 [BD720p][EF162155].mp4
│       │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Ren - 10 [BD720p][E49F1D04].mp4
│       │   ├── [animegrimoire] Chuunibyou demo Koi ga Shitai! Ren - 11 [BD720p][F1C24A0E].mp4
│       │   └── [animegrimoire] Chuunibyou demo Koi ga Shitai! Ren - 12 [BD720p][B770D028].mp4
│       ├── Sakura Trick
│       │   ├── [animegrimoire] Sakura Trick - 01 [720p][30C8E2F7].mp4
│       │   ├── [animegrimoire] Sakura Trick - 02 [720p][8385E0D0].mp4
│       │   ├── [animegrimoire] Sakura Trick - 03 [720p][D0BBF237].mp4
│       │   ├── [animegrimoire] Sakura Trick - 04 [720p][03E2869E].mp4
│       │   ├── [animegrimoire] Sakura Trick - 05 [720p][F39AC778].mp4
│       │   ├── [animegrimoire] Sakura Trick - 06 [720p][C222B33F].mp4
│       │   ├── [animegrimoire] Sakura Trick - 07 [720p][E56B09BB].mp4
│       │   ├── [animegrimoire] Sakura Trick - 08 [720p][57C6DC56].mp4
│       │   ├── [animegrimoire] Sakura Trick - 09 [720p][35DF6476].mp4
│       │   ├── [animegrimoire] Sakura Trick - 10 [720p][B4643786].mp4
│       │   ├── [animegrimoire] Sakura Trick - 11 [720p][BFF4EA3C].mp4
│       │   └── [animegrimoire] Sakura Trick - 12 [720p][07567762].mp4
│       └── Seitokai Yakuindomo Bleep
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 01 [720p][4D709A29].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 02 [720p][DC32EDB3].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 03 [720p][AB93B1A0].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 04 [720p][F98E774D].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 05 [720p][BF51341A].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 06 [720p][F14CD5AA].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 07 [720p][DAC88B51].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 08 [720p][55C8BC92].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 09 [720p][7C0C451A].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 10 [720p][6BD7DD90].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 11 [720p][96F9F0C6].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 12 [720p][76398D01].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 13 [720p][50054487].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 14 [720p][B3FF83A0].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 15 [720p][C1EB8FED].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 16 [720p][C259D70B].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 17 [720p][91D66166].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 18 [720p][817A5AC0].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 19 [720p][1E253D70].mp4
│           ├── [animegrimoire] Seitokai Yakuindomo Bleep - 20-21 (Movie)[720p][B11D27BD].mp4
│           └── [animegrimoire] Seitokai Yakuindomo Bleep - 22 [720p][0317DF73].mp4
├── 2015
│   ├── Fall
│   │   ├── Gochuumon wa Usagi desu ka S2
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka 2 - 01 [BD720p][378E7350].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka 2 - 02 [BD720p][A152CA38].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka 2 - 03 [BD720p][C245BA4B].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka 2 - 04 [BD720p][80522DBD].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka 2 - 05 [BD720p][D4C27809].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka 2 - 06 [BD720p][2A89974C].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka 2 - 07 [BD720p][5A3452C9].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka 2 - 08 [BD720p][849C1EEB].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka 2 - 09 [BD720p][FBE84A4C].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka 2 - 10 [BD720p][BCB8B19F].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi desu Ka 2 - 11 [BD720p][30D7E2DA].mp4
│   │   │   └── [animegrimoire] Gochuumon wa Usagi desu Ka 2 - 12 [BD720p][1D69D8DF].mp4
│   │   ├── Hidan no Aria AA
│   │   │   ├── [animegrimoire] Hidan no Aria AA - 01 [BD720p][7581D132].mp4
│   │   │   ├── [animegrimoire] Hidan no Aria AA - 02 [BD720p][47EA7C7B].mp4
│   │   │   ├── [animegrimoire] Hidan no Aria AA - 03 [BD720p][40AD5028].mp4
│   │   │   ├── [animegrimoire] Hidan no Aria AA - 04 [BD720p][C2F8F5D2].mp4
│   │   │   ├── [animegrimoire] Hidan no Aria AA - 05 [BD720p][ABE559EF].mp4
│   │   │   ├── [animegrimoire] Hidan no Aria AA - 06 [BD720p][64F14D00].mp4
│   │   │   ├── [animegrimoire] Hidan no Aria AA - 07 [BD720p][2C0A3CDB].mp4
│   │   │   ├── [animegrimoire] Hidan no Aria AA - 08 [BD720p][0B2F49B2].mp4
│   │   │   ├── [animegrimoire] Hidan no Aria AA - 09 [BD720p][81FF097B].mp4
│   │   │   ├── [animegrimoire] Hidan no Aria AA - 10 [BD720p][05D01B8D].mp4
│   │   │   ├── [animegrimoire] Hidan no Aria AA - 11 [BD720p][E5C0C902].mp4
│   │   │   └── [animegrimoire] Hidan no Aria AA - 12 [BD720p][A8531934].mp4
│   │   ├── Komori-san wa Kotowarenai!
│   │   │   ├── [animegrimoire] Komori-san wa Kotowarenai! - 01 [BD 720p][54EBEA68].mp4
│   │   │   ├── [animegrimoire] Komori-san wa Kotowarenai! - 02 [BD 720p][745E6C95].mp4
│   │   │   ├── [animegrimoire] Komori-san wa Kotowarenai! - 03 [BD 720p][813F561F].mp4
│   │   │   ├── [animegrimoire] Komori-san wa Kotowarenai! - 04 [BD 720p][498A90D5].mp4
│   │   │   ├── [animegrimoire] Komori-san wa Kotowarenai! - 05 [BD 720p][C9F68AC3].mp4
│   │   │   ├── [animegrimoire] Komori-san wa Kotowarenai! - 06 [BD 720p][A0BB5131].mp4
│   │   │   ├── [animegrimoire] Komori-san wa Kotowarenai! - 07 [BD 720p][CA5AC978].mp4
│   │   │   ├── [animegrimoire] Komori-san wa Kotowarenai! - 08 [BD 720p][9F8F35D7].mp4
│   │   │   ├── [animegrimoire] Komori-san wa Kotowarenai! - 09 [BD 720p][F7E8AE17].mp4
│   │   │   ├── [animegrimoire] Komori-san wa Kotowarenai! - 10 [BD 720p][A2CE7777].mp4
│   │   │   ├── [animegrimoire] Komori-san wa Kotowarenai! - 11 [BD 720p][0923FCD6].mp4
│   │   │   └── [animegrimoire] Komori-san wa Kotowarenai! - 12 [BD 720p][329FD94E].mp4
│   │   ├── Owarimonogatari
│   │   │   ├── [animegrimoire] Owarimonogatari - 01 [BD720p][10E96671].mp4
│   │   │   ├── [animegrimoire] Owarimonogatari - 02 [BD720p][C5ACA2E8].mp4
│   │   │   ├── [animegrimoire] Owarimonogatari - 03 [BD720p][D51ED703].mp4
│   │   │   ├── [animegrimoire] Owarimonogatari - 04 [BD720p][D1F96DCC].mp4
│   │   │   ├── [animegrimoire] Owarimonogatari - 05 [BD720p][9FC11517].mp4
│   │   │   ├── [animegrimoire] Owarimonogatari - 06 [BD720p][C2A885FC].mp4
│   │   │   ├── [animegrimoire] Owarimonogatari - 07 [BD720p][3541C429].mp4
│   │   │   ├── [animegrimoire] Owarimonogatari - 08 [BD720p][35806972].mp4
│   │   │   ├── [animegrimoire] Owarimonogatari - 09 [BD720p][D53A43D3].mp4
│   │   │   ├── [animegrimoire] Owarimonogatari - 10 [BD720p][B2556287].mp4
│   │   │   ├── [animegrimoire] Owarimonogatari - 11 [BD720p][85E8ECAF].mp4
│   │   │   ├── [animegrimoire] Owarimonogatari - 12 [BD720p][00072DF5].mp4
│   │   │   ├── [animegrimoire] Owarimonogatari - 13 [BD720p][A3559C5B].mp4
│   │   │   ├── [animegrimoire] Owarimonogatari S2 - 01~02 (Mayoi Hell) [720p][CCB3A187][E21FF5C7].mp4
│   │   │   ├── [animegrimoire] Owarimonogatari S2 - 03~04 (Hitagi Rendezvous) [720p][AEE01BB3][90040D94].mp4
│   │   │   └── [animegrimoire] Owarimonogatari S2 - 05~07 (Ougi Dark) [END][720p][3C656736][DC2D39EB].mp4
│   │   └── YuruYuri S3
│   │       ├── [animegrimoire] YuruYuri S3 - 01 [720p][F775F4BD].mp4
│   │       ├── [animegrimoire] YuruYuri S3 - 02 [720p][D01C0484].mp4
│   │       ├── [animegrimoire] YuruYuri S3 - 03 [720p][7C38E188].mp4
│   │       ├── [animegrimoire] YuruYuri S3 - 04 [720p][136007E4].mp4
│   │       ├── [animegrimoire] YuruYuri S3 - 05 [720p][F53261E5].mp4
│   │       ├── [animegrimoire] YuruYuri S3 - 06 [720p][33EBE3D9].mp4
│   │       ├── [animegrimoire] YuruYuri S3 - 07 [720p][8DCD8155].mp4
│   │       ├── [animegrimoire] YuruYuri S3 - 08 [720p][2CEC4FA2].mp4
│   │       ├── [animegrimoire] YuruYuri S3 - 09 [720p][F37E62FD].mp4
│   │       ├── [animegrimoire] YuruYuri S3 - 10 [720p][4E94B709].mp4
│   │       ├── [animegrimoire] YuruYuri S3 - 11 [720p][BAC5533F].mp4
│   │       └── [animegrimoire] YuruYuri S3 - 12 [720p][F2F8312B].mp4
│   ├── Spring
│   │   ├── Hello!! Kiniro Mosaic
│   │   │   ├── [animegrimoire] Hello!! Kiniro Mosaic - 01 [BD720p][41D3B932].mp4
│   │   │   ├── [animegrimoire] Hello!! Kiniro Mosaic - 02 [BD720p][4421AA48].mp4
│   │   │   ├── [animegrimoire] Hello!! Kiniro Mosaic - 03 [BD720p][E0F0AA9B].mp4
│   │   │   ├── [animegrimoire] Hello!! Kiniro Mosaic - 04 [BD720p][342B9351].mp4
│   │   │   ├── [animegrimoire] Hello!! Kiniro Mosaic - 05 [BD720p][9F89A802].mp4
│   │   │   ├── [animegrimoire] Hello!! Kiniro Mosaic - 06 [BD720p][4C801BC4].mp4
│   │   │   ├── [animegrimoire] Hello!! Kiniro Mosaic - 07 [BD720p][6F63B0E0].mp4
│   │   │   ├── [animegrimoire] Hello!! Kiniro Mosaic - 08 [BD720p][9267D09C].mp4
│   │   │   ├── [animegrimoire] Hello!! Kiniro Mosaic - 09 [BD720p][06558D64].mp4
│   │   │   ├── [animegrimoire] Hello!! Kiniro Mosaic - 10 [BD720p][18AB565B].mp4
│   │   │   ├── [animegrimoire] Hello!! Kiniro Mosaic - 11 [BD720p][2E477ED5].mp4
│   │   │   └── [animegrimoire] Hello!! Kiniro Mosaic - 12 [BD720p][F52531E2].mp4
│   │   ├── Plastic Memories
│   │   │   ├── [animegrimoire] Plastic Memories - 01 [BD720p][62C72207].mp4
│   │   │   ├── [animegrimoire] Plastic Memories - 02 [BD720p][B6CE739E].mp4
│   │   │   ├── [animegrimoire] Plastic Memories - 03 [BD720p][BECEECFA].mp4
│   │   │   ├── [animegrimoire] Plastic Memories - 04 [BD720p][D13B47B0].mp4
│   │   │   ├── [animegrimoire] Plastic Memories - 05 [BD720p][D08E6C2C].mp4
│   │   │   ├── [animegrimoire] Plastic Memories - 06 [BD720p][C7A7FE95].mp4
│   │   │   ├── [animegrimoire] Plastic Memories - 07 [BD720p][68E8FBCF].mp4
│   │   │   ├── [animegrimoire] Plastic Memories - 08 [BD720p][C2995939].mp4
│   │   │   ├── [animegrimoire] Plastic Memories - 09 [BD720p][C5DC64EB].mp4
│   │   │   ├── [animegrimoire] Plastic Memories - 10 [BD720p][820D1D98].mp4
│   │   │   ├── [animegrimoire] Plastic Memories - 11 [BD720p][88449E17].mp4
│   │   │   ├── [animegrimoire] Plastic Memories - 12 [BD720p][86F5A0C7].mp4
│   │   │   ├── [animegrimoire] Plastic Memories - 13 [BD720p][7D7EAD39].mp4
│   │   │   ├── [animegrimoire] Plastic Memories - NCED [BD720p][3853B76D].mp4
│   │   │   └── [animegrimoire] Plastic Memories - NCOP [BD720p][8DF05F27].mp4
│   │   ├── Re-Kan
│   │   │   ├── [animegrimoire] Re-Kan! - 01 [BD720p][FCFD543A].mp4
│   │   │   ├── [animegrimoire] Re-Kan! - 02 [BD720p][DD74D676].mp4
│   │   │   ├── [animegrimoire] Re-Kan! - 03 [BD720p][552E08C8].mp4
│   │   │   ├── [animegrimoire] Re-Kan! - 04 [BD720p][14E032D9].mp4
│   │   │   ├── [animegrimoire] Re-Kan! - 05 [BD720p][20C04531].mp4
│   │   │   ├── [animegrimoire] Re-Kan! - 06 [BD720p][2CFE6DC7].mp4
│   │   │   ├── [animegrimoire] Re-Kan! - 07 [BD720p] [86E23455].mp4
│   │   │   ├── [animegrimoire] Re-Kan! - 08 [BD720p] [24AD8ED7].mp4
│   │   │   ├── [animegrimoire] Re-Kan! - 09 [BD720p] [73F190FA].mp4
│   │   │   ├── [animegrimoire] Re-Kan! - 10 [BD720p] [BA4E704F].mp4
│   │   │   ├── [animegrimoire] Re-Kan! - 11 [BD720p][BE38B319].mp4
│   │   │   ├── [animegrimoire] Re-Kan! - 12 [BD720p][DFDC79AE].mp4
│   │   │   └── [animegrimoire] Re-Kan! - 13 [BD720p][97A13730].mp4
│   │   └── Show by Rock!!
│   │       ├── [animegrimoire] Show by Rock!! - 01 [BD720p][5BCE3072].mp4
│   │       ├── [animegrimoire] Show by Rock!! - 02 [BD720p][0AEA4603].mp4
│   │       ├── [animegrimoire] Show by Rock!! - 03 [BD720p][DB7F1E2B].mp4
│   │       ├── [animegrimoire] Show by Rock!! - 04 [BD720p][8CAAE407].mp4
│   │       ├── [animegrimoire] Show by Rock!! - 05 [BD720p][6681C76F].mp4
│   │       ├── [animegrimoire] Show by Rock!! - 06 [BD720p][8A2E3CB1].mp4
│   │       ├── [animegrimoire] Show by Rock!! - 07 [BD720p][08C422CC].mp4
│   │       ├── [animegrimoire] Show by Rock!! - 08 [BD720p][A643D955].mp4
│   │       ├── [animegrimoire] Show by Rock!! - 09 [BD720p][74274943].mp4
│   │       ├── [animegrimoire] Show by Rock!! - 10 [BD720p][5D3E4556].mp4
│   │       ├── [animegrimoire] Show by Rock!! - 11 [BD720p][49985DA0].mp4
│   │       ├── [animegrimoire] Show by Rock!! - 12 [BD720p][D457122C].mp4
│   │       ├── [animegrimoire] Show By Rock!! S2 - 01 [BD720p][CC705B01].mp4
│   │       ├── [animegrimoire] Show By Rock!! S2 - 02 [BD720p][08E84CF3].mp4
│   │       ├── [animegrimoire] Show By Rock!! S2 - 03 [BD720p][E5472CFA].mp4
│   │       ├── [animegrimoire] Show By Rock!! S2 - 04 [BD720p][1D0B85E8].mp4
│   │       ├── [animegrimoire] Show By Rock!! S2 - 05 [BD720p][0A35635B].mp4
│   │       ├── [animegrimoire] Show By Rock!! S2 - 06 [BD720p][1E878A06].mp4
│   │       ├── [animegrimoire] Show By Rock!! S2 - 07 [BD720p][8BCD7DDD].mp4
│   │       ├── [animegrimoire] Show By Rock!! S2 - 08 [BD720p][E1740B3B].mp4
│   │       ├── [animegrimoire] Show By Rock!! S2 - 09 [BD720p][17B857E5].mp4
│   │       ├── [animegrimoire] Show By Rock!! S2 - 10 [BD720p][8C27BD1D].mp4
│   │       ├── [animegrimoire] Show By Rock!! S2 - 11 [BD720p][79F55D85].mp4
│   │       └── [animegrimoire] Show By Rock!! S2 - 12 [BD720p][82413EF8].mp4
│   ├── Summer
│   │   ├── Amagi Brilliant Park
│   │   │   ├── [animegrimoire] Amagi Brilliant Park - 01 [BD720p][3957A1E6].mp4
│   │   │   ├── [animegrimoire] Amagi Brilliant Park - 02 [BD720p][2533C85A].mp4
│   │   │   ├── [animegrimoire] Amagi Brilliant Park - 03 [BD720p][67CD9A63].mp4
│   │   │   ├── [animegrimoire] Amagi Brilliant Park - 04 [BD720p][7AEFA972].mp4
│   │   │   ├── [animegrimoire] Amagi Brilliant Park - 05 [BD720p][8437EF61].mp4
│   │   │   ├── [animegrimoire] Amagi Brilliant Park - 06 [BD720p][A7603FF2].mp4
│   │   │   ├── [animegrimoire] Amagi Brilliant Park - 07 [BD720p][DFCE37A2].mp4
│   │   │   ├── [animegrimoire] Amagi Brilliant Park - 08 [BD720p][34A09975].mp4
│   │   │   ├── [animegrimoire] Amagi Brilliant Park - 09 [BD720p][8F8B1591].mp4
│   │   │   ├── [animegrimoire] Amagi Brilliant Park - 10 [BD720p][FCA11347].mp4
│   │   │   ├── [animegrimoire] Amagi Brilliant Park - 11 [BD720p][DDC67368].mp4
│   │   │   ├── [animegrimoire] Amagi Brilliant Park - 12 [BD720p][D9DD07F3].mp4
│   │   │   ├── [animegrimoire] Amagi Brilliant Park - 13 [BD720p][5364ACF1].mp4
│   │   │   └── [animegrimoire] Amagi Brilliant Park - 14 [BD720p][1835E9CD].mp4
│   │   ├── GATE
│   │   │   ├── [animegrimoire] GATE - 01 [720p][D7030705].mp4
│   │   │   ├── [animegrimoire] GATE - 02 [720p][352015B5].mp4
│   │   │   ├── [animegrimoire] GATE - 03 [720p][9486DB38].mp4
│   │   │   ├── [animegrimoire] GATE - 04 [720p][7F94BD1E].mp4
│   │   │   ├── [animegrimoire] GATE - 05 [720p][01111D56].mp4
│   │   │   ├── [animegrimoire] GATE - 06 [720p][41D0A410].mp4
│   │   │   ├── [animegrimoire] GATE - 07 [720p][1798EEAA].mp4
│   │   │   ├── [animegrimoire] GATE - 08 [720p][49AEA316].mp4
│   │   │   ├── [animegrimoire] GATE - 09 [720p][5467205D].mp4
│   │   │   ├── [animegrimoire] GATE - 10 [720p][9A8F3925].mp4
│   │   │   ├── [animegrimoire] GATE - 11 [720p][0A717559].mp4
│   │   │   ├── [animegrimoire] GATE - 12 [720p][8E300041].mp4
│   │   │   ├── [animegrimoire] GATE - 13 [720p][080BB868].mp4
│   │   │   ├── [animegrimoire] GATE - 14 [720p][884BBC79].mp4
│   │   │   ├── [animegrimoire] GATE - 15 [720p][610836E3].mp4
│   │   │   ├── [animegrimoire] GATE - 16 [720p][7F39DA3F].mp4
│   │   │   ├── [animegrimoire] GATE - 17 [720p][B9F63953].mp4
│   │   │   ├── [animegrimoire] GATE - 18 [720p][126B6F34].mp4
│   │   │   ├── [animegrimoire] GATE - 19 [720p][FBCF2819].mp4
│   │   │   ├── [animegrimoire] GATE - 20 [720p][73F7007D].mp4
│   │   │   ├── [animegrimoire] GATE - 21 [720p][452BE31C].mp4
│   │   │   ├── [animegrimoire] GATE - 22 [720p][9FB3AB14].mp4
│   │   │   ├── [animegrimoire] GATE - 23 [720p][332E51B2].mp4
│   │   │   └── [animegrimoire] GATE - 24 [720p][90526E20].mp4
│   │   ├── Gekijouban Date a Live Mayuri Judgement
│   │   │   └── [animegrimoire] Gekijouban Date a Live Mayuri Judgement - 00 [BD720p][47DFB7AF].mp4
│   │   ├── Himouto! Umaruchan
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan - 01 [BD720p][C0DFADA0].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan - 02 [BD720p][5A66EF82].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan - 03 [BD720p][8267AD80].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan - 04 [BD720p][8B8D3660].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan - 05 [BD720p][1EA820AB].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan - 06 [BD720p][41B3B40E].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan - 07 [BD720p][B66A7D8A].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan - 07 [BD720p][DA95EBE4].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan - 08 [BD720p][E5B7E573].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan - 09 [BD720p][7E2B411D].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan - 10 [BD720p][05AB7FC2].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan - 11 [BD720p][44DD80BC].mp4
│   │   │   └── [animegrimoire] Himouto! Umaru-chan - 12 [BD720p][02F4ABFB].mp4
│   │   ├── Himouto! Umaruchan OAD
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan OAD - 01 [BD720p][936ED34B].mp4
│   │   │   └── [animegrimoire] Himouto! Umaru-chan OAD - 02 [BD720p][9EBFF60E].mp4
│   │   ├── Himouto! Umaruchan S
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan S - 01 [BD720p][045697B8].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan S - 02 [BD720p][1D325C64].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan S - 03 [BD720p][9D429830].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan S - 04 [BD720p][13A35CC9].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan S - 05 [BD720p][C665C572].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan S - 06 [BD720p][0CCFF7B4].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan S - 07 [BD720p][0A797903].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan S - 08 [BD720p][BD7755A4].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan S - 09 [BD720p][702276EB].mp4
│   │   │   └── [animegrimoire] Himouto! Umaru-chan S - 10 [BD720p][507C583E].mp4
│   │   ├── Prisma Illya 3
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei Herz! - 01 [BD720p][E99B1075].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei Herz! - 02 [BD720p][455AA06C].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei Herz! - 03 [BD720p][C135DB38].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei Herz! - 04 [BD720p][2CC7A920].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei Herz! - 05 [BD720p][A6B2599E].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei Herz! - 06 [BD720p][760228BA].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei Herz! - 07 [BD720p][21423FA9].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei Herz! - 08 [BD720p][A3F8A7DC].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei Herz! - 09 [BD720p][8F050719].mp4
│   │   │   └── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei Herz! - 10 [BD720p][E4451714].mp4
│   │   ├── Prisma Illya Special 3
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei Herz! Specials - 01 [BD720p][AB64EB2B].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei Herz! Specials - 02 [BD720p][CD412853].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei Herz! Specials - 03 [BD720p][2853568D].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei Herz! Specials - 04 [BD720p][1B528633].mp4
│   │   │   └── [animegrimoire] Fate Kaleid Liner Prisma Illya 2wei Herz! Specials - 05 [BD720p][9D601BA9].mp4
│   │   └── Wakaba Girl
│   │       ├── [Animegrimoire] Wakaba Girl - 01 [720p].mp4
│   │       ├── [Animegrimoire] Wakaba Girl - 02 [720p].mp4
│   │       ├── [Animegrimoire] Wakaba Girl - 03 [720p].mp4
│   │       ├── [Animegrimoire] Wakaba Girl - 04 [720p].mp4
│   │       ├── [Animegrimoire] Wakaba Girl - 05 [720p].mp4
│   │       ├── [Animegrimoire] Wakaba Girl - 06 [720p].mp4
│   │       ├── [Animegrimoire] Wakaba Girl - 07 [720p].mp4
│   │       ├── [Animegrimoire] Wakaba Girl - 08 [720p].mp4
│   │       ├── [Animegrimoire] Wakaba Girl - 09 [720p].mp4
│   │       ├── [Animegrimoire] Wakaba Girl - 10 [720p].mp4
│   │       ├── [Animegrimoire] Wakaba Girl - 11 [720p].mp4
│   │       ├── [Animegrimoire] Wakaba Girl - 12 [720p].mp4
│   │       └── [Animegrimoire] Wakaba Girl - 13 [720p].mp4
│   └── Winter
│       ├── Ongaku Shoujo
│       │   ├── [animegrimoire] Ongaku Shoujo - 01 [720p][891BAD98].mp4
│       │   ├── [animegrimoire] Ongaku Shoujo - 02 [720p][DE252422].mp4
│       │   ├── [animegrimoire] Ongaku Shoujo - 03 [720p][96492C47].mp4
│       │   ├── [animegrimoire] Ongaku Shoujo - 04 [720p][69F7107B].mp4
│       │   ├── [animegrimoire] Ongaku Shoujo - 05 [720p][4D1B9BBB].mp4
│       │   ├── [animegrimoire] Ongaku Shoujo - 06 [720p][D72B7890].mp4
│       │   ├── [animegrimoire] Ongaku Shoujo - 07 [720p][6F0D9F64].mp4
│       │   ├── [animegrimoire] Ongaku Shoujo - 08 [720p][3635A350].mp4
│       │   ├── [animegrimoire] Ongaku Shoujo - 09 [720p][916EF2A3].mp4
│       │   ├── [animegrimoire] Ongaku Shoujo - 10 [720p][A27AF9CC].mp4
│       │   ├── [animegrimoire] Ongaku Shoujo - 11 [720p][916BB36C].mp4
│       │   ├── [animegrimoire] Ongaku Shoujo - 12 [720p][2DC4AC3B].mp4
│       │   └── [animegrimoire] Ongaku Shoujo [720p][2A07F3FF].mp4
│       ├── Tantei Kageki Milky Holmes TD
│       │   ├── [animegrimoire] Tantei Kageki Milky Holmes TD - 01 [720p][7D5A5F72].mp4
│       │   ├── [animegrimoire] Tantei Kageki Milky Holmes TD - 02 [720p][36A81C1A].mp4
│       │   ├── [animegrimoire] Tantei Kageki Milky Holmes TD - 03 [720p][F64FB401].mp4
│       │   ├── [animegrimoire] Tantei Kageki Milky Holmes TD - 04 [720p][96373135].mp4
│       │   ├── [animegrimoire] Tantei Kageki Milky Holmes TD - 05 [720p][D69A67ED].mp4
│       │   ├── [animegrimoire] Tantei Kageki Milky Holmes TD - 06 [720p][98386F9E].mp4
│       │   ├── [animegrimoire] Tantei Kageki Milky Holmes TD - 07 [720p][AB696D8A].mp4
│       │   ├── [animegrimoire] Tantei Kageki Milky Holmes TD - 08 [720p][6C7FA878].mp4
│       │   ├── [animegrimoire] Tantei Kageki Milky Holmes TD - 09 [720p][1D3B06A3].mp4
│       │   ├── [animegrimoire] Tantei Kageki Milky Holmes TD - 10 [720p][10B2498E].mp4
│       │   ├── [animegrimoire] Tantei Kageki Milky Holmes TD - 11 [720p][6C7D08F7].mp4
│       │   └── [animegrimoire] Tantei Kageki Milky Holmes TD - 12 [720p][BDDEEC61].mp4
│       └── Yuri Kuma Arashi
│           ├── [animegrimoire] Yuri Kuma Arashi - 01 [BD720p][5E1070E4].mp4
│           ├── [animegrimoire] Yuri Kuma Arashi - 02 [BD720p][DA553B00].mp4
│           ├── [animegrimoire] Yuri Kuma Arashi - 03 [BD720p][AB91C7CB].mp4
│           ├── [animegrimoire] Yuri Kuma Arashi - 04 [BD720p][542CFE2C].mp4
│           ├── [animegrimoire] Yuri Kuma Arashi - 05 [BD720p][6F00B908].mp4
│           ├── [animegrimoire] Yuri Kuma Arashi - 06 [BD720p][EAB55C74].mp4
│           ├── [animegrimoire] Yuri Kuma Arashi - 07 [BD720p][B3E07F90].mp4
│           ├── [animegrimoire] Yuri Kuma Arashi - 08 [BD720p][4B959498].mp4
│           ├── [animegrimoire] Yuri Kuma Arashi - 09 [BD720p][B3160EB7].mp4
│           ├── [animegrimoire] Yuri Kuma Arashi - 10 [BD720p][E153D90A].mp4
│           ├── [animegrimoire] Yuri Kuma Arashi - 11 [BD720p][DDCCF115].mp4
│           └── [animegrimoire] Yuri Kuma Arashi - 12 [BD720p][C46DB542].mp4
├── 2016
│   ├── Fall
│   │   ├── Brave Witches
│   │   │   ├── [animegrimoire] Brave Witches 01 [BD 720p][5C33AD53].mp4
│   │   │   ├── [animegrimoire] Brave Witches 02 [BD 720p][58116E15].mp4
│   │   │   ├── [animegrimoire] Brave Witches 03 [BD 720p][53DB4F13].mp4
│   │   │   ├── [animegrimoire] Brave Witches 04 [BD 720p][E4104681].mp4
│   │   │   ├── [animegrimoire] Brave Witches 05 [BD 720p][055AF11B].mp4
│   │   │   ├── [animegrimoire] Brave Witches 06 [BD 720p][F4968DEF].mp4
│   │   │   ├── [animegrimoire] Brave Witches 07 [BD 720p][6244956B].mp4
│   │   │   ├── [animegrimoire] Brave Witches 08 [BD 720p][CE9D542D].mp4
│   │   │   ├── [animegrimoire] Brave Witches 09 [BD 720p][5309923A].mp4
│   │   │   ├── [animegrimoire] Brave Witches 10 [BD 720p][642AA6C3].mp4
│   │   │   ├── [animegrimoire] Brave Witches 11 [BD 720p][C79EAB0F].mp4
│   │   │   └── [animegrimoire] Brave Witches 12 [BD 720p][89422984].mp4
│   │   ├── Flip Flappers
│   │   │   ├── [animegrimoire] Flip Flappers - 01 [BD720p][2061E25F].mp4
│   │   │   ├── [animegrimoire] Flip Flappers - 02 [BD720p][23BD9865].mp4
│   │   │   ├── [animegrimoire] Flip Flappers - 03 [BD720p][762211E7].mp4
│   │   │   ├── [animegrimoire] Flip Flappers - 04 [BD720p][6611AE41].mp4
│   │   │   ├── [animegrimoire] Flip Flappers - 05 [BD720p][7551A6CE].mp4
│   │   │   ├── [animegrimoire] Flip Flappers - 06 [BD720p][609B8B97].mp4
│   │   │   ├── [animegrimoire] Flip Flappers - 07 [BD720p][3A8AB0A1].mp4
│   │   │   ├── [animegrimoire] Flip Flappers - 08 [BD720p][2693DC06].mp4
│   │   │   ├── [animegrimoire] Flip Flappers - 09 [BD720p][E85527D5].mp4
│   │   │   ├── [animegrimoire] Flip Flappers - 10 [BD720p][11748978].mp4
│   │   │   ├── [animegrimoire] Flip Flappers - 11 [BD720p][6388C954].mp4
│   │   │   ├── [animegrimoire] Flip Flappers - 12 [BD720p][22AA1B07].mp4
│   │   │   └── [animegrimoire] Flip Flappers - 13 [BD720p][645C5B0D].mp4
│   │   ├── Movies
│   │   │   ├── [animegrimoire] In This Corner of the World [BD720p][EC5FAC65].mp4
│   │   │   └── [animegrimoire] Kiniro Mosaic - Pretty Days [BD720p][2118DBCA].mp4
│   │   ├── Shelter
│   │   │   └── [animegrimoire] Shelter [BD720p x264][B35438C1].mp4
│   │   ├── Stella no Mahou
│   │   │   ├── [animegrimoire] Stella no Mahou - 01 [720p][D962A4EC].mp4
│   │   │   ├── [animegrimoire] Stella no Mahou - 02 [720p][E4330F61].mp4
│   │   │   ├── [animegrimoire] Stella no Mahou - 03 [720p][E5A0E56E].mp4
│   │   │   ├── [animegrimoire] Stella no Mahou - 04 [720p][6E8BF347].mp4
│   │   │   ├── [animegrimoire] Stella no Mahou - 05 [720p][91EC6B29].mp4
│   │   │   ├── [animegrimoire] Stella no Mahou - 06 [720p][427D34B9].mp4
│   │   │   ├── [animegrimoire] Stella no Mahou - 07 [720p][9016B27A].mp4
│   │   │   ├── [animegrimoire] Stella no Mahou - 08 [720p][544932B2].mp4
│   │   │   ├── [animegrimoire] Stella no Mahou - 09 [720p][5F6ED132].mp4
│   │   │   ├── [animegrimoire] Stella no Mahou - 10 [720p][BD9366ED].mp4
│   │   │   ├── [animegrimoire] Stella no Mahou - 11 [720p][C65FE628].mp4
│   │   │   └── [animegrimoire] Stella no Mahou - 12 [720p][14D80146].mp4
│   │   ├── Tantei Opera Milky Holmes
│   │   │   └── [animegrimoire] Tantei Opera Milky Holmes Fun Fun Pearly Night - Ken to Janet no Okurimono [720p][75FB72D9].mp4
│   │   └── Vivid Strike!
│   │       ├── [animegrimoire] Vivid Strike! - 01 [BD720p][E9419312].mp4
│   │       ├── [animegrimoire] Vivid Strike! - 02 [BD720p][44A6A70B].mp4
│   │       ├── [animegrimoire] Vivid Strike! - 03 [BD720p][737BFAEB].mp4
│   │       ├── [animegrimoire] Vivid Strike! - 04 [BD720p][1D14B17D].mp4
│   │       ├── [animegrimoire] Vivid Strike! - 05.5 [BD720p][F157D563].mp4
│   │       ├── [animegrimoire] Vivid Strike! - 05.75 [BD720p][403B9C09].mp4
│   │       ├── [animegrimoire] Vivid Strike! - 05 [BD720p][31BA6E38].mp4
│   │       ├── [animegrimoire] Vivid Strike! - 06 [BD720p][05C30BA8].mp4
│   │       ├── [animegrimoire] Vivid Strike! - 07 [BD720p][FE444830].mp4
│   │       ├── [animegrimoire] Vivid Strike! - 08 [BD720p][EEB73CE5].mp4
│   │       ├── [animegrimoire] Vivid Strike! - 09 [BD720p][9D74733E].mp4
│   │       ├── [animegrimoire] Vivid Strike! - 10 [BD720p][D33ECC52].mp4
│   │       ├── [animegrimoire] Vivid Strike! - 11 [BD720p][797C15B2].mp4
│   │       ├── [animegrimoire] Vivid Strike! - 12 [BD720p][BC3F556C].mp4
│   │       └── [animegrimoire] Vivid Strike! - 13 OVA [BD720p][FE7F2DED].mp4
│   ├── Spring
│   │   ├── Anne Happy
│   │   │   ├── [animegrimoire] Anne Happy - 01 [720p][9592F5BF].mp4
│   │   │   ├── [animegrimoire] Anne Happy - 02 [720p][20C91F2D].mp4
│   │   │   ├── [animegrimoire] Anne Happy - 03 [720p][9A4A0D31].mp4
│   │   │   ├── [animegrimoire] Anne Happy - 04 [720p][840EA692].mp4
│   │   │   ├── [animegrimoire] Anne Happy - 05 [720p][44B03982].mp4
│   │   │   ├── [animegrimoire] Anne Happy - 06 [720p][6FB3B05D].mp4
│   │   │   ├── [animegrimoire] Anne Happy - 07 [720p][05998AFA].mp4
│   │   │   ├── [animegrimoire] Anne Happy - 08 [720p][ADAFDDD2].mp4
│   │   │   ├── [animegrimoire] Anne Happy - 09 [720p][AD28EB09].mp4
│   │   │   ├── [animegrimoire] Anne Happy - 10 [720p][ED44954E].mp4
│   │   │   ├── [animegrimoire] Anne Happy - 11 [720p][F41F155D].mp4
│   │   │   └── [animegrimoire] Anne Happy - 12 [720p][1DA8E427].mp4
│   │   ├── Flying Witch
│   │   │   ├── [AnimeGrimoire] Flying Witch - 01 [720p][510CC4D4].mp4
│   │   │   ├── [AnimeGrimoire] Flying Witch - 02 [720p][05739A1A].mp4
│   │   │   ├── [AnimeGrimoire] Flying Witch - 03 [720p][BC5CA988].mp4
│   │   │   ├── [AnimeGrimoire] Flying Witch - 04 [720p][084D0D17].mp4
│   │   │   ├── [AnimeGrimoire] Flying Witch - 05 [720p][031BC171].mp4
│   │   │   ├── [AnimeGrimoire] Flying Witch - 06 [720p][C29F9016].mp4
│   │   │   ├── [AnimeGrimoire] Flying Witch - 07 [720p][6F4F634D].mp4
│   │   │   ├── [AnimeGrimoire] Flying Witch - 08 [720p][67C7D112].mp4
│   │   │   ├── [AnimeGrimoire] Flying Witch - 09 [720p][32BAE02C].mp4
│   │   │   ├── [AnimeGrimoire] Flying Witch - 10 [720p][8AE698FE].mp4
│   │   │   ├── [AnimeGrimoire] Flying Witch - 11 [720p][B1FB0202].mp4
│   │   │   └── [AnimeGrimoire] Flying Witch - 12 [720p][C6D4D512].mp4
│   │   ├── Koutetsujou no Kabaneri
│   │   │   ├── [animegrimoire] Koutetsujou no Kabaneri - 01 [BD720p][72F71BA2].mp4
│   │   │   ├── [animegrimoire] Koutetsujou no Kabaneri - 02 [BD720p][54EC1A89].mp4
│   │   │   ├── [animegrimoire] Koutetsujou no Kabaneri - 03 [BD720p][BDF5B6D1].mp4
│   │   │   ├── [animegrimoire] Koutetsujou no Kabaneri - 04 [BD720p][68A42F51].mp4
│   │   │   ├── [animegrimoire] Koutetsujou no Kabaneri - 05 [BD720p][7E81C458].mp4
│   │   │   ├── [animegrimoire] Koutetsujou no Kabaneri - 06 [BD720p][C8EF2C6F].mp4
│   │   │   ├── [animegrimoire] Koutetsujou no Kabaneri - 07 [BD720p][499813D9].mp4
│   │   │   ├── [animegrimoire] Koutetsujou no Kabaneri - 08 [BD720p][B4C0F708].mp4
│   │   │   ├── [animegrimoire] Koutetsujou no Kabaneri - 09 [BD720p][72D6934C].mp4
│   │   │   ├── [animegrimoire] Koutetsujou no Kabaneri - 10 [BD720p][D60AFE21].mp4
│   │   │   ├── [animegrimoire] Koutetsujou no Kabaneri - 11 [BD720p][EF85361F].mp4
│   │   │   └── [animegrimoire] Koutetsujou no Kabaneri - 12 [BD720p][A3057295].mp4
│   │   ├── Pan de Peace
│   │   │   ├── [animegrimoire] Pan de Peace! - 01 [720p][24327FBC].mp4
│   │   │   ├── [animegrimoire] Pan de Peace! - 02 [720p][A474B4CB].mp4
│   │   │   ├── [animegrimoire] Pan de Peace! - 03 [720p][D5BC6315].mp4
│   │   │   ├── [animegrimoire] Pan de Peace! - 04 [720p][3BD9532F].mp4
│   │   │   ├── [animegrimoire] Pan de Peace! - 05 [720p][A7E7F11A].mp4
│   │   │   ├── [animegrimoire] Pan de Peace! - 06 [720p][06B02479].mp4
│   │   │   ├── [animegrimoire] Pan de Peace! - 07 [720p][4168751F].mp4
│   │   │   ├── [animegrimoire] Pan de Peace! - 08 [720p][D339ED7F].mp4
│   │   │   ├── [animegrimoire] Pan de Peace! - 09 [720p][7605E139].mp4
│   │   │   ├── [animegrimoire] Pan de Peace! - 10 [720p][BC256162].mp4
│   │   │   ├── [animegrimoire] Pan de Peace! - 11 [720p][ADE5FC37].mp4
│   │   │   ├── [animegrimoire] Pan de Peace! - 12 [720p][056A271D].mp4
│   │   │   └── [animegrimoire] Pan de Peace! - 13 [720p][9E89C413].mp4
│   │   └── Re Zero kara Hajimeru Isekai Seikatsu
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 01 [BD720p][6211FFEE].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 02 [BD720p][94717FC4].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 03 [BD720p][AF02A639].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 04 [BD720p][5D84AB18].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 05 [BD720p][C73D2EA1].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 06 [BD720p][25DD61A1].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 07 [BD720p][1EB32F5D].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 08 [BD720p][2F826E53].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 09 [BD720p][98E8BADF].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 10 [BD720p][D36DB767].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 11 [BD720p][140BEDD2].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 12 [BD720p][0EC22C14].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 13 [BD720p][25BAC0BE].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 14 [BD720p][C7BF1B0B].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 15 [BD720p][A1B4E0DB].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 16 [BD720p][92711650].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 17 [BD720p][3FF94B63].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 18 [BD720p][4E34F18D].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 19 [BD720p][B4F2997E].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 20 [BD720p][F6FA8447].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 21 [BD720p][8425B825].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 22 [BD720p][C7EE28B5].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 23 [BD720p][17B0B4C1].mp4
│   │       ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 24 [BD720p][2C303F38].mp4
│   │       └── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 25 [BD720p][26765844].mp4
│   ├── Summer
│   │   ├── Accel World
│   │   │   └── [animegrimoire] Accel World - Infinite Burst [BD720p][24886486].mp4
│   │   ├── Ange Vierge
│   │   │   ├── [animegrimoire] Ange Vierge - 01 [BD720p][4B248818].mp4
│   │   │   ├── [animegrimoire] Ange Vierge - 02 [BD720p][AFC27B6E].mp4
│   │   │   ├── [animegrimoire] Ange Vierge - 03 [BD720p][D4F026E2].mp4
│   │   │   ├── [animegrimoire] Ange Vierge - 04 [BD720p][AF04F94F].mp4
│   │   │   ├── [animegrimoire] Ange Vierge - 05 [BD720p][E913233E].mp4
│   │   │   ├── [animegrimoire] Ange Vierge - 06 [BD720p][CB7CFEBD].mp4
│   │   │   ├── [animegrimoire] Ange Vierge - 07 [BD720p][9974FFEE].mp4
│   │   │   ├── [animegrimoire] Ange Vierge - 08 [BD720p][97AAB941].mp4
│   │   │   ├── [animegrimoire] Ange Vierge - 09 [BD720p][4B57D85A].mp4
│   │   │   ├── [animegrimoire] Ange Vierge - 10 [BD720p][BF25E2B1].mp4
│   │   │   ├── [animegrimoire] Ange Vierge - 11 [BD720p][F8844267].mp4
│   │   │   └── [animegrimoire] Ange Vierge - 12 [BD720p][7C5977C4].mp4
│   │   ├── Konobi
│   │   │   ├── [animegrimoire] Kono Bijutsubu ni wa Mondai ga Aru! - 01 [720p][82C09CCD].mp4
│   │   │   ├── [animegrimoire] Kono Bijutsubu ni wa Mondai ga Aru! - 02 [720p][A2B89B35].mp4
│   │   │   ├── [animegrimoire] Kono Bijutsubu ni wa Mondai ga Aru! - 03 [720p][714D9F6D].mp4
│   │   │   ├── [animegrimoire] Kono Bijutsubu ni wa Mondai ga Aru! - 04 [720p][DAFB06A7].mp4
│   │   │   ├── [animegrimoire] Kono Bijutsubu ni wa Mondai ga Aru! - 05 [720p][028F6226].mp4
│   │   │   ├── [animegrimoire] Kono Bijutsubu ni wa Mondai ga Aru! - 06 [720p][DF56D21D].mp4
│   │   │   ├── [animegrimoire] Kono Bijutsubu ni wa Mondai ga Aru! - 07 [720p][985C8142].mp4
│   │   │   ├── [animegrimoire] Kono Bijutsubu ni wa Mondai ga Aru! - 08 [720p][C909A0AA].mp4
│   │   │   ├── [animegrimoire] Kono Bijutsubu ni wa Mondai ga Aru! - 10 [720p][A6A11713].mp4
│   │   │   ├── [animegrimoire] Kono Bijutsubu ni wa Mondai ga Aru! - 11 [720p][7F82C97C].mp4
│   │   │   └── [animegrimoire] Kono Bijutsubu ni wa Mondai ga Aru! - 12 [720p][6526430D].mp4
│   │   ├── Movies
│   │   │   ├── [animegrimoire] Kimi no na wa [720p][4D2D2CBA].mp4
│   │   │   └── [animegrimoire] Koe no Katachi [BD720p][EAD3D75C].mp4
│   │   ├── New Game!
│   │   │   ├── [animegrimoire] New Game! - 01 [BD720p][0B190D7C].mp4
│   │   │   ├── [animegrimoire] New Game! - 02 [BD720p][2EC1F3AE].mp4
│   │   │   ├── [animegrimoire] New Game! - 03 [BD720p][27CC3954].mp4
│   │   │   ├── [animegrimoire] New Game! - 04 [BD720p][E2EAA923].mp4
│   │   │   ├── [animegrimoire] New Game! - 05 [BD720p][F74C9AE7].mp4
│   │   │   ├── [animegrimoire] New Game! - 06 [BD720p][B322EFF5].mp4
│   │   │   ├── [animegrimoire] New Game! - 07 [BD720p][7D7B0B7F].mp4
│   │   │   ├── [animegrimoire] New Game! - 08 [BD720p][B6D58736].mp4
│   │   │   ├── [animegrimoire] New Game! - 09 [BD720p] [8F16854F].mp4
│   │   │   ├── [animegrimoire] New Game! - 10 [BD720p] [308DE3D1].mp4
│   │   │   ├── [animegrimoire] New Game! - 11 [BD720p] [2C56DA54].mp4
│   │   │   ├── [animegrimoire] New Game! - 12 [BD720p] [472E75B3].mp4
│   │   │   └── [animegrimoire] New Game! - OVA [480p][D0B2BB0E].mp4
│   │   ├── Prisma Illya 4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! - 01 [BD720p][D843320C].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! - 02 [BD720p][A223A5BC].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! - 03 [BD720p][42262B42].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! - 04 [BD720p][2D1D4FC6].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! - 05 [BD720p][7A4C411D].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! - 06 [BD720p][CCCC29B3].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! - 07 [BD720p][11AC0284].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! - 08 [BD720p][D7ECFCB8].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! - 09 [BD720p][FA0A51B9].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! - 10 [BD720p][D70CCC8B].mp4
│   │   │   ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! - 11 [BD720p][6A4535B0].mp4
│   │   │   └── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! - 12 [BD720p][AE0DB459].mp4
│   │   └── Prisma Illya Special 4
│   │       ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! Specials - 01 [BD720p][BDA2AC08].mp4
│   │       ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! Specials - 02 [BD720p][2AD3B871].mp4
│   │       ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! Specials - 03 [BD720p][0255997B].mp4
│   │       ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! Specials - 04 [BD720p][72728816].mp4
│   │       ├── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! Specials - 05 [BD720p][0FEEB85F].mp4
│   │       └── [animegrimoire] Fate Kaleid Liner Prisma Illya 3rei!! Specials - 06 [BD720p][D5B96306].mp4
│   └── Winter
│       ├── Hai to Gensou
│       │   ├── [animegrimoire] Hai to Gensou no Grimgar - 01 [BD 720p][3113CB84].mp4
│       │   ├── [animegrimoire] Hai to Gensou no Grimgar - 02.5 [BD 720p][7F2C319D].mp4
│       │   ├── [animegrimoire] Hai to Gensou no Grimgar - 02 [BD 720p][F0BEB35D].mp4
│       │   ├── [animegrimoire] Hai to Gensou no Grimgar - 03 [BD 720p][DE6ED02A].mp4
│       │   ├── [animegrimoire] Hai to Gensou no Grimgar - 04 [BD 720p][55EFE9A8].mp4
│       │   ├── [animegrimoire] Hai to Gensou no Grimgar - 05 [BD 720p][823891DC].mp4
│       │   ├── [animegrimoire] Hai to Gensou no Grimgar - 06 [BD 720p][EC99A38B].mp4
│       │   ├── [animegrimoire] Hai to Gensou no Grimgar - 07 [BD 720p][F9EC73E1].mp4
│       │   ├── [animegrimoire] Hai to Gensou no Grimgar - 08 [BD 720p][82D235FD].mp4
│       │   ├── [animegrimoire] Hai to Gensou no Grimgar - 09 [BD720p][97795DC2].mp4
│       │   ├── [animegrimoire] Hai to Gensou no Grimgar - 10 [BD720p][8A2D1400].mp4
│       │   ├── [animegrimoire] Hai to Gensou no Grimgar - 11 [BD720p][65745E46].mp4
│       │   └── [animegrimoire] Hai to Gensou no Grimgar - 12 [BD720p][C9C17960].mp4
│       ├── Koukaku no Pandora
│       │   ├── [animegrimoire] Koukaku no Pandora - 01 [BD720p][02502FA4].mp4
│       │   ├── [animegrimoire] Koukaku no Pandora - 02 [BD720p][D884918A].mp4
│       │   ├── [animegrimoire] Koukaku no Pandora - 03 [BD720p][714FF30A].mp4
│       │   ├── [animegrimoire] Koukaku no Pandora - 04 [BD720p][BCB707CD].mp4
│       │   ├── [animegrimoire] Koukaku no Pandora - 05 [BD720p][65959979].mp4
│       │   ├── [animegrimoire] Koukaku no Pandora - 06 [BD720p][8F3E0354].mp4
│       │   ├── [animegrimoire] Koukaku no Pandora - 07 [BD720p][C1979518].mp4
│       │   ├── [animegrimoire] Koukaku no Pandora - 08 [BD720p][AB65D531].mp4
│       │   ├── [animegrimoire] Koukaku no Pandora - 09 [BD720p][8E929218].mp4
│       │   ├── [animegrimoire] Koukaku no Pandora - 10 [BD720p][44643B6D].mp4
│       │   ├── [animegrimoire] Koukaku no Pandora - 11 [BD720p][92002C6D].mp4
│       │   └── [animegrimoire] Koukaku no Pandora - 12 [BD720p][9D2916E4].mp4
│       └── Koyomimonogatari
│           ├── [animegrimoire] Koyomimonogatari - 01 [BD720p][034ECA05].mp4
│           ├── [animegrimoire] Koyomimonogatari - 02 [BD720p][ABC08621].mp4
│           ├── [animegrimoire] Koyomimonogatari - 03 [BD720p][439D49AE].mp4
│           ├── [animegrimoire] Koyomimonogatari - 04 [BD720p][657A40BA].mp4
│           ├── [animegrimoire] Koyomimonogatari - 05 [BD720p][B31CD8FE].mp4
│           ├── [animegrimoire] Koyomimonogatari - 06 [BD720p][6CAEC011].mp4
│           ├── [animegrimoire] Koyomimonogatari - 07 [BD720p][E2E5AF1A].mp4
│           ├── [animegrimoire] Koyomimonogatari - 08 [BD720p][F839D618].mp4
│           ├── [animegrimoire] Koyomimonogatari - 09 [BD720p][F05E3E3C].mp4
│           ├── [animegrimoire] Koyomimonogatari - 10 [BD720p][87FB53F4].mp4
│           ├── [animegrimoire] Koyomimonogatari - 11 [BD720p][A871071C].mp4
│           └── [animegrimoire] Koyomimonogatari - 12 [BD720p][12265272].mp4
├── 2017
│   ├── Fall
│   │   ├── Blend S
│   │   │   ├── [animegrimoire] Blend S - 01 [BD720p][F442D4F9].mp4
│   │   │   ├── [animegrimoire] Blend S - 02 [BD720p][3344F5D0].mp4
│   │   │   ├── [animegrimoire] Blend S - 03 [BD720p][C479F7CA].mp4
│   │   │   ├── [animegrimoire] Blend S - 04 [BD720p][1A7D3675].mp4
│   │   │   ├── [animegrimoire] Blend S - 05 [BD720p][0EA2E87F].mp4
│   │   │   ├── [animegrimoire] Blend S - 06 [BD720p][A89D6D34].mp4
│   │   │   ├── [animegrimoire] Blend S - 07 [BD720p][D6B39A97].mp4
│   │   │   ├── [animegrimoire] Blend S - 08 [BD720p][D5AAA133].mp4
│   │   │   ├── [animegrimoire] Blend S - 09 [BD720p][2F29FB7B].mp4
│   │   │   ├── [animegrimoire] Blend S - 10 [BD720p][7D43E28F].mp4
│   │   │   ├── [animegrimoire] Blend S - 11 [BD720p][640BB37B].mp4
│   │   │   └── [animegrimoire] Blend S - 12 [BD720p][8D6B7590].mp4
│   │   ├── Boku no Kanojo ga Majimesugiru Sho-bitch na Ken
│   │   │   ├── [animegrimoire] Boku no Kanojo ga Majimesugiru Sho-bitch na Ken - 01 [BD720p][12E3E0D4].mp4
│   │   │   ├── [animegrimoire] Boku no Kanojo ga Majimesugiru Sho-bitch na Ken - 02 [BD720p][911CAA86].mp4
│   │   │   ├── [animegrimoire] Boku no Kanojo ga Majimesugiru Sho-bitch na Ken - 03 [BD720p][80D4D2D1].mp4
│   │   │   ├── [animegrimoire] Boku no Kanojo ga Majimesugiru Sho-bitch na Ken - 04 [BD720p][5A1AEFD0].mp4
│   │   │   ├── [animegrimoire] Boku no Kanojo ga Majimesugiru Sho-bitch na Ken - 05 [BD720p][C2D05410].mp4
│   │   │   ├── [animegrimoire] Boku no Kanojo ga Majimesugiru Sho-bitch na Ken - 06 [BD720p][7EF91E94].mp4
│   │   │   ├── [animegrimoire] Boku no Kanojo ga Majimesugiru Sho-bitch na Ken - 07 [BD720p][51CA0DAC].mp4
│   │   │   ├── [animegrimoire] Boku no Kanojo ga Majimesugiru Sho-bitch na Ken - 08 [BD720p][397FBF8E].mp4
│   │   │   ├── [animegrimoire] Boku no Kanojo ga Majimesugiru Sho-bitch na Ken - 09 [BD720p][83DA3EA2].mp4
│   │   │   ├── [animegrimoire] Boku no Kanojo ga Majimesugiru Sho-bitch na Ken - 10 [BD720p][1858E0FB].mp4
│   │   │   └── [animegrimoire] Boku no Kanojo ga Majimesugiru Sho-bitch na Ken - 11 [BD720p][2DBE5163].mp4
│   │   ├── Fate stay night
│   │   │   └── [animegrimoire] Fate stay night - Heaven's Feel - I. Presage Flower [BD720p][EB6A965B].mp4
│   │   ├── Himouto! Umaruchan R
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan R - 01 [BD720p][39700913].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan R - 02 [BD720p][4DCEC3CF].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan R - 03 [BD720p][C14D91D9].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan R - 04 [BD720p][6DBE99D4].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan R - 05 [BD720p][F00E807E].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan R - 06 [BD720p][D675BC87].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan R - 07 [BD720p][96364C2E].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan R - 08 [BD720p][4C6BF9AA].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan R - 09 [BD720p][974BE0F5].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan R - 10 [BD720p][4B5D8C7E].mp4
│   │   │   ├── [animegrimoire] Himouto! Umaru-chan R - 11 [BD720p][3B06BAEE].mp4
│   │   │   └── [animegrimoire] Himouto! Umaru-chan R - 12 [BD720p][C7EA8079].mp4
│   │   ├── Imouto Sae Ireba Ii
│   │   │   ├── [animegrimoire] Imouto Sae Ireba Ii - 01 [BD720p][F21AEE7A].mp4
│   │   │   ├── [animegrimoire] Imouto Sae Ireba Ii - 02 [BD720p][30A5D49F].mp4
│   │   │   ├── [animegrimoire] Imouto Sae Ireba Ii - 03 [BD720p][000757D2].mp4
│   │   │   ├── [animegrimoire] Imouto Sae Ireba Ii - 04 [BD720p][DA3856B2].mp4
│   │   │   ├── [animegrimoire] Imouto Sae Ireba Ii - 05 [BD720p][2F7CB1E0].mp4
│   │   │   ├── [animegrimoire] Imouto Sae Ireba Ii - 06 [BD720p][90249BB3].mp4
│   │   │   ├── [animegrimoire] Imouto Sae Ireba Ii - 07 [BD720p][6BFB7433].mp4
│   │   │   ├── [animegrimoire] Imouto Sae Ireba Ii - 08 [BD720p][81A533E2].mp4
│   │   │   ├── [animegrimoire] Imouto Sae Ireba Ii - 09 [BD720p][0CA05D99].mp4
│   │   │   ├── [animegrimoire] Imouto Sae Ireba Ii - 10 [BD720p][D53E9368].mp4
│   │   │   ├── [animegrimoire] Imouto Sae Ireba Ii - 11 [BD720p][ED67B096].mp4
│   │   │   └── [animegrimoire] Imouto Sae Ireba Ii - 12 [BD720p][5B4EAD37].mp4
│   │   ├── Konohana Kitan
│   │   │   ├── [animegrimoire] Konohana Kitan - 01 [FB19F4ED].mp4
│   │   │   ├── [animegrimoire] Konohana Kitan - 02 [C170DE2D].mp4
│   │   │   ├── [animegrimoire] Konohana Kitan - 03 [9F55FE68].mp4
│   │   │   ├── [animegrimoire] Konohana Kitan - 04 [CC00CE1B].mp4
│   │   │   ├── [animegrimoire] Konohana Kitan - 05 [581FB225].mp4
│   │   │   ├── [animegrimoire] Konohana Kitan - 06 [D4A8D4E9].mp4
│   │   │   ├── [animegrimoire] Konohana Kitan - 07 [720p][66C42098].mp4
│   │   │   ├── [animegrimoire] Konohana Kitan - 08 [720p][05BCB29C].mp4
│   │   │   ├── [animegrimoire] Konohana Kitan - 09 [720p][1141B61F].mp4
│   │   │   ├── [animegrimoire] Konohana Kitan - 10 [720p][72D1987D].mp4
│   │   │   ├── [animegrimoire] Konohana Kitan - 11 [720p][E24D0C8D].mp4
│   │   │   └── [animegrimoire] Konohana Kitan - 12 [720p][4960BC79].mp4
│   │   ├── Land of the Lustrous
│   │   │   ├── [animegrimoire] Land of the Lustrous - 01 [BD720p][A4A57687].mp4
│   │   │   ├── [animegrimoire] Land of the Lustrous - 02 [BD720p][364CFD5C].mp4
│   │   │   ├── [animegrimoire] Land of the Lustrous - 03 [BD720p][3EB854B8].mp4
│   │   │   ├── [animegrimoire] Land of the Lustrous - 04 [BD720p][E0310D90].mp4
│   │   │   ├── [animegrimoire] Land of the Lustrous - 05 [BD720p][DBB0840E].mp4
│   │   │   ├── [animegrimoire] Land of the Lustrous - 06 [BD720p][81319D9C].mp4
│   │   │   ├── [animegrimoire] Land of the Lustrous - 07 [BD720p][54B7B9C8].mp4
│   │   │   ├── [animegrimoire] Land of the Lustrous - 08 [BD720p][D548FA49].mp4
│   │   │   ├── [animegrimoire] Land of the Lustrous - 09 [BD720p][7DF49102].mp4
│   │   │   ├── [animegrimoire] Land of the Lustrous - 10 [BD720p][FA66EFB9].mp4
│   │   │   ├── [animegrimoire] Land of the Lustrous - 11 [BD720p][243554A8].mp4
│   │   │   └── [animegrimoire] Land of the Lustrous - 12 [BD720p][48513311].mp4
│   │   ├── Movies
│   │   │   ├── [animegrimoire] Godzilla - Part 1, Planet of the Monsters [BD720p][9296C311].mp4
│   │   │   ├── [animegrimoire] Godzilla - Part 2, City on the Edge of Battle [BD720p][CE6D32E8].mp4
│   │   │   └── [animegrimoire] Godzilla - Part 3, The Planet Eater [BD720p][4C04DA4E].mp4
│   │   ├── Net-juu no Susume
│   │   │   ├── [animegrimoire] Net-juu no Susume - 01 [BD720p][1AAE55D6].mp4
│   │   │   ├── [animegrimoire] Net-juu no Susume - 02 [BD720p][36B75ED4].mp4
│   │   │   ├── [animegrimoire] Net-juu no Susume - 03 [BD720p][548723B5].mp4
│   │   │   ├── [animegrimoire] Net-juu no Susume - 04 [BD720p][8E1BF94D].mp4
│   │   │   ├── [animegrimoire] Net-juu no Susume - 05 [BD720p][46A4E69A].mp4
│   │   │   ├── [animegrimoire] Net-juu no Susume - 06 [BD720p][CC1DC084].mp4
│   │   │   ├── [animegrimoire] Net-juu no Susume - 07 [BD720p][7FF4C32F].mp4
│   │   │   ├── [animegrimoire] Net-juu no Susume - 08 [BD720p][3CB7BB26].mp4
│   │   │   ├── [animegrimoire] Net-juu no Susume - 09 [BD720p][42FD92C0].mp4
│   │   │   ├── [animegrimoire] Net-juu no Susume - 10 [BD720p][999B6111].mp4
│   │   │   └── [animegrimoire] Net-juu no Susume - 11 [BD720p][4AACE3F2].mp4
│   │   ├── Our Love Has Always Been 10cm Apart
│   │   │   ├── [animegrimoire] Our Love Has Always Been 10 Centimeters Apart. - 1 [BD720p][6A28898B].mp4
│   │   │   ├── [animegrimoire] Our Love Has Always Been 10 Centimeters Apart. - 2 [BD720p][174167DE].mp4
│   │   │   ├── [animegrimoire] Our Love Has Always Been 10 Centimeters Apart. - 3 [BD720p][B7076297].mp4
│   │   │   ├── [animegrimoire] Our Love Has Always Been 10 Centimeters Apart. - 4 [BD720p][EAE2437D].mp4
│   │   │   ├── [animegrimoire] Our Love Has Always Been 10 Centimeters Apart. - 5 [BD720p][61FD6286].mp4
│   │   │   ├── [animegrimoire] Our Love Has Always Been 10 Centimeters Apart. - 6 [BD720p][D3DD15F9].mp4
│   │   │   ├── [animegrimoire] Our Love Has Always Been 10 Centimeters Apart. - MV 1 [BD720p][E5BEBF66].mp4
│   │   │   └── [animegrimoire] Our Love Has Always Been 10 Centimeters Apart. - MV 2 [BD720p][781BC0FD].mp4
│   │   └── Shoujo Shuumatsu
│   │       ├── [animegrimoire] Shoujo Shuumatsu Ryokou - 01 [720p][0F4BC526].mp4
│   │       ├── [animegrimoire] Shoujo Shuumatsu Ryokou - 02 [720p][4A69F969].mp4
│   │       ├── [animegrimoire] Shoujo Shuumatsu Ryokou - 03 [720p][B9812116].mp4
│   │       ├── [animegrimoire] Shoujo Shuumatsu Ryokou - 04 [720p][09510600].mp4
│   │       ├── [animegrimoire] Shoujo Shuumatsu Ryokou - 05 [720p][D7724247].mp4
│   │       ├── [animegrimoire] Shoujo Shuumatsu Ryokou - 06 [720p][D9E9CA84].mp4
│   │       ├── [animegrimoire] Shoujo Shuumatsu Ryokou - 07 [720p][73C68E39].mp4
│   │       ├── [animegrimoire] Shoujo Shuumatsu Ryokou - 08 [720p][EA1F114A].mp4
│   │       ├── [animegrimoire] Shoujo Shuumatsu Ryokou - 09 [720p][C93713EE].mp4
│   │       ├── [animegrimoire] Shoujo Shuumatsu Ryokou - 10 [720p][2CE00E0D].mp4
│   │       ├── [animegrimoire] Shoujo Shuumatsu Ryokou - 11 [720p][3A20C405].mp4
│   │       └── [animegrimoire] Shoujo Shuumatsu Ryokou - 12 [720p][CDFFC5C1].mp4
│   ├── Spring
│   │   ├── Alice To Zouroku
│   │   │   ├── [animegrimoire] Alice to Zouroku - 10 [720p][810EC100].mp4
│   │   │   ├── [animegrimoire] Alice to Zouroku - 11 [720p][B32E8D6F].mp4
│   │   │   ├── [animegrimoire] Alice to Zouroku - 12 [720p][3CAFB8EC].mp4
│   │   │   ├── [animegrimoire] Alice to Zouroku - 1 [720p][F322CC20].mp4
│   │   │   ├── [animegrimoire] Alice to Zouroku - 2 [720p][537909F2].mp4
│   │   │   ├── [animegrimoire] Alice to Zouroku - 3 [720p][03887A41].mp4
│   │   │   ├── [animegrimoire] Alice to Zouroku - 4 [720p][CE986015].mp4
│   │   │   ├── [animegrimoire] Alice to Zouroku - 5 [720p][DCDB1679].mp4
│   │   │   ├── [animegrimoire] Alice to Zouroku - 6 [720p][B617734C].mp4
│   │   │   ├── [animegrimoire] Alice to Zouroku - 7 [720p][0A35BBB9].mp4
│   │   │   ├── [animegrimoire] Alice to Zouroku - 8 [720p][A1F6B421].mp4
│   │   │   └── [animegrimoire] Alice to Zouroku - 9 [720p][C2FD332F].mp4
│   │   ├── Brave Witches
│   │   │   └── [animegrimoire] Brave Witches - Petersburg Daisenryaku [BD 720p][E62995FB].mp4
│   │   ├── Hinako Note
│   │   │   ├── [animegrimoire] Hinako Note - 01 [720p][D155CCFF].mp4
│   │   │   ├── [animegrimoire] Hinako Note - 02 [720p][C51C2649].mp4
│   │   │   ├── [animegrimoire] Hinako Note - 03 [720p][F8FB2350].mp4
│   │   │   ├── [animegrimoire] Hinako Note - 04 [720p][D0656998].mp4
│   │   │   ├── [animegrimoire] Hinako Note - 05 [720p][39381E5B].mp4
│   │   │   ├── [animegrimoire] Hinako Note - 06 [720p][AC08E5C2].mp4
│   │   │   ├── [animegrimoire] Hinako Note - 07 [720p][ED1FF799].mp4
│   │   │   ├── [animegrimoire] Hinako Note - 08 [720p][78FEFFD4].mp4
│   │   │   ├── [animegrimoire] Hinako Note - 09 [720p][9281BAAC].mp4
│   │   │   ├── [animegrimoire] Hinako Note - 10 [720p][B9B0B1AC].mp4
│   │   │   ├── [animegrimoire] Hinako Note - 11 [720p][69AEF859].mp4
│   │   │   └── [animegrimoire] Hinako Note - 12 [720p][8438BE5C].mp4
│   │   ├── Movies
│   │   │   └── [animegrimoire] Mahouka Koukou no Rettousei - Hoshi wo Yobu Shoujo [BD720p][2450C262].mp4
│   │   ├── Sagrada Reset
│   │   │   ├── [animegrimoire] Sagrada Reset - 01 [720p][4F4C776D].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 02 [720p][2260ECEE].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 03 [720p][8039B8FE].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 04 [720p][88EA2750].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 05 [720p][A77B7A44].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 06 [720p][AAA55A2A].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 07 [720p][E95EF655].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 08 [720p][CC3C5B70].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 09 [720p][7F11F095].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 10 [720p][C49DB2EE].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 11 [720p][FFF341C3].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 12 [720p][C6DA77E5].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 13 [720p][C92BB75C].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 14 [720p][924769CC].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 15 [720p][7484F39B].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 16 [720p][61AC713F].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 17 [720p][94AB99C7].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 18 [720p][F585C49D].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 19 [720p][DEF50D5E].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 20 [720p][73AB3CC0].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 21 [720p][4F9DA776].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 22 [720p][A961CB4B].mp4
│   │   │   ├── [animegrimoire] Sagrada Reset - 23 [720p][C2F82825].mp4
│   │   │   └── [animegrimoire] Sagrada Reset - 24 [720p][73FC344F].mp4
│   │   ├── Sakura Quest
│   │   │   ├── [animegrimoire] Sakura Quest - 01 [720p][7621BA67].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 02 [720p][577D82D6].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 03 [720p][AFD91868].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 04 [720p][C5B3B9CF].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 05 [720p][A9A96369].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 06 [720p][A88BA104].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 07 [720p][E0408319].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 08 [720p][03DEEB87].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 09 [720p][DD12BA91].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 10 [720p][381A1C44].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 11 [720p][E28D0614].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 12 [720p][77D936C0].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 13 [720p][2B1266A7].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 14 [720p][EE544396].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 15 [720p][B050DE68].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 16 [720p][8B545482].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 17 [720p][F00BBBCB].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 18 [720p][27263AFC].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 19 [720p][92E1DFA7].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 20 [720p][EEBD679D].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 21 [720p][836A1A09].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 22 [720p][C46C28C1].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 23 [720p][F2364352].mp4
│   │   │   ├── [animegrimoire] Sakura Quest - 24 [720p][5705E634].mp4
│   │   │   └── [animegrimoire] Sakura Quest - 25 [720p][FFE78867].mp4
│   │   └── Shingeki no Kyojin S2
│   │       ├── [animegrimoire] Shingeki no Kyojin S2 - 26 [720p][810DFB48].mp4
│   │       ├── [animegrimoire] Shingeki no Kyojin S2 - 27 [720p][979EAC4B].mp4
│   │       ├── [animegrimoire] Shingeki no Kyojin S2 - 28 [720p][5338F462].mp4
│   │       ├── [animegrimoire] Shingeki no Kyojin S2 - 29 [720p][4E22E29C].mp4
│   │       ├── [animegrimoire] Shingeki no Kyojin S2 - 30 [720p][5A980F0B].mp4
│   │       ├── [animegrimoire] Shingeki no Kyojin S2 - 31 [720p][0672978E].mp4
│   │       ├── [animegrimoire] Shingeki no Kyojin S2 - 32 [720p][CFC88430].mp4
│   │       ├── [animegrimoire] Shingeki no Kyojin S2 - 33 [720p][58DA3FC8].mp4
│   │       ├── [animegrimoire] Shingeki no Kyojin S2 - 34 [720p][210C111E].mp4
│   │       ├── [animegrimoire] Shingeki no Kyojin S2 - 35 [720p][0C990A15].mp4
│   │       ├── [animegrimoire] Shingeki no Kyojin S2 - 36 [720p][18580002].mp4
│   │       └── [animegrimoire] Shingeki no Kyojin S2 - 37 [720p][904155B5].mp4
│   ├── Summer
│   │   ├── Action Heroine Cheer Fruits
│   │   │   ├── [animegrimoire] Action Heroine Cheer Fruits - 01 [720p][02D67D17].mp4
│   │   │   ├── [animegrimoire] Action Heroine Cheer Fruits - 02 [720p][C935C813].mp4
│   │   │   ├── [animegrimoire] Action Heroine Cheer Fruits - 03 [720p][6142FAD6].mp4
│   │   │   ├── [animegrimoire] Action Heroine Cheer Fruits - 04 [720p][38598EB6].mp4
│   │   │   ├── [animegrimoire] Action Heroine Cheer Fruits - 05 [720p][F6546FB9].mp4
│   │   │   ├── [animegrimoire] Action Heroine Cheer Fruits - 06 [720p][82227948].mp4
│   │   │   ├── [animegrimoire] Action Heroine Cheer Fruits - 07 [720p][38C0E936].mp4
│   │   │   ├── [animegrimoire] Action Heroine Cheer Fruits - 08 [720p][D3D09735].mp4
│   │   │   ├── [animegrimoire] Action Heroine Cheer Fruits - 09 [720p][21D12CFB].mp4
│   │   │   ├── [animegrimoire] Action Heroine Cheer Fruits - 10 [720p][51B97895].mp4
│   │   │   ├── [animegrimoire] Action Heroine Cheer Fruits - 11 [720p][7E337A95].mp4
│   │   │   └── [animegrimoire] Action Heroine Cheer Fruits - 12 [720p][F152B46A].mp4
│   │   ├── Isekai Shokudou
│   │   │   ├── [animegrimoire] Isekai Shokudou - 01 [720p][19D85D6C].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou - 02 [720p][D3814C51].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou - 03 [720p][E42D80B7].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou - 04 [720p][9BA6D183].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou - 05 [720p][C933509D].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou - 06 [720p][081E1946].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou - 07 [720p][0F360072].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou - 08 [720p][CDB9787A].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou - 09 [720p][D77419B9].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou - 10 [720p][276888BB].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou - 11 [720p][359EDF92].mp4
│   │   │   └── [animegrimoire] Isekai Shokudou - 12 [720p][AC65F397].mp4
│   │   ├── Movies
│   │   │   ├── [animegrimoire] Gekijouban Fate Kaleid Liner Prisma Illya Sekka no Chikai [BD720p][461D7E66].mp4
│   │   │   ├── [animegrimoire] Hibike! Euphonium - Todoketai Melody [BD720p][8E906D65].mp4
│   │   │   ├── [animegrimoire] No Game No Life - Zero [BD720p][3ED35329].mp4
│   │   │   └── [animegrimoire] Uchiage Hanabi [BD720p][BA5E83C7].mp4
│   │   ├── Netsuzou Trap
│   │   │   ├── [animegrimoire] Netsuzou TRap - NTR - 01 [720p][D28CD670].mp4
│   │   │   ├── [animegrimoire] Netsuzou TRap - NTR - 02 [720p][0DABB9E0].mp4
│   │   │   ├── [animegrimoire] Netsuzou TRap - NTR - 03 [720p][C84B1E59].mp4
│   │   │   ├── [animegrimoire] Netsuzou TRap - NTR - 04 [720p][4657BB85].mp4
│   │   │   ├── [animegrimoire] Netsuzou TRap - NTR - 05 [720p][BD2E0ACC].mp4
│   │   │   ├── [animegrimoire] Netsuzou TRap - NTR - 06 [720p][CDB4044E].mp4
│   │   │   ├── [animegrimoire] Netsuzou TRap - NTR - 07 [720p][DB34BDCD].mp4
│   │   │   ├── [animegrimoire] Netsuzou TRap - NTR - 08 [720p][3451A501].mp4
│   │   │   ├── [animegrimoire] Netsuzou TRap - NTR - 09 [720p][358CDB58].mp4
│   │   │   ├── [animegrimoire] Netsuzou TRap - NTR - 10 [720p][B021EA4A].mp4
│   │   │   ├── [animegrimoire] Netsuzou TRap - NTR - 11 [720p][8E063D39].mp4
│   │   │   └── [animegrimoire] Netsuzou TRap - NTR - 12 [720p][6747917F].mp4
│   │   ├── Princess Principal
│   │   │   ├── [animegrimoire] Princess Principal - 01 [720p][F95E9F69].mp4
│   │   │   ├── [animegrimoire] Princess Principal - 02 [720p][3EFCAAF3].mp4
│   │   │   ├── [animegrimoire] Princess Principal - 03 [720p][656F41A9].mp4
│   │   │   ├── [animegrimoire] Princess Principal - 04 [720p][52E922BF].mp4
│   │   │   ├── [animegrimoire] Princess Principal - 05 [720p][72B52BA5].mp4
│   │   │   ├── [animegrimoire] Princess Principal - 06 [720p][E0048DFF].mp4
│   │   │   ├── [animegrimoire] Princess Principal - 07 [720p][6E9C5E6E].mp4
│   │   │   ├── [animegrimoire] Princess Principal - 08 [720p][35E6DF87].mp4
│   │   │   ├── [animegrimoire] Princess Principal - 09 [720p][29FCD4C0].mp4
│   │   │   ├── [animegrimoire] Princess Principal - 10 [720p][F9ADF755].mp4
│   │   │   ├── [animegrimoire] Princess Principal - 11 [720p][BC951475].mp4
│   │   │   └── [animegrimoire] Princess Principal - 12 [720p][68EB8C6F].mp4
│   │   ├── Tenshi no 3P!
│   │   │   ├── [animegrimoire] Tenshi no 3P! - 01 [BD720p][2B94E773].mp4
│   │   │   ├── [animegrimoire] Tenshi no 3P! - 02 [BD720p][EEBF5F3A].mp4
│   │   │   ├── [animegrimoire] Tenshi no 3P! - 03 [BD720p][68FA7EA1].mp4
│   │   │   ├── [animegrimoire] Tenshi no 3P! - 04 [BD720p][657A082D].mp4
│   │   │   ├── [animegrimoire] Tenshi no 3P! - 05 [BD720p][13561269].mp4
│   │   │   ├── [animegrimoire] Tenshi no 3P! - 06 [BD720p][9B61850D].mp4
│   │   │   ├── [animegrimoire] Tenshi no 3P! - 07 [BD720p][652720C7].mp4
│   │   │   ├── [animegrimoire] Tenshi no 3P! - 08 [BD720p][A3F4D185].mp4
│   │   │   ├── [animegrimoire] Tenshi no 3P! - 09 [BD720p][0F2226FA].mp4
│   │   │   ├── [animegrimoire] Tenshi no 3P! - 10 [BD720p][9291C921].mp4
│   │   │   ├── [animegrimoire] Tenshi no 3P! - 11 [BD720p][A5EDFA06].mp4
│   │   │   └── [animegrimoire] Tenshi no 3P! - 12 [BD720p][C683201F].mp4
│   │   └── Tsurezure Children
│   │       ├── [animegrimoire] Tsurezure Children - 01 [720p][89EF9D55].mp4
│   │       ├── [animegrimoire] Tsurezure Children - 02 [720p][475926C7].mp4
│   │       ├── [animegrimoire] Tsurezure Children - 03 [720p][6063CA91].mp4
│   │       ├── [animegrimoire] Tsurezure Children - 04 [720p][99FFC7BB].mp4
│   │       ├── [animegrimoire] Tsurezure Children - 05 [720p][BD2109ED].mp4
│   │       ├── [animegrimoire] Tsurezure Children - 06 [720p][95FA7148].mp4
│   │       ├── [animegrimoire] Tsurezure Children - 07 [720p][7EC0A6BD].mp4
│   │       ├── [animegrimoire] Tsurezure Children - 08 [720p][A5007551].mp4
│   │       ├── [animegrimoire] Tsurezure Children - 09 [720p][E67B2A1A].mp4
│   │       ├── [animegrimoire] Tsurezure Children - 10 [720p][D3CC0CBB].mp4
│   │       ├── [animegrimoire] Tsurezure Children - 11 [720p][9FB8AC9C].mp4
│   │       └── [animegrimoire] Tsurezure Children - 12 [720p][14D2A327].mp4
│   └── Winter
│       ├── Kuzu no Honkai
│       │   ├── [animegrimoire] Kuzu no Honkai - 01 [BD720p][6F303DE4].mp4
│       │   ├── [animegrimoire] Kuzu no Honkai - 02 [BD720p][9F6DF89B].mp4
│       │   ├── [animegrimoire] Kuzu no Honkai - 03 [BD720p][56AA74EF].mp4
│       │   ├── [animegrimoire] Kuzu no Honkai - 04 [BD720p][02A252D8].mp4
│       │   ├── [animegrimoire] Kuzu no Honkai - 05 [BD720p][C5792971].mp4
│       │   ├── [animegrimoire] Kuzu no Honkai - 06 [BD720p][2A28078B].mp4
│       │   ├── [animegrimoire] Kuzu no Honkai - 07 [BD720p][6339829F].mp4
│       │   ├── [animegrimoire] Kuzu no Honkai - 08 [BD720p][4D7D945D].mp4
│       │   ├── [animegrimoire] Kuzu no Honkai - 09 [BD720p][C59FDF92].mp4
│       │   ├── [animegrimoire] Kuzu no Honkai - 10 [BD720p][5F700088].mp4
│       │   ├── [animegrimoire] Kuzu no Honkai - 11 [BD720p][3817266D].mp4
│       │   └── [animegrimoire] Kuzu no Honkai - 12 [BD720p][0F8170A9].mp4
│       ├── Nyanko Days
│       │   ├── [animegrimoire] Nyanko Days - 01 [720p][ABCAEF35].mp4
│       │   ├── [animegrimoire] Nyanko Days - 02 [720p][BEF017C8].mp4
│       │   ├── [animegrimoire] Nyanko Days - 03 [720p][4EC52010].mp4
│       │   ├── [animegrimoire] Nyanko Days - 04 [720p][C0F5E0E9].mp4
│       │   ├── [animegrimoire] Nyanko Days - 05 [720p][4805B657].mp4
│       │   ├── [animegrimoire] Nyanko Days - 06 [720p][D4135311].mp4
│       │   ├── [animegrimoire] Nyanko Days - 07 [720p][4FE931A4].mp4
│       │   ├── [animegrimoire] Nyanko Days - 08 [720p][5DA8D78C].mp4
│       │   ├── [animegrimoire] Nyanko Days - 09 [720p][B1804D39].mp4
│       │   ├── [animegrimoire] Nyanko Days - 10 [720p][C392DE7D].mp4
│       │   ├── [animegrimoire] Nyanko Days - 11 [720p][29B75FAC].mp4
│       │   └── [animegrimoire] Nyanko Days - 12 [720p][8FF1F7A7].mp4
│       ├── One Room
│       │   ├── [animegrimoire] One Room - 01 [BD720p][5B627D1A].mp4
│       │   ├── [animegrimoire] One Room - 02 [BD720p][676F4061].mp4
│       │   ├── [animegrimoire] One Room - 03 [BD720p][7D332304].mp4
│       │   ├── [animegrimoire] One Room - 04 [BD720p][E9133E9F].mp4
│       │   ├── [animegrimoire] One Room - 05 [BD720p][873268D0].mp4
│       │   ├── [animegrimoire] One Room - 06 [BD720p][6FF0DA18].mp4
│       │   ├── [animegrimoire] One Room - 07 [BD720p][0B55EF6A].mp4
│       │   ├── [animegrimoire] One Room - 08 [BD720p][E6BDCB48].mp4
│       │   ├── [animegrimoire] One Room - 09 [BD720p][334F090C].mp4
│       │   ├── [animegrimoire] One Room - 10 [BD720p][E8C506C8].mp4
│       │   ├── [animegrimoire] One Room - 11 [BD720p][3675A334].mp4
│       │   ├── [animegrimoire] One Room - 12 [BD720p][225F4F4D].mp4
│       │   ├── [animegrimoire] One Room S2 - 00 [720p][6D7769A8].mp4
│       │   ├── [animegrimoire] One Room S2 - 01 [720p][4F6B28AF].mp4
│       │   ├── [animegrimoire] One Room S2 - 02 [720p][1A82E38A].mp4
│       │   ├── [animegrimoire] One Room S2 - 03 [720p][04C4B54B].mp4
│       │   ├── [animegrimoire] One Room S2 - 04 [720p][11EE3D7B].mp4
│       │   ├── [animegrimoire] One Room S2 - 05 [720p][78391772].mp4
│       │   ├── [animegrimoire] One Room S2 - 06 [720p][517E6304].mp4
│       │   ├── [animegrimoire] One Room S2 - 07 [720p][F9F167B0].mp4
│       │   ├── [animegrimoire] One Room S2 - 08 [720p][3C33E96D].mp4
│       │   ├── [animegrimoire] One Room S2 - 09 [720p][49200050].mp4
│       │   ├── [animegrimoire] One Room S2 - 10 [720p][31E99087].mp4
│       │   ├── [animegrimoire] One Room S2 - 11 [720p][0571AD4D].mp4
│       │   └── [animegrimoire] One Room S2 - 12 [720p][7787DB1E].mp4
│       ├── Quanzhi Gaoshou
│       │   ├── [animegrimoire] Quanzhi Gaoshou - 01 [720p][920AAE0F].mp4
│       │   ├── [animegrimoire] Quanzhi Gaoshou - 02 [720p][1EF26885].mp4
│       │   ├── [animegrimoire] Quanzhi Gaoshou - 03 [720p][C35E0917].mp4
│       │   ├── [animegrimoire] Quanzhi Gaoshou - 04 [720p][679E2EDB].mp4
│       │   ├── [animegrimoire] Quanzhi Gaoshou - 05 [720p][CF49CF79].mp4
│       │   ├── [animegrimoire] Quanzhi Gaoshou - 06 [720p][32FCBE70].mp4
│       │   ├── [animegrimoire] Quanzhi Gaoshou - 07 [720p][D1E53F8D].mp4
│       │   ├── [animegrimoire] Quanzhi Gaoshou - 08 [720p][A559ADB8].mp4
│       │   ├── [animegrimoire] Quanzhi Gaoshou - 09 [720p][26D9F672].mp4
│       │   ├── [animegrimoire] Quanzhi Gaoshou - 10 [720p][28A3D125].mp4
│       │   ├── [animegrimoire] Quanzhi Gaoshou - 11 [720p][2DB0FCA6].mp4
│       │   ├── [animegrimoire] Quanzhi Gaoshou - 12 [720p][86A58521].mp4
│       │   ├── [animegrimoire] The King's Avatar - Specials - 1 [720p][18DD7052].mp4
│       │   ├── [animegrimoire] The King's Avatar - Specials - 2 [720p][9DB1132A].mp4
│       │   └── [animegrimoire] The King's Avatar - Specials - 3 [720p][EFDA458D].mp4
│       └── Urara Meirochou
│           ├── [animegrimoire] Urara Meirochou - 01 [BD720p][46AB8BDE].mp4
│           ├── [animegrimoire] Urara Meirochou - 02 [BD720p][815C4CBD].mp4
│           ├── [animegrimoire] Urara Meirochou - 03 [BD720p][2276B8FB].mp4
│           ├── [animegrimoire] Urara Meirochou - 04 [BD720p][AB660AE0].mp4
│           ├── [animegrimoire] Urara Meirochou - 05 [BD720p][6DF42879].mp4
│           ├── [animegrimoire] Urara Meirochou - 06 [BD720p][FD782A0C].mp4
│           ├── [animegrimoire] Urara Meirochou - 07 [BD720p][2FE5D97D].mp4
│           ├── [animegrimoire] Urara Meirochou - 08 [BD720p][82FA72AB].mp4
│           ├── [animegrimoire] Urara Meirochou - 09 [BD720p][6DC6B9EC].mp4
│           ├── [animegrimoire] Urara Meirochou - 10 [BD720p][5B26E5B9].mp4
│           ├── [animegrimoire] Urara Meirochou - 11 [BD720p][05FF73BC].mp4
│           └── [animegrimoire] Urara Meirochou - 12 [BD720p][C71E46F9].mp4
├── 2018
│   ├── Fall
│   │   ├── Anima Yell
│   │   │   ├── [animegrimoire] Anima Yell - 01 [BD720p][88963B71].mp4
│   │   │   ├── [animegrimoire] Anima Yell - 02 [BD720p][3E635EC7].mp4
│   │   │   ├── [animegrimoire] Anima Yell - 03 [BD720p][B3616863].mp4
│   │   │   ├── [animegrimoire] Anima Yell - 04 [BD720p][2B58E460].mp4
│   │   │   ├── [animegrimoire] Anima Yell - 05 [BD720p][3E1FA809].mp4
│   │   │   ├── [animegrimoire] Anima Yell - 06 [BD720p][10297601].mp4
│   │   │   ├── [animegrimoire] Anima Yell - 07 [BD720p][94A16760].mp4
│   │   │   ├── [animegrimoire] Anima Yell - 08 [BD720p][7CD39035].mp4
│   │   │   ├── [animegrimoire] Anima Yell - 09 [BD720p][C491C750].mp4
│   │   │   ├── [animegrimoire] Anima Yell - 10 [BD720p][1159441C].mp4
│   │   │   ├── [animegrimoire] Anima Yell - 11 [BD720p][0EC1F049].mp4
│   │   │   └── [animegrimoire] Anima Yell - 12 [BD720p][1D180440].mp4
│   │   ├── Himote House
│   │   │   ├── [animegrimoire] Himote House - 01 [720p][CC26F2BD].mp4
│   │   │   ├── [animegrimoire] Himote House - 02 [720p][C1E6762E].mp4
│   │   │   ├── [animegrimoire] Himote House - 03 [720p][009762FB].mp4
│   │   │   ├── [animegrimoire] Himote House - 04 [720p][AF4D3186].mp4
│   │   │   ├── [animegrimoire] Himote House - 05 [720p][7BF5F907].mp4
│   │   │   ├── [animegrimoire] Himote House - 06 [720p][D608A311].mp4
│   │   │   ├── [animegrimoire] Himote House - 07 [720p][CAB24D8C].mp4
│   │   │   ├── [animegrimoire] Himote House - 08 [720p][87EE84C6].mp4
│   │   │   ├── [animegrimoire] Himote House - 09 [720p][5DC58267].mp4
│   │   │   ├── [animegrimoire] Himote House - 10 [720p][DCC0F117].mp4
│   │   │   ├── [animegrimoire] Himote House - 11 [720p][A1CEAF28].mp4
│   │   │   └── [animegrimoire] Himote House - 12 [720p][0CE612C7].mp4
│   │   ├── Irozuku Sekai no Ashita kara
│   │   │   ├── [animegrimoire] Irozuku Sekai no Ashita kara - 01 [720p][0C1906C7].mp4
│   │   │   ├── [animegrimoire] Irozuku Sekai no Ashita kara - 02 [720p][B217C142].mp4
│   │   │   ├── [animegrimoire] Irozuku Sekai no Ashita kara - 03 [720p][99CB1755].mp4
│   │   │   ├── [animegrimoire] Irozuku Sekai no Ashita kara - 04 [720p][1FB6479B].mp4
│   │   │   ├── [animegrimoire] Irozuku Sekai no Ashita kara - 05 [720p][3A06D9CE].mp4
│   │   │   ├── [animegrimoire] Irozuku Sekai no Ashita kara - 06 [720p][EDB801FB].mp4
│   │   │   ├── [animegrimoire] Irozuku Sekai no Ashita kara - 07 [720p][F066EDA8].mp4
│   │   │   ├── [animegrimoire] Irozuku Sekai no Ashita kara - 08 [720p][B462D357].mp4
│   │   │   ├── [animegrimoire] Irozuku Sekai no Ashita kara - 09 [720p][7CED53F6].mp4
│   │   │   ├── [animegrimoire] Irozuku Sekai no Ashita kara - 10 [720p][BA367AF6].mp4
│   │   │   ├── [animegrimoire] Irozuku Sekai no Ashita kara - 11 [720p][35CA24F4].mp4
│   │   │   ├── [animegrimoire] Irozuku Sekai no Ashita kara - 12 [720p][9101EED4].mp4
│   │   │   └── [animegrimoire] Irozuku Sekai no Ashita kara - 13 [720p][ECF8961E].mp4
│   │   ├── Movies
│   │   │   └── [animegrimoire] Re Zero Kara Hajimeru Isekai Seikatsu - Memory Snow [BD720p][B7C607B3].mp4
│   │   ├── Release the Spyce
│   │   │   ├── [animegrimoire] Release the Spyce - 01 [720p][E0159B6E].mp4
│   │   │   ├── [animegrimoire] Release the Spyce - 02 [720p][7A360270].mp4
│   │   │   ├── [animegrimoire] Release the Spyce - 03 [720p][A8EAD658].mp4
│   │   │   ├── [animegrimoire] Release the Spyce - 04 [720p][829EFFE3].mp4
│   │   │   ├── [animegrimoire] Release the Spyce - 05 [720p][F5D5A629].mp4
│   │   │   ├── [animegrimoire] Release the Spyce - 06 [720p][27AA818F].mp4
│   │   │   ├── [animegrimoire] Release the Spyce - 07 [720p][10A67319].mp4
│   │   │   ├── [animegrimoire] Release the Spyce - 08 [720p][B97DC84A].mp4
│   │   │   ├── [animegrimoire] Release the Spyce - 09 [720p][5AF7B16C].mp4
│   │   │   ├── [animegrimoire] Release the Spyce - 10 [720p][008BC598].mp4
│   │   │   ├── [animegrimoire] Release the Spyce - 11 [720p][534984F1].mp4
│   │   │   └── [animegrimoire] Release the Spyce - 12 [720p][DE5AAE23].mp4
│   │   ├── Seishun Buta
│   │   │   ├── [animegrimoire] Seishun Buta Yarou Wa Bunny Girl Senpai No Yume Wo Minai - 01 [BD720p][8FAFEB6C].mp4
│   │   │   ├── [animegrimoire] Seishun Buta Yarou Wa Bunny Girl Senpai No Yume Wo Minai - 02 [BD720p][1969AE9C].mp4
│   │   │   ├── [animegrimoire] Seishun Buta Yarou Wa Bunny Girl Senpai No Yume Wo Minai - 03 [BD720p][7DC56905].mp4
│   │   │   ├── [animegrimoire] Seishun Buta Yarou Wa Bunny Girl Senpai No Yume Wo Minai - 04 [BD720p][828300B5].mp4
│   │   │   ├── [animegrimoire] Seishun Buta Yarou Wa Bunny Girl Senpai No Yume Wo Minai - 05 [BD720p][86BF124A].mp4
│   │   │   ├── [animegrimoire] Seishun Buta Yarou Wa Bunny Girl Senpai No Yume Wo Minai - 06 [BD720p][970BD0A9].mp4
│   │   │   ├── [animegrimoire] Seishun Buta Yarou Wa Bunny Girl Senpai No Yume Wo Minai - 07 [BD720p][9783CA8C].mp4
│   │   │   ├── [animegrimoire] Seishun Buta Yarou Wa Bunny Girl Senpai No Yume Wo Minai - 08 [BD720p][B7045344].mp4
│   │   │   ├── [animegrimoire] Seishun Buta Yarou Wa Bunny Girl Senpai No Yume Wo Minai - 09 [BD720p][9CA7C0B0].mp4
│   │   │   ├── [animegrimoire] Seishun Buta Yarou Wa Bunny Girl Senpai No Yume Wo Minai - 10 [BD720p][AA1FF4BC].mp4
│   │   │   ├── [animegrimoire] Seishun Buta Yarou Wa Bunny Girl Senpai No Yume Wo Minai - 11 [BD720p][483C45EE].mp4
│   │   │   ├── [animegrimoire] Seishun Buta Yarou Wa Bunny Girl Senpai No Yume Wo Minai - 12 [BD720p][FFF3B9C2].mp4
│   │   │   └── [animegrimoire] Seishun Buta Yarou Wa Bunny Girl Senpai No Yume Wo Minai - 13 [BD720p][CA52D240].mp4
│   │   ├── Seishun Buta Yume wo Minai
│   │   │   └── [animegrimoire] Seishun Buta Yarou wa Yumemiru Shoujo no Yume wo Minai - 00 [720p][D733128B].mp4
│   │   ├── Toaru Majutsu no Index III
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 01 [720p][31962674].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 02 [720p][12CCA9A7].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 03 [720p][FE0150D3].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 04 [720p][D14EEF40].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 05 [720p][9F4C1B1B].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 06 [720p][8157E46C].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 07 [720p][B7F6FF2E].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 08 [720p][3F634026].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 09 [720p][06FFFBD4].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 10 [720p][890BD455].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 11 [720p][C1F5A087].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 12 [720p][C0CC6C48].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 13 [720p][1BBD2838].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 14 [720p][3008FA8D].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 15 [720p][E0ED18EE].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 16 [720p][26672EBC].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 17 [720p][6A729361].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 18 [720p][67A31B91].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 19 [720p][1F55B265].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 20 [720p][39BAC4C0].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 21 [720p][258724B8].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 22 [720p][BE0E0589].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 23 [720p][ACCA30DC].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 24 [720p][25CADF54].mp4
│   │   │   ├── [animegrimoire] Toaru Majutsu no Index III - 25 [720p][C43EBFB6].mp4
│   │   │   └── [animegrimoire] Toaru Majutsu no Index III - 26 [720p][9FE9C713].mp4
│   │   ├── Tonari no Kyuuketsuki-san
│   │   │   ├── [animegrimoire] Tonari no Kyuuketsuki-san - 01 [720p][842DEB29].mp4
│   │   │   ├── [animegrimoire] Tonari no Kyuuketsuki-san - 02 [720p][6F20E6C9].mp4
│   │   │   ├── [animegrimoire] Tonari no Kyuuketsuki-san - 03 [720p][DE843E18].mp4
│   │   │   ├── [animegrimoire] Tonari no Kyuuketsuki-san - 04 [720p][CEE38A78].mp4
│   │   │   ├── [animegrimoire] Tonari no Kyuuketsuki-san - 05 [720p][D31F8458].mp4
│   │   │   ├── [animegrimoire] Tonari no Kyuuketsuki-san - 06 [720p][9F5C5CB4].mp4
│   │   │   ├── [animegrimoire] Tonari no Kyuuketsuki-san - 07 [720p][C8AB891F].mp4
│   │   │   ├── [animegrimoire] Tonari no Kyuuketsuki-san - 08 [720p][4156776C].mp4
│   │   │   ├── [animegrimoire] Tonari no Kyuuketsuki-san - 09 [720p][A65B834C].mp4
│   │   │   ├── [animegrimoire] Tonari no Kyuuketsuki-san - 10 [720p][88DE88DD].mp4
│   │   │   ├── [animegrimoire] Tonari no Kyuuketsuki-san - 11 [720p][097F82CB].mp4
│   │   │   └── [animegrimoire] Tonari no Kyuuketsuki-san - 12 [720p][88FF917C].mp4
│   │   ├── Uchi no Maid ga Uzasugiru
│   │   │   ├── [animegrimoire] Uchi no Maid ga Uzasugiru! - 01 [720p][9644B8D7].mp4
│   │   │   ├── [animegrimoire] Uchi no Maid ga Uzasugiru! - 02 [720p][7E7CC211].mp4
│   │   │   ├── [animegrimoire] Uchi no Maid ga Uzasugiru! - 03 [720p][DDAF2530].mp4
│   │   │   ├── [animegrimoire] Uchi no Maid ga Uzasugiru! - 04 [720p][0AAED773].mp4
│   │   │   ├── [animegrimoire] Uchi no Maid ga Uzasugiru! - 05 [720p][8FEADC9B].mp4
│   │   │   ├── [animegrimoire] Uchi no Maid ga Uzasugiru! - 06 [720p][E43EE66C].mp4
│   │   │   ├── [animegrimoire] Uchi no Maid ga Uzasugiru! - 07 [720p][8E84B73B].mp4
│   │   │   ├── [animegrimoire] Uchi no Maid ga Uzasugiru! - 08 [720p][09AB9DC6].mp4
│   │   │   ├── [animegrimoire] Uchi no Maid ga Uzasugiru! - 09 [720p][6D983500].mp4
│   │   │   ├── [animegrimoire] Uchi no Maid ga Uzasugiru! - 10 [720p][529BAA19].mp4
│   │   │   ├── [animegrimoire] Uchi no Maid ga Uzasugiru! - 11 [720p][1EA2211C].mp4
│   │   │   ├── [animegrimoire] Uchi no Maid ga Uzasugiru! - 12 [720p][58D5C5E5].mp4
│   │   │   └── [animegrimoire] Uchi no Maid ga Uzasugiru! - 13 [720p][98036DDE].mp4
│   │   └── Yagate Kimi ni Naru
│   │       ├── [animegrimoire] Yagate Kimi ni Naru - 01 [720p][3267AC75].mp4
│   │       ├── [animegrimoire] Yagate Kimi ni Naru - 02 [720p][FF6964AF].mp4
│   │       ├── [animegrimoire] Yagate Kimi ni Naru - 03 [720p][B6BE54C0].mp4
│   │       ├── [animegrimoire] Yagate Kimi ni Naru - 04 [720p][7E5D3076].mp4
│   │       ├── [animegrimoire] Yagate Kimi ni Naru - 05 [720p][3B14E375].mp4
│   │       ├── [animegrimoire] Yagate Kimi ni Naru - 06 [720p][5245AB93].mp4
│   │       ├── [animegrimoire] Yagate Kimi ni Naru - 07 [720p][64D2734E].mp4
│   │       ├── [animegrimoire] Yagate Kimi ni Naru - 08 [720p][C0D93470].mp4
│   │       ├── [animegrimoire] Yagate Kimi ni Naru - 09 [720p][A43482A0].mp4
│   │       ├── [animegrimoire] Yagate Kimi ni Naru - 10 [720p][851E2601].mp4
│   │       ├── [animegrimoire] Yagate Kimi ni Naru - 11 [720p][977FD031].mp4
│   │       ├── [animegrimoire] Yagate Kimi ni Naru - 12 [720p][5FC9D9FC].mp4
│   │       └── [animegrimoire] Yagate Kimi ni Naru - 13 [720p][91BD0423].mp4
│   ├── Spring
│   │   ├── Amanchu! Advance
│   │   │   ├── [animegrimoire] Amanchu! Advance - 01 [720p][342554C9].mp4
│   │   │   ├── [animegrimoire] Amanchu! Advance - 02 [720p][84070177].mp4
│   │   │   ├── [animegrimoire] Amanchu! Advance - 03 [720p][4586EF8C].mp4
│   │   │   ├── [animegrimoire] Amanchu! Advance - 04 [720p][0B9E1234].mp4
│   │   │   ├── [animegrimoire] Amanchu! Advance - 05 [720p][CCE8EADD].mp4
│   │   │   ├── [animegrimoire] Amanchu! Advance - 06 [720p][7016E787].mp4
│   │   │   ├── [animegrimoire] Amanchu! Advance - 07 [720p][B1355BC9].mp4
│   │   │   ├── [animegrimoire] Amanchu! Advance - 08 [720p][5CCA58D8].mp4
│   │   │   ├── [animegrimoire] Amanchu! Advance - 09 [720p][52849998].mp4
│   │   │   ├── [animegrimoire] Amanchu! Advance - 10 [720p][76D7FF46].mp4
│   │   │   ├── [animegrimoire] Amanchu! Advance - 11 [720p][E0CBB3F3].mp4
│   │   │   └── [animegrimoire] Amanchu! Advance - 12 [720p][2000B15E].mp4
│   │   ├── Asagao to Kase-san
│   │   │   └── [animegrimoire] Asagao to Kase-san - OVA [BD720p][A92C37B9].mp4
│   │   ├── Comic Girls
│   │   │   ├── [animegrimoire] Comic Girls - 01 [720p][E910CA2E].mp4
│   │   │   ├── [animegrimoire] Comic Girls - 02 [720p][044534F4].mp4
│   │   │   ├── [animegrimoire] Comic Girls - 03 [720p][0AA313B1].mp4
│   │   │   ├── [animegrimoire] Comic Girls - 04 [720p][8F35CB07].mp4
│   │   │   ├── [animegrimoire] Comic Girls - 05 [720p][7DC9C3B3].mp4
│   │   │   ├── [animegrimoire] Comic Girls - 06 [720p][3711F721].mp4
│   │   │   ├── [animegrimoire] Comic Girls - 07 [720p][60D06B28].mp4
│   │   │   ├── [animegrimoire] Comic Girls - 08 [720p][85DB68FE].mp4
│   │   │   ├── [animegrimoire] Comic Girls - 09 [720p][BF89ADF1].mp4
│   │   │   ├── [animegrimoire] Comic Girls - 10 [720p][CFFB95B5].mp4
│   │   │   ├── [animegrimoire] Comic Girls - 11 [720p][A74601A8].mp4
│   │   │   └── [animegrimoire] Comic Girls - 12 [720p][F4849A87].mp4
│   │   ├── Fumikiri Jikan
│   │   │   ├── [animegrimoire] Fumikiri Jikan - 01 [720p][5B47B9D6].mp4
│   │   │   ├── [animegrimoire] Fumikiri Jikan - 02 [720p][075281E0].mp4
│   │   │   ├── [animegrimoire] Fumikiri Jikan - 03 [720p][862AFBBE].mp4
│   │   │   ├── [animegrimoire] Fumikiri Jikan - 04 [720p][7123DE9F].mp4
│   │   │   ├── [animegrimoire] Fumikiri Jikan - 05 [720p][98F29CB1].mp4
│   │   │   ├── [animegrimoire] Fumikiri Jikan - 06 [720p][EEDFF3A2].mp4
│   │   │   ├── [animegrimoire] Fumikiri Jikan - 07 [720p][917AC559].mp4
│   │   │   ├── [animegrimoire] Fumikiri Jikan - 08 [720p][ECDCF8D3].mp4
│   │   │   ├── [animegrimoire] Fumikiri Jikan - 09 [720p][5D23D3E3].mp4
│   │   │   ├── [animegrimoire] Fumikiri Jikan - 10 [720p][4EC931E5].mp4
│   │   │   ├── [animegrimoire] Fumikiri Jikan - 11 [720p][AC353C42].mp4
│   │   │   └── [animegrimoire] Fumikiri Jikan - 12 [720p][14467667].mp4
│   │   ├── Golden Kamuy
│   │   │   ├── [animegrimoire] Golden Kamuy - 01 [720p][FB3950DA].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 02 [720p][380A4782].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 03 [720p][F488C5A6].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 04 [720p][58775580].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 05 [720p][888B1A7B].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 06 [720p][32ACF5A9].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 07 [720p][59415A06].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 08 [720p][04B5E5A3].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 09 [720p][C820EE39].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 10 [720p][B74D726F].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 11 [720p][46099882].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 12 [720p][3000421F].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 13 [720p][DAFD9097].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 14 [720p][BFAA9A85].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 15 [720p][4B416988].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 16 [720p][1901CBB1].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 17 [720p][D685B0A5].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 18 [720p][DB3817CE].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 19 [720p][9A136EB6].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 20 [720p][3DEBA9D2].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 21 [720p][F204EC38].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 22 [720p][D9B850BF].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 23 [720p][6DB28E8A].mp4
│   │   │   └── [animegrimoire] Golden Kamuy - 24 [720p][1451CD7F].mp4
│   │   ├── Movies
│   │   │   └── [animegrimoire] Liz and the Blue Bird [BD720p][E66F8661].mp4
│   │   └── Tachibanakan To Lie Angle
│   │       ├── [animegrimoire] Tachibanakan To Lie Angle - 01 [720p][971FE827].mp4
│   │       ├── [animegrimoire] Tachibanakan To Lie Angle - 02 [720p][DB15AF7B].mp4
│   │       ├── [animegrimoire] Tachibanakan To Lie Angle - 03 [720p][369CE68B].mp4
│   │       ├── [animegrimoire] Tachibanakan To Lie Angle - 04 [720p][AF1E1DEA].mp4
│   │       ├── [animegrimoire] Tachibanakan To Lie Angle - 05 [720p][9D71A691].mp4
│   │       ├── [animegrimoire] Tachibanakan To Lie Angle - 06 [720p][F768A490].mp4
│   │       ├── [animegrimoire] Tachibanakan To Lie Angle - 07 [720p][52FC8990].mp4
│   │       ├── [animegrimoire] Tachibanakan To Lie Angle - 08 [720p][096B8F1C].mp4
│   │       ├── [animegrimoire] Tachibanakan To Lie Angle - 09 [720p][85BF7EE1].mp4
│   │       ├── [animegrimoire] Tachibanakan To Lie Angle - 10 [720p][86DC307F].mp4
│   │       ├── [animegrimoire] Tachibanakan To Lie Angle - 11 [720p][181B13FD].mp4
│   │       └── [animegrimoire] Tachibanakan To Lie Angle - 12 [720p][42D231DF].mp4
│   ├── Summer
│   │   ├── Asobi Asobase
│   │   │   ├── [animegrimoire] Asobi Asobase - 01 [720p][C175D737].mp4
│   │   │   ├── [animegrimoire] Asobi Asobase - 02 [720p][F0B8FEE9].mp4
│   │   │   ├── [animegrimoire] Asobi Asobase - 03 [720p][E6FFEBC6].mp4
│   │   │   ├── [animegrimoire] Asobi Asobase - 04 [720p][7CE00D25].mp4
│   │   │   ├── [animegrimoire] Asobi Asobase - 05 [720p][3C45D047].mp4
│   │   │   ├── [animegrimoire] Asobi Asobase - 06 [720p][013DAB81].mp4
│   │   │   ├── [animegrimoire] Asobi Asobase - 07 [720p][E936B847].mp4
│   │   │   ├── [animegrimoire] Asobi Asobase - 08 [720p][4C130F27].mp4
│   │   │   ├── [animegrimoire] Asobi Asobase - 09 [720p][3A2AD75F].mp4
│   │   │   ├── [animegrimoire] Asobi Asobase - 10 [720p][26D580B3].mp4
│   │   │   ├── [animegrimoire] Asobi Asobase - 11 [720p][44D2FA9F].mp4
│   │   │   └── [animegrimoire] Asobi Asobase - 12 [720p][FC53501E].mp4
│   │   ├── Happy Sugar Life
│   │   │   ├── [animegrimoire] Happy Sugar Life - 01 [720p][186DF352].mp4
│   │   │   ├── [animegrimoire] Happy Sugar Life - 02 [720p][D5790CCA].mp4
│   │   │   ├── [animegrimoire] Happy Sugar Life - 03 [720p][A371AC11].mp4
│   │   │   ├── [animegrimoire] Happy Sugar Life - 04 [720p][50134A6C].mp4
│   │   │   ├── [animegrimoire] Happy Sugar Life - 05 [720p][BF31CF21].mp4
│   │   │   ├── [animegrimoire] Happy Sugar Life - 06 [720p][043FB75E].mp4
│   │   │   ├── [animegrimoire] Happy Sugar Life - 07 [720p][F64CAFB0].mp4
│   │   │   ├── [animegrimoire] Happy Sugar Life - 08 [720p][8CBA8CE2].mp4
│   │   │   ├── [animegrimoire] Happy Sugar Life - 09 [720p][D7F7CE3C].mp4
│   │   │   ├── [animegrimoire] Happy Sugar Life - 10 [720p][15387340].mp4
│   │   │   ├── [animegrimoire] Happy Sugar Life - 11 [720p][FF2D5E7F].mp4
│   │   │   └── [animegrimoire] Happy Sugar Life - 12 [720p][722F8918].mp4
│   │   ├── Harukana Receive
│   │   │   ├── [animegrimoire] Harukana Receive - 01 [BD720p][090A070E].mp4
│   │   │   ├── [animegrimoire] Harukana Receive - 02 [BD720p][BFF358CA].mp4
│   │   │   ├── [animegrimoire] Harukana Receive - 03 [BD720p][25A5F5A5].mp4
│   │   │   ├── [animegrimoire] Harukana Receive - 04 [BD720p][1972F433].mp4
│   │   │   ├── [animegrimoire] Harukana Receive - 05 [BD720p][E112CD71].mp4
│   │   │   ├── [animegrimoire] Harukana Receive - 06 [BD720p][236D1817].mp4
│   │   │   ├── [animegrimoire] Harukana Receive - 07 [BD720p][626E4781].mp4
│   │   │   ├── [animegrimoire] Harukana Receive - 08 [BD720p][AC6F882F].mp4
│   │   │   ├── [animegrimoire] Harukana Receive - 09 [BD720p][08EC1D70].mp4
│   │   │   ├── [animegrimoire] Harukana Receive - 10 [BD720p][CC69958F].mp4
│   │   │   ├── [animegrimoire] Harukana Receive - 11 [BD720p][C419B40D].mp4
│   │   │   └── [animegrimoire] Harukana Receive - 12 [BD720p][E0A3B6CD].mp4
│   │   ├── Island
│   │   │   ├── [animegrimoire] Island - 01 [720p][315B643F].mp4
│   │   │   ├── [animegrimoire] Island - 02 [720p][A0693A07].mp4
│   │   │   ├── [animegrimoire] Island - 03 [720p][4B974B70].mp4
│   │   │   ├── [animegrimoire] Island - 04 [720p][CB5E6816].mp4
│   │   │   ├── [animegrimoire] Island - 05 [720p][7A249948].mp4
│   │   │   ├── [animegrimoire] Island - 06 [720p][E8A0B28A].mp4
│   │   │   ├── [animegrimoire] Island - 07 [720p][5A3B3AFA].mp4
│   │   │   ├── [animegrimoire] Island - 08 [720p][918A8FCA].mp4
│   │   │   ├── [animegrimoire] Island - 09 [720p][AF2F9DF5].mp4
│   │   │   ├── [animegrimoire] Island - 10 [720p][D127860C].mp4
│   │   │   ├── [animegrimoire] Island - 11 [720p][3C6E8F70].mp4
│   │   │   └── [animegrimoire] Island - 12 [720p][5C3E07E6].mp4
│   │   ├── Movies
│   │   │   ├── [animegrimoire] I Want to Eat Your Pancreas [BD720p][ECF1DCD5].mp4
│   │   │   ├── [animegrimoire] Mirai no Mirai [BD720p][4895A114].mp4
│   │   │   ├── [animegrimoire] Non Non Biyori Vacation [BD720p][07836464].mp4
│   │   │   └── [animegrimoire] Shikioriori - 00 [720p][D62758AB].mp4
│   │   ├── Shingeki no Kyojin S3
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 38 [720p][E5D1AE0B].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 39 [720p][E3BC9690].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 40 [720p][F8072CFD].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 41 [720p][41B274EF].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 42 [720p][5D2FDD0F].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 43 [720p][4C5555ED].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 44 [720p][0E759174].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 45 [720p][E6F49E2C].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 46 [720p][350A72C5].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 47 [720p][9A016898].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 48 [720p][9865C43D].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 49 [720p][C6715795].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 50 [720p][78465C27].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 51 [720p][BCBB066A].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 52 [720p][692690E3].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 53 [720p][696B5F7B].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 54 [720p][962C185F].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 55 [720p][5E4333FC].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 56 [720p][242BD33D].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 57 [720p][FB4D0A0A].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin S3 - 58 [720p][20F969CD].mp4
│   │   │   └── [animegrimoire] Shingeki no Kyojin S3 - 59 [720p][27D480CF].mp4
│   │   ├── Shoujo Kageki Revue Starlight
│   │   │   ├── [animegrimoire] Shoujo Kageki Revue Starlight - 01 [720p][33051D7B].mp4
│   │   │   ├── [animegrimoire] Shoujo Kageki Revue Starlight - 02 [720p][4EB4EE8B].mp4
│   │   │   ├── [animegrimoire] Shoujo Kageki Revue Starlight - 03 [720p][0B12EC9D].mp4
│   │   │   ├── [animegrimoire] Shoujo Kageki Revue Starlight - 04 [720p][4AC7B2A5].mp4
│   │   │   ├── [animegrimoire] Shoujo Kageki Revue Starlight - 05 [720p][794841BB].mp4
│   │   │   ├── [animegrimoire] Shoujo Kageki Revue Starlight - 06 [720p][847519C1].mp4
│   │   │   ├── [animegrimoire] Shoujo Kageki Revue Starlight - 07 [720p][B115EC14].mp4
│   │   │   ├── [animegrimoire] Shoujo Kageki Revue Starlight - 08 [720p][43B7C775].mp4
│   │   │   ├── [animegrimoire] Shoujo Kageki Revue Starlight - 09 [720p][FB449F9C].mp4
│   │   │   ├── [animegrimoire] Shoujo Kageki Revue Starlight - 10 [720p][74E863C9].mp4
│   │   │   ├── [animegrimoire] Shoujo Kageki Revue Starlight - 11 [720p][8A182798].mp4
│   │   │   └── [animegrimoire] Shoujo Kageki Revue Starlight - 12 [720p][3118C141].mp4
│   │   └── Sunohara-sou no Kanrinin-san
│   │       ├── [animegrimoire] Sunohara-sou no Kanrinin-san - 01 [720p][4C4D12FF].mp4
│   │       ├── [animegrimoire] Sunohara-sou no Kanrinin-san - 02 [720p][C786AC2A].mp4
│   │       ├── [animegrimoire] Sunohara-sou no Kanrinin-san - 03 [720p][FED6AA51].mp4
│   │       ├── [animegrimoire] Sunohara-sou no Kanrinin-san - 04 [720p][B76F3D6F].mp4
│   │       ├── [animegrimoire] Sunohara-sou no Kanrinin-san - 05 [720p][BBE480A9].mp4
│   │       ├── [animegrimoire] Sunohara-sou no Kanrinin-san - 06 [720p][CEB8540E].mp4
│   │       ├── [animegrimoire] Sunohara-sou no Kanrinin-san - 07 [720p][68EF4EF2].mp4
│   │       ├── [animegrimoire] Sunohara-sou no Kanrinin-san - 08 [720p][A6ECC18D].mp4
│   │       ├── [animegrimoire] Sunohara-sou no Kanrinin-san - 09 [720p][0292E2E7].mp4
│   │       ├── [animegrimoire] Sunohara-sou no Kanrinin-san - 10 [720p][8620DD29].mp4
│   │       ├── [animegrimoire] Sunohara-sou no Kanrinin-san - 11 [720p][C7C2AD87].mp4
│   │       └── [animegrimoire] Sunohara-sou no Kanrinin-san - 12 [720p][D5B9CA78].mp4
│   └── Winter
│       ├── Attack on Titan The Roar of Awakening
│       │   └── [animegrimoire] Attack On Titan The Roar of Awakening [BD720p][121400AC].mp4
│       ├── Citrus
│       │   ├── [animegrimoire] Citrus - 01 [720p][727ACE18].mp4
│       │   ├── [animegrimoire] Citrus - 02 [720p][DFCF77FC].mp4
│       │   ├── [animegrimoire] Citrus - 03 [720p][3656274D].mp4
│       │   ├── [animegrimoire] Citrus - 04 [720p][1B2189C4].mp4
│       │   ├── [animegrimoire] Citrus - 05 [720p][A25231C5].mp4
│       │   ├── [animegrimoire] Citrus - 06 [720p][D8251A62].mp4
│       │   ├── [animegrimoire] Citrus - 07 [720p][5D584558].mp4
│       │   ├── [animegrimoire] Citrus - 08 [720p][60EFEAF0].mp4
│       │   ├── [animegrimoire] Citrus - 09 [720p][D6643F54].mp4
│       │   ├── [animegrimoire] Citrus - 10 [720p][CD9991D9].mp4
│       │   ├── [animegrimoire] Citrus - 11 [720p][2429A969].mp4
│       │   └── [animegrimoire] Citrus - 12 [720p][EDF7BECA].mp4
│       ├── Eiga Chuuninbyou demo Koi ga Shitai!
│       │   └── [animegrimoire] Eiga Chuunibyou demo Koi ga Shitai! Take on Me [BD720p][F165DBE8].mp4
│       ├── Hakumei to Mikochi
│       │   ├── [animegrimoire] Hakumei to Mikochi - 01 [720p][332ABF0A].mp4
│       │   ├── [animegrimoire] Hakumei to Mikochi - 02 [720p][BF760103].mp4
│       │   ├── [animegrimoire] Hakumei to Mikochi - 03 [720p][350AE267].mp4
│       │   ├── [animegrimoire] Hakumei to Mikochi - 04 [720p][97AA9E68].mp4
│       │   ├── [animegrimoire] Hakumei to Mikochi - 05 [720p][639581BD].mp4
│       │   ├── [animegrimoire] Hakumei to Mikochi - 06 [720p][7A995350].mp4
│       │   ├── [animegrimoire] Hakumei to Mikochi - 07 [720p][7F182547].mp4
│       │   ├── [animegrimoire] Hakumei to Mikochi - 08 [720p][CDEEBF99].mp4
│       │   ├── [animegrimoire] Hakumei to Mikochi - 09 [720p][2C956F83].mp4
│       │   ├── [animegrimoire] Hakumei to Mikochi - 10 [720p][FE2C0935].mp4
│       │   ├── [animegrimoire] Hakumei to Mikochi - 11 [720p][F5F148F5].mp4
│       │   ├── [animegrimoire] Hakumei to Mikochi - 12 [720p][096369A9].mp4
│       │   └── [animegrimoire] Hakumei to Mikochi - OVA [720p][EC236EB1].mp4
│       ├── Karakai Jouzu no Takagi-san
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san - 01 [720p][BB68FFB8].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san - 02 [720p][1C16FDD0].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san - 03 [720p][67C33E31].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san - 04 [720p][FFEB9D56].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san - 05 [720p][EF5BF17B].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san - 06 [720p][4E985203].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san - 07 [720p][58FAACBF].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san - 08 [720p][E9E6E4D6].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san - 09 [720p][747E5429].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san - 10 [720p][20F497F6].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san - 11 [720p][9A35AD74].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san - 12 [720p][890714EA].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san - OVA [576p][594825F5].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S2 - 01 [720p][19F3E857].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S2 - 02 [720p][DE940E2C].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S2 - 03 [720p][AC605AC1].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S2 - 04 [720p][687C037C].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S2 - 05 [720p][5587E4B2].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S2 - 06 [720p][562CA51A].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S2 - 07 [720p][0BD707AE].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S2 - 08 [720p][E8E8C558].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S2 - 09 [720p][3D7AE7B9].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S2 - 10 [720p][2C0F489C].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S2 - 11 [720p][57AF028B].mp4
│       │   └── [animegrimoire] Karakai Jouzu no Takagi-san S2 - 12 [720p][CE9F25BF].mp4
│       ├── Koi wa Ameagari no You ni
│       │   ├── [animegrimoire] Koi wa Ameagari no You ni - 01 [BD720p][BA566399].mp4
│       │   ├── [animegrimoire] Koi wa Ameagari no You ni - 02 [BD720p][8A9DBE29].mp4
│       │   ├── [animegrimoire] Koi wa Ameagari no You ni - 03 [BD720p][B7C81BD3].mp4
│       │   ├── [animegrimoire] Koi wa Ameagari no You ni - 04 [BD720p][E855C422].mp4
│       │   ├── [animegrimoire] Koi wa Ameagari no You ni - 05 [BD720p][5F73437B].mp4
│       │   ├── [animegrimoire] Koi wa Ameagari no You ni - 06 [BD720p][3281FA6E].mp4
│       │   ├── [animegrimoire] Koi wa Ameagari no You ni - 07 [BD720p][7D7BB1CB].mp4
│       │   ├── [animegrimoire] Koi wa Ameagari no You ni - 08 [BD720p][15240D91].mp4
│       │   ├── [animegrimoire] Koi wa Ameagari no You ni - 09 [BD720p][890DC560].mp4
│       │   ├── [animegrimoire] Koi wa Ameagari no You ni - 10 [BD720p][A1E73E7B].mp4
│       │   ├── [animegrimoire] Koi wa Ameagari no You ni - 11 [BD720p][419FA59D].mp4
│       │   └── [animegrimoire] Koi wa Ameagari no You ni - 12 [BD720p][F79BBE3B].mp4
│       ├── Mitsuboshi Colors
│       │   ├── [animegrimoire] Mitsuboshi Colors - 01 [720p][13DABEFF].mp4
│       │   ├── [animegrimoire] Mitsuboshi Colors - 02 [720p][21AD8511].mp4
│       │   ├── [animegrimoire] Mitsuboshi Colors - 03 [720p][182841F2].mp4
│       │   ├── [animegrimoire] Mitsuboshi Colors - 04 [720p][01A3A147].mp4
│       │   ├── [animegrimoire] Mitsuboshi Colors - 05 [720p][91310425].mp4
│       │   ├── [animegrimoire] Mitsuboshi Colors - 06 [720p][13C57496].mp4
│       │   ├── [animegrimoire] Mitsuboshi Colors - 07 [720p][73E13007].mp4
│       │   ├── [animegrimoire] Mitsuboshi Colors - 08 [720p][A41516BB].mp4
│       │   ├── [animegrimoire] Mitsuboshi Colors - 09 [720p][3684AE3E].mp4
│       │   ├── [animegrimoire] Mitsuboshi Colors - 10 [720p][B92ECA45].mp4
│       │   ├── [animegrimoire] Mitsuboshi Colors - 11 [720p][07BBD4D6].mp4
│       │   └── [animegrimoire] Mitsuboshi Colors - 12 [720p][318CA353].mp4
│       ├── Movies
│       │   └── [animegrimoire] Maquia - When The Promised Flower Blooms [720p][B68D7AB3].mp4
│       ├── ReLIFE OVA
│       │   ├── [animegrimoire] ReLIFE - 14 [720p][21F0260A].mp4
│       │   ├── [animegrimoire] ReLIFE - 15 [720p][FAB79227].mp4
│       │   ├── [animegrimoire] ReLIFE - 16 [720p][A1397130].mp4
│       │   └── [animegrimoire] ReLIFE - 17 [720p][AC0860FE].mp4
│       ├── Ryuuou no Oshigoto!
│       │   ├── [animegrimoire] Ryuuou no Oshigoto! - 01 [720p][A6408AEA].mp4
│       │   ├── [animegrimoire] Ryuuou no Oshigoto! - 02 [720p][0D4BB173].mp4
│       │   ├── [animegrimoire] Ryuuou no Oshigoto! - 03 [720p][A7FA414D].mp4
│       │   ├── [animegrimoire] Ryuuou no Oshigoto! - 04 [720p][7FA0E2E0].mp4
│       │   ├── [animegrimoire] Ryuuou no Oshigoto! - 05 [720p][9FCF2DEB].mp4
│       │   ├── [animegrimoire] Ryuuou no Oshigoto! - 06 [720p][D2EA7923].mp4
│       │   ├── [animegrimoire] Ryuuou no Oshigoto! - 07 [720p][AA894CA3].mp4
│       │   ├── [animegrimoire] Ryuuou no Oshigoto! - 08 [720p][A9657F5E].mp4
│       │   ├── [animegrimoire] Ryuuou no Oshigoto! - 09 [720p][42068921].mp4
│       │   ├── [animegrimoire] Ryuuou no Oshigoto! - 10 [720p][C05C1A3E].mp4
│       │   ├── [animegrimoire] Ryuuou no Oshigoto! - 11 [720p][67489FAB].mp4
│       │   └── [animegrimoire] Ryuuou no Oshigoto! - 12 [720p][3B7EA142].mp4
│       ├── Slow Start
│       │   ├── [animegrimoire] Slow Start - 01 [720p][12E5CB5E].mp4
│       │   ├── [animegrimoire] Slow Start - 02 [720p][E817C515].mp4
│       │   ├── [animegrimoire] Slow Start - 03 [720p][63C38B93].mp4
│       │   ├── [animegrimoire] Slow Start - 04 [720p][B6359E59].mp4
│       │   ├── [animegrimoire] Slow Start - 05 [720p][161CDA6F].mp4
│       │   ├── [animegrimoire] Slow Start - 06 [720p][14A300AA].mp4
│       │   ├── [animegrimoire] Slow Start - 07 [720p][F4B80E3C].mp4
│       │   ├── [animegrimoire] Slow Start - 08 [720p][0C05B04D].mp4
│       │   ├── [animegrimoire] Slow Start - 09 [720p][B86CAB20].mp4
│       │   ├── [animegrimoire] Slow Start - 10 [720p][D8779824].mp4
│       │   ├── [animegrimoire] Slow Start - 11 [720p][24FE054F].mp4
│       │   └── [animegrimoire] Slow Start - 12 [720p][852F4C71].mp4
│       ├── Sora Yori Tooi Basho
│       │   ├── [animegrimoire] Sora Yori Tooi Basho 01 [BD720p][5961B5B3].mp4
│       │   ├── [animegrimoire] Sora Yori Tooi Basho 02 [BD720p][C1C7BC89].mp4
│       │   ├── [animegrimoire] Sora Yori Tooi Basho 03 [BD720p][C2DB7F6C].mp4
│       │   ├── [animegrimoire] Sora Yori Tooi Basho 04 [BD720p][CC952B7E].mp4
│       │   ├── [animegrimoire] Sora Yori Tooi Basho 05 [BD720p][4EB0E168].mp4
│       │   ├── [animegrimoire] Sora Yori Tooi Basho 06 [BD720p][706D19BA].mp4
│       │   ├── [animegrimoire] Sora Yori Tooi Basho 07 [BD720p][94C598D2].mp4
│       │   ├── [animegrimoire] Sora Yori Tooi Basho 08 [BD720p][E909FAA7].mp4
│       │   ├── [animegrimoire] Sora Yori Tooi Basho 09 [BD720p][C062D133].mp4
│       │   ├── [animegrimoire] Sora Yori Tooi Basho 10 [BD720p][368EBBEF].mp4
│       │   ├── [animegrimoire] Sora Yori Tooi Basho 11 [BD720p][541B148E].mp4
│       │   ├── [animegrimoire] Sora Yori Tooi Basho 12 [BD720p][5B791512].mp4
│       │   └── [animegrimoire] Sora Yori Tooi Basho 13 [BD720p][9ABEE5F9].mp4
│       ├── Takunomi
│       │   ├── [animegrimoire] Takunomi - 01 [720p][CADA25E3].mp4
│       │   ├── [animegrimoire] Takunomi - 02 [720p][1CD4FD93].mp4
│       │   ├── [animegrimoire] Takunomi - 03 [720p][981FC0B4].mp4
│       │   ├── [animegrimoire] Takunomi - 04 [720p][5F28E516].mp4
│       │   ├── [animegrimoire] Takunomi - 05 [720p][4E0A7F45].mp4
│       │   ├── [animegrimoire] Takunomi - 06 [720p][6BE768DF].mp4
│       │   ├── [animegrimoire] Takunomi - 07 [720p][C107CF18].mp4
│       │   ├── [animegrimoire] Takunomi - 08 [720p][7BB11FAC].mp4
│       │   ├── [animegrimoire] Takunomi - 09 [720p][AE97D9D6].mp4
│       │   ├── [animegrimoire] Takunomi - 10 [720p][D73B5E4D].mp4
│       │   ├── [animegrimoire] Takunomi - 11 [720p][07DC3376].mp4
│       │   └── [animegrimoire] Takunomi - 12 [720p][5DD604BF].mp4
│       ├── Toji no Miko
│       │   ├── [animegrimoire] Toji no Miko - 01 [720p][2F367A71].mp4
│       │   ├── [animegrimoire] Toji no Miko - 02 [720p][A5074033].mp4
│       │   ├── [animegrimoire] Toji no Miko - 03 [720p][0E44D7CA].mp4
│       │   ├── [animegrimoire] Toji no Miko - 04 [720p][F76D6B4E].mp4
│       │   ├── [animegrimoire] Toji no Miko - 05 [720p][09284A90].mp4
│       │   ├── [animegrimoire] Toji no Miko - 06 [720p][7ACE29FC].mp4
│       │   ├── [animegrimoire] Toji no Miko - 07 [720p][A8FE46C2].mp4
│       │   ├── [animegrimoire] Toji no Miko - 08 [720p][A72A57A1].mp4
│       │   ├── [animegrimoire] Toji no Miko - 09 [720p][38A1844D].mp4
│       │   ├── [animegrimoire] Toji no Miko - 10 [720p][6B29BAC2].mp4
│       │   ├── [animegrimoire] Toji no Miko - 11 [720p][AA31AA41].mp4
│       │   ├── [animegrimoire] Toji no Miko - 12.5 [720p][E220F718].mp4
│       │   ├── [animegrimoire] Toji no Miko - 12 [720p][38CC205F].mp4
│       │   ├── [animegrimoire] Toji no Miko - 13 [720p][29A9C2D6].mp4
│       │   ├── [animegrimoire] Toji no Miko - 14 [720p][8BC44B1C].mp4
│       │   ├── [animegrimoire] Toji no Miko - 15 [720p][E5FA174A].mp4
│       │   ├── [animegrimoire] Toji no Miko - 16 [720p][FD2A9C6E].mp4
│       │   ├── [animegrimoire] Toji no Miko - 17 [720p][6E09C3BA].mp4
│       │   ├── [animegrimoire] Toji no Miko - 18 [720p][B812436D].mp4
│       │   ├── [animegrimoire] Toji no Miko - 19 [720p][EBA967F1].mp4
│       │   ├── [animegrimoire] Toji no Miko - 20 [720p][5F3C6540].mp4
│       │   ├── [animegrimoire] Toji no Miko - 21 [720p][3C298CB0].mp4
│       │   ├── [animegrimoire] Toji no Miko - 22 [720p][C97EF868].mp4
│       │   ├── [animegrimoire] Toji no Miko - 23 [720p][6C1BA347].mp4
│       │   └── [animegrimoire] Toji no Miko - 24 [720p][FFAA2856].mp4
│       └── Violet Evergarden
│           ├── [animegrimoire] Violet Evergarden - 01 [BD720p][1DCF7767].mp4
│           ├── [animegrimoire] Violet Evergarden - 02 [BD720p][01BEEB5B].mp4
│           ├── [animegrimoire] Violet Evergarden - 03 [BD720p][64E6B844].mp4
│           ├── [animegrimoire] Violet Evergarden - 04 [BD720p][7BA23036].mp4
│           ├── [animegrimoire] Violet Evergarden - 05 [BD720p][65A49F40].mp4
│           ├── [animegrimoire] Violet Evergarden - 06 [BD720p][10582390].mp4
│           ├── [animegrimoire] Violet Evergarden - 07 [BD720p][3426EBA7].mp4
│           ├── [animegrimoire] Violet Evergarden - 08 [BD720p][B47CACF2].mp4
│           ├── [animegrimoire] Violet Evergarden - 09 [BD720p][0C858B5F].mp4
│           ├── [animegrimoire] Violet Evergarden - 10 [BD720p][7877ABA8].mp4
│           ├── [animegrimoire] Violet Evergarden - 11 [BD720p][AA7F4EF9].mp4
│           ├── [animegrimoire] Violet Evergarden - 12 [BD720p][442CF7AB].mp4
│           ├── [animegrimoire] Violet Evergarden - 13 [BD720p][D177760B].mp4
│           ├── [animegrimoire] Violet Evergarden - 14 [BD720p][EFA3874D].mp4
│           ├── [animegrimoire] Violet Evergarden - NCED [BD720p][AAB8D977].mp4
│           └── [animegrimoire] Violet Evergarden - NCOP [BD720p][0D73AF5E].mp4
├── 2019
│   ├── Fall
│   │   ├── Assassins Pride
│   │   │   ├── [animegrimoire] Assassins Pride - 01 [720p][E5D64CFE].mp4
│   │   │   ├── [animegrimoire] Assassins Pride - 02 [720p][EF263EF2].mp4
│   │   │   ├── [animegrimoire] Assassins Pride - 03 [720p][10C9D1D0].mp4
│   │   │   ├── [animegrimoire] Assassins Pride - 04 [720p][723D1765].mp4
│   │   │   ├── [animegrimoire] Assassins Pride - 05 [720p][A7B421E3].mp4
│   │   │   ├── [animegrimoire] Assassins Pride - 06 [720p][74619D38].mp4
│   │   │   ├── [animegrimoire] Assassins Pride - 07 [720p][999D2DFF].mp4
│   │   │   ├── [animegrimoire] Assassins Pride - 08 [720p][7330AAE1].mp4
│   │   │   ├── [animegrimoire] Assassins Pride - 09 [720p][E3A394FB].mp4
│   │   │   ├── [animegrimoire] Assassins Pride - 10 [720p][FBA129DE].mp4
│   │   │   ├── [animegrimoire] Assassins Pride - 11 [720p][7FD426B8].mp4
│   │   │   └── [animegrimoire] Assassins Pride - 12 [720p][CE73CF84].mp4
│   │   ├── FGO - Absolute Demonic Front
│   │   │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 00 [720p][39C7148F].mp4
│   │   │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 01 [720p][7BF90CCE].mp4
│   │   │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 02 [720p][DFAFDEA1].mp4
│   │   │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 03 [720p][9AA1AEEC].mp4
│   │   │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 04 [720p][A47ABF4E].mp4
│   │   │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 05 [720p][564F13BB].mp4
│   │   │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 06 [720p][62DFB82A].mp4
│   │   │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 07 [720p][D144A40E].mp4
│   │   │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 08 [720p][D8C20049].mp4
│   │   │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 09 [720p][2F1BB868].mp4
│   │   │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 10 [720p][93144D22].mp4
│   │   │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 11 [720p][02AEAF8D].mp4
│   │   │   └── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 12 [720p][CCC8A43B].mp4
│   │   ├── Fragtime
│   │   │   └── [animegrimoire] Fragtime - 00 [BD720p][0CCF11FF].mp4
│   │   ├── Honzuki no Gekokujou
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 01 [720p][C1301BF8].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 02 [720p][3A4F8202].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 03 [720p][BD421C3F].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 04 [720p][C8ACA205].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 05 [720p][FC905872].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 06 [720p][5FA079F2].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 07 [720p][CDCDBA99].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 08 [720p][47038090].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 09 [720p][408E511B].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 10 [720p][2BD1D1AC].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 11 [720p][A0074E93].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 12 [720p][31E48722].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 13 [720p][FDE04A84].mp4
│   │   │   └── [animegrimoire] Honzuki no Gekokujou - 14 [720p][F0439686].mp4
│   │   ├── Houkago Saikoro Club
│   │   │   ├── [animegrimoire] Houkago Saikoro Club - 01 [720p][B0F4BA27].mp4
│   │   │   ├── [animegrimoire] Houkago Saikoro Club - 02 [720p][11B92FC1].mp4
│   │   │   ├── [animegrimoire] Houkago Saikoro Club - 03 [720p][7AF3343B].mp4
│   │   │   ├── [animegrimoire] Houkago Saikoro Club - 04 [720p][25D00A52].mp4
│   │   │   ├── [animegrimoire] Houkago Saikoro Club - 05 [720p][72DB0CB2].mp4
│   │   │   ├── [animegrimoire] Houkago Saikoro Club - 06 [720p][07B92458].mp4
│   │   │   ├── [animegrimoire] Houkago Saikoro Club - 07 [720p][782767D0].mp4
│   │   │   ├── [animegrimoire] Houkago Saikoro Club - 08 [720p][2421C909].mp4
│   │   │   ├── [animegrimoire] Houkago Saikoro Club - 09 [720p][9267BE23].mp4
│   │   │   ├── [animegrimoire] Houkago Saikoro Club - 10 [720p][3B11FF25].mp4
│   │   │   ├── [animegrimoire] Houkago Saikoro Club - 11 [720p][BA6B0418].mp4
│   │   │   └── [animegrimoire] Houkago Saikoro Club - 12 [720p][362BA538].mp4
│   │   ├── No Guns Life
│   │   │   ├── [animegrimoire] No Guns Life - 01 [720p][914CCE9F].mp4
│   │   │   ├── [animegrimoire] No Guns Life - 02 [720p][0EEB89A3].mp4
│   │   │   ├── [animegrimoire] No Guns Life - 03 [720p][B0C56427].mp4
│   │   │   ├── [animegrimoire] No Guns Life - 04 [720p][E0A47271].mp4
│   │   │   ├── [animegrimoire] No Guns Life - 05 [720p][0EFB1BD0].mp4
│   │   │   ├── [animegrimoire] No Guns Life - 06 [720p][3F861183].mp4
│   │   │   ├── [animegrimoire] No Guns Life - 07 [720p][62BBD548].mp4
│   │   │   ├── [animegrimoire] No Guns Life - 08 [720p][5F500559].mp4
│   │   │   ├── [animegrimoire] No Guns Life - 09 [720p][C86FD06F].mp4
│   │   │   ├── [animegrimoire] No Guns Life - 10 [720p][0FC67BA3].mp4
│   │   │   ├── [animegrimoire] No Guns Life - 11 [720p][03B28620].mp4
│   │   │   └── [animegrimoire] No Guns Life - 12 [720p][BD826996].mp4
│   │   ├── Rifle is Beautiful
│   │   │   ├── [animegrimoire] Rifle is Beautiful - 01 [720p][1335F5B8].mp4
│   │   │   ├── [animegrimoire] Rifle is Beautiful - 02 [720p][72DF42A2].mp4
│   │   │   ├── [animegrimoire] Rifle is Beautiful - 03 [720p][1A0556C2].mp4
│   │   │   ├── [animegrimoire] Rifle is Beautiful - 04 [720p][19E36C30].mp4
│   │   │   ├── [animegrimoire] Rifle is Beautiful - 05 [720p][F5572E02].mp4
│   │   │   ├── [animegrimoire] Rifle is Beautiful - 06 [720p][3D065396].mp4
│   │   │   ├── [animegrimoire] Rifle is Beautiful - 07.5 [720p][9CD93B9A].mp4
│   │   │   ├── [animegrimoire] Rifle is Beautiful - 07 [720p][D1C4AFDE].mp4
│   │   │   ├── [animegrimoire] Rifle is Beautiful - 08 [720p][102957B4].mp4
│   │   │   ├── [animegrimoire] Rifle is Beautiful - 09 [720p][E458994B].mp4
│   │   │   ├── [animegrimoire] Rifle is Beautiful - 10 [720p][B64E1053].mp4
│   │   │   ├── [animegrimoire] Rifle is Beautiful - 11 [720p][9FD3B885].mp4
│   │   │   ├── [animegrimoire] Rifle is Beautiful - 11 [720p][D2B45F70].mp4
│   │   │   └── [animegrimoire] Rifle is Beautiful - 12 [720p][A4FE5F8D].mp4
│   │   ├── Watashi Nouryoku wa Heikinchi de tte Itta yo ne
│   │   │   ├── [animegrimoire] Watashi, Nouryoku wa Heikinchi de tte Itta yo ne! - 01 [720p][8C7A81EF].mp4
│   │   │   ├── [animegrimoire] Watashi, Nouryoku wa Heikinchi de tte Itta yo ne! - 02 [720p][2C63716D].mp4
│   │   │   ├── [animegrimoire] Watashi, Nouryoku wa Heikinchi de tte Itta yo ne! - 03 [720p][96665BAB].mp4
│   │   │   ├── [animegrimoire] Watashi, Nouryoku wa Heikinchi de tte Itta yo ne! - 04 [720p][BFA52DCB].mp4
│   │   │   ├── [animegrimoire] Watashi, Nouryoku wa Heikinchi de tte Itta yo ne! - 05 [720p][39E49EC6].mp4
│   │   │   ├── [animegrimoire] Watashi, Nouryoku wa Heikinchi de tte Itta yo ne! - 06 [720p][212F37F8].mp4
│   │   │   ├── [animegrimoire] Watashi, Nouryoku wa Heikinchi de tte Itta yo ne! - 07 [720p][521E4456].mp4
│   │   │   ├── [animegrimoire] Watashi, Nouryoku wa Heikinchi de tte Itta yo ne! - 08 [720p][D4AFC146].mp4
│   │   │   ├── [animegrimoire] Watashi, Nouryoku wa Heikinchi de tte Itta yo ne! - 09 [720p][F689F7C4].mp4
│   │   │   ├── [animegrimoire] Watashi, Nouryoku wa Heikinchi de tte Itta yo ne! - 10 [720p][D261CC82].mp4
│   │   │   ├── [animegrimoire] Watashi, Nouryoku wa Heikinchi de tte Itta yo ne! - 11 [720p][CCA18031].mp4
│   │   │   └── [animegrimoire] Watashi, Nouryoku wa Heikinchi de tte Itta yo ne! - 12 [720p][2CD34792].mp4
│   │   └── ZX Code Reunion
│   │       ├── [animegrimoire] ZX - Code Reunion - 01 [720p][05D5424E].mp4
│   │       ├── [animegrimoire] ZX - Code Reunion - 02 [720p][C77077EF].mp4
│   │       ├── [animegrimoire] ZX - Code Reunion - 03 [720p][27A6ED49].mp4
│   │       ├── [animegrimoire] ZX - Code Reunion - 04 [720p][37E06345].mp4
│   │       ├── [animegrimoire] ZX - Code Reunion - 05 [720p][A23D3E2D].mp4
│   │       ├── [animegrimoire] ZX - Code Reunion - 06 [720p][3DF5BF3B].mp4
│   │       ├── [animegrimoire] ZX - Code Reunion - 07 [720p][715F40F2].mp4
│   │       ├── [animegrimoire] ZX - Code Reunion - 08 [720p][E6273168].mp4
│   │       ├── [animegrimoire] ZX - Code Reunion - 09 [720p][18AD122C].mp4
│   │       ├── [animegrimoire] ZX - Code Reunion - 10 [720p][90B5C547].mp4
│   │       ├── [animegrimoire] ZX - Code Reunion - 11 [720p][0C8584B6].mp4
│   │       └── [animegrimoire] ZX - Code Reunion - 12 [720p][6DDCF631].mp4
│   ├── Spring
│   │   ├── Aikatsu
│   │   │   ├── [animegrimoire] Aikatsu! Nerawareta Mahou no Aikatsu Card [BD720p][45ACB480].mp4
│   │   │   └── [animegrimoire] Aikatsu! The Movie [BD 720p AAC][2169E352].mp4
│   │   ├── Hitoribocchi no Marumaru Seikatsu
│   │   │   ├── [animegrimoire] Hitoribocchi no Marumaru Seikatsu - 01 [720p][5EEDD573].mp4
│   │   │   ├── [animegrimoire] Hitoribocchi no Marumaru Seikatsu - 02 [720p][109CA93F].mp4
│   │   │   ├── [animegrimoire] Hitoribocchi no Marumaru Seikatsu - 03 [720p][2ED200EC].mp4
│   │   │   ├── [animegrimoire] Hitoribocchi no Marumaru Seikatsu - 04 [720p][4AC1AC29].mp4
│   │   │   ├── [animegrimoire] Hitoribocchi no Marumaru Seikatsu - 05 [720p][D5C390A1].mp4
│   │   │   ├── [animegrimoire] Hitoribocchi no Marumaru Seikatsu - 06 [720p][4308B475].mp4
│   │   │   ├── [animegrimoire] Hitoribocchi no Marumaru Seikatsu - 07 [720p][A51D8E43].mp4
│   │   │   ├── [animegrimoire] Hitoribocchi no Marumaru Seikatsu - 08 [720p][F8577471].mp4
│   │   │   ├── [animegrimoire] Hitoribocchi no Marumaru Seikatsu - 09 [720p][A9271CF9].mp4
│   │   │   ├── [animegrimoire] Hitoribocchi no Marumaru Seikatsu - 10 [720p][2FC67D03].mp4
│   │   │   ├── [animegrimoire] Hitoribocchi no Marumaru Seikatsu - 11 [720p][16431790].mp4
│   │   │   └── [animegrimoire] Hitoribocchi no Marumaru Seikatsu - 12 [720p][434441DC].mp4
│   │   ├── Joshikausei
│   │   │   ├── [animegrimoire] Joshikausei - 01 [720p][460C7291].mp4
│   │   │   ├── [animegrimoire] Joshikausei - 02 [720p][DF2FF470].mp4
│   │   │   ├── [animegrimoire] Joshikausei - 03 [720p][B12DEC3D].mp4
│   │   │   ├── [animegrimoire] Joshikausei - 04 [720p][A2226A46].mp4
│   │   │   ├── [animegrimoire] Joshikausei - 05 [720p][B25D0177].mp4
│   │   │   ├── [animegrimoire] Joshikausei - 06 [720p][796CCC8C].mp4
│   │   │   ├── [animegrimoire] Joshikausei - 07 [720p][986BEF3F].mp4
│   │   │   ├── [animegrimoire] Joshikausei - 08 [720p][F2D53487].mp4
│   │   │   ├── [animegrimoire] Joshikausei - 09 [720p][ECA19D82].mp4
│   │   │   ├── [animegrimoire] Joshikausei - 10 [720p][82099004].mp4
│   │   │   ├── [animegrimoire] Joshikausei - 11 [720p][F17EC1EC].mp4
│   │   │   └── [animegrimoire] Joshikausei - 12 [720p][12AAE0D3].mp4
│   │   ├── Kimetsu no Yaiba
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 01 [720p][F0E0A850].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 02 [720p][9F8ECF40].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 03 [720p][FAB971E8].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 04 [720p][CBCD2D24].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 05 [720p][79B09D76].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 06 [720p][D3B647EC].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 07 [720p][EAA7E074].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 08 [720p][FD486A58].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 09 [720p][DEB943F3].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 10 [720p][9178777D].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 11 [720p][C7D46782].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 12 [720p][E27AF702].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 13 [720p][3240A492].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 14 [720p][79CF49E1].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 15 [720p][4491DDA6].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 16 [720p][01C8DF9A].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 17 [720p][C9E9E471].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 18 [720p][7E91427A].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 19 [720p][EEACE17D].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 20 [720p][82D4FA35].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 21 [720p][93C58591].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 22 [720p][150FEEE1].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 23 [720p][9460885C].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 24 [720p][FD59C246].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - 25 [720p][E54BD684].mp4
│   │   │   └── [animegrimoire] Kimetsu no Yaiba - 26 [720p][4FD099A4].mp4
│   │   ├── Kono Oto Tomare
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 01 [720p][EE973C95].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 02 [720p][E8BED310].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 03 [720p][8F2490F3].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 04 [720p][20E5CDB6].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 05 [720p][6FC16448].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 06 [720p][C85DEF5A].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 07 [720p][18E7A84B].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 08 [720p][CA1668A6].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 09 [720p][98E2FF98].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 09 [720p][E2EC18C1].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 10 [720p][EC64DD2A].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 11 [720p][9FDAC04A].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 12 [720p][F7A9DFD8].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 13 [720p][CA60BDAA].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 14 [720p][ACD6F856].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 15 [720p][7BFE94CA].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 16 [720p][A5CF7172].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 17 [720p][302461D2].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 18 [720p][8F194A06].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 19 [720p][16B80CCC].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 20 [720p][DEE9528A].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 21 [720p][A19E0533].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 22 [720p][31A73DCF].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 23 [720p][D4F8BA63].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 24 [720p][B4C6BA9B].mp4
│   │   │   ├── [animegrimoire] Kono Oto Tomare! - 25 [720p][E7F49760].mp4
│   │   │   └── [animegrimoire] Kono Oto Tomare! - 26 [720p][4A23976B].mp4
│   │   ├── Koutetsujou no Kabaneri Kessen
│   │   │   ├── [animegrimoire] Koutetsujou no Kabaneri Kessen - 01 [BD720p][368A9CDA].mp4
│   │   │   ├── [animegrimoire] Koutetsujou no Kabaneri Kessen - 02 [BD720p][DCC760EF].mp4
│   │   │   └── [animegrimoire] Koutetsujou no Kabaneri Kessen - 03 [BD720p][FDB6224F].mp4
│   │   ├── Sewayaki Kitsune no Senko-san
│   │   │   ├── [animegrimoire] Sewayaki Kitsune no Senko-san - 01 [720p][A320CA54].mp4
│   │   │   ├── [animegrimoire] Sewayaki Kitsune no Senko-san - 02 [720p][3CFE0DD6].mp4
│   │   │   ├── [animegrimoire] Sewayaki Kitsune no Senko-san - 03 [720p][B6842F69].mp4
│   │   │   ├── [animegrimoire] Sewayaki Kitsune no Senko-san - 04 [720p][4E2D63A0].mp4
│   │   │   ├── [animegrimoire] Sewayaki Kitsune no Senko-san - 05 [720p][161B4FA0].mp4
│   │   │   ├── [animegrimoire] Sewayaki Kitsune no Senko-san - 06 [720p][705BE865].mp4
│   │   │   ├── [animegrimoire] Sewayaki Kitsune no Senko-san - 07 [720p][C62CC9AE].mp4
│   │   │   ├── [animegrimoire] Sewayaki Kitsune no Senko-san - 08 [720p][6A8834BF].mp4
│   │   │   ├── [animegrimoire] Sewayaki Kitsune no Senko-san - 09 [720p][5CC74F46].mp4
│   │   │   ├── [animegrimoire] Sewayaki Kitsune no Senko-san - 10 [720p][717B6AB6].mp4
│   │   │   ├── [animegrimoire] Sewayaki Kitsune no Senko-san - 11 [720p][867EF0B5].mp4
│   │   │   └── [animegrimoire] Sewayaki Kitsune no Senko-san - 12 [720p][309A5ABD].mp4
│   │   └── Strike Witches - 501-butai Hasshin Shimasu!
│   │       ├── [animegrimoire] Strike Witches - 501-butai Hasshin Shimasu! - 01 [720p][26DEFB27].mp4
│   │       ├── [animegrimoire] Strike Witches - 501-butai Hasshin Shimasu! - 02 [720p][241D2477].mp4
│   │       ├── [animegrimoire] Strike Witches - 501-butai Hasshin Shimasu! - 03 [720p][0F7772C6].mp4
│   │       ├── [animegrimoire] Strike Witches - 501-butai Hasshin Shimasu! - 04 [720p][67BBCEF1].mp4
│   │       ├── [animegrimoire] Strike Witches - 501-butai Hasshin Shimasu! - 05 [720p][BBAA1F2D].mp4
│   │       ├── [animegrimoire] Strike Witches - 501-butai Hasshin Shimasu! - 06 [720p][A0E971B8].mp4
│   │       ├── [animegrimoire] Strike Witches - 501-butai Hasshin Shimasu! - 07 [720p][040162C0].mp4
│   │       ├── [animegrimoire] Strike Witches - 501-butai Hasshin Shimasu! - 08 [720p][CED71EE4].mp4
│   │       ├── [animegrimoire] Strike Witches - 501-butai Hasshin Shimasu! - 09 [720p][2D3BEBF9].mp4
│   │       ├── [animegrimoire] Strike Witches - 501-butai Hasshin Shimasu! - 10 [720p][D7B83EFB].mp4
│   │       ├── [animegrimoire] Strike Witches - 501-butai Hasshin Shimasu! - 11 [720p][B3A9BC8E].mp4
│   │       └── [animegrimoire] Strike Witches - 501-butai Hasshin Shimasu! - 12 [720p][BE475788].mp4
│   ├── Summer
│   │   ├── Araburu Kisetsu no Otome-domo yo
│   │   │   ├── [animegrimoire] Araburu Kisetsu no Otome-domo yo. - 01 [720p][1A320A58].mp4
│   │   │   ├── [animegrimoire] Araburu Kisetsu no Otome-domo yo. - 02 [720p][2B1C9541].mp4
│   │   │   ├── [animegrimoire] Araburu Kisetsu no Otome-domo yo. - 03 [720p][CFF0925D].mp4
│   │   │   ├── [animegrimoire] Araburu Kisetsu no Otome-domo yo. - 04 [720p][A3AA429F].mp4
│   │   │   ├── [animegrimoire] Araburu Kisetsu no Otome-domo yo. - 05 [720p][BF0B8E78].mp4
│   │   │   ├── [animegrimoire] Araburu Kisetsu no Otome-domo yo. - 06 [720p][165A622E].mp4
│   │   │   ├── [animegrimoire] Araburu Kisetsu no Otome-domo yo. - 07 [720p][F48366F9].mp4
│   │   │   ├── [animegrimoire] Araburu Kisetsu no Otome-domo yo. - 08 [720p][C0500584].mp4
│   │   │   ├── [animegrimoire] Araburu Kisetsu no Otome-domo yo. - 09 [720p][3E739D0D].mp4
│   │   │   ├── [animegrimoire] Araburu Kisetsu no Otome-domo yo. - 10 [720p][2E73924E].mp4
│   │   │   ├── [animegrimoire] Araburu Kisetsu no Otome-domo yo. - 11 [720p][DD76590B].mp4
│   │   │   └── [animegrimoire] Araburu Kisetsu no Otome-domo yo. - 12 [720p][ACD5BF94].mp4
│   │   ├── Cop Craft
│   │   │   ├── [animegrimoire] Cop Craft - 01 [720p][EEBA44EC].mp4
│   │   │   ├── [animegrimoire] Cop Craft - 02 [720p][ADBDCE98].mp4
│   │   │   ├── [animegrimoire] Cop Craft - 03 [720p][A0EEE78B].mp4
│   │   │   ├── [animegrimoire] Cop Craft - 04 [720p][C3060AC7].mp4
│   │   │   ├── [animegrimoire] Cop Craft - 05 [720p][3794C186].mp4
│   │   │   ├── [animegrimoire] Cop Craft - 06 [720p][460CE601].mp4
│   │   │   ├── [animegrimoire] Cop Craft - 07 [720p][0B8C57A5].mp4
│   │   │   ├── [animegrimoire] Cop Craft - 08 [720p][CF7D21E7].mp4
│   │   │   ├── [animegrimoire] Cop Craft - 09.5 [720p][5B788F46].mp4
│   │   │   ├── [animegrimoire] Cop Craft - 09 [720p][2417D150].mp4
│   │   │   ├── [animegrimoire] Cop Craft - 10 [720p][10AAF740].mp4
│   │   │   ├── [animegrimoire] Cop Craft - 11 [720p][A6829C65].mp4
│   │   │   └── [animegrimoire] Cop Craft - 12 [720p][E9D329C7].mp4
│   │   ├── Dr. Stone
│   │   │   ├── [animegrimoire] Dr. Stone - 01 [720p][DB88A982].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 02 [720p][AE15A770].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 03 [720p][74460FB6].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 04 [720p][4A9CD025].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 05 [720p][81C5C31E].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 06 [720p][315A619C].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 07 [720p][CAE79F94].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 08 [720p][0EC0FF63].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 09 [720p][07B7D932].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 10 [720p][A7109898].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 11 [720p][F511B550].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 12 [720p][BAE1B713].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 13 [720p][66BE2D94].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 14 [720p][1C0FCE82].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 15 [720p][FD62CD4E].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 16 [720p][18FC4068].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 17 [720p][B8BEF4BD].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 18 [720p][E2F5CE54].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 19 [720p][48C285B2].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 20 [720p][8D4CCA7B].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 21 [720p][DDED44EF].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 22 [720p][CDE9A244].mp4
│   │   │   ├── [animegrimoire] Dr. Stone - 23 [720p][D5D2D2F5].mp4
│   │   │   └── [animegrimoire] Dr. Stone - 24 [720p][ADC7B4D9].mp4
│   │   ├── HenSuki
│   │   │   ├── [animegrimoire] HenSuki - 01 [720p][BBDF3C4F].mp4
│   │   │   ├── [animegrimoire] HenSuki - 02 [720p][E2B238EF].mp4
│   │   │   ├── [animegrimoire] HenSuki - 03 [720p][B9611A7E].mp4
│   │   │   ├── [animegrimoire] HenSuki - 04 [720p][EAD108CE].mp4
│   │   │   ├── [animegrimoire] HenSuki - 05 [720p][FC535F61].mp4
│   │   │   ├── [animegrimoire] HenSuki - 06 [720p][979768B8].mp4
│   │   │   ├── [animegrimoire] HenSuki - 07 [720p][7BE082F1].mp4
│   │   │   ├── [animegrimoire] HenSuki - 08 [720p][1273CC6F].mp4
│   │   │   ├── [animegrimoire] HenSuki - 09 [720p][747A7F96].mp4
│   │   │   ├── [animegrimoire] HenSuki - 10 [720p][473DA325].mp4
│   │   │   ├── [animegrimoire] HenSuki - 11 [720p][BCE7105C].mp4
│   │   │   └── [animegrimoire] HenSuki - 12 [720p][AF7B1660].mp4
│   │   ├── Honkai Impact 3
│   │   │   ├── [animegrimoire] Honkai Impact 3 Cooking with Valkyries - 10 [720p][186ABC13].mp4
│   │   │   ├── [animegrimoire] Honkai Impact 3 Cooking with Valkyries - 1 [720p][5576BAA6].mp4
│   │   │   ├── [animegrimoire] Honkai Impact 3 Cooking with Valkyries - 2 [720p][FE8C7B93].mp4
│   │   │   ├── [animegrimoire] Honkai Impact 3 Cooking with Valkyries - 3 [720p][918B0726].mp4
│   │   │   ├── [animegrimoire] Honkai Impact 3 Cooking with Valkyries - 4 [720p][BEF0C5BE].mp4
│   │   │   ├── [animegrimoire] Honkai Impact 3 Cooking with Valkyries - 5 [720p][97E31331].mp4
│   │   │   ├── [animegrimoire] Honkai Impact 3 Cooking with Valkyries - 6 [720p][563B30B3].mp4
│   │   │   ├── [animegrimoire] Honkai Impact 3 Cooking with Valkyries - 7 [720p][C00966D2].mp4
│   │   │   ├── [animegrimoire] Honkai Impact 3 Cooking with Valkyries - 8 [720p][336F7574].mp4
│   │   │   ├── [animegrimoire] Honkai Impact 3 Cooking with Valkyries - 9 [720p][4EABA504].mp4
│   │   │   └── [animegrimoire] Honkai Impact 3 Honkai Chronicles - 1 [720p][47509F81].mp4
│   │   ├── Joshikousei no Mudazukai
│   │   │   ├── [animegrimoire] Joshikousei no Mudazukai - 01 [720p][6058983B].mp4
│   │   │   ├── [animegrimoire] Joshikousei no Mudazukai - 02 [720p][CF9F7E40].mp4
│   │   │   ├── [animegrimoire] Joshikousei no Mudazukai - 03 [720p][B6997527].mp4
│   │   │   ├── [animegrimoire] Joshikousei no Mudazukai - 04 [720p][AD9AC52A].mp4
│   │   │   ├── [animegrimoire] Joshikousei no Mudazukai - 05 [720p][32F259F6].mp4
│   │   │   ├── [animegrimoire] Joshikousei no Mudazukai - 06 [720p][504346F6].mp4
│   │   │   ├── [animegrimoire] Joshikousei no Mudazukai - 07 [720p][AE37A7FD].mp4
│   │   │   ├── [animegrimoire] Joshikousei no Mudazukai - 08 [720p][0A9E7048].mp4
│   │   │   ├── [animegrimoire] Joshikousei no Mudazukai - 09 [720p][ECFE44EB].mp4
│   │   │   ├── [animegrimoire] Joshikousei no Mudazukai - 10 [720p][514F1A64].mp4
│   │   │   ├── [animegrimoire] Joshikousei no Mudazukai - 11 [720p][952D0498].mp4
│   │   │   └── [animegrimoire] Joshikousei no Mudazukai - 12 [720p][96AA8E5D].mp4
│   │   ├── Machikado Mazoku
│   │   │   ├── [animegrimoire] Machikado Mazoku - 01 [720p][7175103A].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku - 02 [720p][EB87BBFC].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku - 03 [720p][BE50D0E4].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku - 04 [720p][1A0D8629].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku - 05 [720p][9C749B7C].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku - 06 [720p][584DCB14].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku - 07 [720p][023C2BD0].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku - 08 [720p][7DB1E451].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku - 09 [720p][8A2DB98C].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku - 10 [720p][55112257].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku - 11 [720p][06689B98].mp4
│   │   │   └── [animegrimoire] Machikado Mazoku - 12 [720p][B375BB14].mp4
│   │   ├── Re Stage! Dream Days
│   │   │   ├── [animegrimoire] Re Stage! Dream Days - 01 [720p][98EBC6BD].mp4
│   │   │   ├── [animegrimoire] Re Stage! Dream Days - 02 [720p][A9BA33AA].mp4
│   │   │   ├── [animegrimoire] Re Stage! Dream Days - 03 [720p][01BB08BC].mp4
│   │   │   ├── [animegrimoire] Re Stage! Dream Days - 04 [720p][EF6AE202].mp4
│   │   │   ├── [animegrimoire] Re Stage! Dream Days - 05 [720p][DAC5B50A].mp4
│   │   │   ├── [animegrimoire] Re Stage! Dream Days - 06 [720p][285A9000].mp4
│   │   │   ├── [animegrimoire] Re Stage! Dream Days - 07 [720p][C2239823].mp4
│   │   │   ├── [animegrimoire] Re Stage! Dream Days - 08 [720p][13F42D0A].mp4
│   │   │   ├── [animegrimoire] Re Stage! Dream Days - 09 [720p][200ACFEA].mp4
│   │   │   ├── [animegrimoire] Re Stage! Dream Days - 10 [720p][84DE9D24].mp4
│   │   │   ├── [animegrimoire] Re Stage! Dream Days - 11 [720p][7677214A].mp4
│   │   │   └── [animegrimoire] Re Stage! Dream Days - 12 [720p][57A7000B].mp4
│   │   ├── Sounan desu
│   │   │   ├── [animegrimoire] Sounan desu ka - 01 [BD720p][EBA2CFCC].mp4
│   │   │   ├── [animegrimoire] Sounan desu ka - 02 [BD720p][017F4072].mp4
│   │   │   ├── [animegrimoire] Sounan desu ka - 03 [BD720p][D4FD8D41].mp4
│   │   │   ├── [animegrimoire] Sounan desu ka - 04 [BD720p][6CA2708D].mp4
│   │   │   ├── [animegrimoire] Sounan desu ka - 05 [BD720p][7B4F34F8].mp4
│   │   │   ├── [animegrimoire] Sounan desu ka - 06 [BD720p][EEEFBEC9].mp4
│   │   │   ├── [animegrimoire] Sounan desu ka - 07 [BD720p][478F1420].mp4
│   │   │   ├── [animegrimoire] Sounan desu ka - 08 [BD720p][3B3C7FC8].mp4
│   │   │   ├── [animegrimoire] Sounan desu ka - 09 [BD720p][9CB08568].mp4
│   │   │   ├── [animegrimoire] Sounan desu ka - 10 [BD720p][75FDAEB5].mp4
│   │   │   ├── [animegrimoire] Sounan desu ka - 11 [BD720p][52AAB76B].mp4
│   │   │   └── [animegrimoire] Sounan desu ka - 12 [BD720p][D8E318C9].mp4
│   │   ├── UchiMusume
│   │   │   ├── [animegrimoire] UchiMusume - 01 [720p][27C99CDF].mp4
│   │   │   ├── [animegrimoire] UchiMusume - 02 [720p][A40F2355].mp4
│   │   │   ├── [animegrimoire] UchiMusume - 03 [720p][BB44F9A2].mp4
│   │   │   ├── [animegrimoire] UchiMusume - 04 [720p][D93E07D5].mp4
│   │   │   ├── [animegrimoire] UchiMusume - 05 [720p][75717689].mp4
│   │   │   ├── [animegrimoire] UchiMusume - 06 [720p][82C49892].mp4
│   │   │   ├── [animegrimoire] UchiMusume - 07 [720p][B9DA64D3].mp4
│   │   │   ├── [animegrimoire] UchiMusume - 08 [720p][8E446500].mp4
│   │   │   ├── [animegrimoire] UchiMusume - 09 [720p][6902F754].mp4
│   │   │   ├── [animegrimoire] UchiMusume - 10 [720p][BE296604].mp4
│   │   │   ├── [animegrimoire] UchiMusume - 11 [720p][700F45ED].mp4
│   │   │   └── [animegrimoire] UchiMusume - 12 [720p][8E9C4A72].mp4
│   │   └── Weathering With You
│   │       └── [animegrimoire] Weathering With You - 00 [BD720p][F9637011].mp4
│   └── Winter
│       ├── Egao No Daika
│       │   ├── [animegrimoire] Egao No Daika - 01 [BD720p][F1872E92].mp4
│       │   ├── [animegrimoire] Egao No Daika - 02 [BD720p][56BB72C4].mp4
│       │   ├── [animegrimoire] Egao No Daika - 03 [BD720p][B97FCCE2].mp4
│       │   ├── [animegrimoire] Egao No Daika - 04 [BD720p][3426EC88].mp4
│       │   ├── [animegrimoire] Egao No Daika - 05 [BD720p][8293B78F].mp4
│       │   ├── [animegrimoire] Egao No Daika - 06 [BD720p][765B2B97].mp4
│       │   ├── [animegrimoire] Egao No Daika - 07 [BD720p][BBA2129D].mp4
│       │   ├── [animegrimoire] Egao No Daika - 08 [BD720p][1FCE3974].mp4
│       │   ├── [animegrimoire] Egao No Daika - 09 [BD720p][6B299237].mp4
│       │   ├── [animegrimoire] Egao No Daika - 10 [BD720p][0D2A5A2A].mp4
│       │   ├── [animegrimoire] Egao No Daika - 11 [BD720p][3A351419].mp4
│       │   └── [animegrimoire] Egao No Daika - 12 [BD720p][1725E582].mp4
│       ├── Endro
│       │   ├── [animegrimoire] Endro - 01 [720p][E54A66B9].mp4
│       │   ├── [animegrimoire] Endro - 02 [720p][86B28534].mp4
│       │   ├── [animegrimoire] Endro - 03 [720p][05B2775D].mp4
│       │   ├── [animegrimoire] Endro - 04 [720p][302371FF].mp4
│       │   ├── [animegrimoire] Endro - 05 [720p][08FDD750].mp4
│       │   ├── [animegrimoire] Endro - 06 [720p][7B5079A1].mp4
│       │   ├── [animegrimoire] Endro - 07 [720p][A62BA388].mp4
│       │   ├── [animegrimoire] Endro - 08 [720p][61434F2A].mp4
│       │   ├── [animegrimoire] Endro - 09 [720p][385AEEA2].mp4
│       │   ├── [animegrimoire] Endro - 10 [720p][63E016B3].mp4
│       │   ├── [animegrimoire] Endro - 11 [720p][85D98423].mp4
│       │   └── [animegrimoire] Endro - 12 [720p][68506DBE].mp4
│       ├── Fate stay night
│       │   └── [animegrimoire] Fate stay night Heaven's Feel II. Lost Butterfly [BD720p][F320D5F9].mp4
│       ├── Gekijouban Dungeon ni Deai o Motomeru no wa Machigatte Iru Darouka
│       │   └── [animegrimoire] Gekijouban Dungeon ni Deai o Motomeru no wa Machigatte Iru Darouka - Orion no Ya [720p][CF821DCF].mp4
│       ├── Girly Air Force
│       │   ├── [animegrimoire] Girly Air Force - 01 [BD720p][93E4D123].mp4
│       │   ├── [animegrimoire] Girly Air Force - 02 [BD720p][60AD7866].mp4
│       │   ├── [animegrimoire] Girly Air Force - 03 [BD720p][6C39C6F1].mp4
│       │   ├── [animegrimoire] Girly Air Force - 04 [BD720p][19490F7D].mp4
│       │   ├── [animegrimoire] Girly Air Force - 05 [BD720p][C55E95FE].mp4
│       │   ├── [animegrimoire] Girly Air Force - 06 [BD720p][A94C2D9A].mp4
│       │   ├── [animegrimoire] Girly Air Force - 07 [BD720p][23636AAD].mp4
│       │   ├── [animegrimoire] Girly Air Force - 08 [BD720p][DC05050F].mp4
│       │   ├── [animegrimoire] Girly Air Force - 09 [BD720p][3D2AA619].mp4
│       │   ├── [animegrimoire] Girly Air Force - 10 [BD720p][5D222F47].mp4
│       │   ├── [animegrimoire] Girly Air Force - 11 [BD720p][6A5E0E0D].mp4
│       │   └── [animegrimoire] Girly Air Force - 12 [BD720p][7A4E85F8].mp4
│       ├── Kaguyasama Love
│       │   ├── [animegrimoire] Kaguya-sama Love Is War - 01 [BD720p][A5086BF5].mp4
│       │   ├── [animegrimoire] Kaguya-sama Love Is War - 02 [BD720p][1E53D1B5].mp4
│       │   ├── [animegrimoire] Kaguya-sama Love Is War - 03 [BD720p][8599EF38].mp4
│       │   ├── [animegrimoire] Kaguya-sama Love Is War - 04 [BD720p][725D39B8].mp4
│       │   ├── [animegrimoire] Kaguya-sama Love Is War - 05 [BD720p][ACD7A744].mp4
│       │   ├── [animegrimoire] Kaguya-sama Love Is War - 06 [BD720p][7EDDBFE1].mp4
│       │   ├── [animegrimoire] Kaguya-sama Love Is War - 07 [BD720p][6826C97A].mp4
│       │   ├── [animegrimoire] Kaguya-sama Love Is War - 08 [BD720p][4E7D848E].mp4
│       │   ├── [animegrimoire] Kaguya-sama Love Is War - 09 [BD720p][C2A5A005].mp4
│       │   ├── [animegrimoire] Kaguya-sama Love Is War - 10 [BD720p][18C9203F].mp4
│       │   ├── [animegrimoire] Kaguya-sama Love Is War - 11 [BD720p][29B0FD64].mp4
│       │   └── [animegrimoire] Kaguya-sama Love Is War - 12 [BD720p][82BC92B1].mp4
│       ├── Kemurikusa
│       │   ├── [animegrimoire] Kemurikusa - 01 [BD720p][FB1DB90A].mp4
│       │   ├── [animegrimoire] Kemurikusa - 02 [BD720p][3E001E7A].mp4
│       │   ├── [animegrimoire] Kemurikusa - 03 [BD720p][99CD7A6F].mp4
│       │   ├── [animegrimoire] Kemurikusa - 04 [BD720p][07C5705C].mp4
│       │   ├── [animegrimoire] Kemurikusa - 05 [BD720p][B432AC68].mp4
│       │   ├── [animegrimoire] Kemurikusa - 06 [BD720p][BFF5968D].mp4
│       │   ├── [animegrimoire] Kemurikusa - 07 [BD720p][E5E55433].mp4
│       │   ├── [animegrimoire] Kemurikusa - 08 [BD720p][FA4C809F].mp4
│       │   ├── [animegrimoire] Kemurikusa - 09 [BD720p][D4D51918].mp4
│       │   ├── [animegrimoire] Kemurikusa - 10 [BD720p][E98588A7].mp4
│       │   ├── [animegrimoire] Kemurikusa - 11 [BD720p][CCC89549].mp4
│       │   └── [animegrimoire] Kemurikusa - 12 [BD720p][631E1ACC].mp4
│       ├── Manaria Friends
│       │   ├── [animegrimoire] Manaria Friends - 01 [BD720p][1E20A208].mp4
│       │   ├── [animegrimoire] Manaria Friends - 02 [BD720p][EABBF49F].mp4
│       │   ├── [animegrimoire] Manaria Friends - 03 [BD720p][0755C17D].mp4
│       │   ├── [animegrimoire] Manaria Friends - 04 [BD720p][8A64DAA8].mp4
│       │   ├── [animegrimoire] Manaria Friends - 05 [BD720p][F71CDBC8].mp4
│       │   ├── [animegrimoire] Manaria Friends - 06 [BD720p][911F33D2].mp4
│       │   ├── [animegrimoire] Manaria Friends - 07 [BD720p][5B87FA5C].mp4
│       │   ├── [animegrimoire] Manaria Friends - 08 [BD720p][35A255F3].mp4
│       │   ├── [animegrimoire] Manaria Friends - 09 [BD720p][4742269E].mp4
│       │   └── [animegrimoire] Manaria Friends - 10 [BD720p][CC754A0B].mp4
│       ├── Movies
│       │   └── [animegrimoire] Youjo Senki Movie - 00 [720p][30BBA731].mp4
│       ├── My Roomate Is A Cat
│       │   ├── [animegrimoire] My Roomate Is A Cat - 01 [BD720p][B0FE14E9].mp4
│       │   ├── [animegrimoire] My Roomate Is A Cat - 02 [BD720p][9EE7199D].mp4
│       │   ├── [animegrimoire] My Roomate Is A Cat - 03 [BD720p][69D100B6].mp4
│       │   ├── [animegrimoire] My Roomate Is A Cat - 04 [BD720p][3FB2D3B2].mp4
│       │   ├── [animegrimoire] My Roomate Is A Cat - 05 [BD720p][BA707A67].mp4
│       │   ├── [animegrimoire] My Roomate Is A Cat - 06 [BD720p][6EEB1FBB].mp4
│       │   ├── [animegrimoire] My Roomate Is A Cat - 07 [BD720p][D17E6E61].mp4
│       │   ├── [animegrimoire] My Roomate Is A Cat - 08 [BD720p][C6465C7A].mp4
│       │   ├── [animegrimoire] My Roomate Is A Cat - 09 [BD720p][8D0CF0EF].mp4
│       │   ├── [animegrimoire] My Roomate Is A Cat - 10 [BD720p][E588383B].mp4
│       │   ├── [animegrimoire] My Roomate Is A Cat - 11 [BD720p][75ED02EE].mp4
│       │   └── [animegrimoire] My Roomate Is A Cat - 12 [BD720p][44DD4E04].mp4
│       ├── Pastel Memories
│       │   ├── [animegrimoire] Pastel Memories - 01 [720p][6E158C8B].mp4
│       │   ├── [animegrimoire] Pastel Memories - 02 [720p][4BD6D09E].mp4
│       │   ├── [animegrimoire] Pastel Memories - 03 [720p][AE0B508A].mp4
│       │   ├── [animegrimoire] Pastel Memories - 04 [720p][F253DED5].mp4
│       │   ├── [animegrimoire] Pastel Memories - 05 [720p][09D60998].mp4
│       │   ├── [animegrimoire] Pastel Memories - 06 [720p][F903E941].mp4
│       │   ├── [animegrimoire] Pastel Memories - 07 [720p][DAF281CE].mp4
│       │   ├── [animegrimoire] Pastel Memories - 08 [720p][751668B0].mp4
│       │   ├── [animegrimoire] Pastel Memories - 09 [720p][7A74788E].mp4
│       │   ├── [animegrimoire] Pastel Memories - 10 [720p][A9CD3802].mp4
│       │   ├── [animegrimoire] Pastel Memories - 11 [720p][427ECFF1].mp4
│       │   └── [animegrimoire] Pastel Memories - 12 [720p][F205999A].mp4
│       ├── Tate no Yuusha no Nariagari
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 01 [BD720p][DFF96481].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 02 [BD720p][8E4E7575].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 03 [BD720p][3CF15773].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 04 [BD720p][7EBC4933].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 05 [BD720p][DDB5D2E3].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 06 [BD720p][196DFB54].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 07 [BD720p][FACBE8F5].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 08 [BD720p][AED119AA].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 09 [BD720p][51F7BAE5].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 10 [BD720p][F62BD983].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 11 [BD720p][AB2F9475].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 12 [BD720p][A4BCD52E].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 13 [BD720p][25F93437].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 14 [BD720p][5D969846].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 15 [BD720p][B97FE6DC].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 16 [BD720p][6510C03F].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 17 [BD720p][DBDB5289].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 18 [BD720p][CEE36E9E].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 19 [BD720p][A985D113].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 20 [BD720p][5794BCEB].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 21 [BD720p][3AD531F9].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 22 [BD720p][92591E62].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 23 [BD720p][AF5D1585].mp4
│       │   ├── [animegrimoire] Tate no Yuusha no Nariagari - 24 [BD720p][60CCDC23].mp4
│       │   └── [animegrimoire] Tate no Yuusha no Nariagari - 25 [BD720p][78E6AC1A].mp4
│       ├── Watashi ni Tenshi ga Maiorita!
│       │   ├── [animegrimoire] Watashi ni Tenshi ga Maiorita! - 01 [720p][FFD95A64].mp4
│       │   ├── [animegrimoire] Watashi ni Tenshi ga Maiorita! - 02 [720p][5C801399].mp4
│       │   ├── [animegrimoire] Watashi ni Tenshi ga Maiorita! - 03 [720p][43F49C26].mp4
│       │   ├── [animegrimoire] Watashi ni Tenshi ga Maiorita! - 04 [720p][430B57D1].mp4
│       │   ├── [animegrimoire] Watashi ni Tenshi ga Maiorita! - 05 [720p][BDB43E6F].mp4
│       │   ├── [animegrimoire] Watashi ni Tenshi ga Maiorita! - 06 [720p][56F8BEFC].mp4
│       │   ├── [animegrimoire] Watashi ni Tenshi ga Maiorita! - 07 [720p][FF1FF411].mp4
│       │   ├── [animegrimoire] Watashi ni Tenshi ga Maiorita! - 08 [720p][51CA6505].mp4
│       │   ├── [animegrimoire] Watashi ni Tenshi ga Maiorita! - 09 [720p][738816CB].mp4
│       │   ├── [animegrimoire] Watashi ni Tenshi ga Maiorita! - 10 [720p][D7C35F26].mp4
│       │   ├── [animegrimoire] Watashi ni Tenshi ga Maiorita! - 11 [720p][63C43BB3].mp4
│       │   ├── [animegrimoire] Watashi ni Tenshi ga Maiorita! - 12 [720p][C88C5F83].mp4
│       │   └── [animegrimoire] Watashi ni Tenshi ga Maiorita! - 13 [720p][6056B00C].mp4
│       └── Yuru Camp
│           └── [animegrimoire] Yuru Camp - 00 (OVA) [BD720p][FFC8E9CA].mp4
├── 2020
│   ├── Fall
│   │   ├── Adachi to Shimamura
│   │   │   ├── [animegrimoire] Adachi to Shimamura - 01 [720p][D907AF79].mp4
│   │   │   ├── [animegrimoire] Adachi to Shimamura - 02 [720p][250A7EBA].mp4
│   │   │   ├── [animegrimoire] Adachi to Shimamura - 03 [720p][9709E162].mp4
│   │   │   ├── [animegrimoire] Adachi to Shimamura - 04 [720p][65359E55].mp4
│   │   │   ├── [animegrimoire] Adachi to Shimamura - 05 [720p][D88E04E1].mp4
│   │   │   ├── [animegrimoire] Adachi to Shimamura - 06 [720p][9FD5EA7D].mp4
│   │   │   ├── [animegrimoire] Adachi to Shimamura - 07 [720p][C14497BB].mp4
│   │   │   ├── [animegrimoire] Adachi to Shimamura - 08 [720p][4384A7F8].mp4
│   │   │   ├── [animegrimoire] Adachi to Shimamura - 09 [720p][EBFBC9E7].mp4
│   │   │   ├── [animegrimoire] Adachi to Shimamura - 10 [720p][3CA0F906].mp4
│   │   │   ├── [animegrimoire] Adachi to Shimamura - 11 [720p][612A6FE2].mp4
│   │   │   └── [animegrimoire] Adachi to Shimamura - 12 END [720p][E7AB93EB].mp4
│   │   ├── Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka III
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka III - 01 [720p][E1A92AC9].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka III - 02 [720p][C3CEE706].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka III - 03 [720p][66A99C8C].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka III - 04 [720p][E54EAEBC].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka III - 05 [720p][31C4527A].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka III - 06 [720p][2348B6A4].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka III - 07 [720p][BDD40121].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka III - 08 [720p][CA9E0881].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka III - 09 [720p][4710F5D5].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka III - 10 [720p][3EC6EF54].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka III - 11 [720p][3575CC33].mp4
│   │   │   └── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka III - 12 END [720p][C052F48E].mp4
│   │   ├── Gochuumon wa Usagi Desuka Bloom
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi Desuka Bloom - 01 [720p][E2792967].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi Desuka Bloom - 02 [720p][C0C2C510].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi Desuka Bloom - 03 [720p][DAC30AA3].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi Desuka Bloom - 04 [720p][43BAEA4B].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi Desuka Bloom - 05 [720p][D046BFC1].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi Desuka Bloom - 06 [720p][769E1E9E].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi Desuka Bloom - 07 [720p][705F8078].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi Desuka Bloom - 08 [720p][9223B633].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi Desuka Bloom - 09 [720p][A85DECEB].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi Desuka Bloom - 10 [720p][4EC41169].mp4
│   │   │   ├── [animegrimoire] Gochuumon wa Usagi Desuka Bloom - 11 [720p][8DCB0E53].mp4
│   │   │   └── [animegrimoire] Gochuumon wa Usagi Desuka Bloom - 12 END [720p][EF8CBFFA].mp4
│   │   ├── Golden Kamuy 3rd Season
│   │   │   ├── [animegrimoire] Golden Kamuy 3rd Season - 01 [720p][24AC893C].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy 3rd Season - 02 [720p][21A694F6].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy 3rd Season - 03 [720p][1B799157].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy 3rd Season - 04 [720p][58355C89].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy 3rd Season - 05 [720p][F7C62ABE].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy 3rd Season - 06 [720p][57666C92].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy 3rd Season - 07 [720p][63DD4B59].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy 3rd Season - 08 [720p][AC03F8AA].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy 3rd Season - 09 [720p][1397D3F2].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy 3rd Season - 10 [720p][4AFD66DF].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy 3rd Season - 11 [720p][76452532].mp4
│   │   │   └── [animegrimoire] Golden Kamuy 3rd Season - 12 END [720p][168BB77D].mp4
│   │   ├── Jujutsu Kaisen
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 01 [720p][39F9C3FD].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 02 [720p][AF3BA0F4].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 03 [720p][77013FDE].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 04 [720p][30BFEBE8].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 05 [720p][4DB6500D].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 06 [720p][BBF4CDFA].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 07 [720p][7579ACDF].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 08 [720p][A1C535D5].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 09 [720p][F83FC1FD].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 10 [720p][E3C34DD8].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 11 [720p][4E7DF5AC].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 12 [720p][8CAF916F].mp4
│   │   │   └── [animegrimoire] Jujutsu Kaisen - 13 [720p][3306C185].mp4
│   │   ├── Kami-tachi ni Hirowareta Otoko
│   │   │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko - 01 [720p][1657D517].mp4
│   │   │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko - 02 [720p][E90591F8].mp4
│   │   │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko - 03 [v0][720p][7932C0AC].mp4
│   │   │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko - 04 [v0][720p][F5736C9D].mp4
│   │   │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko - 05 [v0][720p][C1A455C0].mp4
│   │   │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko - 06 [v0][720p][BDA25E86].mp4
│   │   │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko - 07 [v0][720p][D6EEEC03].mp4
│   │   │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko - 08 [v0][720p][9DB14804].mp4
│   │   │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko - 09 [720p][154EFA71].mp4
│   │   │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko - 10 [720p][A7435E8D].mp4
│   │   │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko - 11 [720p][700E3BDF].mp4
│   │   │   └── [animegrimoire] Kami-tachi ni Hirowareta Otoko - 12 END [720p][FE3AE7C4].mp4
│   │   ├── Kuma Kuma Kuma Bear
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear - 01 [720p][23584AE4].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear - 02 [v0][720p][116EE66A].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear - 03 [v0][720p][F36CFABD].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear - 04 [v0][720p][72ECC873].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear - 04v2 [720p][F6F603A9].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear - 05 [v0][720p][C83617BB].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear - 06 [v0][720p][662D896F].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear - 07 [v0][720p][792EF05C].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear - 08 [720p][0A65BC9A].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear - 09 [720p][C82C665C].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear - 10 [720p][138330B2].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear - 11 [720p][409BF6B7].mp4
│   │   │   └── [animegrimoire] Kuma Kuma Kuma Bear - 12 END [720p][B16E7717].mp4
│   │   ├── Maesetsu! Opening Act
│   │   │   ├── [animegrimoire] Maesetsu! Opening Act - 01 [720p][ED70CB06].mp4
│   │   │   ├── [animegrimoire] Maesetsu! Opening Act - 01 [720p][F2ED01BC].mp4
│   │   │   ├── [animegrimoire] Maesetsu! Opening Act - 02 [720p][D6F448E4].mp4
│   │   │   ├── [animegrimoire] Maesetsu! Opening Act - 02 [v0][720p][A5E89388].mp4
│   │   │   ├── [animegrimoire] Maesetsu! Opening Act - 03 [v0][720p][A244EC55].mp4
│   │   │   ├── [animegrimoire] Maesetsu! Opening Act - 04 [v0][720p][C565AB93].mp4
│   │   │   ├── [animegrimoire] Maesetsu! Opening Act - 05 [v0][720p][1F862787].mp4
│   │   │   ├── [animegrimoire] Maesetsu! Opening Act - 06 [v0][720p][8DAE8998].mp4
│   │   │   ├── [animegrimoire] Maesetsu! Opening Act - 07 [v0][720p][74424F04].mp4
│   │   │   ├── [animegrimoire] Maesetsu! Opening Act - 08 [720p][C2D2127F].mp4
│   │   │   ├── [animegrimoire] Maesetsu! Opening Act - 09 [720p][7736DC0A].mp4
│   │   │   ├── [animegrimoire] Maesetsu! Opening Act - 10 [720p][04244E0C].mp4
│   │   │   ├── [animegrimoire] Maesetsu! Opening Act - 11 [720p][34402DFD].mp4
│   │   │   └── [animegrimoire] Maesetsu! Opening Act - 12 END [720p][D4EBE101].mp4
│   │   ├── Mahouka Koukou no Rettousei
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei - Raihousha Hen - 01 [720p][B940963F].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei - Raihousha Hen - 02 [720p][D9D1F7A1].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei - Raihousha Hen - 03 [v0][720p][57C99AD8].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei - Raihousha Hen - 04 [v0][720p][9166044C].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei - Raihousha Hen - 05 [v0][720p][10E7EF5C].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei - Raihousha Hen - 06 [v0][720p][C8CDF57A].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei - Raihousha Hen - 07 [v0][720p][84E2BAF1].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei - Raihousha Hen - 08 [v0][720p][4888AB1C].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei - Raihousha Hen - 09 [v0][720p][E5D6AA9F].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei - Raihousha Hen - 10 [720p][35FC968C].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei - Raihousha Hen - 11 [720p][CC132058].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei - Raihousha Hen - 12 [720p][72336DE7].mp4
│   │   │   └── [animegrimoire] Mahouka Koukou no Rettousei - Raihousha Hen - 13 END [720p][CE99ADA2].mp4
│   │   ├── Majo no Tabitabi
│   │   │   ├── [animegrimoire] Majo no Tabitabi - 01 [720p][953022E5].mp4
│   │   │   ├── [animegrimoire] Majo no Tabitabi - 02 [v0][720p][0EBF7013].mp4
│   │   │   ├── [animegrimoire] Majo no Tabitabi - 03 [v0][720p][2B088C5A].mp4
│   │   │   ├── [animegrimoire] Majo no Tabitabi - 04 [v0][720p][F8F76FA3].mp4
│   │   │   ├── [animegrimoire] Majo no Tabitabi - 05 [v0][720p][DFE2F7D6].mp4
│   │   │   ├── [animegrimoire] Majo no Tabitabi - 06 [v0][720p][D05D477E].mp4
│   │   │   ├── [animegrimoire] Majo no Tabitabi - 07 [v0][720p][2DA1629A].mp4
│   │   │   ├── [animegrimoire] Majo no Tabitabi - 08 [v0][720p][FF4F32D8].mp4
│   │   │   ├── [animegrimoire] Majo no Tabitabi - 09 [v0][720p][B68E6EFA].mp4
│   │   │   ├── [animegrimoire] Majo no Tabitabi - 10 [720p][26E8C44D].mp4
│   │   │   ├── [animegrimoire] Majo no Tabitabi - 11 [720p][3922D8F4].mp4
│   │   │   └── [animegrimoire] Majo no Tabitabi - 12 END [720p][8E9FFFB1].mp4
│   │   ├── Maoujou de Oyasumi
│   │   │   ├── [animegrimoire] Maoujou de Oyasumi - 01 [720p][33C79D9B].mp4
│   │   │   ├── [animegrimoire] Maoujou de Oyasumi - 02 [v0][720p][CD1E4C4E].mp4
│   │   │   ├── [animegrimoire] Maoujou de Oyasumi - 03 [v0][720p][46451FEA].mp4
│   │   │   ├── [animegrimoire] Maoujou de Oyasumi - 04 [v0][720p][A732F936].mp4
│   │   │   ├── [animegrimoire] Maoujou de Oyasumi - 05 [v0][720p][31AD71AB].mp4
│   │   │   ├── [animegrimoire] Maoujou de Oyasumi - 06 [v0][720p][CA3704F5].mp4
│   │   │   ├── [animegrimoire] Maoujou de Oyasumi - 07 [v0][720p][AAB6574F].mp4
│   │   │   ├── [animegrimoire] Maoujou de Oyasumi - 08 [v0][720p][024E737F].mp4
│   │   │   ├── [animegrimoire] Maoujou de Oyasumi - 09 [720p][A00E3535].mp4
│   │   │   ├── [animegrimoire] Maoujou de Oyasumi - 10 [720p][78DFF763].mp4
│   │   │   ├── [animegrimoire] Maoujou de Oyasumi - 11 [720p][9BE3AB71].mp4
│   │   │   └── [animegrimoire] Maoujou de Oyasumi - 12 END [720p][5D3DFCA4].mp4
│   │   ├── Ochikobore Fruit Tart
│   │   │   ├── [animegrimoire] Ochikobore Fruit Tart - 01 [720p][62652029].mp4
│   │   │   ├── [animegrimoire] Ochikobore Fruit Tart - 02 [720p][06644A7B].mp4
│   │   │   ├── [animegrimoire] Ochikobore Fruit Tart - 03 [v0][720p][3BB0693F].mp4
│   │   │   ├── [animegrimoire] Ochikobore Fruit Tart - 04 [v0][720p][3CCA08C7].mp4
│   │   │   ├── [animegrimoire] Ochikobore Fruit Tart - 05 [v0][720p][26D738A4].mp4
│   │   │   ├── [animegrimoire] Ochikobore Fruit Tart - 06 [v0][720p][B6D87AD6].mp4
│   │   │   ├── [animegrimoire] Ochikobore Fruit Tart - 07 [v0][720p][571A745E].mp4
│   │   │   ├── [animegrimoire] Ochikobore Fruit Tart - 08 [720p][9CFFBB3E].mp4
│   │   │   ├── [animegrimoire] Ochikobore Fruit Tart - 09 [720p][8D0ADB27].mp4
│   │   │   ├── [animegrimoire] Ochikobore Fruit Tart - 10 [720p][10649E55].mp4
│   │   │   ├── [animegrimoire] Ochikobore Fruit Tart - 11 [720p][C33A579D].mp4
│   │   │   └── [animegrimoire] Ochikobore Fruit Tart - 12 END [720p][21B1AA0C].mp4
│   │   ├── One Room
│   │   │   ├── [animegrimoire] One Room - Third Season - 01 [720p][6CB89508].mp4
│   │   │   ├── [animegrimoire] One Room - Third Season - 02 [720p][0CFACC96].mp4
│   │   │   ├── [animegrimoire] One Room - Third Season - 03 [720p][2082BD48].mp4
│   │   │   ├── [animegrimoire] One Room - Third Season - 04 [720p][2E0E8575].mp4
│   │   │   ├── [animegrimoire] One Room - Third Season - 05 [720p][9A56FEC5].mp4
│   │   │   ├── [animegrimoire] One Room - Third Season - 06 [720p][6AA2908B].mp4
│   │   │   ├── [animegrimoire] One Room - Third Season - 07 [720p][52C7CA53].mp4
│   │   │   ├── [animegrimoire] One Room - Third Season - 08 [720p][402F4C60].mp4
│   │   │   ├── [animegrimoire] One Room - Third Season - 09 [720p][BE40B8C4].mp4
│   │   │   ├── [animegrimoire] One Room - Third Season - 10 [720p][372B952D].mp4
│   │   │   ├── [animegrimoire] One Room - Third Season - 11 [720p][16DE03EA].mp4
│   │   │   └── [animegrimoire] One Room - Third Season - 12 END [720p][140D8B2A].mp4
│   │   ├── Rail Romanesque
│   │   │   ├── [animegrimoire] Rail Romanesque - 01 [720p][CBD2BD5D].mp4
│   │   │   ├── [animegrimoire] Rail Romanesque - 02 [720p][8248C6C5].mp4
│   │   │   ├── [animegrimoire] Rail Romanesque - 03 [720p][A41C354F].mp4
│   │   │   ├── [animegrimoire] Rail Romanesque - 04 [720p][3B274B2B].mp4
│   │   │   ├── [animegrimoire] Rail Romanesque - 05 [720p][8320D98A].mp4
│   │   │   ├── [animegrimoire] Rail Romanesque - 06 [720p][128C1F54].mp4
│   │   │   ├── [animegrimoire] Rail Romanesque - 07 [720p][FEEE5719].mp4
│   │   │   ├── [animegrimoire] Rail Romanesque - 08 [720p][39CAC7B9].mp4
│   │   │   ├── [animegrimoire] Rail Romanesque - 09 [720p][B6BF5201].mp4
│   │   │   ├── [animegrimoire] Rail Romanesque - 10 [720p][65280246].mp4
│   │   │   ├── [animegrimoire] Rail Romanesque - 11 [720p][DBCDE39E].mp4
│   │   │   └── [animegrimoire] Rail Romanesque - 12 END [720p][5B849E46].mp4
│   │   ├── Senyoku no Sigrdrifa
│   │   │   ├── [animegrimoire] Senyoku no Sigrdrifa - 01 [720p][B35D9342].mp4
│   │   │   ├── [animegrimoire] Senyoku no Sigrdrifa - 02 [720p][6FC0F000].mp4
│   │   │   ├── [animegrimoire] Senyoku no Sigrdrifa - 03 [v0][720p][C3A2DBEA].mp4
│   │   │   ├── [animegrimoire] Senyoku no Sigrdrifa - 04 [v0][720p][CA365A88].mp4
│   │   │   ├── [animegrimoire] Senyoku no Sigrdrifa - 05v2 [720p][5C811FB9].mp4
│   │   │   ├── [animegrimoire] Senyoku no Sigrdrifa - 06 [v0][720p][7543BD57].mp4
│   │   │   ├── [animegrimoire] Senyoku no Sigrdrifa - 07 [v0][720p][264456C6].mp4
│   │   │   ├── [animegrimoire] Senyoku no Sigrdrifa - 08 [v0][720p][F47EAB24].mp4
│   │   │   ├── [animegrimoire] Senyoku no Sigrdrifa - 09 [720p][471023D9].mp4
│   │   │   ├── [animegrimoire] Senyoku no Sigrdrifa - 10 [720p][A4487999].mp4
│   │   │   ├── [animegrimoire] Senyoku no Sigrdrifa - 11 [720p][2F3EA8AB].mp4
│   │   │   └── [animegrimoire] Senyoku no Sigrdrifa - 12 END [720p][78426BB2].mp4
│   │   ├── Shingeki no Kyojin
│   │   │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season - 01 [720p][08482F37].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season - 02 [720p][76612D44].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season - 03 [720p][4B50A3FC].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season - 04 [720p][0A197080].mp4
│   │   │   └── [animegrimoire] Shingeki no Kyojin - The Final Season - 05 [720p][EAAF0424].mp4
│   │   ├── Strike Witches
│   │   │   ├── [animegrimoire] Strike Witches - Road to Berlin - 01 [720p][E9078F64].mp4
│   │   │   ├── [animegrimoire] Strike Witches - Road to Berlin - 02 [720p][AAD70BDE].mp4
│   │   │   ├── [animegrimoire] Strike Witches - Road to Berlin - 03 [720p][CC82F91F].mp4
│   │   │   ├── [animegrimoire] Strike Witches - Road to Berlin - 04 [720p][E483E2F8].mp4
│   │   │   ├── [animegrimoire] Strike Witches - Road to Berlin - 05 [720p][AE62B862].mp4
│   │   │   ├── [animegrimoire] Strike Witches - Road to Berlin - 06 [720p][F4941F0E].mp4
│   │   │   ├── [animegrimoire] Strike Witches - Road to Berlin - 07 [720p][D9B7CC7D].mp4
│   │   │   ├── [animegrimoire] Strike Witches - Road to Berlin - 08 [720p][203EA27D].mp4
│   │   │   ├── [animegrimoire] Strike Witches - Road to Berlin - 09 [720p][D7BD815D].mp4
│   │   │   ├── [animegrimoire] Strike Witches - Road to Berlin - 10 [720p][D70E0476].mp4
│   │   │   ├── [animegrimoire] Strike Witches - Road to Berlin - 11 [720p][6B635763].mp4
│   │   │   └── [animegrimoire] Strike Witches - Road to Berlin - 12 END [720p][B954AC46].mp4
│   │   └── Tonikaku Kawaii
│   │       ├── [animegrimoire] Tonikaku Kawaii - 01 [720p][6186374E].mp4
│   │       ├── [animegrimoire] Tonikaku Kawaii - 02 [720p][13EB20A8].mp4
│   │       ├── [animegrimoire] Tonikaku Kawaii - 03 [720p][E260831D].mp4
│   │       ├── [animegrimoire] Tonikaku Kawaii - 04 [720p][0E44F3C4].mp4
│   │       ├── [animegrimoire] Tonikaku Kawaii - 05 [720p][B401296F].mp4
│   │       ├── [animegrimoire] Tonikaku Kawaii - 06 [720p][FB3685E9].mp4
│   │       ├── [animegrimoire] Tonikaku Kawaii - 07 [720p][FDC46FDC].mp4
│   │       ├── [animegrimoire] Tonikaku Kawaii - 08 [720p][0614EBE8].mp4
│   │       ├── [animegrimoire] Tonikaku Kawaii - 09 [720p][7A413E53].mp4
│   │       ├── [animegrimoire] Tonikaku Kawaii - 10 [720p][7CCE5E82].mp4
│   │       ├── [animegrimoire] Tonikaku Kawaii - 11 [720p][5EB59CCD].mp4
│   │       ├── [animegrimoire] Tonikaku Kawaii - 11 [720p][6DCBC488].mp4
│   │       └── [animegrimoire] Tonikaku Kawaii - 12 END [720p][330861BB].mp4
│   ├── Spring
│   │   ├── Hamefura
│   │   │   ├── [animegrimoire] Hamefura - 01 [720p][2A323F75].mp4
│   │   │   ├── [animegrimoire] Hamefura - 02 [720p][36777B5D].mp4
│   │   │   ├── [animegrimoire] Hamefura - 03 [720p][388A1BAF].mp4
│   │   │   ├── [animegrimoire] Hamefura - 04 [720p][752C9CD1].mp4
│   │   │   ├── [animegrimoire] Hamefura - 05 [720p][2EED269D].mp4
│   │   │   ├── [animegrimoire] Hamefura - 06 [720p][E9B8FF82].mp4
│   │   │   ├── [animegrimoire] Hamefura - 07 [720p][CAD53C2E].mp4
│   │   │   ├── [animegrimoire] Hamefura - 08 [720p][EB1F7713].mp4
│   │   │   ├── [animegrimoire] Hamefura - 09 [720p][C13F5885].mp4
│   │   │   ├── [animegrimoire] Hamefura - 10 [720p][1E52EE67].mp4
│   │   │   ├── [animegrimoire] Hamefura - 11 [720p][E29E478B].mp4
│   │   │   └── [animegrimoire] Hamefura - 12 [720p][26202307].mp4
│   │   ├── Honzuki no
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 15 [720p][AC15B1C1].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 16 [720p][6B505609].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 17 [720p][672A7322].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 18 [720p][4D7B007D].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 19 [720p][386CC56F].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 20 [720p][825C66A3].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 21 [720p][720A8897].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 22 [720p][6984A66E].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 23 [720p][BB92D3B7].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 24 [720p][B5216D40].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 25 [720p][4F7CB8FA].mp4
│   │   │   └── [animegrimoire] Honzuki no Gekokujou - 26 [720p][A83BF749].mp4
│   │   ├── Houkago Teibou
│   │   │   ├── [animegrimoire] Houkago Teibou Nisshi - 01 [720p][3D811E4D].mp4
│   │   │   ├── [animegrimoire] Houkago Teibou Nisshi - 02 [720p][2C4110F8].mp4
│   │   │   └── [animegrimoire] Houkago Teibou Nisshi - 03 [720p][48CEEE5F].mp4
│   │   ├── Jashinchan Dropkick
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick - 01 [720p][40E67FD7].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick - 02 [720p][C91FEB97].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick - 03 [720p][ABFD5E38].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick - 04 [720p][F311345D].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick - 05 [720p][08118E16].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick - 06 [720p][D6881C19].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick - 07 [720p][22FB0738].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick - 08 [720p][D90E5BFD].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick - 09 [720p][C9C0DAFB].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick - 10 [720p][3566D323].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick - 11 [720p][3978A12C].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick - 12 [720p][0145FCBF].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick S2 - 01 [720p][B02791DF].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick S2 - 02 [720p][A7F19725].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick S2 - 03 [720p][AD3B6824].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick S2 - 04 [720p][8AD241CC].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick S2 - 05 [720p][78221AB4].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick S2 - 06 [720p][AECE8794].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick S2 - 07 [720p][D7F25503].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick S2 - 08 [720p][D30A76BC].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick S2 - 09 [720p][2BC3C5EA].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick S2 - 10 [720p][6980E29F].mp4
│   │   │   └── [animegrimoire] Jashin-chan Dropkick S2 - 11 [720p][05125A3B].mp4
│   │   ├── Kabaneri Of
│   │   │   ├── [animegrimoire] Kabaneri Of The Iron Fortress The Movies - 01 [BD720p][B94FA2D4].mp4
│   │   │   ├── [animegrimoire] Kabaneri Of The Iron Fortress The Movies - 02 [BD720p][A4334972].mp4
│   │   │   └── [animegrimoire] Kabaneri Of The Iron Fortress The Movies - 03 [BD720p][20A787F6].mp4
│   │   ├── Kaguyasama wa
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S2 - 01 [720p][EDFAAA1E].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S2 - 02 [720p][F2A3F9C5].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S2 - 03 [720p][1540E5A2].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S2 - 04 [720p][06ED819A].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S2 - 05 [720p][CEFDF0DD].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S2 - 06 [720p][2A017789].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S2 - 07 [720p][14EB33A1].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S2 - 08 [720p][1F2EE9AC].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S2 - 09 [720p][3AD09BE6].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S2 - 10 [720p][C1044E67].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S2 - 11 [720p][D1CC090A].mp4
│   │   │   └── [animegrimoire] Kaguya-sama wa Kokurasetai S2 - 12 [720p][A8163569].mp4
│   │   ├── Kakushigoto
│   │   │   ├── [animegrimoire] Kakushigoto - 01 [720p][48FC907F].mp4
│   │   │   ├── [animegrimoire] Kakushigoto - 02 [720p][4C429E64].mp4
│   │   │   ├── [animegrimoire] Kakushigoto - 03 [720p][BB6D359D].mp4
│   │   │   ├── [animegrimoire] Kakushigoto - 04 [720p][D6FE0244].mp4
│   │   │   ├── [animegrimoire] Kakushigoto - 05 [720p][7F4A1822].mp4
│   │   │   ├── [animegrimoire] Kakushigoto - 06 [720p][3CD0CC78].mp4
│   │   │   ├── [animegrimoire] Kakushigoto - 07 [720p][004FDEF8].mp4
│   │   │   ├── [animegrimoire] Kakushigoto - 08 [720p][8E4DC68F].mp4
│   │   │   ├── [animegrimoire] Kakushigoto - 09 [720p][88412BC9].mp4
│   │   │   ├── [animegrimoire] Kakushigoto - 10 [720p][68C18589].mp4
│   │   │   ├── [animegrimoire] Kakushigoto - 11 [720p][FB7AA4DF].mp4
│   │   │   └── [animegrimoire] Kakushigoto - 12 [720p][7248A459].mp4
│   │   ├── Koisuru Asteroid
│   │   │   ├── [animegrimoire] Koisuru Asteroid - 01 [BD720p][7D52C608].mp4
│   │   │   ├── [animegrimoire] Koisuru Asteroid - 02 [BD720p][C489C83F].mp4
│   │   │   ├── [animegrimoire] Koisuru Asteroid - 03 [BD720p][49A7C18B].mp4
│   │   │   ├── [animegrimoire] Koisuru Asteroid - 04 [BD720p][2CD4138E].mp4
│   │   │   ├── [animegrimoire] Koisuru Asteroid - 05 [BD720p][B589CFCF].mp4
│   │   │   ├── [animegrimoire] Koisuru Asteroid - 06 [BD720p][738346BB].mp4
│   │   │   ├── [animegrimoire] Koisuru Asteroid - 07 [BD720p][B3B52CDE].mp4
│   │   │   ├── [animegrimoire] Koisuru Asteroid - 08 [BD720p][A2A23602].mp4
│   │   │   ├── [animegrimoire] Koisuru Asteroid - 09 [BD720p][12948722].mp4
│   │   │   ├── [animegrimoire] Koisuru Asteroid - 10 [BD720p][9B3C05F8].mp4
│   │   │   ├── [animegrimoire] Koisuru Asteroid - 11 [BD720p][9B9EE2DF].mp4
│   │   │   └── [animegrimoire] Koisuru Asteroid - 12 [BD720p][AC86D5DA].mp4
│   │   ├── Momokuri
│   │   │   ├── [animegrimoire] Momokuri - 01 [BD720p][C245213A].mp4
│   │   │   ├── [animegrimoire] Momokuri - 02 [BD720p][963ECCD8].mp4
│   │   │   ├── [animegrimoire] Momokuri - 03 [BD720p][8D6C0172].mp4
│   │   │   ├── [animegrimoire] Momokuri - 04 [BD720p][C1676790].mp4
│   │   │   ├── [animegrimoire] Momokuri - 05 [BD720p][47B5688D].mp4
│   │   │   ├── [animegrimoire] Momokuri - 06 [BD720p][30125402].mp4
│   │   │   ├── [animegrimoire] Momokuri - 07 [BD720p][68237BA5].mp4
│   │   │   ├── [animegrimoire] Momokuri - 08 [BD720p][94852B46].mp4
│   │   │   ├── [animegrimoire] Momokuri - 09 [BD720p][544C3771].mp4
│   │   │   ├── [animegrimoire] Momokuri - 10 [BD720p][585AE2C5].mp4
│   │   │   ├── [animegrimoire] Momokuri - 11 [BD720p][49467AB3].mp4
│   │   │   ├── [animegrimoire] Momokuri - 12 [BD720p][D44ED0F9].mp4
│   │   │   └── [animegrimoire] Momokuri - 13 [BD720p][177DC054].mp4
│   │   ├── Princess Jellyfish
│   │   │   ├── [animegrimoire] Princess Jellyfish - 01 [BD720p][408E3A65].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish - 02 [BD720p][66BA2160].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish - 03 [BD720p][959E68C3].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish - 04 [BD720p][8877495F].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish - 05 [BD720p][34FE9A17].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish - 06 [BD720p][C2777998].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish - 07 [BD720p][E3EC5D8F].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish - 08 [BD720p][18700632].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish - 09 [BD720p][9CA2A0AB].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish - 10 [BD720p][00EA8232].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish - 11 [BD720p][82AB59E1].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish OVA - 01 [BD720p][434D7104].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish OVA - 02 [BD720p][9351FEEC].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish OVA - 03 [BD720p][1CEE760B].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish OVA - 04 [BD720p][264517AF].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish OVA - 05 [BD720p][98928B4B].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish OVA - 06 [BD720p][20A6D67C].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish OVA - 07 [BD720p][A173CA89].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish OVA - 08 [BD720p][6F5BFF53].mp4
│   │   │   ├── [animegrimoire] Princess Jellyfish OVA - 09 [BD720p][52EDFB91].mp4
│   │   │   └── [animegrimoire] Princess Jellyfish OVA - 10 [BD720p][BEB01843].mp4
│   │   ├── Senryu Girl
│   │   │   ├── [animegrimoire] Senryu Girl - 01 [BD720p][94BABBEB].mp4
│   │   │   ├── [animegrimoire] Senryu Girl - 02 [BD720p][EDCEF040].mp4
│   │   │   ├── [animegrimoire] Senryu Girl - 03 [BD720p][37CA6EA0].mp4
│   │   │   ├── [animegrimoire] Senryu Girl - 04 [BD720p][232BFCC6].mp4
│   │   │   ├── [animegrimoire] Senryu Girl - 05 [BD720p][300AB513].mp4
│   │   │   ├── [animegrimoire] Senryu Girl - 06 [BD720p][93119DD1].mp4
│   │   │   ├── [animegrimoire] Senryu Girl - 07 [BD720p][AAB6D1FC].mp4
│   │   │   ├── [animegrimoire] Senryu Girl - 08 [BD720p][F6855F1C].mp4
│   │   │   ├── [animegrimoire] Senryu Girl - 09 [BD720p][B46FF2FF].mp4
│   │   │   ├── [animegrimoire] Senryu Girl - 10 [BD720p][0A017309].mp4
│   │   │   ├── [animegrimoire] Senryu Girl - 11 [BD720p][B952AAC9].mp4
│   │   │   └── [animegrimoire] Senryu Girl - 12 [BD720p][DB4401A5].mp4
│   │   ├── Tamayomi
│   │   │   ├── [animegrimoire] Tamayomi - 01 [720p][E90CD782].mp4
│   │   │   ├── [animegrimoire] Tamayomi - 02 [720p][5B737991].mp4
│   │   │   ├── [animegrimoire] Tamayomi - 03 [720p][766EF99C].mp4
│   │   │   ├── [animegrimoire] Tamayomi - 04 [720p][2D992BE1].mp4
│   │   │   ├── [animegrimoire] Tamayomi - 05 [720p][6A5EE7FE].mp4
│   │   │   ├── [animegrimoire] Tamayomi - 06 [720p][A3A9BAA0].mp4
│   │   │   ├── [animegrimoire] Tamayomi - 07 [720p][797EEC04].mp4
│   │   │   ├── [animegrimoire] Tamayomi - 08 [720p][6811DBCC].mp4
│   │   │   ├── [animegrimoire] Tamayomi - 09 [720p][9B93158E].mp4
│   │   │   ├── [animegrimoire] Tamayomi - 10 [720p][086B1DAA].mp4
│   │   │   ├── [animegrimoire] Tamayomi - 11 [720p][9F80B6AF].mp4
│   │   │   └── [animegrimoire] Tamayomi - 12 [720p][53D6CF40].mp4
│   │   └── Toaru Kagaku
│   │       ├── [animegrimoire] Toaru Kagaku no Railgun T - 01 [720p][2AC95153].mp4
│   │       ├── [animegrimoire] Toaru Kagaku no Railgun T - 02 [720p][9C39AB93].mp4
│   │       ├── [animegrimoire] Toaru Kagaku no Railgun T - 03 [720p][C86C39AE].mp4
│   │       ├── [animegrimoire] Toaru Kagaku no Railgun T - 04 [720p][FC690B7C].mp4
│   │       ├── [animegrimoire] Toaru Kagaku no Railgun T - 05 [720p][C03A40C3].mp4
│   │       ├── [animegrimoire] Toaru Kagaku no Railgun T - 06 [720p][B4CA2592].mp4
│   │       ├── [animegrimoire] Toaru Kagaku no Railgun T - 07 [720p][A25053AD].mp4
│   │       ├── [animegrimoire] Toaru Kagaku no Railgun T - 08 [720p][6BCA5D94].mp4
│   │       ├── [animegrimoire] Toaru Kagaku no Railgun T - 09 [720p][378A57B7].mp4
│   │       ├── [animegrimoire] Toaru Kagaku no Railgun T - 10 [720p][40D99591].mp4
│   │       ├── [animegrimoire] Toaru Kagaku no Railgun T - 11 [720p][42403538].mp4
│   │       ├── [animegrimoire] Toaru Kagaku no Railgun T - 12 [720p][DC0204CB].mp4
│   │       ├── [animegrimoire] Toaru Kagaku no Railgun T - 13 [720p][7F2876BD].mp4
│   │       ├── [animegrimoire] Toaru Kagaku no Railgun T - 14 [720p][7D0E0B98].mp4
│   │       └── [animegrimoire] Toaru Kagaku no Railgun T - 15 [720p][843712DC].mp4
│   ├── Summer
│   │   ├── Goblin Slayer
│   │   │   └── [animegrimoire] Goblin Slayer - Goblins Crown - 00 [BD720p][2010B232].mp4
│   │   ├── Golden Kamuy
│   │   │   └── [animegrimoire] Golden Kamuy 3rd Season - 01 [720p] [720p][7EC34E43].mp4
│   │   ├── Heya Camp
│   │   │   ├── [animegrimoire] Heya Camp - 00 [BD720p][154C8AD6].mp4
│   │   │   └── [animegrimoire] Heya Camp Special Episode - 00 [BD720p][84EE3595].mp4
│   │   ├── Houkago Teibou
│   │   │   ├── [animegrimoire] Houkago Teibou Nisshi - 04 [720p][D05868F7].mp4
│   │   │   ├── [animegrimoire] Houkago Teibou Nisshi - 05 [720p][9241AB83].mp4
│   │   │   ├── [animegrimoire] Houkago Teibou Nisshi - 06 [720p][1F4CD01B].mp4
│   │   │   ├── [animegrimoire] Houkago Teibou Nisshi - 07 [720p][D40ADF83].mp4
│   │   │   ├── [animegrimoire] Houkago Teibou Nisshi - 08 [720p][6CF26AA0].mp4
│   │   │   ├── [animegrimoire] Houkago Teibou Nisshi - 09 [720p][30EB1A80].mp4
│   │   │   ├── [animegrimoire] Houkago Teibou Nisshi - 10 [720p][A4D9435D].mp4
│   │   │   ├── [animegrimoire] Houkago Teibou Nisshi - 11 [720p][CF8B0430].mp4
│   │   │   └── [animegrimoire] Houkago Teibou Nisshi - 12 [720p][1BBDC35A].mp4
│   │   ├── Jashinchan Dropkick
│   │   │   └── [animegrimoire] Jashin-chan Dropkick S2 - 12 [720p][F5A88B3A].mp4
│   │   ├── Joshikousei No Mudazukai
│   │   │   ├── [animegrimoire] Joshikousei No Mudazukai - 01 [BD720p][3FCD040C].mp4
│   │   │   ├── [animegrimoire] Joshikousei No Mudazukai - 02 [BD720p][D03B455F].mp4
│   │   │   ├── [animegrimoire] Joshikousei No Mudazukai - 03 [BD720p][D3031164].mp4
│   │   │   ├── [animegrimoire] Joshikousei No Mudazukai - 04 [BD720p][0FE010F2].mp4
│   │   │   ├── [animegrimoire] Joshikousei No Mudazukai - 05 [BD720p][F37B2A3D].mp4
│   │   │   ├── [animegrimoire] Joshikousei No Mudazukai - 06 [BD720p][7F81B829].mp4
│   │   │   ├── [animegrimoire] Joshikousei No Mudazukai - 07 [BD720p][6AC1E956].mp4
│   │   │   ├── [animegrimoire] Joshikousei No Mudazukai - 08 [BD720p][005F6630].mp4
│   │   │   ├── [animegrimoire] Joshikousei No Mudazukai - 09 [BD720p][4436EC7A].mp4
│   │   │   ├── [animegrimoire] Joshikousei No Mudazukai - 10 [BD720p][5EC419CD].mp4
│   │   │   ├── [animegrimoire] Joshikousei No Mudazukai - 11 [BD720p][21E1134A].mp4
│   │   │   └── [animegrimoire] Joshikousei No Mudazukai - 12 [BD720p][F6791013].mp4
│   │   ├── Lapis ReLiGHTs
│   │   │   ├── [animegrimoire] Lapis ReLiGHTs - 01 [720p][3527504A].mp4
│   │   │   ├── [animegrimoire] Lapis ReLiGHTs - 02 [720p][7C3A951E].mp4
│   │   │   ├── [animegrimoire] Lapis ReLiGHTs - 03 [720p][05C98E1D].mp4
│   │   │   ├── [animegrimoire] Lapis ReLiGHTs - 04 [720p][DD6B063C].mp4
│   │   │   ├── [animegrimoire] Lapis ReLiGHTs - 05 [720p][082ED8D3].mp4
│   │   │   ├── [animegrimoire] Lapis ReLiGHTs - 06 [720p][AFC8A277].mp4
│   │   │   ├── [animegrimoire] Lapis ReLiGHTs - 07 [720p][4F1DAA8E].mp4
│   │   │   ├── [animegrimoire] Lapis ReLiGHTs - 08 [720p][2440242F].mp4
│   │   │   ├── [animegrimoire] Lapis ReLiGHTs - 09 [720p][EE012ADA].mp4
│   │   │   ├── [animegrimoire] Lapis ReLiGHTs - 10 [720p][EC788CE3].mp4
│   │   │   ├── [animegrimoire] Lapis ReLiGHTs - 11 [720p][F8D7EDCF].mp4
│   │   │   └── [animegrimoire] Lapis ReLiGHTs - 12 [720p][41EDC7F5].mp4
│   │   ├── Mahouka Koukou
│   │   │   └── [animegrimoire] Mahouka Koukou no Rettousei - Raihousha Hen - 01 [720p] [720p][9567FA98].mp4
│   │   ├── Maoujou de
│   │   │   └── [animegrimoire] Maoujou de Oyasumi - 01 [720p] [720p][4C145530].mp4
│   │   ├── Monster Musume
│   │   │   ├── [animegrimoire] Monster Musume no Oisha-san - 01 [720p][CA008A59].mp4
│   │   │   ├── [animegrimoire] Monster Musume no Oisha-san - 02 [720p][3FF44367].mp4
│   │   │   ├── [animegrimoire] Monster Musume no Oisha-san - 03 [720p][F04C3D94].mp4
│   │   │   ├── [animegrimoire] Monster Musume no Oisha-san - 04 [720p][380BC984].mp4
│   │   │   ├── [animegrimoire] Monster Musume no Oisha-san - 05 [720p][257E6DEC].mp4
│   │   │   ├── [animegrimoire] Monster Musume no Oisha-san - 06 [720p][CD17587E].mp4
│   │   │   ├── [animegrimoire] Monster Musume no Oisha-san - 07 [720p][9E43690C].mp4
│   │   │   ├── [animegrimoire] Monster Musume no Oisha-san - 08 [720p][145426D2].mp4
│   │   │   ├── [animegrimoire] Monster Musume no Oisha-san - 09 [720p][03398B0B].mp4
│   │   │   ├── [animegrimoire] Monster Musume no Oisha-san - 10 [720p][4EE63203].mp4
│   │   │   ├── [animegrimoire] Monster Musume no Oisha-san - 11 [720p][3FD5EB2A].mp4
│   │   │   └── [animegrimoire] Monster Musume no Oisha-san - 12 [720p][8EF8549A].mp4
│   │   ├── Re Zero
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 26 [720p][1F3AA710].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 27 [720p][54D9FA60].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 28 [720p][2DFB181A].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 29 [720p][CA86AEE1].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 30 [720p][4B75876A].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 31 [720p][B3812611].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 32 [720p][3F129529].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 33 [720p][025245C4].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 34 [720p][F51452D8].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 35 [720p][D6E38686].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 36 [720p][8F1ED4F4].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 37 [720p][93FA9C6B].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 38 [720p][48EF5956].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - Hyouketsu no Kizuna - 00 [720p][104C6DDD].mp4
│   │   │   └── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - Memory Snow - 00 [720p][20B4CB2F].mp4
│   │   ├── Toaru Kagaku
│   │   │   ├── [animegrimoire] Toaru Kagaku no Railgun T - 16 [720p][DDADA080].mp4
│   │   │   ├── [animegrimoire] Toaru Kagaku no Railgun T - 17 [720p][4E28AEA7].mp4
│   │   │   ├── [animegrimoire] Toaru Kagaku no Railgun T - 18 [720p][D99B0A47].mp4
│   │   │   ├── [animegrimoire] Toaru Kagaku no Railgun T - 19 [720p][1810533E].mp4
│   │   │   ├── [animegrimoire] Toaru Kagaku no Railgun T - 20 [720p][C37D8735].mp4
│   │   │   ├── [animegrimoire] Toaru Kagaku no Railgun T - 21 [720p][681D2EB6].mp4
│   │   │   ├── [animegrimoire] Toaru Kagaku no Railgun T - 22 [720p][451FDB10].mp4
│   │   │   ├── [animegrimoire] Toaru Kagaku no Railgun T - 23 [720p][4FC2456F].mp4
│   │   │   ├── [animegrimoire] Toaru Kagaku no Railgun T - 24 [720p][E175EE9C].mp4
│   │   │   └── [animegrimoire] Toaru Kagaku no Railgun T - 25 [720p][87355743].mp4
│   │   └── Yahari Ore
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Come wa Machigatteiru Kan - 01 [720p][E710B8D3].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Come wa Machigatteiru Kan - 02 [720p][A0305D72].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Come wa Machigatteiru Kan - 03 [720p][33992E5E].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Come wa Machigatteiru Kan - 04 [720p][D708C96F].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Come wa Machigatteiru Kan - 05 [720p][16470394].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Come wa Machigatteiru Kan - 06 [720p][E9664A9B].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Come wa Machigatteiru Kan - 07 [720p][D8AA8F13].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Come wa Machigatteiru Kan - 08 [720p][B84B3D9D].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Come wa Machigatteiru Kan - 09 [720p][D9E355AE].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Come wa Machigatteiru Kan - 10 [720p][0006AB56].mp4
│   │       ├── [animegrimoire] Yahari Ore no Seishun Love Come wa Machigatteiru Kan - 11 [720p][08A3D05E].mp4
│   │       └── [animegrimoire] Yahari Ore no Seishun Love Come wa Machigatteiru Kan - 12 [720p][145386C0].mp4
│   └── Winter
│       ├── Azur Lane
│       │   ├── [animegrimoire] Azur Lane - 01 [720p][1B07171C].mp4
│       │   ├── [animegrimoire] Azur Lane - 02 [720p][E2AB1DCB].mp4
│       │   ├── [animegrimoire] Azur Lane - 03 [720p][42918AB8].mp4
│       │   ├── [animegrimoire] Azur Lane - 04 [720p][2524F0FA].mp4
│       │   ├── [animegrimoire] Azur Lane - 05 [720p][2A16D97C].mp4
│       │   ├── [animegrimoire] Azur Lane - 06 [720p][FC7C5268].mp4
│       │   ├── [animegrimoire] Azur Lane - 07 [720p][7DD060A0].mp4
│       │   ├── [animegrimoire] Azur Lane - 08 [720p][CC8A5999].mp4
│       │   ├── [animegrimoire] Azur Lane - 09 [720p][8833EAF3].mp4
│       │   ├── [animegrimoire] Azur Lane - 10 [720p][4F8DBEFE].mp4
│       │   ├── [animegrimoire] Azur Lane - 11 [720p][9CAEF710].mp4
│       │   └── [animegrimoire] Azur Lane - 12 [720p][8BB467B4].mp4
│       ├── FGO - Absolute Demonic Front
│       │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 13 [720p][BCEB5BE8].mp4
│       │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 14 [720p][47BC2C97].mp4
│       │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 15 [720p][B1EFEB66].mp4
│       │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 16 [720p][D47CFC27].mp4
│       │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 17 [720p][4C496A20].mp4
│       │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 18 [720p][27C08D39].mp4
│       │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 19 [720p][FD60A065].mp4
│       │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 20 [720p][D422D970].mp4
│       │   └── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 21 [720p][81861551].mp4
│       ├── Heya Camp
│       │   ├── [animegrimoire] Heya Camp - 01 [720p][6C20C09A].mp4
│       │   ├── [animegrimoire] Heya Camp - 02 [720p][C4DF9FDB].mp4
│       │   ├── [animegrimoire] Heya Camp - 03 [720p][D40CC9DC].mp4
│       │   ├── [animegrimoire] Heya Camp - 04 [720p][2D33C9AD].mp4
│       │   ├── [animegrimoire] Heya Camp - 05 [720p][10E90919].mp4
│       │   ├── [animegrimoire] Heya Camp - 06 [720p][A4026162].mp4
│       │   ├── [animegrimoire] Heya Camp - 07 [720p][8FF0C833].mp4
│       │   ├── [animegrimoire] Heya Camp - 08 [720p][747C8145].mp4
│       │   ├── [animegrimoire] Heya Camp - 09 [720p][5FE616CF].mp4
│       │   ├── [animegrimoire] Heya Camp - 10 [720p][4CCACA77].mp4
│       │   ├── [animegrimoire] Heya Camp - 11 [720p][09F37A3F].mp4
│       │   └── [animegrimoire] Heya Camp - 12 [720p][FE61DF2A].mp4
│       ├── ID INVADED
│       │   ├── [animegrimoire] ID INVADED - 01 [720p][E9A8035C].mp4
│       │   ├── [animegrimoire] ID INVADED - 02 [720p][A298F0F3].mp4
│       │   ├── [animegrimoire] ID INVADED - 03 [720p][79D769F1].mp4
│       │   ├── [animegrimoire] ID INVADED - 04 [720p][85A17467].mp4
│       │   ├── [animegrimoire] ID INVADED - 05 [720p][08A008A1].mp4
│       │   ├── [animegrimoire] ID INVADED - 06 [720p][78D06DDC].mp4
│       │   ├── [animegrimoire] ID INVADED - 07 [720p][8DBF4A0A].mp4
│       │   ├── [animegrimoire] ID INVADED - 08 [720p][335107A8].mp4
│       │   ├── [animegrimoire] ID INVADED - 09 [720p][3E50C4B3].mp4
│       │   ├── [animegrimoire] ID INVADED - 10 [720p][BA3608A9].mp4
│       │   ├── [animegrimoire] ID INVADED - 11 [720p][0380871E].mp4
│       │   ├── [animegrimoire] ID INVADED - 12 [720p][88703489].mp4
│       │   └── [animegrimoire] ID INVADED - 13 [720p][19F97863].mp4
│       ├── Koisuru Asteroid
│       │   ├── [animegrimoire] Koisuru Asteroid - 01 [720p][76407C67].mp4
│       │   ├── [animegrimoire] Koisuru Asteroid - 02 [720p][99FE7A99].mp4
│       │   ├── [animegrimoire] Koisuru Asteroid - 03 [720p][0802151F].mp4
│       │   ├── [animegrimoire] Koisuru Asteroid - 04 [720p][CC3BA0C7].mp4
│       │   ├── [animegrimoire] Koisuru Asteroid - 05 [720p][AF988A0F].mp4
│       │   ├── [animegrimoire] Koisuru Asteroid - 06 [720p][0977F124].mp4
│       │   ├── [animegrimoire] Koisuru Asteroid - 07 [720p][5D5817E2].mp4
│       │   ├── [animegrimoire] Koisuru Asteroid - 08 [720p][2BBBE118].mp4
│       │   ├── [animegrimoire] Koisuru Asteroid - 09 [720p][1D11DC4A].mp4
│       │   ├── [animegrimoire] Koisuru Asteroid - 10 [720p][619916AA].mp4
│       │   ├── [animegrimoire] Koisuru Asteroid - 11 [720p][35080154].mp4
│       │   └── [animegrimoire] Koisuru Asteroid - 12 [720p][7CEB6DBE].mp4
│       ├── Kyokou Suiri
│       │   ├── [animegrimoire] Kyokou Suiri - 01 [720p][E94A2396].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 02 [720p][696137EC].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 03 [720p][0A1262FD].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 04 [720p][125A0FF7].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 05 [720p][8CB3DEE3].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 06 [720p][95696CF5].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 07 [720p][9434CFC1].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 08 [720p][9B01E6EE].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 09 [720p][14429E0A].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 10 [720p][67F50C98].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 11 [720p][764E42F3].mp4
│       │   └── [animegrimoire] Kyokou Suiri - 12 [720p][7EDB4AE3].mp4
│       ├── Murenase! Seton Gakuen
│       │   ├── [animegrimoire] Murenase! Seton Gakuen - 01 [720p][4E649569].mp4
│       │   ├── [animegrimoire] Murenase! Seton Gakuen - 02 [720p][A6C0D0A6].mp4
│       │   ├── [animegrimoire] Murenase! Seton Gakuen - 03 [720p][21EB5930].mp4
│       │   ├── [animegrimoire] Murenase! Seton Gakuen - 04 [720p][8DD4394C].mp4
│       │   ├── [animegrimoire] Murenase! Seton Gakuen - 05 [720p][19F99ADE].mp4
│       │   ├── [animegrimoire] Murenase! Seton Gakuen - 06 [720p][56E142E4].mp4
│       │   ├── [animegrimoire] Murenase! Seton Gakuen - 07 [720p][18F72399].mp4
│       │   ├── [animegrimoire] Murenase! Seton Gakuen - 08 [720p][1C84ED98].mp4
│       │   ├── [animegrimoire] Murenase! Seton Gakuen - 09 [720p][9FED5040].mp4
│       │   ├── [animegrimoire] Murenase! Seton Gakuen - 10 [720p][B918B651].mp4
│       │   ├── [animegrimoire] Murenase! Seton Gakuen - 11 [720p][B897E130].mp4
│       │   └── [animegrimoire] Murenase! Seton Gakuen - 12 [720p][331F7B03].mp4
│       ├── Nekopara
│       │   ├── [animegrimoire] Nekopara - 01 [720p][F2C9A50C].mp4
│       │   ├── [animegrimoire] Nekopara - 02 [720p][314D2D6E].mp4
│       │   ├── [animegrimoire] Nekopara - 03 [720p][33076D59].mp4
│       │   ├── [animegrimoire] Nekopara - 04 [720p][2BFC5EC7].mp4
│       │   ├── [animegrimoire] Nekopara - 05 [720p][B89EF1DB].mp4
│       │   ├── [animegrimoire] Nekopara - 06 [720p][8EC002D5].mp4
│       │   ├── [animegrimoire] Nekopara - 07 [720p][91E2F802].mp4
│       │   ├── [animegrimoire] Nekopara - 08 [720p][0DF7B463].mp4
│       │   ├── [animegrimoire] Nekopara - 09 [720p][E977E72A].mp4
│       │   ├── [animegrimoire] Nekopara - 10 [720p][317F1262].mp4
│       │   ├── [animegrimoire] Nekopara - 11 [720p][29A272A8].mp4
│       │   └── [animegrimoire] Nekopara - 12 [720p][8240D5C0].mp4
│       ├── Oshi ga Budoukan Ittekuretara Shinu
│       │   ├── [animegrimoire] Oshi ga Budoukan Ittekuretara Shinu - 01 [720p][782E537A].mp4
│       │   ├── [animegrimoire] Oshi ga Budoukan Ittekuretara Shinu - 02 [720p][2748EFC2].mp4
│       │   ├── [animegrimoire] Oshi ga Budoukan Ittekuretara Shinu - 03 [720p][68EE7982].mp4
│       │   ├── [animegrimoire] Oshi ga Budoukan Ittekuretara Shinu - 04 [720p][7C051C31].mp4
│       │   ├── [animegrimoire] Oshi ga Budoukan Ittekuretara Shinu - 05 [720p][136CD5E0].mp4
│       │   ├── [animegrimoire] Oshi ga Budoukan Ittekuretara Shinu - 06 [720p][2C10986F].mp4
│       │   ├── [animegrimoire] Oshi ga Budoukan Ittekuretara Shinu - 07 [720p][2B753BF7].mp4
│       │   ├── [animegrimoire] Oshi ga Budoukan Ittekuretara Shinu - 08 [720p][8A7988CB].mp4
│       │   ├── [animegrimoire] Oshi ga Budoukan Ittekuretara Shinu - 09 [720p][F91D7E86].mp4
│       │   ├── [animegrimoire] Oshi ga Budoukan Ittekuretara Shinu - 10 [720p][BF429CCC].mp4
│       │   ├── [animegrimoire] Oshi ga Budoukan Ittekuretara Shinu - 11 [720p][947FAE56].mp4
│       │   └── [animegrimoire] Oshi ga Budoukan Ittekuretara Shinu - 12 [720p][F8E188E0].mp4
│       ├── Rikei ga Koi ni Ochita no de Shoumei shitemita
│       │   ├── [animegrimoire] Rikei ga Koi ni Ochita no de Shoumei shitemita - 01 [720p][5B92EF5D].mp4
│       │   ├── [animegrimoire] Rikei ga Koi ni Ochita no de Shoumei shitemita - 02 [720p][858B8CFA].mp4
│       │   ├── [animegrimoire] Rikei ga Koi ni Ochita no de Shoumei shitemita - 03 [720p][1BBADEE8].mp4
│       │   ├── [animegrimoire] Rikei ga Koi ni Ochita no de Shoumei shitemita - 04 [720p][C086FE0D].mp4
│       │   ├── [animegrimoire] Rikei ga Koi ni Ochita no de Shoumei shitemita - 05 [720p][F2EA33BC].mp4
│       │   ├── [animegrimoire] Rikei ga Koi ni Ochita no de Shoumei shitemita - 06 [720p][2B4B232B].mp4
│       │   ├── [animegrimoire] Rikei ga Koi ni Ochita no de Shoumei shitemita - 07 [720p][F2465A80].mp4
│       │   ├── [animegrimoire] Rikei ga Koi ni Ochita no de Shoumei shitemita - 08 [720p][1F9D4134].mp4
│       │   ├── [animegrimoire] Rikei ga Koi ni Ochita no de Shoumei shitemita - 09 [720p][16F05034].mp4
│       │   ├── [animegrimoire] Rikei ga Koi ni Ochita no de Shoumei shitemita - 10 [720p][3E413A78].mp4
│       │   ├── [animegrimoire] Rikei ga Koi ni Ochita no de Shoumei shitemita - 11 [720p][42F9276C].mp4
│       │   └── [animegrimoire] Rikei ga Koi ni Ochita no de Shoumei shitemita - 12 [720p][31DFBA05].mp4
│       ├── Show By Rock!! Mashumairesh!!
│       │   ├── [animegrimoire] Show By Rock!! Mashumairesh!! - 01 [720p][D1D92B91].mp4
│       │   ├── [animegrimoire] Show By Rock!! Mashumairesh!! - 02 [720p][E61F6E6F].mp4
│       │   ├── [animegrimoire] Show By Rock!! Mashumairesh!! - 03 [720p][550226CF].mp4
│       │   ├── [animegrimoire] Show By Rock!! Mashumairesh!! - 04 [720p][9762EFE8].mp4
│       │   ├── [animegrimoire] Show By Rock!! Mashumairesh!! - 05 [720p][4B99B4AB].mp4
│       │   ├── [animegrimoire] Show By Rock!! Mashumairesh!! - 06 [720p][0517ABB4].mp4
│       │   ├── [animegrimoire] Show By Rock!! Mashumairesh!! - 07 [720p][6C01A1EB].mp4
│       │   ├── [animegrimoire] Show By Rock!! Mashumairesh!! - 08 [720p][D9D4F994].mp4
│       │   ├── [animegrimoire] Show By Rock!! Mashumairesh!! - 09 [720p][CB4A8C10].mp4
│       │   ├── [animegrimoire] Show By Rock!! Mashumairesh!! - 10 [720p][85A32B8E].mp4
│       │   ├── [animegrimoire] Show By Rock!! Mashumairesh!! - 11 [720p][26368C75].mp4
│       │   └── [animegrimoire] Show By Rock!! Mashumairesh!! - 12 [720p][B24F8DAB].mp4
│       └── Somali to Mori no Kamisama
│           ├── [animegrimoire] Somali to Mori no Kamisama - 01 [720p][E0996B38].mp4
│           ├── [animegrimoire] Somali to Mori no Kamisama - 02 [720p][A9343164].mp4
│           ├── [animegrimoire] Somali to Mori no Kamisama - 03 [720p][A82E16E8].mp4
│           ├── [animegrimoire] Somali to Mori no Kamisama - 04 [720p][F4E909E9].mp4
│           ├── [animegrimoire] Somali to Mori no Kamisama - 05 [720p][7DB10736].mp4
│           ├── [animegrimoire] Somali to Mori no Kamisama - 06 [720p][F0BEA066].mp4
│           ├── [animegrimoire] Somali to Mori no Kamisama - 07 [720p][369C29EE].mp4
│           ├── [animegrimoire] Somali to Mori no Kamisama - 08 [720p][48166E94].mp4
│           ├── [animegrimoire] Somali to Mori no Kamisama - 09 [720p][2A9C11DC].mp4
│           ├── [animegrimoire] Somali to Mori no Kamisama - 10 [720p][8459BF2B].mp4
│           ├── [animegrimoire] Somali to Mori no Kamisama - 11 [720p][ED243B7F].mp4
│           └── [animegrimoire] Somali to Mori no Kamisama - 12 [720p][CD80C49E].mp4
├── 2021
│   ├── Fall
│   │   ├── 86
│   │   │   ├── [animegrimoire] 86 - Eighty Six - 12 [720p][0194EC29].mp4
│   │   │   ├── [animegrimoire] 86 - Eighty Six - 13 [720p][5B05CDAF].mp4
│   │   │   ├── [animegrimoire] 86 - Eighty Six - 14 [720p][4AC9C833].mp4
│   │   │   ├── [animegrimoire] 86 - Eighty Six - 15 [720p][C1F49629].mp4
│   │   │   ├── [animegrimoire] 86 - Eighty Six - 16 [720p][079D59F6].mp4
│   │   │   ├── [animegrimoire] 86 - Eighty Six - 17 [720p][F84114E4].mp4
│   │   │   ├── [animegrimoire] 86 - Eighty Six - 18 [720p][54C40AD8].mp4
│   │   │   ├── [animegrimoire] 86 - Eighty Six - 19 [720p][144A23FE].mp4
│   │   │   ├── [animegrimoire] 86 - Eighty Six - 20 [720p][34B9E77A].mp4
│   │   │   └── [animegrimoire] 86 - Eighty Six - 21 [720p][CD8C1FF9].mp4
│   │   ├── Genjitsu Shugi Yuusha no Oukoku Saikenki
│   │   │   └── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki - 13 END [720p][1B22CDCB].mp4
│   │   ├── Ichigo Mashimaro
│   │   │   ├── [animegrimoire] Ichigo Mashimaro - 00 [BD720p][6357D4B8].mp4
│   │   │   ├── [animegrimoire] Ichigo Mashimaro - 01 [BD720p][7152FE28].mp4
│   │   │   ├── [animegrimoire] Ichigo Mashimaro - 02 [BD720p][B98D88EC].mp4
│   │   │   ├── [animegrimoire] Ichigo Mashimaro - 03 [BD720p][610CE920].mp4
│   │   │   ├── [animegrimoire] Ichigo Mashimaro - 04 [BD720p][4A637B31].mp4
│   │   │   ├── [animegrimoire] Ichigo Mashimaro - 05 [BD720p][F5E631D5].mp4
│   │   │   ├── [animegrimoire] Ichigo Mashimaro - 06 [BD720p][B18E976B].mp4
│   │   │   ├── [animegrimoire] Ichigo Mashimaro - 07 [BD720p][560076A7].mp4
│   │   │   ├── [animegrimoire] Ichigo Mashimaro - 08 [BD720p][F95E685F].mp4
│   │   │   ├── [animegrimoire] Ichigo Mashimaro - 09 [BD720p][39D51B0C].mp4
│   │   │   ├── [animegrimoire] Ichigo Mashimaro - 10 [BD720p][8A835028].mp4
│   │   │   ├── [animegrimoire] Ichigo Mashimaro - 11 [BD720p][6FC3A3E7].mp4
│   │   │   └── [animegrimoire] Ichigo Mashimaro - 12 [BD720p][AA5D6C06].mp4
│   │   ├── Isekai Shokudou S2
│   │   │   ├── [animegrimoire] Isekai Shokudou S2 - 01 [720p][43CA69FA].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou S2 - 02 [720p][9BECBDEE].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou S2 - 03 [720p][F3633EE3].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou S2 - 04 [720p][FB673C41].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou S2 - 05 [720p][406C6C2F].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou S2 - 06 [720p][0179DC68].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou S2 - 07 [720p][5417E82A].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou S2 - 08 [720p][9D9DD7FF].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou S2 - 09 [720p][50852D8D].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou S2 - 10 [720p][52086BEC].mp4
│   │   │   ├── [animegrimoire] Isekai Shokudou S2 - 11 [720p][08237D3E].mp4
│   │   │   └── [animegrimoire] Isekai Shokudou S2 - 12 [720p][F418F127].mp4
│   │   ├── Jahy-sama wa Kujikenai!
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 09 [720p][980C1A88].mp4
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 10 [720p][CC981A20].mp4
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 11 [720p][FD348743].mp4
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 12 [720p][19F72A64].mp4
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 13 [720p][9DD7C11C].mp4
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 14 [720p][358C5363].mp4
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 15 [720p][EC65EB99].mp4
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 16 [720p][476BEFE0].mp4
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 17 [720p][1F36AFBB].mp4
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 18 [720p][FCF2058B].mp4
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 19 [720p][0444D2F5].mp4
│   │   │   └── [animegrimoire] Jahy-sama wa Kujikenai! - 20 END [720p][91A82587].mp4
│   │   ├── Kimetsu no Yaiba
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Mugen Ressha-hen - 01 [720p][356C69DD].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Mugen Ressha-hen - 02 [720p][DA10E976].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Mugen Ressha-hen - 03 [720p][D81ACF30].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Mugen Ressha-hen - 04 [720p][F68AF5B6].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Mugen Ressha-hen - 05 [720p][599BA12C].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Mugen Ressha-hen - 06 [720p][822B7C39].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Mugen Ressha-hen - 07 [720p][9F4B98F7].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Yuukaku Hen - 01 [720p][E433390B].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Yuukaku Hen - 02 [720p][B5A1E8E1].mp4
│   │   │   └── [animegrimoire] Kimetsu no Yaiba - Yuukaku Hen - 03 [720p][5282BAC6].mp4
│   │   ├── Komi-san wa Comyushou desu
│   │   │   ├── [animegrimoire] Komi-san wa Comyushou desu. - 01 [720p][A65441B9].mp4
│   │   │   ├── [animegrimoire] Komi-san wa Comyushou desu. - 02 [720p][73C7B930].mp4
│   │   │   ├── [animegrimoire] Komi-san wa Comyushou desu. - 03 [720p][2292835C].mp4
│   │   │   ├── [animegrimoire] Komi-san wa Comyushou desu. - 04 [720p][5A8AE3AB].mp4
│   │   │   ├── [animegrimoire] Komi-san wa Comyushou desu. - 05 [720p][13D5E789].mp4
│   │   │   ├── [animegrimoire] Komi-san wa Comyushou desu. - 06 [720p][E54729AE].mp4
│   │   │   ├── [animegrimoire] Komi-san wa Comyushou desu. - 07 [720p][81BD389F].mp4
│   │   │   ├── [animegrimoire] Komi-san wa Comyushou desu. - 08 [720p][1DFCE8F4].mp4
│   │   │   ├── [animegrimoire] Komi-san wa Comyushou desu. - 09 [720p][9BC111DC].mp4
│   │   │   └── [animegrimoire] Komi-san wa Comyushou desu. - 10 [720p][EA480E47].mp4
│   │   ├── Mahouka Koukou no Yuutousei
│   │   │   └── [animegrimoire] Mahouka Koukou no Yuutousei - 13 END [720p][517DFEFF].mp4
│   │   ├── Mieruko-chan
│   │   │   ├── [animegrimoire] Mieruko-chan - 01 [720p][47913B46].mp4
│   │   │   ├── [animegrimoire] Mieruko-chan - 02 [720p][FCE33AB2].mp4
│   │   │   ├── [animegrimoire] Mieruko-chan - 03 [720p][C2A4BFAB].mp4
│   │   │   ├── [animegrimoire] Mieruko-chan - 04 [720p][76DF7FCD].mp4
│   │   │   ├── [animegrimoire] Mieruko-chan - 05 [720p][A589636B].mp4
│   │   │   ├── [animegrimoire] Mieruko-chan - 06 [720p][12916CA0].mp4
│   │   │   ├── [animegrimoire] Mieruko-chan - 07 [720p][CBC2427A].mp4
│   │   │   ├── [animegrimoire] Mieruko-chan - 08 [720p][541C066A].mp4
│   │   │   ├── [animegrimoire] Mieruko-chan - 09 [720p][44CC9562].mp4
│   │   │   ├── [animegrimoire] Mieruko-chan - 10 [720p][C095B82E].mp4
│   │   │   ├── [animegrimoire] Mieruko-chan - 11 [720p][D9F3F387].mp4
│   │   │   └── [animegrimoire] Mieruko-chan - 12 [720p][4BCBD478].mp4
│   │   ├── Mushoku Tensei
│   │   │   ├── [animegrimoire] Mushoku Tensei - 12 [720p][C284181F].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei - 13 [720p][517F5A97].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei - 14 [720p][77836401].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei - 15 [720p][6AAD86D4].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei - 16 [720p][C2E321A3].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei - 17 [720p][DEA2189E].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei - 18 [720p][D87F4B2B].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei - 19 [720p][26ADB4B8].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei - 20 [720p][FBEA3BBF].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei - 21 [720p][2E984C44].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei - 22 [720p][3CF2DEE5].mp4
│   │   │   └── [animegrimoire] Mushoku Tensei - 23 [720p][4E4BDB1F].mp4
│   │   ├── Sekai Saikou no Ansatsusha Isekai Kizoku ni Tensei suru
│   │   │   ├── [animegrimoire] Sekai Saikou no Ansatsusha Isekai Kizoku ni Tensei suru - 01 [720p][C1E85126].mp4
│   │   │   ├── [animegrimoire] Sekai Saikou no Ansatsusha Isekai Kizoku ni Tensei suru - 02 [720p][CE67E599].mp4
│   │   │   ├── [animegrimoire] Sekai Saikou no Ansatsusha Isekai Kizoku ni Tensei suru - 03 [720p][7B91F0D0].mp4
│   │   │   ├── [animegrimoire] Sekai Saikou no Ansatsusha Isekai Kizoku ni Tensei suru - 04 [720p][52B5C36B].mp4
│   │   │   ├── [animegrimoire] Sekai Saikou no Ansatsusha Isekai Kizoku ni Tensei suru - 05 [720p][1A612E8D].mp4
│   │   │   ├── [animegrimoire] Sekai Saikou no Ansatsusha Isekai Kizoku ni Tensei suru - 06 [720p][75E5501D].mp4
│   │   │   ├── [animegrimoire] Sekai Saikou no Ansatsusha Isekai Kizoku ni Tensei suru - 07 [720p][B2E79E60].mp4
│   │   │   ├── [animegrimoire] Sekai Saikou no Ansatsusha Isekai Kizoku ni Tensei suru - 08 [720p][50335C94].mp4
│   │   │   ├── [animegrimoire] Sekai Saikou no Ansatsusha Isekai Kizoku ni Tensei suru - 09 [720p][C4A7A93A].mp4
│   │   │   ├── [animegrimoire] Sekai Saikou no Ansatsusha Isekai Kizoku ni Tensei suru - 10 [720p][10B3AEA6].mp4
│   │   │   ├── [animegrimoire] Sekai Saikou no Ansatsusha Isekai Kizoku ni Tensei suru - 11 [720p][BEBB3E6E].mp4
│   │   │   └── [animegrimoire] Sekai Saikou no Ansatsusha Isekai Kizoku ni Tensei suru - 12 [720p][DDD54472].mp4
│   │   ├── Senpai ga Uzai Kouhai no Hanashi
│   │   │   ├── [animegrimoire] Senpai ga Uzai Kouhai no Hanashi - 01 [720p][AD10F0DE].mp4
│   │   │   ├── [animegrimoire] Senpai ga Uzai Kouhai no Hanashi - 02 [720p][4C033C79].mp4
│   │   │   ├── [animegrimoire] Senpai ga Uzai Kouhai no Hanashi - 03 [720p][C0D50EAF].mp4
│   │   │   ├── [animegrimoire] Senpai ga Uzai Kouhai no Hanashi - 04 [720p][C5BB7007].mp4
│   │   │   ├── [animegrimoire] Senpai ga Uzai Kouhai no Hanashi - 05 [720p][996B0EE7].mp4
│   │   │   ├── [animegrimoire] Senpai ga Uzai Kouhai no Hanashi - 06 [720p][E4869073].mp4
│   │   │   ├── [animegrimoire] Senpai ga Uzai Kouhai no Hanashi - 07 [720p][F0F498A8].mp4
│   │   │   ├── [animegrimoire] Senpai ga Uzai Kouhai no Hanashi - 08 [720p][F95D7FBE].mp4
│   │   │   ├── [animegrimoire] Senpai ga Uzai Kouhai no Hanashi - 09 [720p][4801F417].mp4
│   │   │   ├── [animegrimoire] Senpai ga Uzai Kouhai no Hanashi - 10 [720p][8A7609D0].mp4
│   │   │   ├── [animegrimoire] Senpai ga Uzai Kouhai no Hanashi - 11 [720p][1F0C161B].mp4
│   │   │   └── [animegrimoire] Senpai ga Uzai Kouhai no Hanashi - 12 [720p][F663145C].mp4
│   │   ├── Shingeki no Kyojin OAD
│   │   │   ├── [animegrimoire] Shingeki no Kyojin OAD - 01 [720p][328289BF].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin OAD - 02 [720p][1FD89316].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin OAD - 03 [720p][2DEC4247].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin OAD - 04 [720p][0BC8411E].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin OAD - 05 [720p][1CC59402].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin OAD - 06 [720p][AA9FA2B2].mp4
│   │   │   ├── [animegrimoire] Shingeki no Kyojin OAD - 07 [720p][207B9936].mp4
│   │   │   └── [animegrimoire] Shingeki no Kyojin OAD - 08 END [720p][2A901CF9].mp4
│   │   ├── Taishou Otome Otogibanashi
│   │   │   ├── [animegrimoire] Taishou Otome Otogibanashi - 01 [720p][B4185E6C].mp4
│   │   │   ├── [animegrimoire] Taishou Otome Otogibanashi - 02 [720p][8B24E9CA].mp4
│   │   │   ├── [animegrimoire] Taishou Otome Otogibanashi - 03 [720p][D536560C].mp4
│   │   │   ├── [animegrimoire] Taishou Otome Otogibanashi - 04 [720p][337B63CE].mp4
│   │   │   ├── [animegrimoire] Taishou Otome Otogibanashi - 05 [720p][58B1E1D1].mp4
│   │   │   ├── [animegrimoire] Taishou Otome Otogibanashi - 06 [720p][6091A9A4].mp4
│   │   │   ├── [animegrimoire] Taishou Otome Otogibanashi - 07 [720p][8F0AC515].mp4
│   │   │   ├── [animegrimoire] Taishou Otome Otogibanashi - 08 [720p][0C5BFBA4].mp4
│   │   │   ├── [animegrimoire] Taishou Otome Otogibanashi - 09 [720p][3EC9FC3D].mp4
│   │   │   ├── [animegrimoire] Taishou Otome Otogibanashi - 10 [720p][646DE271].mp4
│   │   │   ├── [animegrimoire] Taishou Otome Otogibanashi - 11 [720p][10B47938].mp4
│   │   │   └── [animegrimoire] Taishou Otome Otogibanashi - 12 [720p][1D7E1526].mp4
│   │   ├── World Trigger S3
│   │   │   ├── [animegrimoire] World Trigger S3 - 01 [720p][D62807C2].mp4
│   │   │   ├── [animegrimoire] World Trigger S3 - 02 [720p][1F43A546].mp4
│   │   │   ├── [animegrimoire] World Trigger S3 - 03 [720p][21E538CB].mp4
│   │   │   ├── [animegrimoire] World Trigger S3 - 04 [720p][1C8DDFA3].mp4
│   │   │   ├── [animegrimoire] World Trigger S3 - 05 [720p][C509EF4B].mp4
│   │   │   ├── [animegrimoire] World Trigger S3 - 06 [720p][ECF6F7CE].mp4
│   │   │   ├── [animegrimoire] World Trigger S3 - 07 [720p][5161D102].mp4
│   │   │   ├── [animegrimoire] World Trigger S3 - 08 [720p][9A998087].mp4
│   │   │   ├── [animegrimoire] World Trigger S3 - 09 [720p][88B1287A].mp4
│   │   │   ├── [animegrimoire] World Trigger S3 - 10 [720p][2277A825].mp4
│   │   │   ├── [animegrimoire] World Trigger S3 - 11 [720p][58060045].mp4
│   │   │   └── [animegrimoire] World Trigger S3 - 12 [720p][5D6D7247].mp4
│   │   ├── Yakunara Mug Cup mo S2
│   │   │   ├── [animegrimoire] Yakunara Mug Cup mo S2 - 01 [720p][32A6AA57].mp4
│   │   │   ├── [animegrimoire] Yakunara Mug Cup mo S2 - 02 [720p][CB6A5060].mp4
│   │   │   ├── [animegrimoire] Yakunara Mug Cup mo S2 - 03 [720p][80274B7C].mp4
│   │   │   ├── [animegrimoire] Yakunara Mug Cup mo S2 - 04 [720p][F1AC04B3].mp4
│   │   │   ├── [animegrimoire] Yakunara Mug Cup mo S2 - 05 [720p][29E2065B].mp4
│   │   │   ├── [animegrimoire] Yakunara Mug Cup mo S2 - 06 [720p][DB56B486].mp4
│   │   │   ├── [animegrimoire] Yakunara Mug Cup mo S2 - 07 [720p][2AFF525A].mp4
│   │   │   ├── [animegrimoire] Yakunara Mug Cup mo S2 - 08 [720p][64EC4DDE].mp4
│   │   │   ├── [animegrimoire] Yakunara Mug Cup mo S2 - 09 [720p][092B37F1].mp4
│   │   │   ├── [animegrimoire] Yakunara Mug Cup mo S2 - 10 [720p][AA9ED289].mp4
│   │   │   ├── [animegrimoire] Yakunara Mug Cup mo S2 - 11 [720p][0CA48753].mp4
│   │   │   └── [animegrimoire] Yakunara Mug Cup mo S2 - 12 [720p][D1693292].mp4
│   │   └── Yuuki Yuuna wa Yuusha de Aru
│   │       ├── [animegrimoire] Yuuki Yuuna wa Yuusha de Aru - Dai Mankai no Shou - 01 [720p][9C0AE229].mp4
│   │       ├── [animegrimoire] Yuuki Yuuna wa Yuusha de Aru - Dai Mankai no Shou - 02 [720p][8640FD3C].mp4
│   │       ├── [animegrimoire] Yuuki Yuuna wa Yuusha de Aru - Dai Mankai no Shou - 03 [720p][BEEB5160].mp4
│   │       ├── [animegrimoire] Yuuki Yuuna wa Yuusha de Aru - Dai Mankai no Shou - 04 [720p][30B26DCC].mp4
│   │       ├── [animegrimoire] Yuuki Yuuna wa Yuusha de Aru - Dai Mankai no Shou - 05 [720p][827309CC].mp4
│   │       ├── [animegrimoire] Yuuki Yuuna wa Yuusha de Aru - Dai Mankai no Shou - 06 [720p][A6A44583].mp4
│   │       ├── [animegrimoire] Yuuki Yuuna wa Yuusha de Aru - Dai Mankai no Shou - 07 [720p][792FB98E].mp4
│   │       ├── [animegrimoire] Yuuki Yuuna wa Yuusha de Aru - Dai Mankai no Shou - 08 [720p][5092ACC0].mp4
│   │       ├── [animegrimoire] Yuuki Yuuna wa Yuusha de Aru - Dai Mankai no Shou - 09 [720p][79C37ABB].mp4
│   │       ├── [animegrimoire] Yuuki Yuuna wa Yuusha de Aru - Dai Mankai no Shou - 10 [720p][2CB408AF].mp4
│   │       ├── [animegrimoire] Yuuki Yuuna wa Yuusha de Aru - Dai Mankai no Shou - 11 [720p][63BE49A9].mp4
│   │       └── [animegrimoire] Yuuki Yuuna wa Yuusha de Aru - Dai Mankai no Shou - 12 [720p][93B90351].mp4
│   ├── Spring
│   │   ├── Assault Lily Bouquet
│   │   │   ├── [animegrimoire] Assault Lily Bouquet - 01 [BD720p][7F14E314].mp4
│   │   │   ├── [animegrimoire] Assault Lily Bouquet - 02 [BD720p][5368AD16].mp4
│   │   │   ├── [animegrimoire] Assault Lily Bouquet - 03 [BD720p][DE4DEFFE].mp4
│   │   │   ├── [animegrimoire] Assault Lily Bouquet - 04 [BD720p][47538070].mp4
│   │   │   ├── [animegrimoire] Assault Lily Bouquet - 05 [BD720p][D6E6CC00].mp4
│   │   │   ├── [animegrimoire] Assault Lily Bouquet - 06 [BD720p][D1478791].mp4
│   │   │   ├── [animegrimoire] Assault Lily Bouquet - 07 [BD720p][4B4BB493].mp4
│   │   │   ├── [animegrimoire] Assault Lily Bouquet - 08 [BD720p][D195C9BF].mp4
│   │   │   ├── [animegrimoire] Assault Lily Bouquet - 09 [BD720p][1A77F9FC].mp4
│   │   │   ├── [animegrimoire] Assault Lily Bouquet - 10 [BD720p][5728A7F3].mp4
│   │   │   ├── [animegrimoire] Assault Lily Bouquet - 11 [BD720p][32BE1F9F].mp4
│   │   │   └── [animegrimoire] Assault Lily Bouquet - 12 [BD720p][8B2C3D8E].mp4
│   │   ├── Black Bullet
│   │   │   ├── [animegrimoire] Black Bullet - 01 [BD720p][C18FD85D].mp4
│   │   │   ├── [animegrimoire] Black Bullet - 02 [BD720p][D9FE801D].mp4
│   │   │   ├── [animegrimoire] Black Bullet - 03 [BD720p][D3BFB92C].mp4
│   │   │   ├── [animegrimoire] Black Bullet - 04 [BD720p][31CB34EC].mp4
│   │   │   ├── [animegrimoire] Black Bullet - 05 [BD720p][81E1991C].mp4
│   │   │   ├── [animegrimoire] Black Bullet - 06 [BD720p][24473955].mp4
│   │   │   ├── [animegrimoire] Black Bullet - 07 [BD720p][D3142381].mp4
│   │   │   ├── [animegrimoire] Black Bullet - 08 [BD720p][DF8B9475].mp4
│   │   │   ├── [animegrimoire] Black Bullet - 09 [BD720p][54160F38].mp4
│   │   │   ├── [animegrimoire] Black Bullet - 10 [BD720p][476CEC3A].mp4
│   │   │   ├── [animegrimoire] Black Bullet - 11 [BD720p][6DC644D9].mp4
│   │   │   ├── [animegrimoire] Black Bullet - 12 [BD720p][F4F33BAA].mp4
│   │   │   └── [animegrimoire] Black Bullet - 13 [BD720p][7E4A98F6].mp4
│   │   ├── Blue Reflection Ray
│   │   │   ├── [animegrimoire] Blue Reflection Ray - 01 [720p][83CC11F1].mp4
│   │   │   ├── [animegrimoire] Blue Reflection Ray - 02 [720p][5CE9E729].mp4
│   │   │   ├── [animegrimoire] Blue Reflection Ray - 03 [720p][E3130B5D].mp4
│   │   │   ├── [animegrimoire] Blue Reflection Ray - 04 [v0][720p][09F7717D].mp4
│   │   │   ├── [animegrimoire] Blue Reflection Ray - 05 [v0][720p][2F59CE08].mp4
│   │   │   ├── [animegrimoire] Blue Reflection Ray - 06 [v0][720p][1DB4D77C].mp4
│   │   │   ├── [animegrimoire] Blue Reflection Ray - 07 [v0][720p][84DEC4C6].mp4
│   │   │   ├── [animegrimoire] Blue Reflection Ray - 08 [v0][720p][6439CB67].mp4
│   │   │   ├── [animegrimoire] Blue Reflection Ray - 09 [v0][720p][8EB8C2E0].mp4
│   │   │   ├── [animegrimoire] Blue Reflection Ray - 10 [v0][720p][80A9C27A].mp4
│   │   │   ├── [animegrimoire] Blue Reflection Ray - 11 [v0][720p][ABFFFCFC].mp4
│   │   │   ├── [animegrimoire] Blue Reflection Ray - 12 [v0][720p][F47CF9A7].mp4
│   │   │   ├── [animegrimoire] Blue Reflection Ray - 13 [v0][720p][3EBD38F3].mp4
│   │   │   └── [animegrimoire] Blue Reflection Ray - 14 [v0][720p][79CC467B].mp4
│   │   ├── Hige o Soru Soshite Joshikousei o Hirou
│   │   │   ├── [animegrimoire] Hige o Soru. Soshite Joshikousei o Hirou. - 01 [720p][39030602].mp4
│   │   │   ├── [animegrimoire] Hige o Soru. Soshite Joshikousei o Hirou. - 02 [720p][20E8A2FB].mp4
│   │   │   ├── [animegrimoire] Hige o Soru. Soshite Joshikousei o Hirou. - 03 [720p][4CDDC62B].mp4
│   │   │   ├── [animegrimoire] Hige o Soru. Soshite Joshikousei o Hirou. - 04 [720p][97922543].mp4
│   │   │   ├── [animegrimoire] Hige o Soru. Soshite Joshikousei o Hirou. - 05 [720p][0745A430].mp4
│   │   │   ├── [animegrimoire] Hige o Soru. Soshite Joshikousei o Hirou. - 06 [720p][C463A093].mp4
│   │   │   ├── [animegrimoire] Hige o Soru. Soshite Joshikousei o Hirou. - 07 [720p][7AC3CD52].mp4
│   │   │   ├── [animegrimoire] Hige o Soru. Soshite Joshikousei o Hirou. - 08 [720p][0505E46E].mp4
│   │   │   ├── [animegrimoire] Hige o Soru. Soshite Joshikousei o Hirou. - 09 [720p][50AC9796].mp4
│   │   │   ├── [animegrimoire] Hige o Soru. Soshite Joshikousei o Hirou. - 10 [720p][1B747B6D].mp4
│   │   │   ├── [animegrimoire] Hige o Soru. Soshite Joshikousei o Hirou. - 11 [720p][F250E2A3].mp4
│   │   │   ├── [animegrimoire] Hige o Soru. Soshite Joshikousei o Hirou. - 12 [720p][DFDFB7F6].mp4
│   │   │   └── [animegrimoire] Hige o Soru. Soshite Joshikousei o Hirou. - 13 END [720p][1677E568].mp4
│   │   ├── Kimetsu no Yaiba
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Mt. Natagumo Arc [720p][D8904CB0].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Mugen Ressha Hen [BD 720p][32CC17C3].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Siblings Bond [720p][8CD17C04].mp4
│   │   │   └── [animegrimoire] Kimetsu no Yaiba - The Hashira Meeting Arc [720p][6344A318].mp4
│   │   ├── Kono Subarashii Sekai ni Shukufuku wo!
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! - 01 [BD720p][42FA4873].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! - 02 [BD720p][7ACC8501].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! - 03 [BD720p][C04214BB].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! - 04 [BD720p][EFAE60D8].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! - 05 [BD720p][A2772752].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! - 06 [BD720p][ED946D2B].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! - 07 [BD720p][CE19F5C6].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! - 08 [BD720p][52296D35].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! - 09 [BD720p][01750336].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! - 10 [BD720p][E0EEBE49].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! - 11 [BD720p][92DAD6D5].mp4
│   │   │   └── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! - 12 [BD720p][E8C45D2A].mp4
│   │   ├── Seijo no Maryoku wa Bannou Desu
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu - 01 [720p][8C1517BC].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu - 02v2 [720p][345E75A2].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu - 03 [720p][7FDE2FD1].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu - 04 [720p][0D44FF91].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu - 05 [v0][720p][DDBDC245].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu - 06 [v0][720p][095148E1].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu - 07 [v0][720p][12B3E73F].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu - 08 [v0][720p][D048CBB8].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu - 09 [v0][720p][8DE3230D].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu - 10 [720p][13ACC9E5].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu - 11 [720p][9BA4D46D].mp4
│   │   │   └── [animegrimoire] Seijo no Maryoku wa Bannou Desu - 12 END [v0][720p][B5698202].mp4
│   │   ├── Slime Taoshite 300-nen Shiranai Uchi ni Level Max ni Nattemashita
│   │   │   ├── [animegrimoire] Slime Taoshite 300-nen Shiranai Uchi ni Level Max ni Nattemashita - 01 [720p][5C5A6554].mp4
│   │   │   ├── [animegrimoire] Slime Taoshite 300-nen Shiranai Uchi ni Level Max ni Nattemashita - 02 [720p][5EA71F4C].mp4
│   │   │   ├── [animegrimoire] Slime Taoshite 300-nen Shiranai Uchi ni Level Max ni Nattemashita - 03 [720p][B4C1B956].mp4
│   │   │   ├── [animegrimoire] Slime Taoshite 300-nen Shiranai Uchi ni Level Max ni Nattemashita - 04 [720p][FE6D1814].mp4
│   │   │   ├── [animegrimoire] Slime Taoshite 300-nen Shiranai Uchi ni Level Max ni Nattemashita - 05 [720p][001491AD].mp4
│   │   │   ├── [animegrimoire] Slime Taoshite 300-nen Shiranai Uchi ni Level Max ni Nattemashita - 06 [720p][171144E8].mp4
│   │   │   ├── [animegrimoire] Slime Taoshite 300-nen Shiranai Uchi ni Level Max ni Nattemashita - 07 [720p][D475CB18].mp4
│   │   │   ├── [animegrimoire] Slime Taoshite 300-nen Shiranai Uchi ni Level Max ni Nattemashita - 08 [720p][52D0CE4C].mp4
│   │   │   ├── [animegrimoire] Slime Taoshite 300-nen Shiranai Uchi ni Level Max ni Nattemashita - 09 [720p][C8AF8BB4].mp4
│   │   │   ├── [animegrimoire] Slime Taoshite 300-nen Shiranai Uchi ni Level Max ni Nattemashita - 10 [720p][3AC32BCE].mp4
│   │   │   ├── [animegrimoire] Slime Taoshite 300-nen Shiranai Uchi ni Level Max ni Nattemashita - 11 [720p][FBB68314].mp4
│   │   │   └── [animegrimoire] Slime Taoshite 300-nen Shiranai Uchi ni Level Max ni Nattemashita - 12 END [720p][13EE3D72].mp4
│   │   ├── Super Cub
│   │   │   ├── [animegrimoire] Super Cub - 01 [720p][5545459D].mp4
│   │   │   ├── [animegrimoire] Super Cub - 02 [720p][04CCD969].mp4
│   │   │   ├── [animegrimoire] Super Cub - 03 [720p][890CB497].mp4
│   │   │   ├── [animegrimoire] Super Cub - 04 [720p][6C8BA376].mp4
│   │   │   ├── [animegrimoire] Super Cub - 05 [v0][720p][558610D9].mp4
│   │   │   ├── [animegrimoire] Super Cub - 06 [v0][720p][CA7BAF1C].mp4
│   │   │   ├── [animegrimoire] Super Cub - 07 [v0][720p][626BF716].mp4
│   │   │   ├── [animegrimoire] Super Cub - 08 [v0][720p][4188B98D].mp4
│   │   │   ├── [animegrimoire] Super Cub - 09 [v0][720p][0AC8DB1C].mp4
│   │   │   ├── [animegrimoire] Super Cub - 10 [v0][720p][D3BEC579].mp4
│   │   │   ├── [animegrimoire] Super Cub - 11 [720p][4613BC77].mp4
│   │   │   └── [animegrimoire] Super Cub - 12 END [v0][720p][79B6CE39].mp4
│   │   ├── Yama no Susume
│   │   │   ├── [animegrimoire] Yama no Susume - 01 [BD720p][7CA65A08].mp4
│   │   │   ├── [animegrimoire] Yama no Susume - 02 [BD720p][65749D17].mp4
│   │   │   ├── [animegrimoire] Yama no Susume - 03 [BD720p][6E38E3D1].mp4
│   │   │   ├── [animegrimoire] Yama no Susume - 04 [BD720p][F1D562E2].mp4
│   │   │   ├── [animegrimoire] Yama no Susume - 05 [BD720p][C25FEF1B].mp4
│   │   │   ├── [animegrimoire] Yama no Susume - 06 [BD720p][EE7BB657].mp4
│   │   │   ├── [animegrimoire] Yama no Susume - 07 [BD720p][7DDAAF02].mp4
│   │   │   ├── [animegrimoire] Yama no Susume - 08 [BD720p][858D3D36].mp4
│   │   │   ├── [animegrimoire] Yama no Susume - 09 [BD720p][252F9D2C].mp4
│   │   │   ├── [animegrimoire] Yama no Susume - 10 [BD720p][1D677F4D].mp4
│   │   │   ├── [animegrimoire] Yama no Susume - 11 [BD720p][901C6CA2].mp4
│   │   │   ├── [animegrimoire] Yama no Susume - 12 [BD720p][960402B6].mp4
│   │   │   └── [animegrimoire] Yama no Susume - 13 [BD720p][4D55D2FF].mp4
│   │   ├── Yama no Susume 2nd Season
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 01 [BD720p][43D74ABD].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 02 [BD720p][4BA52FC2].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 03 [BD720p][9790CDE6].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 04 [BD720p][708DE74E].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 05 [BD720p][3C512EEC].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 06 [BD720p][93156B5F].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 07 [BD720p][EE0909FF].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 08 [BD720p][8E66F37C].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 09 [BD720p][031178F7].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 10 [BD720p][2DE57838].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 11 [BD720p][2E334124].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 12 [BD720p][99413EB4].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 13 [BD720p][241D14AB].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 14 [BD720p][7B4D764D].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 15 [BD720p][7480E6B5].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 16 [BD720p][BAB5E423].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 17 [BD720p][E3801CE0].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 18 [BD720p][BE4DF447].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 19 [BD720p][DE48D407].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 20 [BD720p][95457BEC].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 21 [BD720p][7D65D973].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 22 [BD720p][B1550AF2].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season - 23 [BD720p][C810A6A9].mp4
│   │   │   └── [animegrimoire] Yama no Susume 2nd Season - 24 [BD720p][DEF4262F].mp4
│   │   ├── Yama no Susume 2nd Season OVA
│   │   │   ├── [animegrimoire] Yama no Susume 2nd Season OVA - 06 [BD720p][75A24CB1].mp4
│   │   │   └── [animegrimoire] Yama no Susume 2nd Season OVA - 25 [BD720p][2A0AA0D4].mp4
│   │   ├── Yama no Susume 3rd Season
│   │   │   ├── [animegrimoire] Yama no Susume 3rd Season - 01 [BD720p][1B4151B0].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 3rd Season - 02 [BD720p][A087D1A9].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 3rd Season - 03 [BD720p][BDAD7161].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 3rd Season - 04 [BD720p][AAFFDA5C].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 3rd Season - 05 [BD720p][7B738097].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 3rd Season - 06 [BD720p][EF735B98].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 3rd Season - 07 [BD720p][35D5A2FD].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 3rd Season - 08 [BD720p][137ACE5A].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 3rd Season - 09 [BD720p][69B904DC].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 3rd Season - 10 [BD720p][226CC3EF].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 3rd Season - 11 [BD720p][2AFA28B3].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 3rd Season - 12 [BD720p][652DA709].mp4
│   │   │   ├── [animegrimoire] Yama no Susume 3rd Season - 13 [BD720p][623D625A].mp4
│   │   │   └── [animegrimoire] Yama no Susume 3rd Season - 14 [BD720p][B198F888].mp4
│   │   └── Yama no Susume Omoide Present
│   │       └── [animegrimoire] Yama no Susume Omoide Present - 00 [BD720p][89FDF5B8].mp4
│   ├── Summer
│   │   ├── Fushigi na Somera-chan
│   │   │   ├── [animegrimoire] Fushigi na Somera-chan - 01 [720p][79F3F187].mp4
│   │   │   ├── [animegrimoire] Fushigi na Somera-chan - 02 [720p][0096A3D8].mp4
│   │   │   ├── [animegrimoire] Fushigi na Somera-chan - 03 [720p][66B01BB9].mp4
│   │   │   ├── [animegrimoire] Fushigi na Somera-chan - 04 [720p][7ED3B707].mp4
│   │   │   ├── [animegrimoire] Fushigi na Somera-chan - 05 [720p][3FE6939C].mp4
│   │   │   ├── [animegrimoire] Fushigi na Somera-chan - 06 [720p][3C8C1577].mp4
│   │   │   ├── [animegrimoire] Fushigi na Somera-chan - 07 [720p][1F98A109].mp4
│   │   │   ├── [animegrimoire] Fushigi na Somera-chan - 08 [720p][FAA9D067].mp4
│   │   │   ├── [animegrimoire] Fushigi na Somera-chan - 09 [720p][A7069CF4].mp4
│   │   │   ├── [animegrimoire] Fushigi na Somera-chan - 10 [720p][789AF3D5].mp4
│   │   │   ├── [animegrimoire] Fushigi na Somera-chan - 11 [720p][7E015875].mp4
│   │   │   └── [animegrimoire] Fushigi na Somera-chan - 12 [720p][3775AD19].mp4
│   │   ├── Genjitsu Shugi Yuusha no Oukoku Saikenki
│   │   │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki - 01 [720p][C472EDAA].mp4
│   │   │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki - 02 [720p][B6C3FE9D].mp4
│   │   │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki - 03 [720p][56E555A4].mp4
│   │   │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki - 04 [720p][A09DA6D8].mp4
│   │   │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki - 05 [720p][B5C1EB49].mp4
│   │   │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki - 06 [720p][0A455929].mp4
│   │   │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki - 07 [720p][9969C992].mp4
│   │   │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki - 08 [720p][79F5BAB8].mp4
│   │   │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki - 09 [720p][3A1281BA].mp4
│   │   │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki - 10 [720p][B5673186].mp4
│   │   │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki - 11 [720p][6B4A6ECC].mp4
│   │   │   └── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki - 12 [720p][C7FDA4AE].mp4
│   │   ├── Giovanni no Shima
│   │   │   └── [animegrimoire] Giovanni no Shima - Movie [720p][6F2A4C01].mp4
│   │   ├── Jahy-sama wa Kujikenai!
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 01 [720p][3E4D1C63].mp4
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 02 [720p][53A72A2E].mp4
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 03 [720p][0A542D9B].mp4
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 04 [720p][1DB51747].mp4
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 05 [720p][7BF42D86].mp4
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 06 [720p][25356737].mp4
│   │   │   ├── [animegrimoire] Jahy-sama wa Kujikenai! - 07 [720p][3D3DA473].mp4
│   │   │   └── [animegrimoire] Jahy-sama wa Kujikenai! - 08 [720p][BAEC2986].mp4
│   │   ├── Kanojo mo Kanojo
│   │   │   ├── [animegrimoire] Kanojo mo Kanojo - 01 [720p][AD96A9BB].mp4
│   │   │   ├── [animegrimoire] Kanojo mo Kanojo - 02 [720p][10BF2ECD].mp4
│   │   │   ├── [animegrimoire] Kanojo mo Kanojo - 03 [720p][DDB6AC50].mp4
│   │   │   ├── [animegrimoire] Kanojo mo Kanojo - 04 [720p][76DA1CAC].mp4
│   │   │   ├── [animegrimoire] Kanojo mo Kanojo - 05 [720p][1A03982A].mp4
│   │   │   ├── [animegrimoire] Kanojo mo Kanojo - 06 [720p][902A6BA0].mp4
│   │   │   ├── [animegrimoire] Kanojo mo Kanojo - 07 [720p][513BF7FA].mp4
│   │   │   ├── [animegrimoire] Kanojo mo Kanojo - 08 [720p][1E9EEF1B].mp4
│   │   │   ├── [animegrimoire] Kanojo mo Kanojo - 09 [720p][9A68D3E2].mp4
│   │   │   ├── [animegrimoire] Kanojo mo Kanojo - 10 [720p][DD49E165].mp4
│   │   │   ├── [animegrimoire] Kanojo mo Kanojo - 11 [720p][084AEF9A].mp4
│   │   │   └── [animegrimoire] Kanojo mo Kanojo - 12 END [720p][940B9B3E].mp4
│   │   ├── Kobayashi-san Chi no Maid Dragon S
│   │   │   ├── [animegrimoire] Kobayashi-san Chi no Maid Dragon S - 01 [720p][DEA45EB2].mp4
│   │   │   ├── [animegrimoire] Kobayashi-san Chi no Maid Dragon S - 02 [720p][BF047E6F].mp4
│   │   │   ├── [animegrimoire] Kobayashi-san Chi no Maid Dragon S - 03 [720p][0B3AF735].mp4
│   │   │   ├── [animegrimoire] Kobayashi-san Chi no Maid Dragon S - 04 [720p][750091A7].mp4
│   │   │   ├── [animegrimoire] Kobayashi-san Chi no Maid Dragon S - 05 [720p][DBACC19B].mp4
│   │   │   ├── [animegrimoire] Kobayashi-san Chi no Maid Dragon S - 06 [720p][E0F41DBD].mp4
│   │   │   ├── [animegrimoire] Kobayashi-san Chi no Maid Dragon S - 07 [720p][017CA6DA].mp4
│   │   │   ├── [animegrimoire] Kobayashi-san Chi no Maid Dragon S - 08 [720p][A09BD7CE].mp4
│   │   │   ├── [animegrimoire] Kobayashi-san Chi no Maid Dragon S - 09 [720p][80E9760A].mp4
│   │   │   ├── [animegrimoire] Kobayashi-san Chi no Maid Dragon S - 10 [720p][EF6F71DF].mp4
│   │   │   ├── [animegrimoire] Kobayashi-san Chi no Maid Dragon S - 11 [720p][28941F83].mp4
│   │   │   └── [animegrimoire] Kobayashi-san Chi no Maid Dragon S - 12 END [720p][01BCAFF7].mp4
│   │   ├── Kono Subarashii Sekai ni Shukufuku wo! 2
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! 2 - 01 [BD720p][DE6D34AE].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! 2 - 02 [BD720p][4A8B812D].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! 2 - 03 [BD720p][1804CE30].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! 2 - 04 [BD720p][D8E182DD].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! 2 - 05 [BD720p][791A7B67].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! 2 - 06 [BD720p][CFB7FAD2].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! 2 - 07 [BD720p][526360F9].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! 2 - 08 [BD720p][1D221B14].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! 2 - 09 [BD720p][BB1BA9F6].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! 2 - 10 [BD720p][4DDE5AA0].mp4
│   │   │   └── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! 2 - 11 [BD720p][7B341626].mp4
│   │   ├── Made in Abyss
│   │   │   └── [animegrimoire] Made in Abyss - Fukaki Tamashii no Reimei Movie - 00 [720p][529B6F04].mp4
│   │   ├── Mahouka Koukou no Yuutousei
│   │   │   ├── [animegrimoire] Mahouka Koukou no Yuutousei - 01 [720p][8ABF1BDA].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Yuutousei - 02 [720p][745C0A25].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Yuutousei - 03 [720p][1DE65A5D].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Yuutousei - 04 [720p][56C86CEA].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Yuutousei - 05 [720p][4D112E4E].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Yuutousei - 06 [720p][6621DC87].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Yuutousei - 07 [720p][1DC4DE35].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Yuutousei - 08 [720p][A3132DAE].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Yuutousei - 09 [720p][B99F2401].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Yuutousei - 10 [720p][6D260FBF].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Yuutousei - 11 [720p][B6656CD3].mp4
│   │   │   └── [animegrimoire] Mahouka Koukou no Yuutousei - 12 [720p][59304DC0].mp4
│   │   ├── Maiko-san Chi no Makanai-san
│   │   │   ├── [animegrimoire] Maiko-san Chi no Makanai-san - 06 [720p][AFDAE025].mp4
│   │   │   ├── [animegrimoire] Maiko-san Chi no Makanai-san - 07 [720p][582DF357].mp4
│   │   │   └── [animegrimoire] Maiko-san Chi no Makanai-san - 08 [720p][23E7C209].mp4
│   │   ├── Megami-ryou no Ryoubo-kun
│   │   │   ├── [animegrimoire] Megami-ryou no Ryoubo-kun - 01 [720p][AA5A5A02].mp4
│   │   │   ├── [animegrimoire] Megami-ryou no Ryoubo-kun - 02 [720p][2687599E].mp4
│   │   │   ├── [animegrimoire] Megami-ryou no Ryoubo-kun - 03 [720p][B7EE4267].mp4
│   │   │   ├── [animegrimoire] Megami-ryou no Ryoubo-kun - 04 [720p][1687CD32].mp4
│   │   │   ├── [animegrimoire] Megami-ryou no Ryoubo-kun - 05 [720p][1C6F859A].mp4
│   │   │   ├── [animegrimoire] Megami-ryou no Ryoubo-kun - 06 [720p][C4981277].mp4
│   │   │   ├── [animegrimoire] Megami-ryou no Ryoubo-kun - 07 [720p][847F77A0].mp4
│   │   │   ├── [animegrimoire] Megami-ryou no Ryoubo-kun - 08 [720p][3A21B2D4].mp4
│   │   │   ├── [animegrimoire] Megami-ryou no Ryoubo-kun - 09 [720p][B110A686].mp4
│   │   │   └── [animegrimoire] Megami-ryou no Ryoubo-kun - 10 END [720p][23E29130].mp4
│   │   ├── Tantei wa Mou Shindeiru
│   │   │   ├── [animegrimoire] Tantei wa Mou Shindeiru - 01 [720p][BCCE1CE5].mp4
│   │   │   ├── [animegrimoire] Tantei wa Mou Shindeiru - 02 [720p][18877536].mp4
│   │   │   ├── [animegrimoire] Tantei wa Mou Shindeiru - 03 [720p][FAD1C36B].mp4
│   │   │   ├── [animegrimoire] Tantei wa Mou Shindeiru - 04 [720p][E920D6F5].mp4
│   │   │   ├── [animegrimoire] Tantei wa Mou Shindeiru - 05 [720p][F6602299].mp4
│   │   │   ├── [animegrimoire] Tantei wa Mou Shindeiru - 06 [720p][7EA4DE38].mp4
│   │   │   ├── [animegrimoire] Tantei wa Mou Shindeiru - 07 [720p][EFF84939].mp4
│   │   │   ├── [animegrimoire] Tantei wa Mou Shindeiru - 08 [720p][3FBB0DC4].mp4
│   │   │   ├── [animegrimoire] Tantei wa Mou Shindeiru - 09 [720p][0C2A232D].mp4
│   │   │   ├── [animegrimoire] Tantei wa Mou Shindeiru - 10 [720p][74C828B9].mp4
│   │   │   ├── [animegrimoire] Tantei wa Mou Shindeiru - 11 [720p][7B265FD8].mp4
│   │   │   └── [animegrimoire] Tantei wa Mou Shindeiru - 12 END [720p][938CBFFB].mp4
│   │   └── Tonikaku Kawaii
│   │       └── [animegrimoire] Tonikaku Kawaii - SNS [720p][E257ED93].mp4
│   └── Winter
│       ├── Azur Lane
│       │   ├── [animegrimoire] Azur Lane - Bisoku Zenshin! - 01 [720p][64552E34].mp4
│       │   ├── [animegrimoire] Azur Lane - Bisoku Zenshin! - 02 [720p][B750D6E6].mp4
│       │   ├── [animegrimoire] Azur Lane - Bisoku Zenshin! - 03 [720p][AAFFD16F].mp4
│       │   ├── [animegrimoire] Azur Lane - Bisoku Zenshin! - 04 [720p][DFE53B68].mp4
│       │   ├── [animegrimoire] Azur Lane - Bisoku Zenshin! - 05 [720p][489E52D1].mp4
│       │   ├── [animegrimoire] Azur Lane - Bisoku Zenshin! - 06 [720p][F74541FB].mp4
│       │   ├── [animegrimoire] Azur Lane - Bisoku Zenshin! - 07 [720p][4FD07C09].mp4
│       │   ├── [animegrimoire] Azur Lane - Bisoku Zenshin! - 08 [720p][39ED0EBC].mp4
│       │   ├── [animegrimoire] Azur Lane - Bisoku Zenshin! - 09 [720p][A33EAB02].mp4
│       │   ├── [animegrimoire] Azur Lane - Bisoku Zenshin! - 10 [720p][C95DF1CC].mp4
│       │   ├── [animegrimoire] Azur Lane - Bisoku Zenshin! - 11 [720p][11644097].mp4
│       │   └── [animegrimoire] Azur Lane - Bisoku Zenshin! - 12 END [720p][FF6DDC40].mp4
│       ├── Dr. Stone
│       │   ├── [animegrimoire] Dr. Stone - Stone Wars - 01 [720p][3BB238F9].mp4
│       │   ├── [animegrimoire] Dr. Stone - Stone Wars - 02 [720p][50EA9C1B].mp4
│       │   ├── [animegrimoire] Dr. Stone - Stone Wars - 03 [720p][C1580BCD].mp4
│       │   ├── [animegrimoire] Dr. Stone - Stone Wars - 04 [720p][D13463A6].mp4
│       │   ├── [animegrimoire] Dr. Stone - Stone Wars - 05 [720p][531942BA].mp4
│       │   ├── [animegrimoire] Dr. Stone - Stone Wars - 06 [720p][8683D15D].mp4
│       │   ├── [animegrimoire] Dr. Stone - Stone Wars - 07 [720p][3A41D3C5].mp4
│       │   ├── [animegrimoire] Dr. Stone - Stone Wars - 08 [720p][F64E9D12].mp4
│       │   ├── [animegrimoire] Dr. Stone - Stone Wars - 09 [720p][B15EF56C].mp4
│       │   ├── [animegrimoire] Dr. Stone - Stone Wars - 10 [720p][8260B022].mp4
│       │   └── [animegrimoire] Dr. Stone - Stone Wars - 11 END [720p][3E58086C].mp4
│       ├── Fate Grand Order
│       │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 13 [720p][74263D81].mp4
│       │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 14 [720p][22E9F43A].mp4
│       │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 15 [720p][1056CFC4].mp4
│       │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 16 [720p][2F022A30].mp4
│       │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 17 [720p][77170AFC].mp4
│       │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 18 [720p][0A7B2DAF].mp4
│       │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 19 [720p][6DF1F5B8].mp4
│       │   ├── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 20 [720p][6B77A00C].mp4
│       │   └── [animegrimoire] Fate Grand Order - Absolute Demonic Front Babylonia - 21 [720p][219F696B].mp4
│       ├── Hataraku Saibou!!
│       │   ├── [animegrimoire] Hataraku Saibou!! - 01 [720p][02D864F9].mp4
│       │   ├── [animegrimoire] Hataraku Saibou!! - 02 [720p][7045DB09].mp4
│       │   ├── [animegrimoire] Hataraku Saibou!! - 03 [720p][7341ADDA].mp4
│       │   ├── [animegrimoire] Hataraku Saibou!! - 04 [720p][6157FC78].mp4
│       │   ├── [animegrimoire] Hataraku Saibou!! - 05 [720p][E5FAD645].mp4
│       │   ├── [animegrimoire] Hataraku Saibou!! - 06 [720p][90CA77CA].mp4
│       │   ├── [animegrimoire] Hataraku Saibou!! - 07 [720p][0B002809].mp4
│       │   └── [animegrimoire] Hataraku Saibou!! - 08 END [720p][2D37CFD6].mp4
│       ├── Hataraku Saibou Black
│       │   ├── [animegrimoire] Hataraku Saibou Black - 01 [720p][0AB4D90A].mp4
│       │   ├── [animegrimoire] Hataraku Saibou Black - 02 [720p][6E54304D].mp4
│       │   ├── [animegrimoire] Hataraku Saibou Black - 03 [720p][207D7EB7].mp4
│       │   ├── [animegrimoire] Hataraku Saibou Black - 04 [720p][D1F8E4E2].mp4
│       │   ├── [animegrimoire] Hataraku Saibou Black - 05 [720p][A6BFEA24].mp4
│       │   ├── [animegrimoire] Hataraku Saibou Black - 06 [720p][97CFDDE6].mp4
│       │   ├── [animegrimoire] Hataraku Saibou Black - 07 [720p][8EE9652C].mp4
│       │   ├── [animegrimoire] Hataraku Saibou Black - 08 [720p][A9D4861F].mp4
│       │   ├── [animegrimoire] Hataraku Saibou Black - 09 [720p][D5899CE7].mp4
│       │   ├── [animegrimoire] Hataraku Saibou Black - 10 [720p][5A497C23].mp4
│       │   ├── [animegrimoire] Hataraku Saibou Black - 11 [720p][1BA99822].mp4
│       │   ├── [animegrimoire] Hataraku Saibou Black - 12 [720p][44D3D7A3].mp4
│       │   └── [animegrimoire] Hataraku Saibou Black - 13 END [720p][0684381D].mp4
│       ├── Jujutsu Kaisen
│       │   ├── [animegrimoire] Jujutsu Kaisen - 14 [720p][3C7693B8].mp4
│       │   ├── [animegrimoire] Jujutsu Kaisen - 15 [720p][987625D0].mp4
│       │   ├── [animegrimoire] Jujutsu Kaisen - 16 [720p][8B0DAB72].mp4
│       │   ├── [animegrimoire] Jujutsu Kaisen - 17 [720p][28C7A46D].mp4
│       │   ├── [animegrimoire] Jujutsu Kaisen - 18 [720p][C2EF0BF9].mp4
│       │   ├── [animegrimoire] Jujutsu Kaisen - 19 [720p][E1BDF75B].mp4
│       │   ├── [animegrimoire] Jujutsu Kaisen - 20 [720p][2C71362E].mp4
│       │   ├── [animegrimoire] Jujutsu Kaisen - 21 [720p][1957E54E].mp4
│       │   ├── [animegrimoire] Jujutsu Kaisen - 22 [720p][088E6A5E].mp4
│       │   ├── [animegrimoire] Jujutsu Kaisen - 23 [720p][A86B896B].mp4
│       │   └── [animegrimoire] Jujutsu Kaisen - 24 END [720p][83F66832].mp4
│       ├── Kono Subarashii Sekai ni Shukufuku wo!
│       │   └── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! - Kurenai Densetsu - Movie [720p][D12CC198].mp4
│       ├── Kujira no Kora wa Sajou ni Utau
│       │   ├── [animegrimoire] Kujira no Kora wa Sajou ni Utau - 01 [720p][551F7D68].mp4
│       │   ├── [animegrimoire] Kujira no Kora wa Sajou ni Utau - 02 [720p][8D1CD0D7].mp4
│       │   ├── [animegrimoire] Kujira no Kora wa Sajou ni Utau - 03 [720p][0BD3E104].mp4
│       │   ├── [animegrimoire] Kujira no Kora wa Sajou ni Utau - 04 [720p][E0638218].mp4
│       │   ├── [animegrimoire] Kujira no Kora wa Sajou ni Utau - 05 [720p][233CA80C].mp4
│       │   ├── [animegrimoire] Kujira no Kora wa Sajou ni Utau - 06 [720p][EFB07EA2].mp4
│       │   ├── [animegrimoire] Kujira no Kora wa Sajou ni Utau - 07 [720p][482E731E].mp4
│       │   ├── [animegrimoire] Kujira no Kora wa Sajou ni Utau - 08 [720p][7FCE242A].mp4
│       │   ├── [animegrimoire] Kujira no Kora wa Sajou ni Utau - 09 [720p][C1B69613].mp4
│       │   ├── [animegrimoire] Kujira no Kora wa Sajou ni Utau - 10 [720p][C108A5A0].mp4
│       │   ├── [animegrimoire] Kujira no Kora wa Sajou ni Utau - 11 [720p][584012B2].mp4
│       │   └── [animegrimoire] Kujira no Kora wa Sajou ni Utau - 12 [720p][362FB106].mp4
│       ├── Log Horizon
│       │   ├── [animegrimoire] Log Horizon - Entaku Houkai - 01 [720p][0790AE89].mp4
│       │   ├── [animegrimoire] Log Horizon - Entaku Houkai - 02 [720p][F5BEFB45].mp4
│       │   ├── [animegrimoire] Log Horizon - Entaku Houkai - 03 [720p][8D7EAA07].mp4
│       │   ├── [animegrimoire] Log Horizon - Entaku Houkai - 04 [720p][242CB8AB].mp4
│       │   ├── [animegrimoire] Log Horizon - Entaku Houkai - 05 [720p][1E96C623].mp4
│       │   ├── [animegrimoire] Log Horizon - Entaku Houkai - 05v2 [720p][0568E5CE].mp4
│       │   ├── [animegrimoire] Log Horizon - Entaku Houkai - 06 [720p][83590260].mp4
│       │   ├── [animegrimoire] Log Horizon - Entaku Houkai - 07 [720p][DA155D68].mp4
│       │   ├── [animegrimoire] Log Horizon - Entaku Houkai - 07v2 [720p][4E3C445E].mp4
│       │   ├── [animegrimoire] Log Horizon - Entaku Houkai - 08 [720p][2C188AC7].mp4
│       │   ├── [animegrimoire] Log Horizon - Entaku Houkai - 09 [720p][D0A840BE].mp4
│       │   ├── [animegrimoire] Log Horizon - Entaku Houkai - 10 [720p][D2AE1C2B].mp4
│       │   ├── [animegrimoire] Log Horizon - Entaku Houkai - 11 [720p][085CF12D].mp4
│       │   └── [animegrimoire] Log Horizon - Entaku Houkai - 12 [720p][43E662CF].mp4
│       ├── Mushoku Tensei
│       │   ├── [animegrimoire] Mushoku Tensei - Isekai Ittara Honki Dasu - 01 [720p][73B98A19].mp4
│       │   ├── [animegrimoire] Mushoku Tensei - Isekai Ittara Honki Dasu - 02 [720p][C822B257].mp4
│       │   ├── [animegrimoire] Mushoku Tensei - Isekai Ittara Honki Dasu - 03 [720p][4A58C7CF].mp4
│       │   ├── [animegrimoire] Mushoku Tensei - Isekai Ittara Honki Dasu - 04 [720p][6CF9127F].mp4
│       │   ├── [animegrimoire] Mushoku Tensei - Isekai Ittara Honki Dasu - 05 [720p][9E2BDF8A].mp4
│       │   ├── [animegrimoire] Mushoku Tensei - Isekai Ittara Honki Dasu - 06v2 [720p][7B3A2656].mp4
│       │   ├── [animegrimoire] Mushoku Tensei - Isekai Ittara Honki Dasu - 07 [720p][FFF598FD].mp4
│       │   ├── [animegrimoire] Mushoku Tensei - Isekai Ittara Honki Dasu - 08 [720p][0581A2E9].mp4
│       │   ├── [animegrimoire] Mushoku Tensei - Isekai Ittara Honki Dasu - 09 [720p][0CD0F735].mp4
│       │   ├── [animegrimoire] Mushoku Tensei - Isekai Ittara Honki Dasu - 10 [720p][004638E2].mp4
│       │   └── [animegrimoire] Mushoku Tensei - Isekai Ittara Honki Dasu - 11 END [720p][556BA1CB].mp4
│       ├── Non Non Biyori Nonstop
│       │   ├── [animegrimoire] Non Non Biyori Nonstop - 01 [720p][8A898CDB].mp4
│       │   ├── [animegrimoire] Non Non Biyori Nonstop - 02 [720p][202995B6].mp4
│       │   ├── [animegrimoire] Non Non Biyori Nonstop - 03 [720p][498241CF].mp4
│       │   ├── [animegrimoire] Non Non Biyori Nonstop - 04 [720p][774ED94E].mp4
│       │   ├── [animegrimoire] Non Non Biyori Nonstop - 05 [720p][1D012F14].mp4
│       │   ├── [animegrimoire] Non Non Biyori Nonstop - 06 [720p][167277F5].mp4
│       │   ├── [animegrimoire] Non Non Biyori Nonstop - 07 [720p][485FEA48].mp4
│       │   ├── [animegrimoire] Non Non Biyori Nonstop - 08 [720p][E84A63FB].mp4
│       │   ├── [animegrimoire] Non Non Biyori Nonstop - 09 [720p][7185151B].mp4
│       │   ├── [animegrimoire] Non Non Biyori Nonstop - 10 [720p][715B8018].mp4
│       │   ├── [animegrimoire] Non Non Biyori Nonstop - 11 [720p][F11EE878].mp4
│       │   ├── [animegrimoire] Non Non Biyori Nonstop - 12 END [720p][3F2EABB7].mp4
│       │   └── [animegrimoire] Non Non Biyori Repeat - 13 [BD720p][3583C1F5].mp4
│       ├── Re.Zero kara Hajimeru Isekai Seikatsu 2nd Season Part 2
│       │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu 2nd Season Part 2 - 01 [720p][ECFAF222].mp4
│       │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu 2nd Season Part 2 - 02 [720p][D0774E58].mp4
│       │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu 2nd Season Part 2 - 03 [720p][F6E6A87E].mp4
│       │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu 2nd Season Part 2 - 04 [720p][1430AD4D].mp4
│       │   ├── [animegrimoire] Re.Zero kara Hajimeru Isekai Seikatsu 2nd Season Part 2 - 05 [720p][876FBE81].mp4
│       │   ├── [animegrimoire] Re.Zero kara Hajimeru Isekai Seikatsu 2nd Season Part 2 - 06 [720p][74E81F60].mp4
│       │   ├── [animegrimoire] Re.Zero kara Hajimeru Isekai Seikatsu 2nd Season Part 2 - 07 [720p][9E9F4CA1].mp4
│       │   ├── [animegrimoire] Re.Zero kara Hajimeru Isekai Seikatsu 2nd Season Part 2 - 08 [720p][E108032A].mp4
│       │   ├── [animegrimoire] Re.Zero kara Hajimeru Isekai Seikatsu 2nd Season Part 2 - 09 [720p][9D51609F].mp4
│       │   ├── [animegrimoire] Re.Zero kara Hajimeru Isekai Seikatsu 2nd Season Part 2 - 10 [720p][54724C12].mp4
│       │   ├── [animegrimoire] Re.Zero kara Hajimeru Isekai Seikatsu 2nd Season Part 2 - 11 [720p][4497C200].mp4
│       │   └── [animegrimoire] Re.Zero kara Hajimeru Isekai Seikatsu 2nd Season Part 2 - 12 END [720p][3AC02EE7].mp4
│       ├── Shingeki no Kyojin
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season - 06 [720p][A1051BD1].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season - 07 [720p][833FBC4A].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season - 08 [720p][3D7918EC].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season - 09 [720p][025F6A0F].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season - 10 [720p][6EEFBBEC].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season - 11 [720p][0F58A08B].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season - 12 [720p][802A2538].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season - 13 [720p][C705A91D].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season - 14 [720p][6A5ABCC9].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season - 15 [720p][B8CFCED9].mp4
│       │   └── [animegrimoire] Shingeki no Kyojin - The Final Season - 16 END [720p][2C7B161D].mp4
│       ├── Show by Rock!! Stars!!
│       │   ├── [animegrimoire] Show by Rock!! Stars!! - 01 [720p][0B906FA7].mp4
│       │   ├── [animegrimoire] Show by Rock!! Stars!! - 02 [720p][A993F668].mp4
│       │   ├── [animegrimoire] Show by Rock!! Stars!! - 03 [720p][1A595B65].mp4
│       │   ├── [animegrimoire] Show by Rock!! Stars!! - 04 [720p][A990D65C].mp4
│       │   ├── [animegrimoire] Show by Rock!! Stars!! - 05 [720p][73B4A868].mp4
│       │   ├── [animegrimoire] Show by Rock!! Stars!! - 06 [720p][96D20793].mp4
│       │   ├── [animegrimoire] Show by Rock!! Stars!! - 07 [720p][3303C4A1].mp4
│       │   ├── [animegrimoire] Show by Rock!! Stars!! - 08 [720p][0218EF23].mp4
│       │   ├── [animegrimoire] Show by Rock!! Stars!! - 09 [720p][A687060A].mp4
│       │   ├── [animegrimoire] Show by Rock!! Stars!! - 10 [720p][C3CBBD38].mp4
│       │   ├── [animegrimoire] Show by Rock!! Stars!! - 11 [720p][402E8A19].mp4
│       │   └── [animegrimoire] Show by Rock!! Stars!! - 12 END [720p][4CE8C03B].mp4
│       ├── Ura Sekai Picnic
│       │   ├── [animegrimoire] Ura Sekai Picnic - 01 [720p][C0C37B05].mp4
│       │   ├── [animegrimoire] Ura Sekai Picnic - 02 [720p][83F32A37].mp4
│       │   ├── [animegrimoire] Ura Sekai Picnic - 03 [720p][F85C3B87].mp4
│       │   ├── [animegrimoire] Ura Sekai Picnic - 04 [720p][A7154E11].mp4
│       │   ├── [animegrimoire] Ura Sekai Picnic - 05 [720p][94B0D7A0].mp4
│       │   ├── [animegrimoire] Ura Sekai Picnic - 06 [720p][0F01430C].mp4
│       │   ├── [animegrimoire] Ura Sekai Picnic - 07 [720p][E171C2BB].mp4
│       │   ├── [animegrimoire] Ura Sekai Picnic - 08 [720p][4AF9833F].mp4
│       │   ├── [animegrimoire] Ura Sekai Picnic - 09 [720p][83B6BDE5].mp4
│       │   ├── [animegrimoire] Ura Sekai Picnic - 10 [720p][FEA0C8D2].mp4
│       │   ├── [animegrimoire] Ura Sekai Picnic - 11 [720p][FD751F35].mp4
│       │   └── [animegrimoire] Ura Sekai Picnic - 12 END [720p][31444B0B].mp4
│       ├── World Trigger 2nd Season
│       │   ├── [animegrimoire] World Trigger 2nd Season - 01 [720p][19CB4C8E].mp4
│       │   ├── [animegrimoire] World Trigger 2nd Season - 02 [720p][5B6E4886].mp4
│       │   ├── [animegrimoire] World Trigger 2nd Season - 03 [720p][649FB221].mp4
│       │   ├── [animegrimoire] World Trigger 2nd Season - 04 [720p][4967C384].mp4
│       │   ├── [animegrimoire] World Trigger 2nd Season - 05 [720p][D5BA93B2].mp4
│       │   ├── [animegrimoire] World Trigger 2nd Season - 06 [720p][1AA6B494].mp4
│       │   ├── [animegrimoire] World Trigger 2nd Season - 07 [720p][12B8EB3F].mp4
│       │   ├── [animegrimoire] World Trigger 2nd Season - 08 [720p][1515B31C].mp4
│       │   ├── [animegrimoire] World Trigger 2nd Season - 09 [720p][81026773].mp4
│       │   ├── [animegrimoire] World Trigger 2nd Season - 10 [720p][FC8DB975].mp4
│       │   ├── [animegrimoire] World Trigger 2nd Season - 11 [720p][7B3A7CFD].mp4
│       │   └── [animegrimoire] World Trigger 2nd Season - 12 [720p][B47694F1].mp4
│       ├── World Witches Hasshin Shimasu!
│       │   ├── [animegrimoire] World Witches Hasshin Shimasu! - 01 [720p][9C0CFDDE].mp4
│       │   ├── [animegrimoire] World Witches Hasshin Shimasu! - 02 [720p][BD346AED].mp4
│       │   ├── [animegrimoire] World Witches Hasshin Shimasu! - 03 [720p][954F54ED].mp4
│       │   ├── [animegrimoire] World Witches Hasshin Shimasu! - 04 [720p][A95EE5C1].mp4
│       │   ├── [animegrimoire] World Witches Hasshin Shimasu! - 05 [720p][8221EB34].mp4
│       │   ├── [animegrimoire] World Witches Hasshin Shimasu! - 06 [720p][880656B1].mp4
│       │   ├── [animegrimoire] World Witches Hasshin Shimasu! - 07 [720p][CA30206A].mp4
│       │   ├── [animegrimoire] World Witches Hasshin Shimasu! - 08 [720p][A7B8B2E4].mp4
│       │   ├── [animegrimoire] World Witches Hasshin Shimasu! - 09 [720p][5AB61B45].mp4
│       │   ├── [animegrimoire] World Witches Hasshin Shimasu! - 10 [720p][3A8C9707].mp4
│       │   ├── [animegrimoire] World Witches Hasshin Shimasu! - 11 [720p][FB33F6F6].mp4
│       │   ├── [animegrimoire] World Witches Hasshin Shimasu! - 12 END [720p][768DF7CF].mp4
│       │   └── [animegrimoire] World Witches Hasshin Shimasu! - 12v2 END [720p][E838B2B4].mp4
│       └── Yuru Camp Season 2
│           ├── [animegrimoire] Yuru Camp Season 2 - 01 [720p][6BE8BE14].mp4
│           ├── [animegrimoire] Yuru Camp Season 2 - 02 [720p][098A24E9].mp4
│           ├── [animegrimoire] Yuru Camp Season 2 - 03 [720p][618F2A14].mp4
│           ├── [animegrimoire] Yuru Camp Season 2 - 04 [720p][C0468607].mp4
│           ├── [animegrimoire] Yuru Camp Season 2 - 05 [720p][8E874D06].mp4
│           ├── [animegrimoire] Yuru Camp Season 2 - 06 [720p][E289D55D].mp4
│           ├── [animegrimoire] Yuru Camp Season 2 - 07 [720p][A5489E24].mp4
│           ├── [animegrimoire] Yuru Camp Season 2 - 08 [720p][A7EC8121].mp4
│           ├── [animegrimoire] Yuru Camp Season 2 - 09 [720p][4E3C9E94].mp4
│           ├── [animegrimoire] Yuru Camp Season 2 - 10 [720p][A6B844A7].mp4
│           ├── [animegrimoire] Yuru Camp Season 2 - 11 [720p][2B5497DD].mp4
│           ├── [animegrimoire] Yuru Camp Season 2 - 12 [720p][4625825C].mp4
│           └── [animegrimoire] Yuru Camp Season 2 - 13 END [720p][36D6A938].mp4
├── 2022
│   ├── Fall
│   │   ├── Bocchi the Rock!
│   │   │   ├── [animegrimoire] Bocchi the Rock! - 01 [720p][D1D9C919].mp4
│   │   │   ├── [animegrimoire] Bocchi the Rock! - 02 [720p][C9FC74E7].mp4
│   │   │   ├── [animegrimoire] Bocchi the Rock! - 03 [720p][5305D214].mp4
│   │   │   ├── [animegrimoire] Bocchi the Rock! - 04 [720p][176E7AFF].mp4
│   │   │   ├── [animegrimoire] Bocchi the Rock! - 05 [720p][0BF07F3A].mp4
│   │   │   ├── [animegrimoire] Bocchi the Rock! - 06 [720p][8616EA2A].mp4
│   │   │   ├── [animegrimoire] Bocchi the Rock! - 07 [720p][EEDCAC63].mp4
│   │   │   ├── [animegrimoire] Bocchi the Rock! - 08 [720p][3F98C16B].mp4
│   │   │   ├── [animegrimoire] Bocchi the Rock! - 09 [720p][35C77187].mp4
│   │   │   ├── [animegrimoire] Bocchi the Rock! - 10 [720p][144C0F3B].mp4
│   │   │   ├── [animegrimoire] Bocchi the Rock! - 11 [720p][20C1662F].mp4
│   │   │   └── [animegrimoire] Bocchi the Rock! - 12 [720p][B8B911A9].mp4
│   │   ├── Do It Yourself!!
│   │   │   ├── [animegrimoire] Do It Yourself!! - 01 [720p][8D25572B].mp4
│   │   │   ├── [animegrimoire] Do It Yourself!! - 02 [720p][157E3546].mp4
│   │   │   ├── [animegrimoire] Do It Yourself!! - 03 [720p][3C0E4987].mp4
│   │   │   ├── [animegrimoire] Do It Yourself!! - 04 [720p][D449538F].mp4
│   │   │   ├── [animegrimoire] Do It Yourself!! - 05 [720p][E882B097].mp4
│   │   │   ├── [animegrimoire] Do It Yourself!! - 06 [720p][72C0BF19].mp4
│   │   │   ├── [animegrimoire] Do It Yourself!! - 07 [720p][6525F6F8].mp4
│   │   │   ├── [animegrimoire] Do It Yourself!! - 08 [720p][B9301F44].mp4
│   │   │   ├── [animegrimoire] Do It Yourself!! - 09 [720p][DBC036CF].mp4
│   │   │   ├── [animegrimoire] Do It Yourself!! - 10 [720p][674A6014].mp4
│   │   │   ├── [animegrimoire] Do It Yourself!! - 11 [720p][DFD12E39].mp4
│   │   │   └── [animegrimoire] Do It Yourself!! - 12 [720p][356E8501].mp4
│   │   ├── Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 12 [720p][2FD0D1CE].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 13 [720p][170D64CA].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 14 [720p][FEF56D49].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 15 [720p][38AF47BF].mp4
│   │   │   └── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 16 [720p][7B72EE02].mp4
│   │   ├── Golden Kamuy
│   │   │   ├── [animegrimoire] Golden Kamuy - 37 [720p][CBF094AB].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 38 [720p][B5FBC4D5].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 39 [720p][03FA8147].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 40 [720p][B3F5A112].mp4
│   │   │   ├── [animegrimoire] Golden Kamuy - 41 [720p][8A077A5F].mp4
│   │   │   └── [animegrimoire] Golden Kamuy - 42 [720p][09A69BCF].mp4
│   │   ├── Pop Team Epic S2
│   │   │   ├── [animegrimoire] Pop Team Epic S2 - 01 [720p][2195090F].mp4
│   │   │   ├── [animegrimoire] Pop Team Epic S2 - 02 [720p][BDDDD114].mp4
│   │   │   ├── [animegrimoire] Pop Team Epic S2 - 03 [720p][2035F814].mp4
│   │   │   ├── [animegrimoire] Pop Team Epic S2 - 04 [720p][4E0E7B48].mp4
│   │   │   ├── [animegrimoire] Pop Team Epic S2 - 05 [720p][F8E6964C].mp4
│   │   │   ├── [animegrimoire] Pop Team Epic S2 - 06 [720p][3A7BBDE7].mp4
│   │   │   ├── [animegrimoire] Pop Team Epic S2 - 06v2 [720p][8A2B2623].mp4
│   │   │   ├── [animegrimoire] Pop Team Epic S2 - 07 [720p][9946881E].mp4
│   │   │   ├── [animegrimoire] Pop Team Epic S2 - 08 [720p][155999C0].mp4
│   │   │   ├── [animegrimoire] Pop Team Epic S2 - 09 [720p][1009B33F].mp4
│   │   │   ├── [animegrimoire] Pop Team Epic S2 - 10 [720p][BC4AB59F].mp4
│   │   │   ├── [animegrimoire] Pop Team Epic S2 - 11 [720p][4A78C647].mp4
│   │   │   └── [animegrimoire] Pop Team Epic S2 - 12 [720p][D348BFAA].mp4
│   │   ├── Shinmai Renkinjutsushi no Tenpo Keiei
│   │   │   ├── [animegrimoire] Shinmai Renkinjutsushi no Tenpo Keiei - 01 [720p][DEC2B536].mp4
│   │   │   ├── [animegrimoire] Shinmai Renkinjutsushi no Tenpo Keiei - 02 [720p][B870ABFE].mp4
│   │   │   ├── [animegrimoire] Shinmai Renkinjutsushi no Tenpo Keiei - 03 [720p][5FD9E392].mp4
│   │   │   ├── [animegrimoire] Shinmai Renkinjutsushi no Tenpo Keiei - 04 [720p][9C2DCBDB].mp4
│   │   │   ├── [animegrimoire] Shinmai Renkinjutsushi no Tenpo Keiei - 05 [720p][3A7777B5].mp4
│   │   │   ├── [animegrimoire] Shinmai Renkinjutsushi no Tenpo Keiei - 06 [720p][9AB205F9].mp4
│   │   │   ├── [animegrimoire] Shinmai Renkinjutsushi no Tenpo Keiei - 07 [720p][3713B5CA].mp4
│   │   │   ├── [animegrimoire] Shinmai Renkinjutsushi no Tenpo Keiei - 08 [720p][4413638C].mp4
│   │   │   ├── [animegrimoire] Shinmai Renkinjutsushi no Tenpo Keiei - 09 [720p][05CFB543].mp4
│   │   │   ├── [animegrimoire] Shinmai Renkinjutsushi no Tenpo Keiei - 10 [720p][B677CD5A].mp4
│   │   │   ├── [animegrimoire] Shinmai Renkinjutsushi no Tenpo Keiei - 11 [720p][2DE757DF].mp4
│   │   │   └── [animegrimoire] Shinmai Renkinjutsushi no Tenpo Keiei - 12 [720p][21505F63].mp4
│   │   ├── Spy x Family
│   │   │   ├── [animegrimoire] Spy x Family - 13 [720p][478AEAFE].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 14 [720p][E496C83C].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 15 [720p][A9D36740].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 16 [720p][8355BE07].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 17 [720p][07ACE06B].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 18 [720p][5A7D1AA0].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 19 [720p][AB92FCC9].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 20 [720p][747E3AEA].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 21 [720p][5C68C200].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 22 [720p][853D5391].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 23 [720p][C7C6070B].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 24 [720p][5AB388D5].mp4
│   │   │   └── [animegrimoire] Spy x Family - 25 [720p][DCFC7C01].mp4
│   │   └── Yama no Susume
│   │       ├── [animegrimoire] Yama no Susume - Next Summit - 01 [720p][4C8F7DD5].mp4
│   │       ├── [animegrimoire] Yama no Susume - Next Summit - 02 [720p][8F84472E].mp4
│   │       ├── [animegrimoire] Yama no Susume - Next Summit - 03 [720p][8E4715FF].mp4
│   │       ├── [animegrimoire] Yama no Susume - Next Summit - 04 [720p][58A706A5].mp4
│   │       ├── [animegrimoire] Yama no Susume - Next Summit - 05 [720p][A4932281].mp4
│   │       ├── [animegrimoire] Yama no Susume - Next Summit - 06 [720p][EA6279F7].mp4
│   │       ├── [animegrimoire] Yama no Susume - Next Summit - 07 [720p][9318589B].mp4
│   │       ├── [animegrimoire] Yama no Susume - Next Summit - 08 [720p][69BD230F].mp4
│   │       ├── [animegrimoire] Yama no Susume - Next Summit - 09 [720p][3750BF9E].mp4
│   │       ├── [animegrimoire] Yama no Susume - Next Summit - 10 [720p][826235F9].mp4
│   │       ├── [animegrimoire] Yama no Susume - Next Summit - 11 [720p][7ED357CF].mp4
│   │       └── [animegrimoire] Yama no Susume - Next Summit - 12 [720p][D0DEED6A].mp4
│   ├── Spring
│   │   ├── Aharen-san wa Hakarenai
│   │   │   ├── [animegrimoire] Aharen-san wa Hakarenai - 01 [720p][3491A885].mp4
│   │   │   ├── [animegrimoire] Aharen-san wa Hakarenai - 02 [720p][43045E18].mp4
│   │   │   ├── [animegrimoire] Aharen-san wa Hakarenai - 03 [720p][F5059F6D].mp4
│   │   │   ├── [animegrimoire] Aharen-san wa Hakarenai - 04 [720p][C70EDA3B].mp4
│   │   │   ├── [animegrimoire] Aharen-san wa Hakarenai - 05 [720p][E22927B2].mp4
│   │   │   ├── [animegrimoire] Aharen-san wa Hakarenai - 06 [720p][87E2E5B2].mp4
│   │   │   ├── [animegrimoire] Aharen-san wa Hakarenai - 07 [720p][3BB6C769].mp4
│   │   │   ├── [animegrimoire] Aharen-san wa Hakarenai - 08 [720p][FE0D9ECA].mp4
│   │   │   ├── [animegrimoire] Aharen-san wa Hakarenai - 09 [720p][10D4E84B].mp4
│   │   │   ├── [animegrimoire] Aharen-san wa Hakarenai - 10 [720p][1FE9DB47].mp4
│   │   │   ├── [animegrimoire] Aharen-san wa Hakarenai - 11 [720p][5231AD6D].mp4
│   │   │   └── [animegrimoire] Aharen-san wa Hakarenai - 12 [720p][E50129A7].mp4
│   │   ├── Date a Live IV
│   │   │   ├── [animegrimoire] Date a Live IV - 01 [720p][FED71300].mp4
│   │   │   ├── [animegrimoire] Date a Live IV - 02 [720p][F52A5526].mp4
│   │   │   ├── [animegrimoire] Date a Live IV - 03 [720p][E0A7CC61].mp4
│   │   │   ├── [animegrimoire] Date a Live IV - 04 [720p][17E72F95].mp4
│   │   │   ├── [animegrimoire] Date a Live IV - 05 [720p][461B21B7].mp4
│   │   │   ├── [animegrimoire] Date a Live IV - 06 [720p][7226742A].mp4
│   │   │   ├── [animegrimoire] Date a Live IV - 07 [720p][85D9E528].mp4
│   │   │   ├── [animegrimoire] Date a Live IV - 08 [720p][1656E7D7].mp4
│   │   │   ├── [animegrimoire] Date a Live IV - 09 [720p][C2303D0F].mp4
│   │   │   ├── [animegrimoire] Date a Live IV - 10 [720p][96555D97].mp4
│   │   │   ├── [animegrimoire] Date a Live IV - 10 [720p][C287D5A0].mp4
│   │   │   ├── [animegrimoire] Date a Live IV - 11 [720p][E1CE849C].mp4
│   │   │   └── [animegrimoire] Date a Live IV - 12 [720p][58FBA66E].mp4
│   │   ├── Estab-Life
│   │   │   ├── [animegrimoire] Estab-Life - Great Escape - 01 [720p][20FAAC06].mp4
│   │   │   ├── [animegrimoire] Estab-Life - Great Escape - 02 [720p][E54DC36B].mp4
│   │   │   ├── [animegrimoire] Estab-Life - Great Escape - 03 [720p][11897CD6].mp4
│   │   │   ├── [animegrimoire] Estab-Life - Great Escape - 04 [720p][4F95B9C7].mp4
│   │   │   ├── [animegrimoire] Estab-Life - Great Escape - 05 [720p][47A29D91].mp4
│   │   │   ├── [animegrimoire] Estab-Life - Great Escape - 06 [720p][3A8A9304].mp4
│   │   │   ├── [animegrimoire] Estab-Life - Great Escape - 07 [720p][D867B185].mp4
│   │   │   ├── [animegrimoire] Estab-Life - Great Escape - 08 [720p][0A69A016].mp4
│   │   │   ├── [animegrimoire] Estab-Life - Great Escape - 09 [720p][1C89A95D].mp4
│   │   │   ├── [animegrimoire] Estab-Life - Great Escape - 10 [720p][B2DC578B].mp4
│   │   │   ├── [animegrimoire] Estab-Life - Great Escape - 11 [720p][6200A0C5].mp4
│   │   │   └── [animegrimoire] Estab-Life - Great Escape - 12 [720p][85E16650].mp4
│   │   ├── Healer Girl
│   │   │   ├── [animegrimoire] Healer Girl - 01 [720p][CFC7A741].mp4
│   │   │   ├── [animegrimoire] Healer Girl - 02 [720p][CAE0694D].mp4
│   │   │   ├── [animegrimoire] Healer Girl - 03 [720p][3E6EA450].mp4
│   │   │   ├── [animegrimoire] Healer Girl - 04 [720p][9E8F5FBE].mp4
│   │   │   ├── [animegrimoire] Healer Girl - 05 [720p][9475E227].mp4
│   │   │   ├── [animegrimoire] Healer Girl - 06 [720p][8A9BD218].mp4
│   │   │   ├── [animegrimoire] Healer Girl - 07 [720p][8955CA1B].mp4
│   │   │   ├── [animegrimoire] Healer Girl - 08 [720p][0E1F40AE].mp4
│   │   │   ├── [animegrimoire] Healer Girl - 09 [720p][DFB80857].mp4
│   │   │   ├── [animegrimoire] Healer Girl - 10 [720p][4BA8809C].mp4
│   │   │   ├── [animegrimoire] Healer Girl - 10 [720p][F04A7D89].mp4
│   │   │   ├── [animegrimoire] Healer Girl - 11 [720p][748C09FA].mp4
│   │   │   └── [animegrimoire] Healer Girl - 12 [720p][AF50AB5A].mp4
│   │   ├── Honzuki no Gekokujou
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 27 [720p][56892C1A].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 28 [720p][00E1707F].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 29 [720p][83441A1F].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 30 [720p][9509E755].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 31 [720p][A00A613B].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 32 [720p][B6FFD3B1].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 33 [720p][DE1F62C6].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 34 [720p][BA567A3E].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 35 [720p][0C9388BE].mp4
│   │   │   ├── [animegrimoire] Honzuki no Gekokujou - 35 [720p][F456D657].mp4
│   │   │   └── [animegrimoire] Honzuki no Gekokujou - 36 [720p][1041A59A].mp4
│   │   ├── Kaguya-sama wa Kokurasetai S3
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S3 - 01 [720p][764617D1].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S3 - 02 [720p][BCA70E92].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S3 - 03 [720p][70F78921].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S3 - 04 [720p][5B784A9B].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S3 - 05 [720p][7289BC8A].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S3 - 06 [720p][5A0C56D0].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S3 - 07 [720p][F6B709C8].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S3 - 08 [720p][580120C6].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S3 - 09 [720p][4E434029].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S3 - 10 [720p][9DC2F805].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S3 - 11 [720p][5565BE37].mp4
│   │   │   ├── [animegrimoire] Kaguya-sama wa Kokurasetai S3 - 12 [720p][AB48DCCF].mp4
│   │   │   └── [animegrimoire] Kaguya-sama wa Kokurasetai S3 - 13 [720p][840B524F].mp4
│   │   ├── Kawaii dake ja Nai Shikimori-san
│   │   │   ├── [animegrimoire] Kawaii dake ja Nai Shikimori-san - 01 [720p][25519339].mp4
│   │   │   ├── [animegrimoire] Kawaii dake ja Nai Shikimori-san - 02 [720p][47CC3F97].mp4
│   │   │   ├── [animegrimoire] Kawaii dake ja Nai Shikimori-san - 03 [720p][CA8FAC06].mp4
│   │   │   ├── [animegrimoire] Kawaii dake ja Nai Shikimori-san - 04 [720p][F4784E2B].mp4
│   │   │   ├── [animegrimoire] Kawaii dake ja Nai Shikimori-san - 05 [720p][33B55DBB].mp4
│   │   │   ├── [animegrimoire] Kawaii dake ja Nai Shikimori-san - 06 [720p][DA46AFE6].mp4
│   │   │   ├── [animegrimoire] Kawaii dake ja Nai Shikimori-san - 07 [720p][B124B02C].mp4
│   │   │   ├── [animegrimoire] Kawaii dake ja Nai Shikimori-san - 08 [720p][34C04BCE].mp4
│   │   │   ├── [animegrimoire] Kawaii dake ja Nai Shikimori-san - 09 [720p][9B765F0D].mp4
│   │   │   ├── [animegrimoire] Kawaii dake ja Nai Shikimori-san - 10 [720p][839CF5C1].mp4
│   │   │   └── [animegrimoire] Kawaii dake ja Nai Shikimori-san - 11 [720p][19F030EE].mp4
│   │   ├── Kobayashi-san Chi no Maid Dragon S2
│   │   │   └── [animegrimoire] Kobayashi-san Chi no Maid Dragon S2 - 13 [720p][CF99C5F6].mp4
│   │   ├── Machikado Mazoku S2
│   │   │   ├── [animegrimoire] Machikado Mazoku S2 - 01 [720p][7B9C3834].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku S2 - 02 [720p][34A193BE].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku S2 - 03 [720p][6A87D5DF].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku S2 - 04 [720p][5BF7F63B].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku S2 - 05 [720p][72266BAB].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku S2 - 06 [720p][18BBD8D9].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku S2 - 07 [720p][5FAD395E].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku S2 - 08 [720p][90447A4D].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku S2 - 09 [720p][3E388834].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku S2 - 10 [720p][2612E15E].mp4
│   │   │   ├── [animegrimoire] Machikado Mazoku S2 - 11 [720p][4B845BD8].mp4
│   │   │   └── [animegrimoire] Machikado Mazoku S2 - 12 [720p][29C78B11].mp4
│   │   ├── RPG Fudousan
│   │   │   ├── [animegrimoire] RPG Fudousan - 01 [720p][026BAF11].mp4
│   │   │   ├── [animegrimoire] RPG Fudousan - 02 [720p][46722694].mp4
│   │   │   ├── [animegrimoire] RPG Fudousan - 03 [720p][EA216157].mp4
│   │   │   ├── [animegrimoire] RPG Fudousan - 04 [720p][43007447].mp4
│   │   │   ├── [animegrimoire] RPG Fudousan - 05 [720p][882F6786].mp4
│   │   │   ├── [animegrimoire] RPG Fudousan - 06 [720p][C577A855].mp4
│   │   │   ├── [animegrimoire] RPG Fudousan - 07 [720p][FDA2A954].mp4
│   │   │   ├── [animegrimoire] RPG Fudousan - 08 [720p][36D94955].mp4
│   │   │   ├── [animegrimoire] RPG Fudousan - 09 [720p][17D88757].mp4
│   │   │   ├── [animegrimoire] RPG Fudousan - 10 [720p][E6AF07A2].mp4
│   │   │   ├── [animegrimoire] RPG Fudousan - 11 [720p][6B5AE7E4].mp4
│   │   │   └── [animegrimoire] RPG Fudousan - 12 [720p][8B4F5E53].mp4
│   │   ├── Spy x Family
│   │   │   ├── [animegrimoire] Spy x Family - 01 [720p][EDB51050].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 02 [720p][2065DE22].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 03 [720p][60F00EA2].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 04 [720p][21945267].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 05 [720p][BE481AEF].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 06 [720p][48BA9947].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 07 [720p][A38DB657].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 08 [720p][1A7FDF2E].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 09 [720p][605EE124].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 10 [720p][9C4C3E85].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 11 [720p][22C2CCEF].mp4
│   │   │   └── [animegrimoire] Spy x Family - 12 [720p][CB593306].mp4
│   │   └── Tate no Yuusha no Nariagari S2
│   │       ├── [animegrimoire] Tate no Yuusha no Nariagari S2 - 01 [720p][AE13EA7A].mp4
│   │       ├── [animegrimoire] Tate no Yuusha no Nariagari S2 - 02 [720p][AE3A0B01].mp4
│   │       ├── [animegrimoire] Tate no Yuusha no Nariagari S2 - 03 [720p][0F46267D].mp4
│   │       ├── [animegrimoire] Tate no Yuusha no Nariagari S2 - 04 [720p][8970E978].mp4
│   │       ├── [animegrimoire] Tate no Yuusha no Nariagari S2 - 05 [720p][744E780C].mp4
│   │       ├── [animegrimoire] Tate no Yuusha no Nariagari S2 - 06 [720p][F95CBB08].mp4
│   │       ├── [animegrimoire] Tate no Yuusha no Nariagari S2 - 07 [720p][E6B68049].mp4
│   │       ├── [animegrimoire] Tate no Yuusha no Nariagari S2 - 08 [720p][A8E277A7].mp4
│   │       ├── [animegrimoire] Tate no Yuusha no Nariagari S2 - 09 [720p][7CDE996A].mp4
│   │       ├── [animegrimoire] Tate no Yuusha no Nariagari S2 - 10 [720p][8D1DE2C7].mp4
│   │       ├── [animegrimoire] Tate no Yuusha no Nariagari S2 - 11 [720p][464AE817].mp4
│   │       ├── [animegrimoire] Tate no Yuusha no Nariagari S2 - 12 [720p][4609B32D].mp4
│   │       └── [animegrimoire] Tate no Yuusha no Nariagari S2 - 13 [720p][6C2030CB].mp4
│   ├── Summer
│   │   ├── Dr Stone
│   │   │   └── [animegrimoire] Dr. Stone - Ryuusui - 01 [720p][1B511095].mp4
│   │   ├── Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 01 [720p][8EA1CB14].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 02 [720p][BFA5F745].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 03 [720p][E6EB0036].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 04 [720p][7E921BCB].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 05 [720p][B917E2FE].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 06 [720p][900A47C2].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 07 [720p][A974CA6C].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 08 [720p][14946FFC].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 09 [720p][ABDB1B4F].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 10 [720p][9761F7A0].mp4
│   │   │   └── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 11 [720p][1DC5FB77].mp4
│   │   ├── Isekai Yakkyoku
│   │   │   ├── [animegrimoire] Isekai Yakkyoku - 01 [720p][54A2EDA8].mp4
│   │   │   ├── [animegrimoire] Isekai Yakkyoku - 02 [720p][81C955CE].mp4
│   │   │   ├── [animegrimoire] Isekai Yakkyoku - 03 [720p][BFDCED80].mp4
│   │   │   ├── [animegrimoire] Isekai Yakkyoku - 04 [720p][3CC37D3F].mp4
│   │   │   ├── [animegrimoire] Isekai Yakkyoku - 05 [720p][E8955855].mp4
│   │   │   ├── [animegrimoire] Isekai Yakkyoku - 06 [720p][4E9DC526].mp4
│   │   │   ├── [animegrimoire] Isekai Yakkyoku - 07 [720p][9C73AF40].mp4
│   │   │   ├── [animegrimoire] Isekai Yakkyoku - 08 [720p][47F0EF91].mp4
│   │   │   ├── [animegrimoire] Isekai Yakkyoku - 09 [720p][8D97A475].mp4
│   │   │   ├── [animegrimoire] Isekai Yakkyoku - 10 [720p][9DCD220D].mp4
│   │   │   ├── [animegrimoire] Isekai Yakkyoku - 11 [720p][2320066B].mp4
│   │   │   └── [animegrimoire] Isekai Yakkyoku - 12 [720p][6512F6DB].mp4
│   │   ├── Jashin-chan Dropkick X
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick X - 01 [720p][0CD57AA8].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick X - 02 [720p][499402D8].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick X - 03 [720p][0A9C12EB].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick X - 04 [720p][D1A1A921].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick X - 05 [720p][66B49A82].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick X - 06 [720p][ABB87B89].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick X - 07 [720p][A2D2E223].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick X - 08 [720p][29514169].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick X - 09 [720p][19A43687].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick X - 10 [720p][B092B90E].mp4
│   │   │   ├── [animegrimoire] Jashin-chan Dropkick X - 11 [720p][DED6CA53].mp4
│   │   │   └── [animegrimoire] Jashin-chan Dropkick X - 12 [720p][69BC59AF].mp4
│   │   ├── Josee to Tora to Sakana-tachi
│   │   │   └── [animegrimoire] Josee to Tora to Sakana-tachi [720p][91E0749B].mp4
│   │   ├── Luminous Witches
│   │   │   ├── [animegrimoire] Luminous Witches - 01 [720p][BC671BFC].mp4
│   │   │   ├── [animegrimoire] Luminous Witches - 02 [720p][11CF6C80].mp4
│   │   │   ├── [animegrimoire] Luminous Witches - 03 [720p][6EF0D258].mp4
│   │   │   ├── [animegrimoire] Luminous Witches - 04 [720p][C0548BE6].mp4
│   │   │   ├── [animegrimoire] Luminous Witches - 05 [720p][7456D926].mp4
│   │   │   ├── [animegrimoire] Luminous Witches - 06 [720p][52598660].mp4
│   │   │   ├── [animegrimoire] Luminous Witches - 07 [720p][66B9061B].mp4
│   │   │   ├── [animegrimoire] Luminous Witches - 08 [720p][9F469F7E].mp4
│   │   │   ├── [animegrimoire] Luminous Witches - 09 [720p][EDCFDB62].mp4
│   │   │   ├── [animegrimoire] Luminous Witches - 10 [720p][AB2A51A5].mp4
│   │   │   ├── [animegrimoire] Luminous Witches - 11 [720p][60A95818].mp4
│   │   │   └── [animegrimoire] Luminous Witches - 12 [720p][4C7909D5].mp4
│   │   ├── Lycoris Recoil
│   │   │   ├── [animegrimoire] Lycoris Recoil - 01 [720p][EC9A6442].mp4
│   │   │   ├── [animegrimoire] Lycoris Recoil - 02 [720p][16B4D281].mp4
│   │   │   ├── [animegrimoire] Lycoris Recoil - 03 [720p][98A34476].mp4
│   │   │   ├── [animegrimoire] Lycoris Recoil - 04 [720p][3A9482D6].mp4
│   │   │   ├── [animegrimoire] Lycoris Recoil - 05 [720p][5033DE21].mp4
│   │   │   ├── [animegrimoire] Lycoris Recoil - 06 [720p][5CF308E4].mp4
│   │   │   ├── [animegrimoire] Lycoris Recoil - 07 [720p][2E8D0816].mp4
│   │   │   ├── [animegrimoire] Lycoris Recoil - 08 [720p][4F356910].mp4
│   │   │   ├── [animegrimoire] Lycoris Recoil - 09 [720p][38554090].mp4
│   │   │   ├── [animegrimoire] Lycoris Recoil - 10 [720p][80BF4478].mp4
│   │   │   ├── [animegrimoire] Lycoris Recoil - 11 [720p][03A7D520].mp4
│   │   │   ├── [animegrimoire] Lycoris Recoil - 12 [720p][F337EF31].mp4
│   │   │   └── [animegrimoire] Lycoris Recoil - 13 [720p][1091E74F].mp4
│   │   ├── Made in Abyss
│   │   │   ├── [animegrimoire] Made in Abyss - Retsujitsu no Ougonkyou - 01 [720p][2AF43234].mp4
│   │   │   ├── [animegrimoire] Made in Abyss - Retsujitsu no Ougonkyou - 02 [720p][F5874415].mp4
│   │   │   ├── [animegrimoire] Made in Abyss - Retsujitsu no Ougonkyou - 03 [720p][2678B69B].mp4
│   │   │   ├── [animegrimoire] Made in Abyss - Retsujitsu no Ougonkyou - 04 [720p][D215C950].mp4
│   │   │   ├── [animegrimoire] Made in Abyss - Retsujitsu no Ougonkyou - 05 [720p][1C139AE8].mp4
│   │   │   ├── [animegrimoire] Made in Abyss - Retsujitsu no Ougonkyou - 06 [720p][44AC4F85].mp4
│   │   │   ├── [animegrimoire] Made in Abyss - Retsujitsu no Ougonkyou - 07 [720p][3D17EE35].mp4
│   │   │   ├── [animegrimoire] Made in Abyss - Retsujitsu no Ougonkyou - 08 [720p][D8C2A9C5].mp4
│   │   │   ├── [animegrimoire] Made in Abyss - Retsujitsu no Ougonkyou - 09 [720p][89CD489B].mp4
│   │   │   ├── [animegrimoire] Made in Abyss - Retsujitsu no Ougonkyou - 10 [720p][F1D11F1B].mp4
│   │   │   ├── [animegrimoire] Made in Abyss - Retsujitsu no Ougonkyou - 11 [720p][83EB8630].mp4
│   │   │   └── [animegrimoire] Made in Abyss - Retsujitsu no Ougonkyou - 12 [720p][5A299D8A].mp4
│   │   ├── Mamahaha no Tsurego ga Motokano datta
│   │   │   ├── [animegrimoire] Mamahaha no Tsurego ga Motokano datta - 01 [720p][D3AABE45].mp4
│   │   │   ├── [animegrimoire] Mamahaha no Tsurego ga Motokano datta - 02 [720p][B5AEFF2D].mp4
│   │   │   ├── [animegrimoire] Mamahaha no Tsurego ga Motokano datta - 03 [720p][5C40D6FC].mp4
│   │   │   ├── [animegrimoire] Mamahaha no Tsurego ga Motokano datta - 04 [720p][117070B1].mp4
│   │   │   ├── [animegrimoire] Mamahaha no Tsurego ga Motokano datta - 05 [720p][D67C2533].mp4
│   │   │   ├── [animegrimoire] Mamahaha no Tsurego ga Motokano datta - 06 [720p][B305365E].mp4
│   │   │   ├── [animegrimoire] Mamahaha no Tsurego ga Motokano datta - 07 [720p][5C7F8BC2].mp4
│   │   │   ├── [animegrimoire] Mamahaha no Tsurego ga Motokano datta - 08 [720p][33F535EB].mp4
│   │   │   ├── [animegrimoire] Mamahaha no Tsurego ga Motokano datta - 09 [720p][1B4C77FA].mp4
│   │   │   ├── [animegrimoire] Mamahaha no Tsurego ga Motokano datta - 10 [720p][EC12F4D1].mp4
│   │   │   ├── [animegrimoire] Mamahaha no Tsurego ga Motokano datta - 11 [720p][F23691BD].mp4
│   │   │   └── [animegrimoire] Mamahaha no Tsurego ga Motokano datta - 12 [720p][26E36A29].mp4
│   │   ├── Prima Doll
│   │   │   ├── [animegrimoire] Prima Doll - 01 [720p][07DED15B].mp4
│   │   │   ├── [animegrimoire] Prima Doll - 02 [720p][D5171973].mp4
│   │   │   ├── [animegrimoire] Prima Doll - 03 [720p][D10E6DE8].mp4
│   │   │   ├── [animegrimoire] Prima Doll - 04 [720p][F4E71306].mp4
│   │   │   ├── [animegrimoire] Prima Doll - 05 [720p][7ADDB4C2].mp4
│   │   │   ├── [animegrimoire] Prima Doll - 06 [720p][7EB55177].mp4
│   │   │   ├── [animegrimoire] Prima Doll - 07 [720p][5820EE36].mp4
│   │   │   ├── [animegrimoire] Prima Doll - 08 [720p][FC675A31].mp4
│   │   │   ├── [animegrimoire] Prima Doll - 09 [720p][6A04225A].mp4
│   │   │   ├── [animegrimoire] Prima Doll - 10 [720p][C41D65F0].mp4
│   │   │   ├── [animegrimoire] Prima Doll - 11 [720p][1D399A55].mp4
│   │   │   └── [animegrimoire] Prima Doll - 12 [720p][5A6665A2].mp4
│   │   ├── Soredemo Ayumu wa Yosetekuru
│   │   │   ├── [animegrimoire] Soredemo Ayumu wa Yosetekuru - 01 [720p][CF44EC75].mp4
│   │   │   ├── [animegrimoire] Soredemo Ayumu wa Yosetekuru - 02 [720p][E6804A39].mp4
│   │   │   ├── [animegrimoire] Soredemo Ayumu wa Yosetekuru - 03 [720p][7237303D].mp4
│   │   │   ├── [animegrimoire] Soredemo Ayumu wa Yosetekuru - 04 [720p][21BB2B4F].mp4
│   │   │   ├── [animegrimoire] Soredemo Ayumu wa Yosetekuru - 05 [720p][559B6CEA].mp4
│   │   │   ├── [animegrimoire] Soredemo Ayumu wa Yosetekuru - 06 [720p][AD3C6958].mp4
│   │   │   ├── [animegrimoire] Soredemo Ayumu wa Yosetekuru - 07 [720p][563D3FA8].mp4
│   │   │   ├── [animegrimoire] Soredemo Ayumu wa Yosetekuru - 08 [720p][A30CBD6E].mp4
│   │   │   ├── [animegrimoire] Soredemo Ayumu wa Yosetekuru - 09 [720p][982D5B52].mp4
│   │   │   ├── [animegrimoire] Soredemo Ayumu wa Yosetekuru - 10 [720p][B3AF4B32].mp4
│   │   │   ├── [animegrimoire] Soredemo Ayumu wa Yosetekuru - 11 [720p][2F820C01].mp4
│   │   │   └── [animegrimoire] Soredemo Ayumu wa Yosetekuru - 12 [720p][3F64B494].mp4
│   │   └── Warau Arsnotoria Sun!
│   │       ├── [animegrimoire] Warau Arsnotoria Sun! - 01 [720p][9886756C].mp4
│   │       ├── [animegrimoire] Warau Arsnotoria Sun! - 02 [720p][5A267AA7].mp4
│   │       ├── [animegrimoire] Warau Arsnotoria Sun! - 03 [720p][2977C0ED].mp4
│   │       ├── [animegrimoire] Warau Arsnotoria Sun! - 04 [720p][75657B6C].mp4
│   │       ├── [animegrimoire] Warau Arsnotoria Sun! - 05 [720p][765CA834].mp4
│   │       ├── [animegrimoire] Warau Arsnotoria Sun! - 06 [720p][C2A8C2F8].mp4
│   │       ├── [animegrimoire] Warau Arsnotoria Sun! - 07 [720p][C770309B].mp4
│   │       ├── [animegrimoire] Warau Arsnotoria Sun! - 08 [720p][B3081C09].mp4
│   │       ├── [animegrimoire] Warau Arsnotoria Sun! - 09 [720p][C6CCE146].mp4
│   │       ├── [animegrimoire] Warau Arsnotoria Sun! - 10 [720p][FDC2C19B].mp4
│   │       ├── [animegrimoire] Warau Arsnotoria Sun! - 11 [720p][9B908710].mp4
│   │       └── [animegrimoire] Warau Arsnotoria Sun! - 12 [720p][83C4C0D9].mp4
│   └── Winter
│       ├── Akebi-chan no Sailor Fuku
│       │   ├── [animegrimoire] Akebi-chan no Sailor Fuku - 01 [720p][F86BE195].mp4
│       │   ├── [animegrimoire] Akebi-chan no Sailor Fuku - 02 [720p][9ADB3D23].mp4
│       │   ├── [animegrimoire] Akebi-chan no Sailor Fuku - 03 [720p][E6B9B4F3].mp4
│       │   ├── [animegrimoire] Akebi-chan no Sailor Fuku - 04 [720p][AC5D4EED].mp4
│       │   ├── [animegrimoire] Akebi-chan no Sailor Fuku - 05 [720p][A1B54EFD].mp4
│       │   ├── [animegrimoire] Akebi-chan no Sailor Fuku - 06 [720p][5E057A81].mp4
│       │   ├── [animegrimoire] Akebi-chan no Sailor Fuku - 07 [720p][23F8B94C].mp4
│       │   ├── [animegrimoire] Akebi-chan no Sailor Fuku - 08 [720p][A8B98BD0].mp4
│       │   ├── [animegrimoire] Akebi-chan no Sailor Fuku - 09 [720p][4B0508AE].mp4
│       │   ├── [animegrimoire] Akebi-chan no Sailor Fuku - 10 [720p][62EBA51A].mp4
│       │   ├── [animegrimoire] Akebi-chan no Sailor Fuku - 11 [720p][AD74DD02].mp4
│       │   └── [animegrimoire] Akebi-chan no Sailor Fuku - 12 END [720p][D75E0315].mp4
│       ├── Fantasy Bishoujo Juniku Ojisan to
│       │   ├── [animegrimoire] Fantasy Bishoujo Juniku Ojisan to - 01 [720p][4BD7FC8D].mp4
│       │   ├── [animegrimoire] Fantasy Bishoujo Juniku Ojisan to - 02 [720p][36B2132C].mp4
│       │   ├── [animegrimoire] Fantasy Bishoujo Juniku Ojisan to - 03 [720p][0F812B5D].mp4
│       │   ├── [animegrimoire] Fantasy Bishoujo Juniku Ojisan to - 04 [720p][D913438F].mp4
│       │   ├── [animegrimoire] Fantasy Bishoujo Juniku Ojisan to - 05 [720p][EA6B1C82].mp4
│       │   ├── [animegrimoire] Fantasy Bishoujo Juniku Ojisan to - 06 [720p][77533BF4].mp4
│       │   ├── [animegrimoire] Fantasy Bishoujo Juniku Ojisan to - 07 [720p][49FF68B2].mp4
│       │   ├── [animegrimoire] Fantasy Bishoujo Juniku Ojisan to - 08 [720p][C83FD768].mp4
│       │   ├── [animegrimoire] Fantasy Bishoujo Juniku Ojisan to - 09 [720p][9E6F9BD1].mp4
│       │   ├── [animegrimoire] Fantasy Bishoujo Juniku Ojisan to - 10 [720p][74174D6D].mp4
│       │   ├── [animegrimoire] Fantasy Bishoujo Juniku Ojisan to - 11 [720p][6DC003AB].mp4
│       │   └── [animegrimoire] Fantasy Bishoujo Juniku Ojisan to - 12 END [720p][3AF425E7].mp4
│       ├── Genjitsu Shugi Yuusha no Oukoku Saikenki Part 2
│       │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki Part 2 - 01 [720p][B1ECEDC0].mp4
│       │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki Part 2 - 02 [720p][96E84763].mp4
│       │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki Part 2 - 03 [720p][3318E4C1].mp4
│       │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki Part 2 - 04 [720p][65028AEB].mp4
│       │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki Part 2 - 05 [720p][EE28CE37].mp4
│       │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki Part 2 - 06 [720p][2A85DB52].mp4
│       │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki Part 2 - 07 [720p][8622494A].mp4
│       │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki Part 2 - 08 [720p][DBEA812B].mp4
│       │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki Part 2 - 09 [720p][629D42BE].mp4
│       │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki Part 2 - 10 [720p][FF551F9F].mp4
│       │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki Part 2 - 11 [720p][2669DC99].mp4
│       │   ├── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki Part 2 - 12 [720p][ECD5CB60].mp4
│       │   └── [animegrimoire] Genjitsu Shugi Yuusha no Oukoku Saikenki Part 2 - 13 END [720p][947E30A8].mp4
│       ├── Girls' Frontline
│       │   ├── [animegrimoire] Girls' Frontline - 01 [720p].mp4
│       │   ├── [animegrimoire] Girls' Frontline - 02 [720p].mp4
│       │   ├── [animegrimoire] Girls' Frontline - 03 [720p].mp4
│       │   ├── [animegrimoire] Girls' Frontline - 04 [720p].mp4
│       │   ├── [animegrimoire] Girls' Frontline - 05 [720p].mp4
│       │   ├── [animegrimoire] Girls' Frontline - 06 [720p].mp4
│       │   ├── [animegrimoire] Girls' Frontline - 07 [720p].mp4
│       │   ├── [animegrimoire] Girls' Frontline - 08 [720p].mp4
│       │   ├── [animegrimoire] Girls' Frontline - 09 [720p].mp4
│       │   ├── [animegrimoire] Girls' Frontline - 10 [720p].mp4
│       │   ├── [animegrimoire] Girls' Frontline - 11 [720p].mp4
│       │   └── [animegrimoire] Girls' Frontline - 12 [720p].mp4
│       ├── Girls und Panzer das Finale
│       │   ├── [animegrimoire] Girls und Panzer das Finale - 01 [720p][DAD7EF0E].mp4
│       │   └── [animegrimoire] Girls und Panzer das Finale - 02 [720p][58B4AE54].mp4
│       ├── Hamefura S2
│       │   └── [animegrimoire] Hamefura S2 - OVA [720p][9F03EAF5].mp4
│       ├── Ichigo Mashimaro
│       │   ├── [animegrimoire] Ichigo Mashimaro - 13 [BD720p][49D7AF4C].mp4
│       │   ├── [animegrimoire] Ichigo Mashimaro - 14 [BD720p][9205D51F].mp4
│       │   ├── [animegrimoire] Ichigo Mashimaro - 15 [BD720p][5193DD36].mp4
│       │   ├── [animegrimoire] Ichigo Mashimaro - 16 [BD720p][3CD28E57].mp4
│       │   └── [animegrimoire] Ichigo Mashimaro - 17 [BD720p][9F2D1FFC].mp4
│       ├── Kaginado
│       │   └── [animegrimoire] Kaginado - 12 [720p][4403E1EE].mp4
│       ├── Karakai Jouzu no Takagi-san S3
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S3 - 01 [720p][E4A82BE8].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S3 - 02 [720p][72F9DE65].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S3 - 03 [720p][342CE77C].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S3 - 04 [720p][F96466DD].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S3 - 05 [720p][97115383].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S3 - 06 [720p][E6F6E537].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S3 - 07 [720p][993A680F].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S3 - 08 [720p][4B227C16].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S3 - 09 [720p][C803BFFE].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S3 - 10 [720p][3695BF77].mp4
│       │   ├── [animegrimoire] Karakai Jouzu no Takagi-san S3 - 11 [720p][19EAEF14].mp4
│       │   └── [animegrimoire] Karakai Jouzu no Takagi-san S3 - 12 [720p][ABF745F2].mp4
│       ├── Kenja no Deshi wo Nanoru Kenja
│       │   ├── [animegrimoire] Kenja no Deshi wo Nanoru Kenja - 01 [720p][0DB761C5].mp4
│       │   ├── [animegrimoire] Kenja no Deshi wo Nanoru Kenja - 02 [720p][F21BE8C9].mp4
│       │   ├── [animegrimoire] Kenja no Deshi wo Nanoru Kenja - 03 [720p][284CD64F].mp4
│       │   ├── [animegrimoire] Kenja no Deshi wo Nanoru Kenja - 04 [720p][74CC1635].mp4
│       │   ├── [animegrimoire] Kenja no Deshi wo Nanoru Kenja - 05 [720p][D4B06531].mp4
│       │   ├── [animegrimoire] Kenja no Deshi wo Nanoru Kenja - 06 [720p][38AB8ABE].mp4
│       │   ├── [animegrimoire] Kenja no Deshi wo Nanoru Kenja - 07 [720p][626AE317].mp4
│       │   ├── [animegrimoire] Kenja no Deshi wo Nanoru Kenja - 08 [720p][52622AB9].mp4
│       │   ├── [animegrimoire] Kenja no Deshi wo Nanoru Kenja - 09 [720p][4C9E8BE7].mp4
│       │   ├── [animegrimoire] Kenja no Deshi wo Nanoru Kenja - 10 [720p][E03C23F0].mp4
│       │   ├── [animegrimoire] Kenja no Deshi wo Nanoru Kenja - 11 [720p][35DCA20D].mp4
│       │   └── [animegrimoire] Kenja no Deshi wo Nanoru Kenja - 12 [720p][6E73D56B].mp4
│       ├── Kimetsu no Yaiba
│       │   ├── [animegrimoire] Kimetsu no Yaiba - Yuukaku Hen - 04 [720p][197E239D].mp4
│       │   ├── [animegrimoire] Kimetsu no Yaiba - Yuukaku Hen - 05 [720p][3A7EDFC2].mp4
│       │   ├── [animegrimoire] Kimetsu no Yaiba - Yuukaku Hen - 06 [720p][85CC3AC0].mp4
│       │   ├── [animegrimoire] Kimetsu no Yaiba - Yuukaku Hen - 07 [720p][CA384935].mp4
│       │   ├── [animegrimoire] Kimetsu no Yaiba - Yuukaku Hen - 08 [720p][ECC50CEF].mp4
│       │   ├── [animegrimoire] Kimetsu no Yaiba - Yuukaku Hen - 09 [720p][A1B2492B].mp4
│       │   ├── [animegrimoire] Kimetsu no Yaiba - Yuukaku Hen - 10 [720p][E7C4C30D].mp4
│       │   └── [animegrimoire] Kimetsu no Yaiba - Yuukaku Hen - 11 END [720p][6E29FA99].mp4
│       ├── Komi-san wa Comyushou desu
│       │   ├── [animegrimoire] Komi-san wa Comyushou desu. - 11 [720p][EA6A831C].mp4
│       │   └── [animegrimoire] Komi-san wa Comyushou desu. - 12 [720p][A951FD0D].mp4
│       ├── Mahouka Koukou no Rettousei
│       │   ├── [animegrimoire] Mahouka Koukou no Rettousei - Tsuioku Hen - [720p][445F9296].mp4
│       │   ├── Final Chapter - What Is Regrowth [73C3A7AF].mp4
│       │   ├── Lesson Five - What Is Spirit Magic [2E36AAF0].mp4
│       │   ├── Lesson Four - What Is The Nine School Competition [35DEFEE8].mp4
│       │   ├── Lesson One - What Is The Magic High School [0079F4FF].mp4
│       │   ├── Lesson Six - What Is The Thesis Competition [092F6C0E].mp4
│       │   ├── Lesson Three - What Are Magicians [5864AE51].mp4
│       │   └── Lesson Two - What Is Magic [8CF036DA].mp4
│       ├── Shingeki no Kyojin
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season Part 2 - 01 [720p][649AA686].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season Part 2 - 02 [720p][6BD62C5E].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season Part 2 - 03 [720p][CC67916B].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season Part 2 - 04 [720p][0E997ED7].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season Part 2 - 05 [720p][B9DE0907].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season Part 2 - 06 [720p][60950FAB].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season Part 2 - 07 [720p][65187BE9].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season Part 2 - 08 [720p][7D6E3558].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season Part 2 - 09 [720p][4EA6FA15].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season Part 2 - 10 [720p][CD841DF9].mp4
│       │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season Part 2 - 11 [720p][6ECBDC97].mp4
│       │   └── [animegrimoire] Shingeki no Kyojin - The Final Season Part 2 - 12 END [720p][337C9658].mp4
│       ├── Slow Loop
│       │   ├── [animegrimoire] Slow Loop - 01 [720p][EEC19E1D].mp4
│       │   ├── [animegrimoire] Slow Loop - 02 [720p][DD3CE0DE].mp4
│       │   ├── [animegrimoire] Slow Loop - 03 [720p][4C5410AA].mp4
│       │   ├── [animegrimoire] Slow Loop - 04 [720p][18FBB51E].mp4
│       │   ├── [animegrimoire] Slow Loop - 05 [720p][547D8A04].mp4
│       │   ├── [animegrimoire] Slow Loop - 06 [720p][DEC78787].mp4
│       │   ├── [animegrimoire] Slow Loop - 07 [720p][CD751694].mp4
│       │   ├── [animegrimoire] Slow Loop - 08 [720p][55BAD6A9].mp4
│       │   ├── [animegrimoire] Slow Loop - 09 [720p][D064F5D9].mp4
│       │   ├── [animegrimoire] Slow Loop - 10 [720p][007AE48A].mp4
│       │   ├── [animegrimoire] Slow Loop - 11 [720p][0E744C38].mp4
│       │   └── [animegrimoire] Slow Loop - 12 END [720p][9F5C5ADD].mp4
│       ├── Sorairo Utility
│       │   └── [animegrimoire] Sorairo Utility - OVA [720p][A3B4AFDA].mp4
│       └── World Trigger S3
│           ├── [animegrimoire] World Trigger S3 - 13 [720p][4881CA90].mp4
│           └── [animegrimoire] World Trigger S3 - 14 [720p][C558EC88].mp4
├── 2023
│   ├── Fall
│   │   ├── Arknights
│   │   │   ├── [animegrimoire] Arknights - Fuyukomori Kaerimichi - 09 [720p][DC0F0F43].mp4
│   │   │   ├── [animegrimoire] Arknights - Fuyukomori Kaerimichi - 10 [720p][8E2D235C].mp4
│   │   │   ├── [animegrimoire] Arknights - Fuyukomori Kaerimichi - 11 [720p][48A92DF2].mp4
│   │   │   ├── [animegrimoire] Arknights - Fuyukomori Kaerimichi - 12 [720p][95796D27].mp4
│   │   │   ├── [animegrimoire] Arknights - Fuyukomori Kaerimichi - 13 [720p][758C2758].mp4
│   │   │   ├── [animegrimoire] Arknights - Fuyukomori Kaerimichi - 14 [720p][F3AB98ED].mp4
│   │   │   ├── [animegrimoire] Arknights - Fuyukomori Kaerimichi - 15 [720p][B2CDC9B4].mp4
│   │   │   ├── [animegrimoire] Arknights - Fuyukomori Kaerimichi - 16 [720p][CAB8671B].mp4
│   │   │   ├── [animegrimoire] Arknights - Reimei Zensou - 01v2 [720p][6A628557].mp4
│   │   │   ├── [animegrimoire] Arknights - Reimei Zensou - 02v2 [720p][C418556D].mp4
│   │   │   ├── [animegrimoire] Arknights - Reimei Zensou - 03v2 [720p][823C06A0].mp4
│   │   │   ├── [animegrimoire] Arknights - Reimei Zensou - 04 [720p][DBA7679D].mp4
│   │   │   ├── [animegrimoire] Arknights - Reimei Zensou - 05 [720p][460A6777].mp4
│   │   │   ├── [animegrimoire] Arknights - Reimei Zensou - 06 [720p][13F64074].mp4
│   │   │   ├── [animegrimoire] Arknights - Reimei Zensou - 07 [720p][D99B5C23].mp4
│   │   │   └── [animegrimoire] Arknights - Reimei Zensou - 08v2 [720p][EBE65DB4].mp4
│   │   ├── Dekoboko Majo no Oyako Jijou
│   │   │   ├── [animegrimoire] Dekoboko Majo no Oyako Jijou - 01 [720p][33C39ECA].mp4
│   │   │   ├── [animegrimoire] Dekoboko Majo no Oyako Jijou - 02 [720p][EF78AFAC].mp4
│   │   │   ├── [animegrimoire] Dekoboko Majo no Oyako Jijou - 03 [720p][7FA040F1].mp4
│   │   │   ├── [animegrimoire] Dekoboko Majo no Oyako Jijou - 04 [720p][0177B986].mp4
│   │   │   ├── [animegrimoire] Dekoboko Majo no Oyako Jijou - 05 [720p][120EAE84].mp4
│   │   │   ├── [animegrimoire] Dekoboko Majo no Oyako Jijou - 06 [720p][2D5526FF].mp4
│   │   │   ├── [animegrimoire] Dekoboko Majo no Oyako Jijou - 07 [720p][76435C92].mp4
│   │   │   ├── [animegrimoire] Dekoboko Majo no Oyako Jijou - 08 [720p][7C71D60A].mp4
│   │   │   ├── [animegrimoire] Dekoboko Majo no Oyako Jijou - 09 [720p][FE931841].mp4
│   │   │   ├── [animegrimoire] Dekoboko Majo no Oyako Jijou - 10 [720p][BEDE6B97].mp4
│   │   │   ├── [animegrimoire] Dekoboko Majo no Oyako Jijou - 11 [720p][B46F7FE0].mp4
│   │   │   └── [animegrimoire] Dekoboko Majo no Oyako Jijou - 12 [720p][97921FAA].mp4
│   │   ├── Dr Stone S3
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 12 [720p][D1A44DE8].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 13 [720p][611F3428].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 14 [720p][014D8E22].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 15 [720p][C883023A].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 16 [720p][4774E819].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 17 [720p][35635C2E].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 18 [720p][556A563D].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 19 [720p][EAACBA63].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 20 [720p][036FCEE9].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 21 [720p][94A59AA6].mp4
│   │   │   └── [animegrimoire] Dr. Stone S3 - 22 [720p][5C0D3002].mp4
│   │   ├── Hikikomari Kyuuketsuki no Monmon
│   │   │   ├── [animegrimoire] Hikikomari Kyuuketsuki no Monmon - 01 [720p][36E2CD71].mp4
│   │   │   ├── [animegrimoire] Hikikomari Kyuuketsuki no Monmon - 02 [720p][C0040224].mp4
│   │   │   ├── [animegrimoire] Hikikomari Kyuuketsuki no Monmon - 03 [720p][A20B1057].mp4
│   │   │   ├── [animegrimoire] Hikikomari Kyuuketsuki no Monmon - 04 [720p][E372D9B7].mp4
│   │   │   ├── [animegrimoire] Hikikomari Kyuuketsuki no Monmon - 05 [720p][EB2841D9].mp4
│   │   │   ├── [animegrimoire] Hikikomari Kyuuketsuki no Monmon - 06 [720p][7490D03F].mp4
│   │   │   ├── [animegrimoire] Hikikomari Kyuuketsuki no Monmon - 07 [720p][2FFEE498].mp4
│   │   │   ├── [animegrimoire] Hikikomari Kyuuketsuki no Monmon - 08 [720p][F6488086].mp4
│   │   │   ├── [animegrimoire] Hikikomari Kyuuketsuki no Monmon - 09 [720p][1CF963B1].mp4
│   │   │   ├── [animegrimoire] Hikikomari Kyuuketsuki no Monmon - 10 [720p][FC5860AB].mp4
│   │   │   ├── [animegrimoire] Hikikomari Kyuuketsuki no Monmon - 11 [720p][7BB97F82].mp4
│   │   │   └── [animegrimoire] Hikikomari Kyuuketsuki no Monmon - 12 [720p][91F1ED69].mp4
│   │   ├── Hoshikuzu Telepath
│   │   │   ├── [animegrimoire] Hoshikuzu Telepath - 01 [720p][04104549].mp4
│   │   │   ├── [animegrimoire] Hoshikuzu Telepath - 02 [720p][D184001F].mp4
│   │   │   ├── [animegrimoire] Hoshikuzu Telepath - 03 [720p][ED83A57C].mp4
│   │   │   ├── [animegrimoire] Hoshikuzu Telepath - 04 [720p][248FD077].mp4
│   │   │   ├── [animegrimoire] Hoshikuzu Telepath - 05 [720p][9DA2A154].mp4
│   │   │   ├── [animegrimoire] Hoshikuzu Telepath - 06 [720p][3B159FF8].mp4
│   │   │   ├── [animegrimoire] Hoshikuzu Telepath - 07 [720p][F5D504B6].mp4
│   │   │   ├── [animegrimoire] Hoshikuzu Telepath - 08 [720p][0D5E88DB].mp4
│   │   │   ├── [animegrimoire] Hoshikuzu Telepath - 09 [720p][FB92A679].mp4
│   │   │   ├── [animegrimoire] Hoshikuzu Telepath - 10 [720p][B01C4E8E].mp4
│   │   │   ├── [animegrimoire] Hoshikuzu Telepath - 11 [720p][65663AB9].mp4
│   │   │   └── [animegrimoire] Hoshikuzu Telepath - 12 [720p][7774DB91].mp4
│   │   ├── Jujutsu Kaisen
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 36 [720p][8F922DFF].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 37 [720p][AB4851F1].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 38 [720p][FAD46067].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 39 [720p][7349220A].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 40 [720p][83601566].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 41 [720p][8D00A9C2].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 42 [720p][B019C225].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 43 [720p][6F1E2C10].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 44 [720p][CDF7285B].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 45 [720p][02FBA4B5].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 46 [720p][F9CAB54E].mp4
│   │   │   └── [animegrimoire] Jujutsu Kaisen - 47 [720p][85647211].mp4
│   │   ├── Kusuriya no Hitorigoto
│   │   │   ├── [animegrimoire] Kusuriya no Hitorigoto - 01 [720p][74034BC3].mp4
│   │   │   ├── [animegrimoire] Kusuriya no Hitorigoto - 02 [720p][51BB944F].mp4
│   │   │   ├── [animegrimoire] Kusuriya no Hitorigoto - 03 [720p][8B8F31BC].mp4
│   │   │   ├── [animegrimoire] Kusuriya no Hitorigoto - 04 [720p][0760FB0F].mp4
│   │   │   ├── [animegrimoire] Kusuriya no Hitorigoto - 05 [720p][6590A05A].mp4
│   │   │   ├── [animegrimoire] Kusuriya no Hitorigoto - 06 [720p][5B17B045].mp4
│   │   │   ├── [animegrimoire] Kusuriya no Hitorigoto - 07 [720p][1F726086].mp4
│   │   │   ├── [animegrimoire] Kusuriya no Hitorigoto - 08 [720p][1D067C07].mp4
│   │   │   ├── [animegrimoire] Kusuriya no Hitorigoto - 09 [720p][17006970].mp4
│   │   │   ├── [animegrimoire] Kusuriya no Hitorigoto - 10 [720p][C3567C6A].mp4
│   │   │   ├── [animegrimoire] Kusuriya no Hitorigoto - 11 [720p][F362330F].mp4
│   │   │   └── [animegrimoire] Kusuriya no Hitorigoto - 12 [720p][4F0A4BED].mp4
│   │   ├── Potion-danomi de Ikinobimasu!
│   │   │   ├── [animegrimoire] Potion-danomi de Ikinobimasu! - 01 [720p][F41FA034].mp4
│   │   │   ├── [animegrimoire] Potion-danomi de Ikinobimasu! - 02 [720p][7EF2E96E].mp4
│   │   │   ├── [animegrimoire] Potion-danomi de Ikinobimasu! - 03 [720p][085531BD].mp4
│   │   │   ├── [animegrimoire] Potion-danomi de Ikinobimasu! - 04 [720p][0D3B2FC3].mp4
│   │   │   ├── [animegrimoire] Potion-danomi de Ikinobimasu! - 05 [720p][AE8F1A6C].mp4
│   │   │   ├── [animegrimoire] Potion-danomi de Ikinobimasu! - 06 [720p][6CE419FE].mp4
│   │   │   ├── [animegrimoire] Potion-danomi de Ikinobimasu! - 07 [720p][29451E32].mp4
│   │   │   ├── [animegrimoire] Potion-danomi de Ikinobimasu! - 08 [720p][E77E388F].mp4
│   │   │   ├── [animegrimoire] Potion-danomi de Ikinobimasu! - 09 [720p][AD66336B].mp4
│   │   │   ├── [animegrimoire] Potion-danomi de Ikinobimasu! - 10 [720p][04069B32].mp4
│   │   │   ├── [animegrimoire] Potion-danomi de Ikinobimasu! - 11 [720p][93947C57].mp4
│   │   │   └── [animegrimoire] Potion-danomi de Ikinobimasu! - 12 [720p][D27611D8].mp4
│   │   ├── Psycho-Pass Movie
│   │   │   └── [animegrimoire] Psycho-Pass Movie - Providence [720p][0666FE5B].mp4
│   │   ├── Seijo no Maryoku wa Bannou Desu S2
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu S2 - 01 [720p][58F732FE].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu S2 - 02 [720p][5BB4319E].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu S2 - 03 [720p][3DB904D2].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu S2 - 04 [720p][B4277840].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu S2 - 05 [720p][DD47A86E].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu S2 - 06 [720p][DB75ED26].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu S2 - 07 [720p][89ECE83C].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu S2 - 08 [720p][B47597C0].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu S2 - 09 [720p][36B53A9D].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu S2 - 10 [720p][1257BB2F].mp4
│   │   │   ├── [animegrimoire] Seijo no Maryoku wa Bannou Desu S2 - 11 [720p][754E5DC1].mp4
│   │   │   └── [animegrimoire] Seijo no Maryoku wa Bannou Desu S2 - 12 [720p][17189034].mp4
│   │   ├── Shingeki no Kyojin
│   │   │   ├── [animegrimoire] Shingeki no Kyojin - The Final Season Part 3 - 01 [720p][1C262969].mp4
│   │   │   └── [animegrimoire] Shingeki no Kyojin - The Final Season Part 3 - 02 [720p][5309255F].mp4
│   │   ├── Sousou no Frieren
│   │   │   ├── [animegrimoire] Sousou no Frieren - 01 [720p][2CDE0C3D].mp4
│   │   │   ├── [animegrimoire] Sousou no Frieren - 02 [720p][C48E4F28].mp4
│   │   │   ├── [animegrimoire] Sousou no Frieren - 03 [720p][E6CACD9C].mp4
│   │   │   ├── [animegrimoire] Sousou no Frieren - 04 [720p][7D76623B].mp4
│   │   │   ├── [animegrimoire] Sousou no Frieren - 05 [720p][ADE31071].mp4
│   │   │   ├── [animegrimoire] Sousou no Frieren - 06 [720p][511A14F8].mp4
│   │   │   ├── [animegrimoire] Sousou no Frieren - 07 [720p][08DE22F5].mp4
│   │   │   ├── [animegrimoire] Sousou no Frieren - 08 [720p][07E0C102].mp4
│   │   │   ├── [animegrimoire] Sousou no Frieren - 09 [720p][EFA46290].mp4
│   │   │   ├── [animegrimoire] Sousou no Frieren - 10 [720p][01DC6C6F].mp4
│   │   │   ├── [animegrimoire] Sousou no Frieren - 11 [720p][9350D43F].mp4
│   │   │   ├── [animegrimoire] Sousou no Frieren - 12 [720p][C78D6A45].mp4
│   │   │   ├── [animegrimoire] Sousou no Frieren - 13 [720p][12D02F6D].mp4
│   │   │   ├── [animegrimoire] Sousou no Frieren - 14 [720p][260D06AB].mp4
│   │   │   ├── [animegrimoire] Sousou no Frieren - 15 [720p][282E0D53].mp4
│   │   │   └── [animegrimoire] Sousou no Frieren - 16 [720p][178B8FE3].mp4
│   │   ├── Spy x Family
│   │   │   ├── [animegrimoire] Spy x Family - 26 [720p][E2E9DF88].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 27 [720p][EC0FB512].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 28 [720p][6BF15FAE].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 29 [720p][733A03EB].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 30 [720p][DE22DFB2].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 31 [720p][4D5963FE].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 32 [720p][101DD7B6].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 33 [720p][E1E1EA25].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 34 [720p][7D6C00B3].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 35 [720p][84A69576].mp4
│   │   │   ├── [animegrimoire] Spy x Family - 36 [720p][C32437BC].mp4
│   │   │   └── [animegrimoire] Spy x Family - 37 [720p][18E4A790].mp4
│   │   ├── Tate no Yuusha no Nariagari S3
│   │   │   ├── [animegrimoire] Tate no Yuusha no Nariagari S3 - 01 [720p][355C91AE].mp4
│   │   │   ├── [animegrimoire] Tate no Yuusha no Nariagari S3 - 02 [720p][59535B5A].mp4
│   │   │   ├── [animegrimoire] Tate no Yuusha no Nariagari S3 - 03 [720p][A1EBAEDA].mp4
│   │   │   ├── [animegrimoire] Tate no Yuusha no Nariagari S3 - 04 [720p][E10DF083].mp4
│   │   │   ├── [animegrimoire] Tate no Yuusha no Nariagari S3 - 05 [720p][959ABAC6].mp4
│   │   │   ├── [animegrimoire] Tate no Yuusha no Nariagari S3 - 06 [720p][CF61D15C].mp4
│   │   │   ├── [animegrimoire] Tate no Yuusha no Nariagari S3 - 07 [720p][4F0850DF].mp4
│   │   │   ├── [animegrimoire] Tate no Yuusha no Nariagari S3 - 08 [720p][1B2A8328].mp4
│   │   │   ├── [animegrimoire] Tate no Yuusha no Nariagari S3 - 09 [720p][697C0A68].mp4
│   │   │   ├── [animegrimoire] Tate no Yuusha no Nariagari S3 - 10 [720p][391CA2D5].mp4
│   │   │   ├── [animegrimoire] Tate no Yuusha no Nariagari S3 - 11 [720p][231804AC].mp4
│   │   │   └── [animegrimoire] Tate no Yuusha no Nariagari S3 - 12 [720p][1AE945C4].mp4
│   │   ├── Tearmoon Teikoku Monogatari
│   │   │   ├── [animegrimoire] Tearmoon Teikoku Monogatari - 01 [720p][536B8A93].mp4
│   │   │   ├── [animegrimoire] Tearmoon Teikoku Monogatari - 02 [720p][ADF582C4].mp4
│   │   │   ├── [animegrimoire] Tearmoon Teikoku Monogatari - 03 [720p][8B8D7F91].mp4
│   │   │   ├── [animegrimoire] Tearmoon Teikoku Monogatari - 04 [720p][89B8B5B2].mp4
│   │   │   ├── [animegrimoire] Tearmoon Teikoku Monogatari - 05 [720p][46ACE8C1].mp4
│   │   │   ├── [animegrimoire] Tearmoon Teikoku Monogatari - 06 [720p][6DA63CC6].mp4
│   │   │   ├── [animegrimoire] Tearmoon Teikoku Monogatari - 07 [720p][96F441F7].mp4
│   │   │   ├── [animegrimoire] Tearmoon Teikoku Monogatari - 08 [720p][AF13EA42].mp4
│   │   │   ├── [animegrimoire] Tearmoon Teikoku Monogatari - 09 [720p][1FE8AB5E].mp4
│   │   │   ├── [animegrimoire] Tearmoon Teikoku Monogatari - 10 [720p][D0B80381].mp4
│   │   │   ├── [animegrimoire] Tearmoon Teikoku Monogatari - 11 [720p][24B1245F].mp4
│   │   │   └── [animegrimoire] Tearmoon Teikoku Monogatari - 12 [720p][704D6F49].mp4
│   │   └── Watashi no Oshi wa Akuyaku Reijou
│   │       ├── [animegrimoire] Watashi no Oshi wa Akuyaku Reijou - 01 [720p][02B3648F].mp4
│   │       ├── [animegrimoire] Watashi no Oshi wa Akuyaku Reijou - 02 [720p][D551D470].mp4
│   │       ├── [animegrimoire] Watashi no Oshi wa Akuyaku Reijou - 03 [720p][EDB71485].mp4
│   │       ├── [animegrimoire] Watashi no Oshi wa Akuyaku Reijou - 04 [720p][73BCB323].mp4
│   │       ├── [animegrimoire] Watashi no Oshi wa Akuyaku Reijou - 05 [720p][E68E8023].mp4
│   │       ├── [animegrimoire] Watashi no Oshi wa Akuyaku Reijou - 06 [720p][059EF837].mp4
│   │       ├── [animegrimoire] Watashi no Oshi wa Akuyaku Reijou - 07 [720p][E73A1797].mp4
│   │       ├── [animegrimoire] Watashi no Oshi wa Akuyaku Reijou - 08 [720p][6B0EE76D].mp4
│   │       ├── [animegrimoire] Watashi no Oshi wa Akuyaku Reijou - 09 [720p][0627A5B1].mp4
│   │       ├── [animegrimoire] Watashi no Oshi wa Akuyaku Reijou - 10 [720p][6A4360E8].mp4
│   │       ├── [animegrimoire] Watashi no Oshi wa Akuyaku Reijou - 11 [720p][5ADB765E].mp4
│   │       └── [animegrimoire] Watashi no Oshi wa Akuyaku Reijou - 12 [720p][05A6C86A].mp4
│   ├── Spring
│   │   ├── Bofuri S2
│   │   │   ├── [animegrimoire] Bofuri S2 - 11 [720p][54BB868C].mp4
│   │   │   └── [animegrimoire] Bofuri S2 - 12 [720p][E11B03EC].mp4
│   │   ├── Boku no Kokoro no Yabai Yatsu
│   │   │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 01 [720p][AA64C571].mp4
│   │   │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 02 [720p][A06E706A].mp4
│   │   │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 03 [720p][060CDF36].mp4
│   │   │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 04 [720p][AD60C9A0].mp4
│   │   │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 05 [720p][D3D90B76].mp4
│   │   │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 06 [720p][36A4024F].mp4
│   │   │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 07 [720p][B7053255].mp4
│   │   │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 08 [720p][932F75B7].mp4
│   │   │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 09 [720p][15DF1BC2].mp4
│   │   │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 10 [720p][4D80E336].mp4
│   │   │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 11 [720p][496F2313].mp4
│   │   │   └── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 12 [720p][4925EEAF].mp4
│   │   ├── Dr Stone S3
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 01 [720p][9FF6921C].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 02 [720p][1FC6438E].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 03 [720p][6F54034A].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 04 [720p][54F52FAA].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 05 [720p][B5258452].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 06 [720p][EE331E32].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 07 [720p][6D9F8828].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 08 [720p][5C9C7B09].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 09 [720p][CD9AD6CB].mp4
│   │   │   ├── [animegrimoire] Dr. Stone S3 - 10 [720p][DD083CA1].mp4
│   │   │   └── [animegrimoire] Dr. Stone S3 - 11 [720p][D5AD115D].mp4
│   │   ├── Edomae Elf
│   │   │   ├── [animegrimoire] Edomae Elf - 01 [720p][4707F8A1].mp4
│   │   │   ├── [animegrimoire] Edomae Elf - 02 [720p][77D5D241].mp4
│   │   │   ├── [animegrimoire] Edomae Elf - 03 [720p][5B24A694].mp4
│   │   │   ├── [animegrimoire] Edomae Elf - 04 [720p][DE8013BD].mp4
│   │   │   ├── [animegrimoire] Edomae Elf - 05 [720p][D0CFAF82].mp4
│   │   │   ├── [animegrimoire] Edomae Elf - 06 [720p][FCAAF100].mp4
│   │   │   ├── [animegrimoire] Edomae Elf - 07 [720p][6D111ED3].mp4
│   │   │   ├── [animegrimoire] Edomae Elf - 08 [720p][2A6CC60F].mp4
│   │   │   ├── [animegrimoire] Edomae Elf - 09 [720p][669D1E51].mp4
│   │   │   ├── [animegrimoire] Edomae Elf - 10 [720p][EE6A2A20].mp4
│   │   │   ├── [animegrimoire] Edomae Elf - 11 [720p][EACA7FC2].mp4
│   │   │   └── [animegrimoire] Edomae Elf - 12 [720p][DDE2B93B].mp4
│   │   ├── Hirogaru Sky! Precure
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 11 [720p][F9245A4D].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 12 [720p][01FA9A5C].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 13 [720p][7C4F6A70].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 14 [720p][88053FB5].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 15 [720p][580B8BCE].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 16 [720p][BE7105F5].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 17 [720p][A9F35DD3].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 18 [720p][CAC26FE3].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 19 [720p][9505E769].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 20 [720p][5FC38BE0].mp4
│   │   │   └── [animegrimoire] Hirogaru Sky! Precure - 21 [720p][632D8C42].mp4
│   │   ├── Jujutsu Kaisen 0
│   │   │   └── [animegrimoire] Jujutsu Kaisen 0 - Movie [AEE57ECF][9D1307BF].mp4
│   │   ├── Karakai Jouzu no Takagi-san Movie
│   │   │   └── [animegrimoire] Karakai Jouzu no Takagi-san Movie [720p][B842307F].mp4
│   │   ├── Kawaisugi Crisis
│   │   │   ├── [animegrimoire] Kawaisugi Crisis - 01 [720p][5001F814].mp4
│   │   │   ├── [animegrimoire] Kawaisugi Crisis - 02 [720p][4262F1E3].mp4
│   │   │   ├── [animegrimoire] Kawaisugi Crisis - 03 [720p][5703CD25].mp4
│   │   │   ├── [animegrimoire] Kawaisugi Crisis - 04 [720p][E22B4D36].mp4
│   │   │   ├── [animegrimoire] Kawaisugi Crisis - 05 [720p][CBE44FEA].mp4
│   │   │   ├── [animegrimoire] Kawaisugi Crisis - 06 [720p][C9E5E9D4].mp4
│   │   │   ├── [animegrimoire] Kawaisugi Crisis - 07 [720p][612DC109].mp4
│   │   │   ├── [animegrimoire] Kawaisugi Crisis - 08 [720p][89707946].mp4
│   │   │   ├── [animegrimoire] Kawaisugi Crisis - 09 [720p][E9420EF3].mp4
│   │   │   ├── [animegrimoire] Kawaisugi Crisis - 10 [720p][99DF2CEA].mp4
│   │   │   ├── [animegrimoire] Kawaisugi Crisis - 11 [720p][91848E4B].mp4
│   │   │   └── [animegrimoire] Kawaisugi Crisis - 12 [720p][245F6DC1].mp4
│   │   ├── Kimetsu no Yaiba
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Katanakaji no Sato-hen - 01 [720p][FAF67AA6].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Katanakaji no Sato-hen - 02 [720p][36B761BF].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Katanakaji no Sato-hen - 03 [720p][6E56AC72].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Katanakaji no Sato-hen - 04 [720p][A036803E].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Katanakaji no Sato-hen - 05 [720p][C009FCBA].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Katanakaji no Sato-hen - 06 [720p][BF9346F8].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Katanakaji no Sato-hen - 07 [720p][318166C9].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Katanakaji no Sato-hen - 08 [720p][3C0E9EB7].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Katanakaji no Sato-hen - 09 [720p][0A563F53].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Katanakaji no Sato-hen - 10 [720p][62540797].mp4
│   │   │   └── [animegrimoire] Kimetsu no Yaiba - Katanakaji no Sato-hen - 11 [720p][F9073539].mp4
│   │   ├── Kono Subarashii Sekai ni Bakuen wo!
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Bakuen wo! - 01 [720p][F506C971].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Bakuen wo! - 02 [720p][EF7F104D].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Bakuen wo! - 03 [720p][9836F5AD].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Bakuen wo! - 04 [720p][4130956C].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Bakuen wo! - 05 [720p][4FC0D9A1].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Bakuen wo! - 06 [720p][4173F933].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Bakuen wo! - 07 [720p][4D58AA7E].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Bakuen wo! - 08 [720p][C0B09E08].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Bakuen wo! - 09 [720p][70DEAC3B].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Bakuen wo! - 10 [720p][8DDAFE4B].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Bakuen wo! - 11 [720p][EF9B6D0D].mp4
│   │   │   └── [animegrimoire] Kono Subarashii Sekai ni Bakuen wo! - 12 [720p][842AE2AE].mp4
│   │   ├── Kubo-san wa Mob wo Yurusanai
│   │   │   ├── [animegrimoire] Kubo-san wa Mob wo Yurusanai - 01 [720p][B95E5146].mp4
│   │   │   ├── [animegrimoire] Kubo-san wa Mob wo Yurusanai - 02 [720p][8AE01335].mp4
│   │   │   ├── [animegrimoire] Kubo-san wa Mob wo Yurusanai - 03 [720p][FF3861B2].mp4
│   │   │   ├── [animegrimoire] Kubo-san wa Mob wo Yurusanai - 04 [720p][E9CAAF30].mp4
│   │   │   ├── [animegrimoire] Kubo-san wa Mob wo Yurusanai - 05 [720p][7068A49B].mp4
│   │   │   ├── [animegrimoire] Kubo-san wa Mob wo Yurusanai - 06 [720p][BB89BC01].mp4
│   │   │   ├── [animegrimoire] Kubo-san wa Mob wo Yurusanai - 07 [720p][F1B5ACA9].mp4
│   │   │   ├── [animegrimoire] Kubo-san wa Mob wo Yurusanai - 08 [720p][0EF1C47C].mp4
│   │   │   ├── [animegrimoire] Kubo-san wa Mob wo Yurusanai - 09 [720p][457C9806].mp4
│   │   │   ├── [animegrimoire] Kubo-san wa Mob wo Yurusanai - 10 [720p][55E52073].mp4
│   │   │   ├── [animegrimoire] Kubo-san wa Mob wo Yurusanai - 11 [720p][08CBE57D].mp4
│   │   │   └── [animegrimoire] Kubo-san wa Mob wo Yurusanai - 12 [720p][A7DE53C7].mp4
│   │   ├── Kuma Kuma Kuma Bear S2
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear S2 - 01 [720p][88967060].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear S2 - 02 [720p][3DC4CFE7].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear S2 - 03 [720p][FB877A3D].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear S2 - 04 [720p][78CA8F01].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear S2 - 05 [720p][89E2B47A].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear S2 - 06 [720p][E6BC031E].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear S2 - 07 [720p][43B91620].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear S2 - 08 [720p][95276211].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear S2 - 09 [720p][089E2D76].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear S2 - 10 [720p][3A075301].mp4
│   │   │   ├── [animegrimoire] Kuma Kuma Kuma Bear S2 - 11 [720p][18022AAE].mp4
│   │   │   └── [animegrimoire] Kuma Kuma Kuma Bear S2 - 12 [720p][B46AAF71].mp4
│   │   ├── Megami no Cafe Terrace
│   │   │   ├── [animegrimoire] Megami no Cafe Terrace - 01 [720p][5E12ED91].mp4
│   │   │   ├── [animegrimoire] Megami no Cafe Terrace - 02 [720p][048B0428].mp4
│   │   │   ├── [animegrimoire] Megami no Cafe Terrace - 03 [720p][BC5E013D].mp4
│   │   │   ├── [animegrimoire] Megami no Cafe Terrace - 04 [720p][43D3C1FA].mp4
│   │   │   ├── [animegrimoire] Megami no Cafe Terrace - 05 [720p][A98BC787].mp4
│   │   │   ├── [animegrimoire] Megami no Cafe Terrace - 06 [720p][6708D905].mp4
│   │   │   ├── [animegrimoire] Megami no Cafe Terrace - 07 [720p][25300D5F].mp4
│   │   │   ├── [animegrimoire] Megami no Cafe Terrace - 08 [720p][1933D41B].mp4
│   │   │   ├── [animegrimoire] Megami no Cafe Terrace - 09 [720p][AB289BED].mp4
│   │   │   ├── [animegrimoire] Megami no Cafe Terrace - 10 [720p][70179390].mp4
│   │   │   ├── [animegrimoire] Megami no Cafe Terrace - 11 [720p][E47FBF12].mp4
│   │   │   └── [animegrimoire] Megami no Cafe Terrace - 12 [720p][364FA0CD].mp4
│   │   ├── Oshi no Ko
│   │   │   ├── [animegrimoire] Oshi no Ko - 01 [720p][C7ED8CFD].mp4
│   │   │   ├── [animegrimoire] Oshi no Ko - 02 [720p][ABA5E540].mp4
│   │   │   ├── [animegrimoire] Oshi no Ko - 03 [720p][B059A5E4].mp4
│   │   │   ├── [animegrimoire] Oshi no Ko - 04 [720p][ACA646AF].mp4
│   │   │   ├── [animegrimoire] Oshi no Ko - 05 [720p][70E0717B].mp4
│   │   │   ├── [animegrimoire] Oshi no Ko - 06 [720p][BE193166].mp4
│   │   │   ├── [animegrimoire] Oshi no Ko - 07.5 [720p][0BE0469A].mp4
│   │   │   ├── [animegrimoire] Oshi no Ko - 07 [720p][5960A028].mp4
│   │   │   ├── [animegrimoire] Oshi no Ko - 08 [720p][531F13A1].mp4
│   │   │   ├── [animegrimoire] Oshi no Ko - 09 [720p][979B07C4].mp4
│   │   │   ├── [animegrimoire] Oshi no Ko - 10 [720p][5A1BBC20].mp4
│   │   │   └── [animegrimoire] Oshi no Ko - 11 [720p][8B13762E].mp4
│   │   ├── Tensei Kizoku no Isekai Boukenroku
│   │   │   ├── [animegrimoire] Tensei Kizoku no Isekai Boukenroku - 01 [720p][E797825B].mp4
│   │   │   ├── [animegrimoire] Tensei Kizoku no Isekai Boukenroku - 02 [720p][DB8E3F5A].mp4
│   │   │   ├── [animegrimoire] Tensei Kizoku no Isekai Boukenroku - 03 [720p][EDBF20CE].mp4
│   │   │   ├── [animegrimoire] Tensei Kizoku no Isekai Boukenroku - 04 [720p][ACD4348D].mp4
│   │   │   ├── [animegrimoire] Tensei Kizoku no Isekai Boukenroku - 05 [720p][4E5C76C1].mp4
│   │   │   ├── [animegrimoire] Tensei Kizoku no Isekai Boukenroku - 06 [720p][D9B4190D].mp4
│   │   │   ├── [animegrimoire] Tensei Kizoku no Isekai Boukenroku - 07 [720p][E428F9FD].mp4
│   │   │   ├── [animegrimoire] Tensei Kizoku no Isekai Boukenroku - 08 [720p][1CD63B54].mp4
│   │   │   ├── [animegrimoire] Tensei Kizoku no Isekai Boukenroku - 09 [720p][6D41344E].mp4
│   │   │   ├── [animegrimoire] Tensei Kizoku no Isekai Boukenroku - 10 [720p][951167C3].mp4
│   │   │   ├── [animegrimoire] Tensei Kizoku no Isekai Boukenroku - 11 [720p][1FAA314A].mp4
│   │   │   └── [animegrimoire] Tensei Kizoku no Isekai Boukenroku - 12 [720p][34CB2A45].mp4
│   │   ├── Tensei shitara Slime Datta Ken Movie
│   │   │   └── [animegrimoire] Tensei shitara Slime Datta Ken Movie - Guren no Kizuna-hen [720p][D7738E4A].mp4
│   │   ├── Tokyo Mew Mew New
│   │   │   ├── [animegrimoire] Tokyo Mew Mew New - 13 [720p][48737A37].mp4
│   │   │   ├── [animegrimoire] Tokyo Mew Mew New - 14 [720p][047020D5].mp4
│   │   │   ├── [animegrimoire] Tokyo Mew Mew New - 15 [720p][EA52EAC3].mp4
│   │   │   ├── [animegrimoire] Tokyo Mew Mew New - 16 [720p][7C68BC41].mp4
│   │   │   ├── [animegrimoire] Tokyo Mew Mew New - 17 [720p][442F9BC4].mp4
│   │   │   ├── [animegrimoire] Tokyo Mew Mew New - 18 [720p][07DA7338].mp4
│   │   │   ├── [animegrimoire] Tokyo Mew Mew New - 19 [720p][55F560D7].mp4
│   │   │   ├── [animegrimoire] Tokyo Mew Mew New - 20 [720p][A669CA08].mp4
│   │   │   ├── [animegrimoire] Tokyo Mew Mew New - 21 [720p][896FD649].mp4
│   │   │   ├── [animegrimoire] Tokyo Mew Mew New - 22 [720p][480262E7].mp4
│   │   │   ├── [animegrimoire] Tokyo Mew Mew New - 23 [720p][16DD48A2].mp4
│   │   │   └── [animegrimoire] Tokyo Mew Mew New - 24 [720p][F0A5932D].mp4
│   │   ├── Tonikaku Kawaii S2
│   │   │   ├── [animegrimoire] Tonikaku Kawaii S2 - 01 [720p][58CB5BA6].mp4
│   │   │   ├── [animegrimoire] Tonikaku Kawaii S2 - 02 [720p][00F07613].mp4
│   │   │   ├── [animegrimoire] Tonikaku Kawaii S2 - 03 [720p][062E3458].mp4
│   │   │   ├── [animegrimoire] Tonikaku Kawaii S2 - 04 [720p][2A0C2927].mp4
│   │   │   ├── [animegrimoire] Tonikaku Kawaii S2 - 05 [720p][844186D7].mp4
│   │   │   ├── [animegrimoire] Tonikaku Kawaii S2 - 06 [720p][94ECD453].mp4
│   │   │   ├── [animegrimoire] Tonikaku Kawaii S2 - 07 [720p][475E98BA].mp4
│   │   │   ├── [animegrimoire] Tonikaku Kawaii S2 - 08 [720p][D39AAB07].mp4
│   │   │   ├── [animegrimoire] Tonikaku Kawaii S2 - 09 [720p][4DB12C38].mp4
│   │   │   ├── [animegrimoire] Tonikaku Kawaii S2 - 10 [720p][F6E4DAAC].mp4
│   │   │   ├── [animegrimoire] Tonikaku Kawaii S2 - 11 [720p][BE2E5A08].mp4
│   │   │   └── [animegrimoire] Tonikaku Kawaii S2 - 12 [720p][7D982D65].mp4
│   │   ├── Watashi ni Tenshi ga Maiorita!
│   │   │   └── [animegrimoire] Watashi ni Tenshi ga Maiorita! - Precious Friends [720p][D53F2F48].mp4
│   │   └── Watashi no Yuri wa Oshigoto desu!
│   │       ├── [animegrimoire] Watashi no Yuri wa Oshigoto desu! - 01 [720p][7352D16E].mp4
│   │       ├── [animegrimoire] Watashi no Yuri wa Oshigoto desu! - 02 [720p][50886A6A].mp4
│   │       ├── [animegrimoire] Watashi no Yuri wa Oshigoto desu! - 03 [720p][35C3EB7A].mp4
│   │       ├── [animegrimoire] Watashi no Yuri wa Oshigoto desu! - 04 [720p][233E6D47].mp4
│   │       ├── [animegrimoire] Watashi no Yuri wa Oshigoto desu! - 05 [720p][CC7B4B08].mp4
│   │       ├── [animegrimoire] Watashi no Yuri wa Oshigoto desu! - 06 [720p][A9DDC477].mp4
│   │       ├── [animegrimoire] Watashi no Yuri wa Oshigoto desu! - 07 [720p][C2A3A78F].mp4
│   │       ├── [animegrimoire] Watashi no Yuri wa Oshigoto desu! - 08 [720p][2B5D4427].mp4
│   │       ├── [animegrimoire] Watashi no Yuri wa Oshigoto desu! - 09 [720p][2A18304C].mp4
│   │       ├── [animegrimoire] Watashi no Yuri wa Oshigoto desu! - 10 [720p][D97B8A63].mp4
│   │       ├── [animegrimoire] Watashi no Yuri wa Oshigoto desu! - 11 [720p][D100154F].mp4
│   │       └── [animegrimoire] Watashi no Yuri wa Oshigoto desu! - 12 [720p][8696A535].mp4
│   ├── Summer
│   │   ├── Hataraku Maou-sama S2
│   │   │   ├── [animegrimoire] Hataraku Maou-sama S2 - 13 [720p][81B94F71].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama S2 - 14 [720p][B7308A9D].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama S2 - 15 [720p][59D216EC].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama S2 - 16 [720p][31F3120A].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama S2 - 17 [720p][3E0F6D34].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama S2 - 18 [720p][77C5BAF9].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama S2 - 19 [720p][3572D5A1].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama S2 - 20 [720p][79BDD9CF].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama S2 - 21 [720p][11AC2F66].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama S2 - 22 [720p][A415E6FF].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama S2 - 23 [720p][6EACF6E5].mp4
│   │   │   ├── [animegrimoire] Hataraku Maou-sama S2 - 24 [720p][07FA879F].mp4
│   │   │   └── [animegrimoire] Hataraku Maou-sama S2 - SP1 [720p][2785074D].mp4
│   │   ├── Hirogaru Sky! Precure
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 22 [720p][DB896B41].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 23 [720p][47CA05D6].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 24 [720p][78F65BF4].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 25 [720p][8872FED2].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 26 [720p][E7849579].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 27 [720p][7A74FFFB].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 28 [720p][55882A27].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 29 [720p][8D866CA4].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 30 [720p][B535006F].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 31 [720p][88114346].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 32 [720p][9AD71A2D].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 33 [720p][21244E9E].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 34 [720p][21742488].mp4
│   │   │   ├── [animegrimoire] Hirogaru Sky! Precure - 35 [720p][4C26F1A8].mp4
│   │   │   └── [animegrimoire] Hirogaru Sky! Precure - 36 [720p][AF4415F8].mp4
│   │   ├── Jujutsu Kaisen
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 25 [720p][014689BB].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 26 [720p][520CF5A0].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 27 [720p][C236F7A0].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 28 [720p][A92D31AC].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 29 [720p][68BD50C1].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 30 [720p][BD2F14FE].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 31 [720p][44E485FF].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 32 [720p][9FD20C93].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 33 [720p][DFB60785].mp4
│   │   │   ├── [animegrimoire] Jujutsu Kaisen - 34 [720p][10595069].mp4
│   │   │   └── [animegrimoire] Jujutsu Kaisen - 35 [720p][FC877024].mp4
│   │   ├── Mushoku Tensei S2
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 00 [720p][0958A343].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 01 [720p][F8946189].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 02 [720p][17C6325B].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 03 [720p][166AAB9F].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 04 [720p][82AD3C06].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 05 [720p][B316B6CD].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 06 [720p][809B3E0A].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 07 [720p][49FF7B91].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 08 [720p][2E36E055].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 09 [720p][B595D2D2].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 10 [720p][3D5F93C8].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 11 [720p][AA98A7EC].mp4
│   │   │   └── [animegrimoire] Mushoku Tensei S2 - 12 [720p][DEA00719].mp4
│   │   ├── Ryza no Atelier
│   │   │   ├── [animegrimoire] Ryza no Atelier - 01 [720p][7F952333].mp4
│   │   │   ├── [animegrimoire] Ryza no Atelier - 02 [720p][9D21DAB3].mp4
│   │   │   ├── [animegrimoire] Ryza no Atelier - 03 [720p][F39B2F2D].mp4
│   │   │   ├── [animegrimoire] Ryza no Atelier - 04 [720p][990A9D80].mp4
│   │   │   ├── [animegrimoire] Ryza no Atelier - 05 [720p][2C5DDF88].mp4
│   │   │   ├── [animegrimoire] Ryza no Atelier - 06 [720p][FA6CF1D5].mp4
│   │   │   ├── [animegrimoire] Ryza no Atelier - 07 [720p][9DBBBD1F].mp4
│   │   │   ├── [animegrimoire] Ryza no Atelier - 08 [720p][2D6C6302].mp4
│   │   │   ├── [animegrimoire] Ryza no Atelier - 09 [720p][1E8B65F2].mp4
│   │   │   ├── [animegrimoire] Ryza no Atelier - 10 [720p][E2F8E5E4].mp4
│   │   │   ├── [animegrimoire] Ryza no Atelier - 11 [720p][83F4F573].mp4
│   │   │   └── [animegrimoire] Ryza no Atelier - 12 [720p][4F065B32].mp4
│   │   ├── Shiro Seijo to Kuro Bokushi
│   │   │   ├── [animegrimoire] Shiro Seijo to Kuro Bokushi - 01 [720p][18081D8E].mp4
│   │   │   ├── [animegrimoire] Shiro Seijo to Kuro Bokushi - 02 [720p][822EF4BD].mp4
│   │   │   ├── [animegrimoire] Shiro Seijo to Kuro Bokushi - 03 [720p][8EAFEFFC].mp4
│   │   │   ├── [animegrimoire] Shiro Seijo to Kuro Bokushi - 04 [720p][A00FA258].mp4
│   │   │   ├── [animegrimoire] Shiro Seijo to Kuro Bokushi - 05 [720p][2AB3AD97].mp4
│   │   │   ├── [animegrimoire] Shiro Seijo to Kuro Bokushi - 06 [720p][E7E18905].mp4
│   │   │   ├── [animegrimoire] Shiro Seijo to Kuro Bokushi - 07 [720p][4517CE77].mp4
│   │   │   ├── [animegrimoire] Shiro Seijo to Kuro Bokushi - 08 [720p][FC6E8541].mp4
│   │   │   ├── [animegrimoire] Shiro Seijo to Kuro Bokushi - 09 [720p][1C63D322].mp4
│   │   │   ├── [animegrimoire] Shiro Seijo to Kuro Bokushi - 10 [720p][363F6495].mp4
│   │   │   ├── [animegrimoire] Shiro Seijo to Kuro Bokushi - 11 [720p][C95AE6C3].mp4
│   │   │   └── [animegrimoire] Shiro Seijo to Kuro Bokushi - 12 [720p][342C6C73].mp4
│   │   ├── Spy Kyoushitsu
│   │   │   ├── [animegrimoire] Spy Kyoushitsu - 13 [720p][E879C81A].mp4
│   │   │   ├── [animegrimoire] Spy Kyoushitsu - 14 [720p][D91A43F1].mp4
│   │   │   ├── [animegrimoire] Spy Kyoushitsu - 15 [720p][96A57D9C].mp4
│   │   │   ├── [animegrimoire] Spy Kyoushitsu - 16 [720p][393EDB05].mp4
│   │   │   ├── [animegrimoire] Spy Kyoushitsu - 17 [720p][18BCA2AF].mp4
│   │   │   ├── [animegrimoire] Spy Kyoushitsu - 18 [720p][E511EAD5].mp4
│   │   │   ├── [animegrimoire] Spy Kyoushitsu - 19 [720p][2F61BF28].mp4
│   │   │   ├── [animegrimoire] Spy Kyoushitsu - 20 [720p][9D62250E].mp4
│   │   │   ├── [animegrimoire] Spy Kyoushitsu - 21 [720p][8007163A].mp4
│   │   │   ├── [animegrimoire] Spy Kyoushitsu - 22 [720p][E3C80FF1].mp4
│   │   │   ├── [animegrimoire] Spy Kyoushitsu - 23 [720p][AE263809].mp4
│   │   │   └── [animegrimoire] Spy Kyoushitsu - 24 [720p][FF595262].mp4
│   │   ├── Suki na Ko ga Megane wo Wasureta
│   │   │   ├── [animegrimoire] Suki na Ko ga Megane wo Wasureta - 01 [720p][C33CAB69].mp4
│   │   │   ├── [animegrimoire] Suki na Ko ga Megane wo Wasureta - 02 [720p][46817747].mp4
│   │   │   ├── [animegrimoire] Suki na Ko ga Megane wo Wasureta - 03 [720p][D0AA9B46].mp4
│   │   │   ├── [animegrimoire] Suki na Ko ga Megane wo Wasureta - 04 [720p][6F51E705].mp4
│   │   │   ├── [animegrimoire] Suki na Ko ga Megane wo Wasureta - 05 [720p][67F6FE47].mp4
│   │   │   ├── [animegrimoire] Suki na Ko ga Megane wo Wasureta - 06 [720p][50D19BD2].mp4
│   │   │   ├── [animegrimoire] Suki na Ko ga Megane wo Wasureta - 07 [720p][ED04F203].mp4
│   │   │   ├── [animegrimoire] Suki na Ko ga Megane wo Wasureta - 08 [720p][1B0C97FA].mp4
│   │   │   ├── [animegrimoire] Suki na Ko ga Megane wo Wasureta - 09 [720p][3C85BF62].mp4
│   │   │   ├── [animegrimoire] Suki na Ko ga Megane wo Wasureta - 10 [720p][DB5E922E].mp4
│   │   │   ├── [animegrimoire] Suki na Ko ga Megane wo Wasureta - 11 [720p][D1F9DF0A].mp4
│   │   │   ├── [animegrimoire] Suki na Ko ga Megane wo Wasureta - 12 [720p][48A7549A].mp4
│   │   │   └── [animegrimoire] Suki na Ko ga Megane wo Wasureta - 13 [720p][F403E96E].mp4
│   │   ├── Uchi no Kaisha no Chiisai Senpai no Hanashi
│   │   │   ├── [animegrimoire] Uchi no Kaisha no Chiisai Senpai no Hanashi - 01 [720p][8E9E457A].mp4
│   │   │   ├── [animegrimoire] Uchi no Kaisha no Chiisai Senpai no Hanashi - 02 [720p][21EB6450].mp4
│   │   │   ├── [animegrimoire] Uchi no Kaisha no Chiisai Senpai no Hanashi - 03 [720p][C4B238A1].mp4
│   │   │   ├── [animegrimoire] Uchi no Kaisha no Chiisai Senpai no Hanashi - 04 [720p][2F9EBE0D].mp4
│   │   │   ├── [animegrimoire] Uchi no Kaisha no Chiisai Senpai no Hanashi - 05 [720p][2AFFE846].mp4
│   │   │   ├── [animegrimoire] Uchi no Kaisha no Chiisai Senpai no Hanashi - 06 [720p][F581747E].mp4
│   │   │   ├── [animegrimoire] Uchi no Kaisha no Chiisai Senpai no Hanashi - 07 [720p][B53A8488].mp4
│   │   │   ├── [animegrimoire] Uchi no Kaisha no Chiisai Senpai no Hanashi - 07 [720p][E5866881].mp4
│   │   │   ├── [animegrimoire] Uchi no Kaisha no Chiisai Senpai no Hanashi - 08 [720p][F12A3232].mp4
│   │   │   ├── [animegrimoire] Uchi no Kaisha no Chiisai Senpai no Hanashi - 09 [720p][C5EC2B9F].mp4
│   │   │   ├── [animegrimoire] Uchi no Kaisha no Chiisai Senpai no Hanashi - 10 [720p][36D374AF].mp4
│   │   │   ├── [animegrimoire] Uchi no Kaisha no Chiisai Senpai no Hanashi - 11 [720p][0ED6176C].mp4
│   │   │   └── [animegrimoire] Uchi no Kaisha no Chiisai Senpai no Hanashi - 12 [720p][892CFD0A].mp4
│   │   └── Yumemiru Danshi wa Genjitsushugisha
│   │       ├── [animegrimoire] Yumemiru Danshi wa Genjitsushugisha - 01 [720p][E700788A].mp4
│   │       ├── [animegrimoire] Yumemiru Danshi wa Genjitsushugisha - 02 [720p][31CB51A4].mp4
│   │       ├── [animegrimoire] Yumemiru Danshi wa Genjitsushugisha - 03 [720p][6210C10D].mp4
│   │       ├── [animegrimoire] Yumemiru Danshi wa Genjitsushugisha - 04 [720p][6409326B].mp4
│   │       ├── [animegrimoire] Yumemiru Danshi wa Genjitsushugisha - 05 [720p][CADB97B7].mp4
│   │       ├── [animegrimoire] Yumemiru Danshi wa Genjitsushugisha - 06 [720p][501DD7E5].mp4
│   │       ├── [animegrimoire] Yumemiru Danshi wa Genjitsushugisha - 07 [720p][7D39B855].mp4
│   │       ├── [animegrimoire] Yumemiru Danshi wa Genjitsushugisha - 08 [720p][8AC6A3D0].mp4
│   │       ├── [animegrimoire] Yumemiru Danshi wa Genjitsushugisha - 09 [720p][16D8E414].mp4
│   │       ├── [animegrimoire] Yumemiru Danshi wa Genjitsushugisha - 10 [720p][2E3DEFBD].mp4
│   │       ├── [animegrimoire] Yumemiru Danshi wa Genjitsushugisha - 11 [720p][62763FF8].mp4
│   │       └── [animegrimoire] Yumemiru Danshi wa Genjitsushugisha - 12 [720p][46EAAC35].mp4
│   └── Winter
│       ├── Bofuri S2
│       │   ├── [animegrimoire] Bofuri S2 - 01 [720p][41AE2A3B].mp4
│       │   ├── [animegrimoire] Bofuri S2 - 02 [720p][B47D1F38].mp4
│       │   ├── [animegrimoire] Bofuri S2 - 03 [720p][8A611C43].mp4
│       │   ├── [animegrimoire] Bofuri S2 - 04 [720p][116BB54F].mp4
│       │   ├── [animegrimoire] Bofuri S2 - 05 [720p][485F6DE8].mp4
│       │   ├── [animegrimoire] Bofuri S2 - 06 [720p][4D3E5432].mp4
│       │   ├── [animegrimoire] Bofuri S2 - 07 [720p][AD86E534].mp4
│       │   ├── [animegrimoire] Bofuri S2 - 08 [720p][D839EA6E].mp4
│       │   ├── [animegrimoire] Bofuri S2 - 09 [720p][2AC19BB0].mp4
│       │   └── [animegrimoire] Bofuri S2 - 10 [720p][62D46D45].mp4
│       ├── Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4
│       │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 17 [720p][4672C701].mp4
│       │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 18 [720p][1BC7F953].mp4
│       │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 19 [720p][063D2C38].mp4
│       │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 20 [720p][06FD4EB4].mp4
│       │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 21 [720p][F69846D4].mp4
│       │   └── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S4 - 22 [720p][B62F487E].mp4
│       ├── Hirogaru Sky! Precure
│       │   ├── [animegrimoire] Hirogaru Sky! Precure - 01 [720p][EDD69597].mp4
│       │   ├── [animegrimoire] Hirogaru Sky! Precure - 02 [720p][20DEFA5B].mp4
│       │   ├── [animegrimoire] Hirogaru Sky! Precure - 03 [720p][5588A6B0].mp4
│       │   ├── [animegrimoire] Hirogaru Sky! Precure - 04 [720p][7D32EFAB].mp4
│       │   ├── [animegrimoire] Hirogaru Sky! Precure - 05 [720p][D6F390B5].mp4
│       │   ├── [animegrimoire] Hirogaru Sky! Precure - 06 [720p][844B1D6B].mp4
│       │   ├── [animegrimoire] Hirogaru Sky! Precure - 07 [720p][76A86A02].mp4
│       │   ├── [animegrimoire] Hirogaru Sky! Precure - 08 [720p][B64C9905].mp4
│       │   ├── [animegrimoire] Hirogaru Sky! Precure - 09 [720p][9003D680].mp4
│       │   └── [animegrimoire] Hirogaru Sky! Precure - 10 [720p][CFED4F4C].mp4
│       ├── Kami-tachi ni Hirowareta Otoko S2
│       │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko S2 - 01v2 [720p][06CBDD4F].mp4
│       │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko S2 - 02 [720p][F64CEB1F].mp4
│       │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko S2 - 03 [720p][D4560602].mp4
│       │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko S2 - 04 [720p][3EB0C2AD].mp4
│       │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko S2 - 05 [720p][EF25F6A0].mp4
│       │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko S2 - 06 [720p][4432A90E].mp4
│       │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko S2 - 07 [720p][3D052AA2].mp4
│       │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko S2 - 08 [720p][19DF3D82].mp4
│       │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko S2 - 09 [720p][EDE8CBEA].mp4
│       │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko S2 - 10 [720p][9820296F].mp4
│       │   ├── [animegrimoire] Kami-tachi ni Hirowareta Otoko S2 - 11 [720p][7CD7E74F].mp4
│       │   └── [animegrimoire] Kami-tachi ni Hirowareta Otoko S2 - 12 [720p][11095F7C].mp4
│       ├── Kyokou Suiri
│       │   ├── [animegrimoire] Kyokou Suiri - 14 [720p][13E9387C].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 15 [720p][203824BC].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 16 [720p][854CE5BE].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 17 [720p][2F72A8C4].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 18 [720p][532AF8CB].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 19 [720p][C689178E].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 20 [720p][45650F1C].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 21 [720p][E4778196].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 22 [720p][56D3D133].mp4
│       │   ├── [animegrimoire] Kyokou Suiri - 23 [720p][88ADBB56].mp4
│       │   └── [animegrimoire] Kyokou Suiri - 24 [720p][63773B36].mp4
│       ├── Mou Ippon!
│       │   ├── [animegrimoire] Mou Ippon! - 01 [720p][DE1D274E].mp4
│       │   ├── [animegrimoire] Mou Ippon! - 02 [720p][A54F99FF].mp4
│       │   ├── [animegrimoire] Mou Ippon! - 03 [720p][0E4EDF70].mp4
│       │   ├── [animegrimoire] Mou Ippon! - 04 [720p][ACBE23B4].mp4
│       │   ├── [animegrimoire] Mou Ippon! - 05 [720p][22E0815F].mp4
│       │   ├── [animegrimoire] Mou Ippon! - 06 [720p][E2324B61].mp4
│       │   ├── [animegrimoire] Mou Ippon! - 07 [720p][B561514A].mp4
│       │   ├── [animegrimoire] Mou Ippon! - 08 [720p][9D421BDF].mp4
│       │   ├── [animegrimoire] Mou Ippon! - 09 [720p][8B6021BF].mp4
│       │   ├── [animegrimoire] Mou Ippon! - 10 [720p][62750F45].mp4
│       │   ├── [animegrimoire] Mou Ippon! - 11 [720p][4767AC99].mp4
│       │   ├── [animegrimoire] Mou Ippon! - 12 [720p][7F404F0E].mp4
│       │   └── [animegrimoire] Mou Ippon! - 13 [720p][42618A4E].mp4
│       ├── Nijiyon Animation
│       │   ├── [animegrimoire] Nijiyon Animation - 01 [720p][8AD60733].mp4
│       │   ├── [animegrimoire] Nijiyon Animation - 02 [720p][B49E8E5A].mp4
│       │   ├── [animegrimoire] Nijiyon Animation - 03 [720p][C67A8D2A].mp4
│       │   ├── [animegrimoire] Nijiyon Animation - 04 [720p][B9A3A258].mp4
│       │   ├── [animegrimoire] Nijiyon Animation - 05 [720p][DF886532].mp4
│       │   ├── [animegrimoire] Nijiyon Animation - 06 [720p][75EAC186].mp4
│       │   ├── [animegrimoire] Nijiyon Animation - 07 [720p][37F62826].mp4
│       │   ├── [animegrimoire] Nijiyon Animation - 08 [720p][A089E7B3].mp4
│       │   ├── [animegrimoire] Nijiyon Animation - 09 [720p][56B262A3].mp4
│       │   ├── [animegrimoire] Nijiyon Animation - 10 [720p][D85826BE].mp4
│       │   ├── [animegrimoire] Nijiyon Animation - 11 [720p][7875FAAF].mp4
│       │   └── [animegrimoire] Nijiyon Animation - 12 [720p][DC28F703].mp4
│       ├── Oniichan wa Oshimai!
│       │   ├── [animegrimoire] Oniichan wa Oshimai! - 01 [720p][CE1E80EA].mp4
│       │   ├── [animegrimoire] Oniichan wa Oshimai! - 02 [720p][49FC4BB9].mp4
│       │   ├── [animegrimoire] Oniichan wa Oshimai! - 03 [720p][985943DC].mp4
│       │   ├── [animegrimoire] Oniichan wa Oshimai! - 04 [720p][0DA96A4D].mp4
│       │   ├── [animegrimoire] Oniichan wa Oshimai! - 05 [720p][C75D2C17].mp4
│       │   ├── [animegrimoire] Oniichan wa Oshimai! - 06 [720p][7C499DCD].mp4
│       │   ├── [animegrimoire] Oniichan wa Oshimai! - 07 [720p][EC1A8AEC].mp4
│       │   ├── [animegrimoire] Oniichan wa Oshimai! - 08 [720p][04C883F4].mp4
│       │   ├── [animegrimoire] Oniichan wa Oshimai! - 09 [720p][2EB50A91].mp4
│       │   ├── [animegrimoire] Oniichan wa Oshimai! - 10 [720p][DD7FCE11].mp4
│       │   ├── [animegrimoire] Oniichan wa Oshimai! - 11 [720p][62719EF7].mp4
│       │   └── [animegrimoire] Oniichan wa Oshimai! - 12 [720p][E7FD0C48].mp4
│       ├── Rougo ni Sonaete Isekai de 8-manmai no Kinka wo Tamemasu
│       │   ├── [animegrimoire] Rougo ni Sonaete Isekai de 8-manmai no Kinka wo Tamemasu - 01 [720p][0C38DC59].mp4
│       │   ├── [animegrimoire] Rougo ni Sonaete Isekai de 8-manmai no Kinka wo Tamemasu - 02 [720p][032482AE].mp4
│       │   ├── [animegrimoire] Rougo ni Sonaete Isekai de 8-manmai no Kinka wo Tamemasu - 03 [720p][0FAD7FF1].mp4
│       │   ├── [animegrimoire] Rougo ni Sonaete Isekai de 8-manmai no Kinka wo Tamemasu - 04 [720p][079B7C6A].mp4
│       │   ├── [animegrimoire] Rougo ni Sonaete Isekai de 8-manmai no Kinka wo Tamemasu - 05 [720p][6A81FE22].mp4
│       │   ├── [animegrimoire] Rougo ni Sonaete Isekai de 8-manmai no Kinka wo Tamemasu - 06 [720p][12901775].mp4
│       │   ├── [animegrimoire] Rougo ni Sonaete Isekai de 8-manmai no Kinka wo Tamemasu - 07 [720p][7DC30E40].mp4
│       │   ├── [animegrimoire] Rougo ni Sonaete Isekai de 8-manmai no Kinka wo Tamemasu - 08 [720p][E1FF7E87].mp4
│       │   ├── [animegrimoire] Rougo ni Sonaete Isekai de 8-manmai no Kinka wo Tamemasu - 09 [720p][140D612F].mp4
│       │   ├── [animegrimoire] Rougo ni Sonaete Isekai de 8-manmai no Kinka wo Tamemasu - 10 [720p][8F2D8393].mp4
│       │   ├── [animegrimoire] Rougo ni Sonaete Isekai de 8-manmai no Kinka wo Tamemasu - 11 [720p][75216A38].mp4
│       │   └── [animegrimoire] Rougo ni Sonaete Isekai de 8-manmai no Kinka wo Tamemasu - 12 [720p][775297E2].mp4
│       ├── Shingeki no Kyojin
│       │   └── [animegrimoire] Shingeki no Kyojin - The Final Season Part 3 - 01 [720p][4B2F658F].mp4
│       ├── Spy Kyoushitsu
│       │   ├── [animegrimoire] Spy Kyoushitsu - 01 [720p][437D9337].mp4
│       │   ├── [animegrimoire] Spy Kyoushitsu - 02 [720p][E7997DA4].mp4
│       │   ├── [animegrimoire] Spy Kyoushitsu - 03 [720p][F526E8E6].mp4
│       │   ├── [animegrimoire] Spy Kyoushitsu - 04 [720p][02DBBDC2].mp4
│       │   ├── [animegrimoire] Spy Kyoushitsu - 05 [720p][EFD4F6F6].mp4
│       │   ├── [animegrimoire] Spy Kyoushitsu - 06 [720p][9A1D9395].mp4
│       │   ├── [animegrimoire] Spy Kyoushitsu - 07 [720p][3A0D2D34].mp4
│       │   ├── [animegrimoire] Spy Kyoushitsu - 08 [720p][295671D1].mp4
│       │   ├── [animegrimoire] Spy Kyoushitsu - 09 [720p][6DEA94CA].mp4
│       │   ├── [animegrimoire] Spy Kyoushitsu - 10 [720p][A31DA2B9].mp4
│       │   ├── [animegrimoire] Spy Kyoushitsu - 11 [720p][A912BB77].mp4
│       │   └── [animegrimoire] Spy Kyoushitsu - 12 [720p][1986BE54].mp4
│       ├── Tensei Oujo to Tensai Reijou no Mahou Kakumei
│       │   ├── [animegrimoire] Tensei Oujo to Tensai Reijou no Mahou Kakumei - 01 [720p][A29BFCF4].mp4
│       │   ├── [animegrimoire] Tensei Oujo to Tensai Reijou no Mahou Kakumei - 02 [720p][0E84ABED].mp4
│       │   ├── [animegrimoire] Tensei Oujo to Tensai Reijou no Mahou Kakumei - 03 [720p][9C0FC749].mp4
│       │   ├── [animegrimoire] Tensei Oujo to Tensai Reijou no Mahou Kakumei - 04 [720p][06497519].mp4
│       │   ├── [animegrimoire] Tensei Oujo to Tensai Reijou no Mahou Kakumei - 05 [720p][8DD2D98D].mp4
│       │   ├── [animegrimoire] Tensei Oujo to Tensai Reijou no Mahou Kakumei - 06 [720p][0E394CAC].mp4
│       │   ├── [animegrimoire] Tensei Oujo to Tensai Reijou no Mahou Kakumei - 07 [720p][2ABE234D].mp4
│       │   ├── [animegrimoire] Tensei Oujo to Tensai Reijou no Mahou Kakumei - 08 [720p][939C057A].mp4
│       │   ├── [animegrimoire] Tensei Oujo to Tensai Reijou no Mahou Kakumei - 09 [720p][F0B357D5].mp4
│       │   ├── [animegrimoire] Tensei Oujo to Tensai Reijou no Mahou Kakumei - 10 [720p][2C2B5555].mp4
│       │   ├── [animegrimoire] Tensei Oujo to Tensai Reijou no Mahou Kakumei - 11 [720p][2B88B818].mp4
│       │   └── [animegrimoire] Tensei Oujo to Tensai Reijou no Mahou Kakumei - 12 [720p][D110EADF].mp4
│       └── Tomo-chan wa Onnanoko!
│           ├── [animegrimoire] Tomo-chan wa Onnanoko! - 01 [720p][A263A099].mp4
│           ├── [animegrimoire] Tomo-chan wa Onnanoko! - 02 [720p][1DCD1DBB].mp4
│           ├── [animegrimoire] Tomo-chan wa Onnanoko! - 03 [720p][774F041C].mp4
│           ├── [animegrimoire] Tomo-chan wa Onnanoko! - 04 [720p][F4159A08].mp4
│           ├── [animegrimoire] Tomo-chan wa Onnanoko! - 05 [720p][6C42AD37].mp4
│           ├── [animegrimoire] Tomo-chan wa Onnanoko! - 06 [720p][33844D03].mp4
│           ├── [animegrimoire] Tomo-chan wa Onnanoko! - 07 [720p][5959795D].mp4
│           ├── [animegrimoire] Tomo-chan wa Onnanoko! - 08 [720p][7A24B27A].mp4
│           ├── [animegrimoire] Tomo-chan wa Onnanoko! - 09 [720p][82364681].mp4
│           ├── [animegrimoire] Tomo-chan wa Onnanoko! - 10v2 [720p][45F37B7D].mp4
│           ├── [animegrimoire] Tomo-chan wa Onnanoko! - 11 [720p][7A3283D7].mp4
│           ├── [animegrimoire] Tomo-chan wa Onnanoko! - 12 [720p][8AAAD467].mp4
│           └── [animegrimoire] Tomo-chan wa Onnanoko! - 13 [720p][1C9EA596].mp4
├── 2024
│   ├── Fall
│   │   ├── Acro Trip
│   │   │   ├── [animegrimoire] Acro Trip - 01 [720p][86F64C3B].mp4
│   │   │   ├── [animegrimoire] Acro Trip - 02 [720p][A4740BAA].mp4
│   │   │   ├── [animegrimoire] Acro Trip - 03 [720p][96D6786B].mp4
│   │   │   ├── [animegrimoire] Acro Trip - 04 [720p][D484F125].mp4
│   │   │   ├── [animegrimoire] Acro Trip - 05 [720p][71F089E4].mp4
│   │   │   ├── [animegrimoire] Acro Trip - 06 [720p][79C1FB7D].mp4
│   │   │   ├── [animegrimoire] Acro Trip - 07 [720p][98F454C5].mp4
│   │   │   ├── [animegrimoire] Acro Trip - 08 [720p][3868BAB4].mp4
│   │   │   ├── [animegrimoire] Acro Trip - 09 [720p][16DD4BCB].mp4
│   │   │   ├── [animegrimoire] Acro Trip - 10 [720p][87050BEA].mp4
│   │   │   ├── [animegrimoire] Acro Trip - 11 [720p][2F703C3E].mp4
│   │   │   └── [animegrimoire] Acro Trip - 12 [720p][2F6CEE51].mp4
│   │   ├── Arifureta Shokugyou de Sekai Saikyou S3
│   │   │   ├── [animegrimoire] Arifureta Shokugyou de Sekai Saikyou S3 - 01 [720p][9BE2444C].mp4
│   │   │   ├── [animegrimoire] Arifureta Shokugyou de Sekai Saikyou S3 - 02 [720p][615387C].mp4
│   │   │   ├── [animegrimoire] Arifureta Shokugyou de Sekai Saikyou S3 - 03 [720p][10263172].mp4
│   │   │   ├── [animegrimoire] Arifureta Shokugyou de Sekai Saikyou S3 - 04 [720p][5C156C42].mp4
│   │   │   ├── [animegrimoire] Arifureta Shokugyou de Sekai Saikyou S3 - 05 [720p][7EBEC322].mp4
│   │   │   ├── [animegrimoire] Arifureta Shokugyou de Sekai Saikyou S3 - 06 [720p][A0BBE944].mp4
│   │   │   ├── [animegrimoire] Arifureta Shokugyou de Sekai Saikyou S3 - 07 [720p][E747F75].mp4
│   │   │   ├── [animegrimoire] Arifureta Shokugyou de Sekai Saikyou S3 - 08 [720p][6E628460].mp4
│   │   │   ├── [animegrimoire] Arifureta Shokugyou de Sekai Saikyou S3 - 09 [720p][54EE0ABC].mp4
│   │   │   ├── [animegrimoire] Arifureta Shokugyou de Sekai Saikyou S3 - 10 [720p][6087EB5].mp4
│   │   │   └── [animegrimoire] Arifureta Shokugyou de Sekai Saikyou S3 - 11 [720p][CFC5540].mp4
│   │   ├── Atri - My Dear Moments
│   │   │   └── [animegrimoire] Atri - My Dear Moments - 13 [720p][CB64C977].mp4
│   │   ├── Delico's Nursery
│   │   │   ├── [animegrimoire] Delico's Nursery - 06.5 [720p][ECF48EB2].mp4
│   │   │   ├── [animegrimoire] Delico's Nursery - 07 [720p][C135B548].mp4
│   │   │   ├── [animegrimoire] Delico's Nursery - 08 [720p][60BDA799].mp4
│   │   │   ├── [animegrimoire] Delico's Nursery - 09 [720p][BA83CFC4].mp4
│   │   │   ├── [animegrimoire] Delico's Nursery - 10 [720p][3937A24F].mp4
│   │   │   ├── [animegrimoire] Delico's Nursery - 11 [720p][57A5CAD7].mp4
│   │   │   ├── [animegrimoire] Delico's Nursery - 12 [720p][8E3C636B].mp4
│   │   │   └── [animegrimoire] Delico's Nursery - 13 [720p][66291DEF].mp4
│   │   ├── Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S5
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S5 - 01 [720p][F7766EC2].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S5 - 02 [720p][24A25310].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S5 - 03 [720p][E9A7628E].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S5 - 04 [720p][E1DA49DD].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S5 - 05 [720p][219651E4].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S5 - 06 [720p][5E51319].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S5 - 07 [720p][A625D224].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S5 - 08 [720p][A6049431].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S5 - 09 [720p][50B85570].mp4
│   │   │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S5 - 10 [720p][746D3DD4].mp4
│   │   │   └── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S5 - 11 [720p][4259270].mp4
│   │   ├── Kimi wa Meido-sama
│   │   │   ├── [animegrimoire] Kimi wa Meido-sama - 01 [720p][3B80756F].mp4
│   │   │   ├── [animegrimoire] Kimi wa Meido-sama - 02 [720p][FC9071B4].mp4
│   │   │   ├── [animegrimoire] Kimi wa Meido-sama - 03 [720p][9324DFB9].mp4
│   │   │   ├── [animegrimoire] Kimi wa Meido-sama - 04 [720p][3F741933].mp4
│   │   │   ├── [animegrimoire] Kimi wa Meido-sama - 05 [720p][CCC7FD29].mp4
│   │   │   ├── [animegrimoire] Kimi wa Meido-sama - 06 [720p][5DAFE1C8].mp4
│   │   │   ├── [animegrimoire] Kimi wa Meido-sama - 07 [720p][881EA63E].mp4
│   │   │   ├── [animegrimoire] Kimi wa Meido-sama - 08 [720p][BD9361F0].mp4
│   │   │   ├── [animegrimoire] Kimi wa Meido-sama - 09 [720p][CE62D674].mp4
│   │   │   ├── [animegrimoire] Kimi wa Meido-sama - 10 [720p][509F75AA].mp4
│   │   │   ├── [animegrimoire] Kimi wa Meido-sama - 11 [720p][2917E04C].mp4
│   │   │   └── [animegrimoire] Kimi wa Meido-sama - 12 [720p][3A1E4A7E].mp4
│   │   ├── Mahoutsukai ni Narenakatta Onnanoko no Hanashi
│   │   │   ├── [animegrimoire] Mahoutsukai ni Narenakatta Onnanoko no Hanashi - 01 [720p][34F44398].mp4
│   │   │   ├── [animegrimoire] Mahoutsukai ni Narenakatta Onnanoko no Hanashi - 02 [720p][CA258705].mp4
│   │   │   ├── [animegrimoire] Mahoutsukai ni Narenakatta Onnanoko no Hanashi - 03 [720p][DB4A4CD2].mp4
│   │   │   ├── [animegrimoire] Mahoutsukai ni Narenakatta Onnanoko no Hanashi - 04 [720p][7771612E].mp4
│   │   │   ├── [animegrimoire] Mahoutsukai ni Narenakatta Onnanoko no Hanashi - 05 [720p][AEE9194B].mp4
│   │   │   ├── [animegrimoire] Mahoutsukai ni Narenakatta Onnanoko no Hanashi - 06 [720p][4F9244A2].mp4
│   │   │   ├── [animegrimoire] Mahoutsukai ni Narenakatta Onnanoko no Hanashi - 07 [720p][9445BF1C].mp4
│   │   │   ├── [animegrimoire] Mahoutsukai ni Narenakatta Onnanoko no Hanashi - 08 [720p][C164D2FE].mp4
│   │   │   ├── [animegrimoire] Mahoutsukai ni Narenakatta Onnanoko no Hanashi - 09 [720p][13BCA9CD].mp4
│   │   │   ├── [animegrimoire] Mahoutsukai ni Narenakatta Onnanoko no Hanashi - 10 [720p][2C71D154].mp4
│   │   │   ├── [animegrimoire] Mahoutsukai ni Narenakatta Onnanoko no Hanashi - 11 [720p][173FE39E].mp4
│   │   │   └── [animegrimoire] Mahoutsukai ni Narenakatta Onnanoko no Hanashi - 12 [720p][FFD75FC9].mp4
│   │   ├── Maou-sama, Retry! R
│   │   │   ├── [animegrimoire] Maou-sama, Retry! R - 01 [720p][2B705D8C].mp4
│   │   │   ├── [animegrimoire] Maou-sama, Retry! R - 02 [720p][3E2EFACB].mp4
│   │   │   ├── [animegrimoire] Maou-sama, Retry! R - 03 [720p][A7A4FC1A].mp4
│   │   │   ├── [animegrimoire] Maou-sama, Retry! R - 04 [720p][1FFE513].mp4
│   │   │   ├── [animegrimoire] Maou-sama, Retry! R - 05 [720p][C54613DD].mp4
│   │   │   ├── [animegrimoire] Maou-sama, Retry! R - 06 [720p][B2410AC].mp4
│   │   │   ├── [animegrimoire] Maou-sama, Retry! R - 07 [720p][CDECA6AF].mp4
│   │   │   ├── [animegrimoire] Maou-sama, Retry! R - 08 [720p][45BA4320].mp4
│   │   │   ├── [animegrimoire] Maou-sama, Retry! R - 09 [720p][1F09F133].mp4
│   │   │   ├── [animegrimoire] Maou-sama, Retry! R - 10 [720p][A95AB739].mp4
│   │   │   ├── [animegrimoire] Maou-sama, Retry! R - 11 [720p][28784CE].mp4
│   │   │   └── [animegrimoire] Maou-sama, Retry! R - 12 [720p][1BCCC305].mp4
│   │   ├── Monogatari Series - Off & Monster Season
│   │   │   ├── [animegrimoire] Monogatari Series - Off & Monster Season - 12 [720p][650CC626].mp4
│   │   │   ├── [animegrimoire] Monogatari Series - Off & Monster Season - 13 [720p][60394066].mp4
│   │   │   └── [animegrimoire] Monogatari Series - Off & Monster Season - 14 [720p][9008E63D].mp4
│   │   ├── Party kara Tsuihou sareta Sono Chiyushi, Jitsu wa Saikyou ni Tsuki
│   │   │   ├── [animegrimoire] Party kara Tsuihou sareta Sono Chiyushi, Jitsu wa Saikyou ni Tsuki - 01 [720p][E366F27C].mp4
│   │   │   ├── [animegrimoire] Party kara Tsuihou sareta Sono Chiyushi, Jitsu wa Saikyou ni Tsuki - 02 [720p][95DD25CC].mp4
│   │   │   ├── [animegrimoire] Party kara Tsuihou sareta Sono Chiyushi, Jitsu wa Saikyou ni Tsuki - 03 [720p][C5D9CD4F].mp4
│   │   │   ├── [animegrimoire] Party kara Tsuihou sareta Sono Chiyushi, Jitsu wa Saikyou ni Tsuki - 04 [720p][3ECDD226].mp4
│   │   │   ├── [animegrimoire] Party kara Tsuihou sareta Sono Chiyushi, Jitsu wa Saikyou ni Tsuki - 05 [720p][159A259A].mp4
│   │   │   ├── [animegrimoire] Party kara Tsuihou sareta Sono Chiyushi, Jitsu wa Saikyou ni Tsuki - 06 [720p][586BBDE4].mp4
│   │   │   ├── [animegrimoire] Party kara Tsuihou sareta Sono Chiyushi, Jitsu wa Saikyou ni Tsuki - 07 [720p][4A58D6E7].mp4
│   │   │   ├── [animegrimoire] Party kara Tsuihou sareta Sono Chiyushi, Jitsu wa Saikyou ni Tsuki - 08 [720p][3E204AD8].mp4
│   │   │   ├── [animegrimoire] Party kara Tsuihou sareta Sono Chiyushi, Jitsu wa Saikyou ni Tsuki - 09 [720p][6FAF4B07].mp4
│   │   │   ├── [animegrimoire] Party kara Tsuihou sareta Sono Chiyushi, Jitsu wa Saikyou ni Tsuki - 10 [720p][CA38B157].mp4
│   │   │   ├── [animegrimoire] Party kara Tsuihou sareta Sono Chiyushi, Jitsu wa Saikyou ni Tsuki - 11 [720p][269BBB34].mp4
│   │   │   └── [animegrimoire] Party kara Tsuihou sareta Sono Chiyushi, Jitsu wa Saikyou ni Tsuki - 12 [720p][B4219618].mp4
│   │   ├── Puniru wa Kawaii Slime
│   │   │   ├── [animegrimoire] Puniru wa Kawaii Slime - 01 [720p][ED49114].mp4
│   │   │   ├── [animegrimoire] Puniru wa Kawaii Slime - 02 [720p][C2139579].mp4
│   │   │   ├── [animegrimoire] Puniru wa Kawaii Slime - 03 [720p][7F4DA9E0].mp4
│   │   │   ├── [animegrimoire] Puniru wa Kawaii Slime - 04 [720p][4DFC879B].mp4
│   │   │   ├── [animegrimoire] Puniru wa Kawaii Slime - 05 [720p][2A83E01].mp4
│   │   │   ├── [animegrimoire] Puniru wa Kawaii Slime - 06 [720p][AD7908E9].mp4
│   │   │   ├── [animegrimoire] Puniru wa Kawaii Slime - 07 [720p][261C5BB9].mp4
│   │   │   ├── [animegrimoire] Puniru wa Kawaii Slime - 08 [720p][345D3B5E].mp4
│   │   │   ├── [animegrimoire] Puniru wa Kawaii Slime - 09 [720p][F1192AE9].mp4
│   │   │   ├── [animegrimoire] Puniru wa Kawaii Slime - 10 [720p][7B57EEC3].mp4
│   │   │   ├── [animegrimoire] Puniru wa Kawaii Slime - 11 [720p][2976D98].mp4
│   │   │   └── [animegrimoire] Puniru wa Kawaii Slime - 12 [720p][D7AFE4ED].mp4
│   │   ├── Re Zero kara Hajimeru Isekai Seikatsu
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 51v2 [720p][54AC7E3A].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 52 [720p][D730B1A2].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 53 [720p][44C4CDF7].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 54 [720p][953E25CB].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 55 [720p][7A88146B].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 56 [720p][2AA3B40E].mp4
│   │   │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 57 [720p][6F2642D0].mp4
│   │   │   └── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 58 [720p][D9641EEF].mp4
│   │   ├── Tsuma, Shougakusei ni Naru
│   │   │   ├── [animegrimoire] Tsuma, Shougakusei ni Naru - 01 [720p][DDA05EE5].mp4
│   │   │   ├── [animegrimoire] Tsuma, Shougakusei ni Naru - 02 [720p][B710BB37].mp4
│   │   │   ├── [animegrimoire] Tsuma, Shougakusei ni Naru - 03 [720p][B87CB7EF].mp4
│   │   │   ├── [animegrimoire] Tsuma, Shougakusei ni Naru - 04 [720p][D7FDECFF].mp4
│   │   │   ├── [animegrimoire] Tsuma, Shougakusei ni Naru - 05 [720p][4380B970].mp4
│   │   │   ├── [animegrimoire] Tsuma, Shougakusei ni Naru - 06 [720p][1F811175].mp4
│   │   │   ├── [animegrimoire] Tsuma, Shougakusei ni Naru - 07 [720p][A4DEBE7F].mp4
│   │   │   ├── [animegrimoire] Tsuma, Shougakusei ni Naru - 08 [720p][EB13C0EE].mp4
│   │   │   ├── [animegrimoire] Tsuma, Shougakusei ni Naru - 09 [720p][354634E8].mp4
│   │   │   ├── [animegrimoire] Tsuma, Shougakusei ni Naru - 10 [720p][B075664A].mp4
│   │   │   ├── [animegrimoire] Tsuma, Shougakusei ni Naru - 11 [720p][78603756].mp4
│   │   │   └── [animegrimoire] Tsuma, Shougakusei ni Naru - 12 [720p][9256BFF9].mp4
│   │   └── Wonderful Precure!
│   │       ├── [animegrimoire] Wonderful Precure! - 36 [720p][150C3A15].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 37 [720p][8CC30804].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 38 [720p][649B3699].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 39 [720p][2A2867C8].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 40 [720p][D40E84EF].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 41 [720p][F3527C05].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 42 [720p][E842E08].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 43 [720p][4B3A787E].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 44 [720p][850470CA].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 45 [720p][A50CA74C].mp4
│   │       └── [animegrimoire] Wonderful Precure! - 46 [720p][10A2AC6D].mp4
│   ├── Spring
│   │   ├── Date a Live V
│   │   │   ├── [animegrimoire] Date a Live V - 01 [720p][6E137C45].mp4
│   │   │   ├── [animegrimoire] Date a Live V - 02 [720p][09D2FC7B].mp4
│   │   │   ├── [animegrimoire] Date a Live V - 03 [720p][ABB368F2].mp4
│   │   │   ├── [animegrimoire] Date a Live V - 04 [720p][37BFFEB0].mp4
│   │   │   ├── [animegrimoire] Date a Live V - 05 [720p][ADB164AE].mp4
│   │   │   ├── [animegrimoire] Date a Live V - 06 [720p][3B4E50F3].mp4
│   │   │   ├── [animegrimoire] Date a Live V - 07 [720p][BC130081].mp4
│   │   │   ├── [animegrimoire] Date a Live V - 08 [720p][327F1599].mp4
│   │   │   ├── [animegrimoire] Date a Live V - 09 [720p][B63215AC].mp4
│   │   │   ├── [animegrimoire] Date a Live V - 10 [720p][EBE4B84C].mp4
│   │   │   ├── [animegrimoire] Date a Live V - 11 [720p][A0DA4D64].mp4
│   │   │   └── [animegrimoire] Date a Live V - 12 [720p][3AC50D49].mp4
│   │   ├── Henjin no Salad Bowl
│   │   │   ├── [animegrimoire] Henjin no Salad Bowl - 01 [720p][D97768A5].mp4
│   │   │   ├── [animegrimoire] Henjin no Salad Bowl - 02 [720p][A37197B0].mp4
│   │   │   ├── [animegrimoire] Henjin no Salad Bowl - 03 [720p][AACD295D].mp4
│   │   │   ├── [animegrimoire] Henjin no Salad Bowl - 04 [720p][52AA719F].mp4
│   │   │   ├── [animegrimoire] Henjin no Salad Bowl - 05 [720p][5627F513].mp4
│   │   │   ├── [animegrimoire] Henjin no Salad Bowl - 06 [720p][72834CBE].mp4
│   │   │   ├── [animegrimoire] Henjin no Salad Bowl - 07 [720p][3460C74B].mp4
│   │   │   ├── [animegrimoire] Henjin no Salad Bowl - 08 [720p][ED19CFD3].mp4
│   │   │   ├── [animegrimoire] Henjin no Salad Bowl - 09 [720p][1F6B6C79].mp4
│   │   │   ├── [animegrimoire] Henjin no Salad Bowl - 10 [720p][9AE42DF2].mp4
│   │   │   ├── [animegrimoire] Henjin no Salad Bowl - 11 [720p][0AD18697].mp4
│   │   │   └── [animegrimoire] Henjin no Salad Bowl - 12 [720p][8A251203].mp4
│   │   ├── Hibike! Euphonium
│   │   │   └── [animegrimoire] Hibike! Euphonium - Chikai no Finale [720p][99319CA6].mp4
│   │   ├── Hibike! Euphonium S3
│   │   │   ├── [animegrimoire] Hibike! Euphonium S3 - 01 [720p][C25B0BF2].mp4
│   │   │   ├── [animegrimoire] Hibike! Euphonium S3 - 02 [720p][C845E125].mp4
│   │   │   ├── [animegrimoire] Hibike! Euphonium S3 - 03 [720p][CC07B727].mp4
│   │   │   ├── [animegrimoire] Hibike! Euphonium S3 - 04 [720p][2BC8EA11].mp4
│   │   │   ├── [animegrimoire] Hibike! Euphonium S3 - 05 [720p][0139F347].mp4
│   │   │   ├── [animegrimoire] Hibike! Euphonium S3 - 06 [720p][5449CA22].mp4
│   │   │   ├── [animegrimoire] Hibike! Euphonium S3 - 07 [720p][F94D4610].mp4
│   │   │   ├── [animegrimoire] Hibike! Euphonium S3 - 08 [720p][DCBC4BDD].mp4
│   │   │   ├── [animegrimoire] Hibike! Euphonium S3 - 09 [720p][7AE4908B].mp4
│   │   │   ├── [animegrimoire] Hibike! Euphonium S3 - 10 [720p][27CB1E5A].mp4
│   │   │   ├── [animegrimoire] Hibike! Euphonium S3 - 11 [720p][247CB17E].mp4
│   │   │   ├── [animegrimoire] Hibike! Euphonium S3 - 12 [720p][1E88E0C7].mp4
│   │   │   └── [animegrimoire] Hibike! Euphonium S3 - 13 [720p][A7344466].mp4
│   │   ├── Kimetsu no Yaiba
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Hashira Geiko-hen - 01 [720p][FABA212C].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Hashira Geiko-hen - 02 [720p][B43438C1].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Hashira Geiko-hen - 03 [720p][F870A94A].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Hashira Geiko-hen - 04 [720p][B3B99740].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Hashira Geiko-hen - 05 [720p][61B54CFB].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Hashira Geiko-hen - 06 [720p][8D542056].mp4
│   │   │   ├── [animegrimoire] Kimetsu no Yaiba - Hashira Geiko-hen - 07 [720p][0ABE2EBB].mp4
│   │   │   └── [animegrimoire] Kimetsu no Yaiba - Hashira Geiko-hen - 08 [720p][C1BCD66E].mp4
│   │   ├── Kono Subarashii Sekai ni Shukufuku wo! S3
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! S3 - 01 [720p][B9FB0F72].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! S3 - 02 [720p][BDA780C8].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! S3 - 03 [720p][851A0DAB].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! S3 - 04 [720p][FBEF8FA7].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! S3 - 05 [720p][1A08D8E6].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! S3 - 06 [720p][928A48D5].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! S3 - 07 [720p][3BBF9311].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! S3 - 08 [720p][4D8F14D5].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! S3 - 09 [720p][7C4BCC55].mp4
│   │   │   ├── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! S3 - 10 [720p][CCB49C97].mp4
│   │   │   └── [animegrimoire] Kono Subarashii Sekai ni Shukufuku wo! S3 - 11 [720p][69C182FF].mp4
│   │   ├── Mahouka Koukou no Rettousei S3
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei S3 - 01 [720p][5771A849].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei S3 - 02 [720p][EF7C597A].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei S3 - 03 [720p][846E38F4].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei S3 - 04 [720p][BC67ECF2].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei S3 - 05 [720p][0EE1BD1D].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei S3 - 06 [720p][2AC3211F].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei S3 - 07 [720p][CD5C0181].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei S3 - 08 [720p][9670C548].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei S3 - 09 [720p][4A64CAD2].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei S3 - 10 [720p][739FB2EF].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei S3 - 11 [720p][ADE0C531].mp4
│   │   │   ├── [animegrimoire] Mahouka Koukou no Rettousei S3 - 12 [720p][C8035A2B].mp4
│   │   │   └── [animegrimoire] Mahouka Koukou no Rettousei S3 - 13 [720p][64B93DC0].mp4
│   │   ├── Mushoku Tensei S2
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 13 [720p][BF6FA7B7].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 14 [720p][19C7E913].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 15 [720p][0AF76ECD].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 16 [720p][F36ED381].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 17 [720p][EDD62D07].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 18 [720p][536AB29A].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 19 [720p][AD1C5735].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 20 [720p][28C05460].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 21 [720p][D1A0CF16].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 22 [720p][F2ACF5BA].mp4
│   │   │   ├── [animegrimoire] Mushoku Tensei S2 - 23 [720p][4A1941E7].mp4
│   │   │   └── [animegrimoire] Mushoku Tensei S2 - 24 [720p][14BDE880].mp4
│   │   ├── Sasayaku You ni Koi wo Utau
│   │   │   ├── [animegrimoire] Sasayaku You ni Koi wo Utau - 01 [720p][90553FB8].mp4
│   │   │   ├── [animegrimoire] Sasayaku You ni Koi wo Utau - 02 [720p][49B88546].mp4
│   │   │   ├── [animegrimoire] Sasayaku You ni Koi wo Utau - 03 [720p][24D7513F].mp4
│   │   │   ├── [animegrimoire] Sasayaku You ni Koi wo Utau - 04 [720p][B7C7154A].mp4
│   │   │   ├── [animegrimoire] Sasayaku You ni Koi wo Utau - 05 [720p][122C217F].mp4
│   │   │   ├── [animegrimoire] Sasayaku You ni Koi wo Utau - 06 [720p][B14D1EEE].mp4
│   │   │   ├── [animegrimoire] Sasayaku You ni Koi wo Utau - 07 [720p][687BCE51].mp4
│   │   │   ├── [animegrimoire] Sasayaku You ni Koi wo Utau - 08 [720p][62BEBF88].mp4
│   │   │   ├── [animegrimoire] Sasayaku You ni Koi wo Utau - 09 [720p][E367B45D].mp4
│   │   │   ├── [animegrimoire] Sasayaku You ni Koi wo Utau - 10 [720p][00D63278].mp4
│   │   │   ├── [animegrimoire] Sasayaku You ni Koi wo Utau - 11 [720p][559E6162].mp4
│   │   │   └── [animegrimoire] Sasayaku You ni Koi wo Utau - 12 [720p][2FA1B1A1].mp4
│   │   ├── Shuumatsu Train Doko e Iku
│   │   │   ├── [animegrimoire] Shuumatsu Train Doko e Iku - 01 [720p][ABF01610].mp4
│   │   │   ├── [animegrimoire] Shuumatsu Train Doko e Iku - 02 [720p][E49825A6].mp4
│   │   │   ├── [animegrimoire] Shuumatsu Train Doko e Iku - 03 [720p][DC2189C7].mp4
│   │   │   ├── [animegrimoire] Shuumatsu Train Doko e Iku - 04 [720p][6E45A13F].mp4
│   │   │   ├── [animegrimoire] Shuumatsu Train Doko e Iku - 05 [720p][4415BADF].mp4
│   │   │   ├── [animegrimoire] Shuumatsu Train Doko e Iku - 06 [720p][0BD24C0A].mp4
│   │   │   ├── [animegrimoire] Shuumatsu Train Doko e Iku - 07 [720p][535EF158].mp4
│   │   │   ├── [animegrimoire] Shuumatsu Train Doko e Iku - 08 [720p][EC7A96F6].mp4
│   │   │   ├── [animegrimoire] Shuumatsu Train Doko e Iku - 09 [720p][36E967CC].mp4
│   │   │   ├── [animegrimoire] Shuumatsu Train Doko e Iku - 10 [720p][9FFADDD2].mp4
│   │   │   ├── [animegrimoire] Shuumatsu Train Doko e Iku - 11.5 [720p][548D2ABB].mp4
│   │   │   ├── [animegrimoire] Shuumatsu Train Doko e Iku - 11 [720p][4BD79EC8].mp4
│   │   │   └── [animegrimoire] Shuumatsu Train Doko e Iku - 12 [720p][18384A13].mp4
│   │   ├── Spice and Wolf
│   │   │   ├── [animegrimoire] Spice and Wolf - 01 [720p][18E32B8A].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 02 [720p][0EF2B4CC].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 03 [720p][D70A2161].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 04 [720p][29AF801C].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 05 [720p][02003CF6].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 06 [720p][437ABCF4].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 07 [720p][A12B2226].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 08 [720p][71C35632].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 09 [720p][52E0A8C7].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 10 [720p][2BE89F8E].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 11 [720p][89EABFF9].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 12 [720p][9CBE2128].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 13 [720p][C1BB9E8D].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 14 [720p][0B332F8C].mp4
│   │   │   └── [animegrimoire] Spice and Wolf - 15 [720p][12CCE422].mp4
│   │   ├── Tensei Shitara Slime Datta Ken
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 48.5 [720p][7C149AA6].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 49 [720p][B3F4929C].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 50 [720p][084CDB83].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 51 [720p][7511FFC8].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 52 [720p][5F6F9713].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 53 [720p][EA8872CE].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 54 [720p][1E665596].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 55 [720p][D8EE5702].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 56 [720p][F9CCFC98].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 57 [720p][7E5B596F].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 58 [720p][BFD3575A].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 59 [720p][08EC0E33].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 60 [720p][B8802566].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 61 [720p][0CEA0BD8].mp4
│   │   │   └── [animegrimoire] Tensei Shitara Slime Datta Ken - 62 [720p][D0CA65ED].mp4
│   │   ├── Wonderful Precure!
│   │   │   ├── [animegrimoire] Wonderful Precure! - 10 [720p][823CDA4A].mp4
│   │   │   ├── [animegrimoire] Wonderful Precure! - 11 [720p][581E404A].mp4
│   │   │   ├── [animegrimoire] Wonderful Precure! - 12 [720p][1FEA371B].mp4
│   │   │   ├── [animegrimoire] Wonderful Precure! - 13 [720p][F6EBBE34].mp4
│   │   │   ├── [animegrimoire] Wonderful Precure! - 14 [720p][FBF6AF71].mp4
│   │   │   ├── [animegrimoire] Wonderful Precure! - 15 [720p][13957820].mp4
│   │   │   ├── [animegrimoire] Wonderful Precure! - 16 [720p][27BDDEB8].mp4
│   │   │   ├── [animegrimoire] Wonderful Precure! - 17 [720p][23825FFE].mp4
│   │   │   ├── [animegrimoire] Wonderful Precure! - 18 [720p][CA1642DC].mp4
│   │   │   ├── [animegrimoire] Wonderful Precure! - 19 [720p][12B26BE5].mp4
│   │   │   ├── [animegrimoire] Wonderful Precure! - 20 [720p][F6A884A5].mp4
│   │   │   ├── [animegrimoire] Wonderful Precure! - 21 [720p][5265E5A3].mp4
│   │   │   ├── [animegrimoire] Wonderful Precure! - 22 [720p][C34B345D].mp4
│   │   │   └── [animegrimoire] Wonderful Precure! - 23 [720p][FA23AD81].mp4
│   │   └── Yuru Camp S3
│   │       ├── [animegrimoire] Yuru Camp S3 - 01 [720p][0D25B993].mp4
│   │       ├── [animegrimoire] Yuru Camp S3 - 02 [720p][35DA0180].mp4
│   │       ├── [animegrimoire] Yuru Camp S3 - 03 [720p][D990DBB6].mp4
│   │       ├── [animegrimoire] Yuru Camp S3 - 04 [720p][E3D69156].mp4
│   │       ├── [animegrimoire] Yuru Camp S3 - 05 [720p][AAAA436A].mp4
│   │       ├── [animegrimoire] Yuru Camp S3 - 06 [720p][389B0A43].mp4
│   │       ├── [animegrimoire] Yuru Camp S3 - 07 [720p][875606EA].mp4
│   │       ├── [animegrimoire] Yuru Camp S3 - 08 [720p][7F65ECCD].mp4
│   │       ├── [animegrimoire] Yuru Camp S3 - 09 [720p][7591EDB0].mp4
│   │       ├── [animegrimoire] Yuru Camp S3 - 10 [720p][D6082284].mp4
│   │       ├── [animegrimoire] Yuru Camp S3 - 11 [720p][2FF72E71].mp4
│   │       └── [animegrimoire] Yuru Camp S3 - 12 [720p][E616D3A9].mp4
│   ├── Summer
│   │   ├── Atri
│   │   │   ├── [animegrimoire] Atri - My Dear Moments - 01 [720p][E1452FD7].mp4
│   │   │   ├── [animegrimoire] Atri - My Dear Moments - 02 [720p][027181EC].mp4
│   │   │   ├── [animegrimoire] Atri - My Dear Moments - 03 [720p][22746EAE].mp4
│   │   │   ├── [animegrimoire] Atri - My Dear Moments - 04 [720p][38219CDE].mp4
│   │   │   ├── [animegrimoire] Atri - My Dear Moments - 05 [720p][7C03D61B].mp4
│   │   │   ├── [animegrimoire] Atri - My Dear Moments - 06 [720p][D515EBA0].mp4
│   │   │   ├── [animegrimoire] Atri - My Dear Moments - 07 [720p][E8A492B1].mp4
│   │   │   ├── [animegrimoire] Atri - My Dear Moments - 08 [720p][9915D9E5].mp4
│   │   │   ├── [animegrimoire] Atri - My Dear Moments - 09 [720p][953F8813].mp4
│   │   │   ├── [animegrimoire] Atri - My Dear Moments - 10 [720p][FB11C90D].mp4
│   │   │   ├── [animegrimoire] Atri - My Dear Moments - 11 [720p][AE1B07E2].mp4
│   │   │   └── [animegrimoire] Atri - My Dear Moments - 12 [720p][CEF48A50].mp4
│   │   ├── Delico's Nursery
│   │   │   ├── [animegrimoire] Delico's Nursery - 01 [720p].mp4
│   │   │   ├── [animegrimoire] Delico's Nursery - 02 [720p].mp4
│   │   │   ├── [animegrimoire] Delico's Nursery - 03 [720p].mp4
│   │   │   ├── [animegrimoire] Delico's Nursery - 04 [720p].mp4
│   │   │   ├── [animegrimoire] Delico's Nursery - 05 [720p].mp4
│   │   │   └── [animegrimoire] Delico's Nursery - 06 [720p].mp4
│   │   ├── Isekai Yururi Kikou
│   │   │   ├── [animegrimoire] Isekai Yururi Kikou - 01 [720p][F1FF7001].mp4
│   │   │   ├── [animegrimoire] Isekai Yururi Kikou - 02 [720p][8B43175D].mp4
│   │   │   ├── [animegrimoire] Isekai Yururi Kikou - 03 [720p][0181CAB7].mp4
│   │   │   ├── [animegrimoire] Isekai Yururi Kikou - 04 [720p][A85EDB22].mp4
│   │   │   ├── [animegrimoire] Isekai Yururi Kikou - 05 [720p][6BC75D2C].mp4
│   │   │   ├── [animegrimoire] Isekai Yururi Kikou - 06 [720p][BAD9CE94].mp4
│   │   │   ├── [animegrimoire] Isekai Yururi Kikou - 07 [720p][D33DB891].mp4
│   │   │   ├── [animegrimoire] Isekai Yururi Kikou - 08 [720p][71829963].mp4
│   │   │   ├── [animegrimoire] Isekai Yururi Kikou - 09 [720p][9E529098].mp4
│   │   │   ├── [animegrimoire] Isekai Yururi Kikou - 10 [720p][F597231C].mp4
│   │   │   ├── [animegrimoire] Isekai Yururi Kikou - 11 [720p][A6A7085C].mp4
│   │   │   └── [animegrimoire] Isekai Yururi Kikou - 12 [720p][5C36C08D].mp4
│   │   ├── Koi wa Futago de Warikirenai
│   │   │   ├── [animegrimoire] Koi wa Futago de Warikirenai - 01 [720p][8A95452B].mp4
│   │   │   ├── [animegrimoire] Koi wa Futago de Warikirenai - 02 [720p][F7505A88].mp4
│   │   │   ├── [animegrimoire] Koi wa Futago de Warikirenai - 03 [720p][2067C1C8].mp4
│   │   │   ├── [animegrimoire] Koi wa Futago de Warikirenai - 04 [720p][40CE0627].mp4
│   │   │   ├── [animegrimoire] Koi wa Futago de Warikirenai - 05 [720p][EB490D66].mp4
│   │   │   ├── [animegrimoire] Koi wa Futago de Warikirenai - 06 [720p][B5652A8E].mp4
│   │   │   ├── [animegrimoire] Koi wa Futago de Warikirenai - 07 [720p][14FB3329].mp4
│   │   │   ├── [animegrimoire] Koi wa Futago de Warikirenai - 08 [720p][04DB3181].mp4
│   │   │   ├── [animegrimoire] Koi wa Futago de Warikirenai - 09 [720p][2B7803D2].mp4
│   │   │   ├── [animegrimoire] Koi wa Futago de Warikirenai - 10 [720p][62BA3A79].mp4
│   │   │   ├── [animegrimoire] Koi wa Futago de Warikirenai - 11 [720p][7FA3B80D].mp4
│   │   │   └── [animegrimoire] Koi wa Futago de Warikirenai - 12 [720p][3039B84A].mp4
│   │   ├── Make Heroine ga Oosugiru!
│   │   │   ├── [animegrimoire] Make Heroine ga Oosugiru! - 01 [720p][63CB9D25].mp4
│   │   │   ├── [animegrimoire] Make Heroine ga Oosugiru! - 02 [720p][5CD9D05F].mp4
│   │   │   ├── [animegrimoire] Make Heroine ga Oosugiru! - 03 [720p][1A1F92DD].mp4
│   │   │   ├── [animegrimoire] Make Heroine ga Oosugiru! - 04 [720p][D4E83689].mp4
│   │   │   ├── [animegrimoire] Make Heroine ga Oosugiru! - 05 [720p][4D093485].mp4
│   │   │   ├── [animegrimoire] Make Heroine ga Oosugiru! - 06 [720p][9D365648].mp4
│   │   │   ├── [animegrimoire] Make Heroine ga Oosugiru! - 07 [720p][50A241AC].mp4
│   │   │   ├── [animegrimoire] Make Heroine ga Oosugiru! - 08 [720p][30C7ADB5].mp4
│   │   │   ├── [animegrimoire] Make Heroine ga Oosugiru! - 09 [720p][AA32869E].mp4
│   │   │   ├── [animegrimoire] Make Heroine ga Oosugiru! - 10 [720p][F1824EC3].mp4
│   │   │   ├── [animegrimoire] Make Heroine ga Oosugiru! - 11 [720p][DE955651].mp4
│   │   │   └── [animegrimoire] Make Heroine ga Oosugiru! - 12 [720p][14D8A4E2].mp4
│   │   ├── Mayonaka Punch
│   │   │   ├── [animegrimoire] Mayonaka Punch - 01 [720p][73733A55].mp4
│   │   │   ├── [animegrimoire] Mayonaka Punch - 02 [720p][D8A8C9D2].mp4
│   │   │   ├── [animegrimoire] Mayonaka Punch - 03 [720p][08DAFF7D].mp4
│   │   │   ├── [animegrimoire] Mayonaka Punch - 04 [720p][08EAF5E8].mp4
│   │   │   ├── [animegrimoire] Mayonaka Punch - 05 [720p][66FF3C36].mp4
│   │   │   ├── [animegrimoire] Mayonaka Punch - 06 [720p][8714C7C0].mp4
│   │   │   ├── [animegrimoire] Mayonaka Punch - 07 [720p][42687A27].mp4
│   │   │   ├── [animegrimoire] Mayonaka Punch - 08 [720p][D32C333C].mp4
│   │   │   ├── [animegrimoire] Mayonaka Punch - 09 [720p][6796A18C].mp4
│   │   │   ├── [animegrimoire] Mayonaka Punch - 10 [720p][3AEAE4E4].mp4
│   │   │   ├── [animegrimoire] Mayonaka Punch - 11 [720p][7374A3FB].mp4
│   │   │   └── [animegrimoire] Mayonaka Punch - 12 [720p][EF7665CE].mp4
│   │   ├── Monogatari Series
│   │   │   ├── [animegrimoire] Monogatari Series - Off & Monster Season - 01 [720p][8E34D90E].mp4
│   │   │   ├── [animegrimoire] Monogatari Series - Off & Monster Season - 02 [720p][9750D31A].mp4
│   │   │   ├── [animegrimoire] Monogatari Series - Off & Monster Season - 03 [720p][BC97C562].mp4
│   │   │   ├── [animegrimoire] Monogatari Series - Off & Monster Season - 04 [720p][61C09F60].mp4
│   │   │   ├── [animegrimoire] Monogatari Series - Off & Monster Season - 05 [720p][C4874568].mp4
│   │   │   ├── [animegrimoire] Monogatari Series - Off & Monster Season - 06.5 [720p][ACC83412].mp4
│   │   │   ├── [animegrimoire] Monogatari Series - Off & Monster Season - 06 [720p][EFA781A8].mp4
│   │   │   ├── [animegrimoire] Monogatari Series - Off & Monster Season - 07 [720p][082050CF].mp4
│   │   │   ├── [animegrimoire] Monogatari Series - Off & Monster Season - 08 [720p][80573015].mp4
│   │   │   ├── [animegrimoire] Monogatari Series - Off & Monster Season - 09 [720p][E608668B].mp4
│   │   │   ├── [animegrimoire] Monogatari Series - Off & Monster Season - 09v2 [720p][A842FCDA].mp4
│   │   │   ├── [animegrimoire] Monogatari Series - Off & Monster Season - 10 [720p][5E0D1617].mp4
│   │   │   └── [animegrimoire] Monogatari Series - Off & Monster Season - 11 [720p][F6FD5D6C].mp4
│   │   ├── Na Nare Hana Nare
│   │   │   ├── [animegrimoire] Na Nare Hana Nare - 01 [720p][35C38E8C].mp4
│   │   │   ├── [animegrimoire] Na Nare Hana Nare - 02 [720p][D85AB47B].mp4
│   │   │   ├── [animegrimoire] Na Nare Hana Nare - 03 [720p][E84E7046].mp4
│   │   │   ├── [animegrimoire] Na Nare Hana Nare - 04 [720p][9FBBB4A5].mp4
│   │   │   ├── [animegrimoire] Na Nare Hana Nare - 05 [720p][1D739F77].mp4
│   │   │   ├── [animegrimoire] Na Nare Hana Nare - 06 [720p][8C830D29].mp4
│   │   │   ├── [animegrimoire] Na Nare Hana Nare - 07 [720p][318CDD96].mp4
│   │   │   ├── [animegrimoire] Na Nare Hana Nare - 08 [720p][CA9B89BA].mp4
│   │   │   ├── [animegrimoire] Na Nare Hana Nare - 09 [720p][AC0CC292].mp4
│   │   │   ├── [animegrimoire] Na Nare Hana Nare - 10 [720p][2F5F722A].mp4
│   │   │   ├── [animegrimoire] Na Nare Hana Nare - 11 [720p][022EE177].mp4
│   │   │   └── [animegrimoire] Na Nare Hana Nare - 12 [720p][68C364E4].mp4
│   │   ├── Shikanoko Nokonoko Koshitantan
│   │   │   ├── [animegrimoire] Shikanoko Nokonoko Koshitantan - 01 [720p][F7B8782B].mp4
│   │   │   ├── [animegrimoire] Shikanoko Nokonoko Koshitantan - 01v2 [720p][A6B173A6].mp4
│   │   │   ├── [animegrimoire] Shikanoko Nokonoko Koshitantan - 02 [720p][5140D7BD].mp4
│   │   │   ├── [animegrimoire] Shikanoko Nokonoko Koshitantan - 03 [720p][8033E86D].mp4
│   │   │   ├── [animegrimoire] Shikanoko Nokonoko Koshitantan - 04 [720p][6F5F2335].mp4
│   │   │   ├── [animegrimoire] Shikanoko Nokonoko Koshitantan - 05 [720p][8D34F870].mp4
│   │   │   ├── [animegrimoire] Shikanoko Nokonoko Koshitantan - 06 [720p][D9DEF35D].mp4
│   │   │   ├── [animegrimoire] Shikanoko Nokonoko Koshitantan - 07 [720p][44BFA70C].mp4
│   │   │   ├── [animegrimoire] Shikanoko Nokonoko Koshitantan - 08 [720p][14DDAC87].mp4
│   │   │   ├── [animegrimoire] Shikanoko Nokonoko Koshitantan - 09 [720p][FCD96083].mp4
│   │   │   ├── [animegrimoire] Shikanoko Nokonoko Koshitantan - 10 [720p][3FE402FD].mp4
│   │   │   ├── [animegrimoire] Shikanoko Nokonoko Koshitantan - 11 [720p][7E5E85D7].mp4
│   │   │   └── [animegrimoire] Shikanoko Nokonoko Koshitantan - 12 [720p][990C6070].mp4
│   │   ├── Shoushimin Series
│   │   │   ├── [animegrimoire] Shoushimin Series - 01 [720p][4E7BCFBC].mp4
│   │   │   ├── [animegrimoire] Shoushimin Series - 02 [720p][8CFE1C5B].mp4
│   │   │   ├── [animegrimoire] Shoushimin Series - 03 [720p][926DE965].mp4
│   │   │   ├── [animegrimoire] Shoushimin Series - 04 [720p][B28FF1FE].mp4
│   │   │   ├── [animegrimoire] Shoushimin Series - 05 [720p][58992C7A].mp4
│   │   │   ├── [animegrimoire] Shoushimin Series - 06 [720p][603BA652].mp4
│   │   │   ├── [animegrimoire] Shoushimin Series - 07 [720p][B7894748].mp4
│   │   │   ├── [animegrimoire] Shoushimin Series - 08 [720p][2144FADF].mp4
│   │   │   ├── [animegrimoire] Shoushimin Series - 09 [720p][3885D053].mp4
│   │   │   └── [animegrimoire] Shoushimin Series - 10 [720p][351107B9].mp4
│   │   ├── Spice and Wolf
│   │   │   ├── [animegrimoire] Spice and Wolf - 16 [720p][D1901A97].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 17 [720p][1D67C693].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 18 [720p][C8CA3E10].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 19 [720p][31A6EAFC].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 20 [720p][F306A517].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 21 [720p][0003F3DE].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 22 [720p][D927105C].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 23 [720p][A1F9C212].mp4
│   │   │   ├── [animegrimoire] Spice and Wolf - 24 [720p][EB66D7FF].mp4
│   │   │   └── [animegrimoire] Spice and Wolf - 25 [720p][232AE9A1].mp4
│   │   ├── Spy x Family
│   │   │   └── [animegrimoire] Spy x Family - Code White [720p][7F4F4B90].mp4
│   │   ├── Tensei Shitara Slime Datta Ken
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 63 [720p][C9B798D9].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 64 [720p][0A4519C5].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 65.5 [720p][719CD9B1].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 65 [720p][C90859A6].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 66v2 [720p][16223B79].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 67 [720p][9EC0ADB8].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 68 [720p][932B5FC8].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 69 [720p][0DE25D3C].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 70 [720p][DEDBE2C6].mp4
│   │   │   ├── [animegrimoire] Tensei Shitara Slime Datta Ken - 71 [720p][F45C9CA2].mp4
│   │   │   └── [animegrimoire] Tensei Shitara Slime Datta Ken - 72 [720p][B8A7BD57].mp4
│   │   ├── Tensui no Sakuna-hime
│   │   │   ├── [animegrimoire] Tensui no Sakuna-hime - 01 [720p][AB690597].mp4
│   │   │   ├── [animegrimoire] Tensui no Sakuna-hime - 02 [720p][14366CD1].mp4
│   │   │   ├── [animegrimoire] Tensui no Sakuna-hime - 03 [720p][81EC17CE].mp4
│   │   │   ├── [animegrimoire] Tensui no Sakuna-hime - 04 [720p][D77E22C1].mp4
│   │   │   ├── [animegrimoire] Tensui no Sakuna-hime - 05 [720p][83F66F79].mp4
│   │   │   ├── [animegrimoire] Tensui no Sakuna-hime - 06 [720p][52C2CAC5].mp4
│   │   │   ├── [animegrimoire] Tensui no Sakuna-hime - 07 [720p][6C7EB4F7].mp4
│   │   │   ├── [animegrimoire] Tensui no Sakuna-hime - 08 [720p][8E0EA9EA].mp4
│   │   │   ├── [animegrimoire] Tensui no Sakuna-hime - 09 [720p][80F120AD].mp4
│   │   │   ├── [animegrimoire] Tensui no Sakuna-hime - 10 [720p][9A42745D].mp4
│   │   │   ├── [animegrimoire] Tensui no Sakuna-hime - 11 [720p][6AA6684A].mp4
│   │   │   ├── [animegrimoire] Tensui no Sakuna-hime - 12 [720p][CC531DB2].mp4
│   │   │   └── [animegrimoire] Tensui no Sakuna-hime - 13 [720p][E5084859].mp4
│   │   ├── Tokidoki Bosotto Russia-go de Dereru Tonari no Alya-san
│   │   │   ├── [animegrimoire] Tokidoki Bosotto Russia-go de Dereru Tonari no Alya-san - 01 [720p][B9468EB3].mp4
│   │   │   ├── [animegrimoire] Tokidoki Bosotto Russia-go de Dereru Tonari no Alya-san - 02 [720p][9C321E8B].mp4
│   │   │   ├── [animegrimoire] Tokidoki Bosotto Russia-go de Dereru Tonari no Alya-san - 03 [720p][940F0481].mp4
│   │   │   ├── [animegrimoire] Tokidoki Bosotto Russia-go de Dereru Tonari no Alya-san - 04 [720p][103DBCF7].mp4
│   │   │   ├── [animegrimoire] Tokidoki Bosotto Russia-go de Dereru Tonari no Alya-san - 05 [720p][F6776A52].mp4
│   │   │   ├── [animegrimoire] Tokidoki Bosotto Russia-go de Dereru Tonari no Alya-san - 06 [720p][67621AF4].mp4
│   │   │   ├── [animegrimoire] Tokidoki Bosotto Russia-go de Dereru Tonari no Alya-san - 07 [720p][32B0A3B8].mp4
│   │   │   ├── [animegrimoire] Tokidoki Bosotto Russia-go de Dereru Tonari no Alya-san - 08 [720p][457AC7BB].mp4
│   │   │   ├── [animegrimoire] Tokidoki Bosotto Russia-go de Dereru Tonari no Alya-san - 09 [720p][904AE0AC].mp4
│   │   │   ├── [animegrimoire] Tokidoki Bosotto Russia-go de Dereru Tonari no Alya-san - 10 [720p][A3DAA702].mp4
│   │   │   ├── [animegrimoire] Tokidoki Bosotto Russia-go de Dereru Tonari no Alya-san - 11 [720p][930D5B27].mp4
│   │   │   └── [animegrimoire] Tokidoki Bosotto Russia-go de Dereru Tonari no Alya-san - 12 [720p][355FF420].mp4
│   │   └── Wonderful Precure!
│   │       ├── [animegrimoire] Wonderful Precure! - 24 [720p][147D2432].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 25 [720p][B2CBF677].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 26 [720p][F0D2CD44].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 27 [720p][7C929D31].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 28 [720p][0FC1455D].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 29 [720p][49A22C23].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 30 [720p][636A1D1E].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 31 [720p][C846A939].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 32 [720p][55AE9CFF].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 33 [720p][60F11635].mp4
│   │       ├── [animegrimoire] Wonderful Precure! - 34 [720p][917B8DF2].mp4
│   │       └── [animegrimoire] Wonderful Precure! - 35 [720p][B5CCB43C].mp4
│   └── Winter
│       ├── Isekai de Mofumofu Nadenade suru Tame ni Ganbattemasu
│       │   ├── [animegrimoire] Isekai de Mofumofu Nadenade suru Tame ni Ganbattemasu - 01 [720p][47644632].mp4
│       │   ├── [animegrimoire] Isekai de Mofumofu Nadenade suru Tame ni Ganbattemasu - 02 [720p][6AC56A29].mp4
│       │   ├── [animegrimoire] Isekai de Mofumofu Nadenade suru Tame ni Ganbattemasu - 03 [720p][8E8E733E].mp4
│       │   ├── [animegrimoire] Isekai de Mofumofu Nadenade suru Tame ni Ganbattemasu - 04 [720p][FFE3E53C].mp4
│       │   ├── [animegrimoire] Isekai de Mofumofu Nadenade suru Tame ni Ganbattemasu - 05 [720p][1BDED719].mp4
│       │   ├── [animegrimoire] Isekai de Mofumofu Nadenade suru Tame ni Ganbattemasu - 06 [720p][0925059A].mp4
│       │   ├── [animegrimoire] Isekai de Mofumofu Nadenade suru Tame ni Ganbattemasu - 07 [720p][38FD8934].mp4
│       │   ├── [animegrimoire] Isekai de Mofumofu Nadenade suru Tame ni Ganbattemasu - 08 [720p][B468C891].mp4
│       │   ├── [animegrimoire] Isekai de Mofumofu Nadenade suru Tame ni Ganbattemasu - 09 [720p][2E1296A1].mp4
│       │   ├── [animegrimoire] Isekai de Mofumofu Nadenade suru Tame ni Ganbattemasu - 10 [720p][5D605A83].mp4
│       │   ├── [animegrimoire] Isekai de Mofumofu Nadenade suru Tame ni Ganbattemasu - 11 [720p][962F2FE2].mp4
│       │   └── [animegrimoire] Isekai de Mofumofu Nadenade suru Tame ni Ganbattemasu - 12 [720p][2AB983B0].mp4
│       ├── Kusuriya no Hitorigoto
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 13 [720p][E8D07013].mp4
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 14 [720p][392A85A5].mp4
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 15 [720p][8627EA47].mp4
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 16 [720p][C690E498].mp4
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 17 [720p][FD0B8507].mp4
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 18 [720p][ECD5735D].mp4
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 19 [720p][03871DCD].mp4
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 20 [720p][A33D231D].mp4
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 21 [720p][046A9B78].mp4
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 22 [720p][B8FE2B7D].mp4
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 23 [720p][C9814532].mp4
│       │   └── [animegrimoire] Kusuriya no Hitorigoto - 24 [720p][6808829B].mp4
│       ├── Mahou Shoujo ni Akogarete
│       │   ├── [animegrimoire] Mahou Shoujo ni Akogarete - 01 [720p][B17E1869].mp4
│       │   ├── [animegrimoire] Mahou Shoujo ni Akogarete - 02 [720p][85BE2D1C].mp4
│       │   ├── [animegrimoire] Mahou Shoujo ni Akogarete - 03 [720p][76CD274C].mp4
│       │   ├── [animegrimoire] Mahou Shoujo ni Akogarete - 04 [720p][48432FDB].mp4
│       │   ├── [animegrimoire] Mahou Shoujo ni Akogarete - 05 [720p][22E08E52].mp4
│       │   ├── [animegrimoire] Mahou Shoujo ni Akogarete - 06 [720p][5AE4FBF1].mp4
│       │   ├── [animegrimoire] Mahou Shoujo ni Akogarete - 07 [720p][C472B26E].mp4
│       │   ├── [animegrimoire] Mahou Shoujo ni Akogarete - 08 [720p][6E2F7FD3].mp4
│       │   ├── [animegrimoire] Mahou Shoujo ni Akogarete - 09 [720p][2B51CC21].mp4
│       │   ├── [animegrimoire] Mahou Shoujo ni Akogarete - 10 [720p][2AF329FB].mp4
│       │   ├── [animegrimoire] Mahou Shoujo ni Akogarete - 11 [720p][599F8F60].mp4
│       │   ├── [animegrimoire] Mahou Shoujo ni Akogarete - 12 [720p][852C399B].mp4
│       │   └── [animegrimoire] Mahou Shoujo ni Akogarete - 13 [720p][8CE2E370].mp4
│       ├── Natsu e no Tunnel Sayonara no Deguchi
│       │   └── [animegrimoire] Natsu e no Tunnel Sayonara no Deguchi [720p][87DB79DC].mp4
│       ├── Sousou no Frieren
│       │   ├── [animegrimoire] Sousou no Frieren - 17 [720p][5C885A95].mp4
│       │   ├── [animegrimoire] Sousou no Frieren - 18 [720p][32DF2288].mp4
│       │   ├── [animegrimoire] Sousou no Frieren - 19 [720p][FEEA6986].mp4
│       │   ├── [animegrimoire] Sousou no Frieren - 20 [720p][7CC392A3].mp4
│       │   ├── [animegrimoire] Sousou no Frieren - 21 [720p][E7F08F64].mp4
│       │   ├── [animegrimoire] Sousou no Frieren - 22 [720p][4F57EF81].mp4
│       │   ├── [animegrimoire] Sousou no Frieren - 23 [720p][27862E24].mp4
│       │   ├── [animegrimoire] Sousou no Frieren - 24 [720p][F5E45363].mp4
│       │   ├── [animegrimoire] Sousou no Frieren - 25 [720p][BBF16EE0].mp4
│       │   ├── [animegrimoire] Sousou no Frieren - 26 [720p][7894C857].mp4
│       │   ├── [animegrimoire] Sousou no Frieren - 27 [720p][6ABD9684].mp4
│       │   └── [animegrimoire] Sousou no Frieren - 28 [720p][60468014].mp4
│       └── Wonderful Precure!
│           ├── [animegrimoire] Wonderful Precure! - 01 [720p][0CD2FFC6].mp4
│           ├── [animegrimoire] Wonderful Precure! - 02 [720p][D176E940].mp4
│           ├── [animegrimoire] Wonderful Precure! - 03 [720p][55375A88].mp4
│           ├── [animegrimoire] Wonderful Precure! - 04 [720p][C6ECF7DC].mp4
│           ├── [animegrimoire] Wonderful Precure! - 05 [720p][DD80A033].mp4
│           ├── [animegrimoire] Wonderful Precure! - 06 [720p][A07EB878].mp4
│           ├── [animegrimoire] Wonderful Precure! - 07 [720p][2A129F74].mp4
│           ├── [animegrimoire] Wonderful Precure! - 08 [720p][B6B4110E].mp4
│           └── [animegrimoire] Wonderful Precure! - 09 [720p][95645B6C].mp4
├── 2025
│   └── Winter
│       ├── Arifureta Shokugyou de Sekai Saikyou S3
│       │   ├── [animegrimoire] Arifureta Shokugyou de Sekai Saikyou S3 - 12 [720p][7EC7B117].mp4
│       │   ├── [animegrimoire] Arifureta Shokugyou de Sekai Saikyou S3 - 13 [720p][1B520532].mp4
│       │   ├── [animegrimoire] Arifureta Shokugyou de Sekai Saikyou S3 - 14 [720p][C3A678F7].mp4
│       │   ├── [animegrimoire] Arifureta Shokugyou de Sekai Saikyou S3 - 15 [720p][7777F148].mp4
│       │   └── [animegrimoire] Arifureta Shokugyou de Sekai Saikyou S3 - 16 [720p][CFA8B86A].mp4
│       ├── Class no Daikirai na Joshi to Kekkon suru Koto ni Natta
│       │   ├── [animegrimoire] Class no Daikirai na Joshi to Kekkon suru Koto ni Natta - 01 [720p][A4870A9E].mp4
│       │   ├── [animegrimoire] Class no Daikirai na Joshi to Kekkon suru Koto ni Natta - 02 [720p][C7D27109].mp4
│       │   ├── [animegrimoire] Class no Daikirai na Joshi to Kekkon suru Koto ni Natta - 03 [720p][CD28FDDA].mp4
│       │   ├── [animegrimoire] Class no Daikirai na Joshi to Kekkon suru Koto ni Natta - 04 [720p][C05AB921].mp4
│       │   ├── [animegrimoire] Class no Daikirai na Joshi to Kekkon suru Koto ni Natta - 05 [720p][C6F6007B].mp4
│       │   ├── [animegrimoire] Class no Daikirai na Joshi to Kekkon suru Koto ni Natta - 06 [720p][3BB989F3].mp4
│       │   ├── [animegrimoire] Class no Daikirai na Joshi to Kekkon suru Koto ni Natta - 07 [720p][FCB634F].mp4
│       │   ├── [animegrimoire] Class no Daikirai na Joshi to Kekkon suru Koto ni Natta - 08 [720p][3C5622F5].mp4
│       │   └── [animegrimoire] Class no Daikirai na Joshi to Kekkon suru Koto ni Natta - 09 [720p][695068FE].mp4
│       ├── Dr. Stone S4
│       │   ├── [animegrimoire] Dr. Stone S4 - 01 [720p][48869EEE].mp4
│       │   ├── [animegrimoire] Dr. Stone S4 - 02 [720p][CACEC793].mp4
│       │   ├── [animegrimoire] Dr. Stone S4 - 03 [720p][E88C2B01].mp4
│       │   ├── [animegrimoire] Dr. Stone S4 - 04 [720p][669A11AD].mp4
│       │   ├── [animegrimoire] Dr. Stone S4 - 05 [720p][2E37E34D].mp4
│       │   ├── [animegrimoire] Dr. Stone S4 - 06 [720p][D4E78FAE].mp4
│       │   ├── [animegrimoire] Dr. Stone S4 - 07 [720p][22E17C53].mp4
│       │   ├── [animegrimoire] Dr. Stone S4 - 08 [720p][2879FAB1].mp4
│       │   └── [animegrimoire] Dr. Stone S4 - 09 [720p][BE4D9209].mp4
│       ├── Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S5
│       │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S5 - 12 [720p][65293AE2].mp4
│       │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S5 - 13 [720p][AF814777].mp4
│       │   ├── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S5 - 14 [720p][5F935F71].mp4
│       │   └── [animegrimoire] Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka S5 - 15 [720p][2C417F0].mp4
│       ├── Fate Strange Fake
│       │   └── [animegrimoire] Fate Strange Fake - 01 [720p][A7623E6].mp4
│       ├── Hana wa Saku, Shura no Gotoku
│       │   ├── [animegrimoire] Hana wa Saku, Shura no Gotoku - 01 [720p][4D3A09D6].mp4
│       │   ├── [animegrimoire] Hana wa Saku, Shura no Gotoku - 02 [720p][C901F115].mp4
│       │   ├── [animegrimoire] Hana wa Saku, Shura no Gotoku - 03 [720p][2FFE2431].mp4
│       │   ├── [animegrimoire] Hana wa Saku, Shura no Gotoku - 04 [720p][DACEE889].mp4
│       │   ├── [animegrimoire] Hana wa Saku, Shura no Gotoku - 05 [720p][48A6F4DD].mp4
│       │   ├── [animegrimoire] Hana wa Saku, Shura no Gotoku - 06 [720p][5487A5E1].mp4
│       │   ├── [animegrimoire] Hana wa Saku, Shura no Gotoku - 07 [720p][A6650470].mp4
│       │   ├── [animegrimoire] Hana wa Saku, Shura no Gotoku - 08 [720p][277F256C].mp4
│       │   └── [animegrimoire] Hana wa Saku, Shura no Gotoku - 09 [720p][A7FFB2D2].mp4
│       ├── Hokkyoku Hyakkaten no Concierge-san
│       │   └── [animegrimoire] Hokkyoku Hyakkaten no Concierge-san - 00 [720p][B43D1ABD].mp4
│       ├── Honey Lemon Soda
│       │   ├── [animegrimoire] Honey Lemon Soda - 01 [720p][BE423B30].mp4
│       │   ├── [animegrimoire] Honey Lemon Soda - 02 [720p][D5A438E9].mp4
│       │   ├── [animegrimoire] Honey Lemon Soda - 03 [720p][19DDE70A].mp4
│       │   ├── [animegrimoire] Honey Lemon Soda - 04 [720p][7C093271].mp4
│       │   ├── [animegrimoire] Honey Lemon Soda - 05 [720p][A0BEF1E5].mp4
│       │   ├── [animegrimoire] Honey Lemon Soda - 06 [720p][834044DE].mp4
│       │   ├── [animegrimoire] Honey Lemon Soda - 07 [720p][5CBB17C4].mp4
│       │   ├── [animegrimoire] Honey Lemon Soda - 08 [720p][A26D6FB3].mp4
│       │   └── [animegrimoire] Honey Lemon Soda - 09 [720p][F8B6062E].mp4
│       ├── Izure Saikyou no Renkinjutsushi
│       │   ├── [animegrimoire] Izure Saikyou no Renkinjutsushi - 01 [720p][E3975823].mp4
│       │   ├── [animegrimoire] Izure Saikyou no Renkinjutsushi - 02 [720p][786A7F39].mp4
│       │   ├── [animegrimoire] Izure Saikyou no Renkinjutsushi - 03 [720p][B8380583].mp4
│       │   ├── [animegrimoire] Izure Saikyou no Renkinjutsushi - 04 [720p][168B949F].mp4
│       │   ├── [animegrimoire] Izure Saikyou no Renkinjutsushi - 05 [720p][CCD69CE0].mp4
│       │   ├── [animegrimoire] Izure Saikyou no Renkinjutsushi - 06 [720p][4C99952F].mp4
│       │   ├── [animegrimoire] Izure Saikyou no Renkinjutsushi - 07 [720p][388C6C8B].mp4
│       │   ├── [animegrimoire] Izure Saikyou no Renkinjutsushi - 08 [720p][2E4C0140].mp4
│       │   ├── [animegrimoire] Izure Saikyou no Renkinjutsushi - 09 [720p][356DF806].mp4
│       │   └── [animegrimoire] Izure Saikyou no Renkinjutsushi - 10 [720p][B092A591].mp4
│       ├── Kimi to Idol Precure
│       │   ├── [animegrimoire] Kimi to Idol Precure - 01 [720p][8C2A111E].mp4
│       │   ├── [animegrimoire] Kimi to Idol Precure - 02 [720p][78C9EF6F].mp4
│       │   ├── [animegrimoire] Kimi to Idol Precure - 03 [720p][B868E0D1].mp4
│       │   ├── [animegrimoire] Kimi to Idol Precure - 04 [720p][275A3835].mp4
│       │   └── [animegrimoire] Kimi to Idol Precure - 05 [720p][CE3CAF4].mp4
│       ├── Kuroiwa Medaka ni Watashi no Kawaii ga Tsuujinai
│       │   ├── [animegrimoire] Kuroiwa Medaka ni Watashi no Kawaii ga Tsuujinai - 01 [720p][6F42453C].mp4
│       │   ├── [animegrimoire] Kuroiwa Medaka ni Watashi no Kawaii ga Tsuujinai - 02 [720p][3CAC09D9].mp4
│       │   ├── [animegrimoire] Kuroiwa Medaka ni Watashi no Kawaii ga Tsuujinai - 03 [720p][AA48E896].mp4
│       │   ├── [animegrimoire] Kuroiwa Medaka ni Watashi no Kawaii ga Tsuujinai - 04 [720p][ABFC2BE3].mp4
│       │   ├── [animegrimoire] Kuroiwa Medaka ni Watashi no Kawaii ga Tsuujinai - 05 [720p][C3BEF96B].mp4
│       │   ├── [animegrimoire] Kuroiwa Medaka ni Watashi no Kawaii ga Tsuujinai - 06 [720p][1FD71402].mp4
│       │   ├── [animegrimoire] Kuroiwa Medaka ni Watashi no Kawaii ga Tsuujinai - 07 [720p][10406E61].mp4
│       │   ├── [animegrimoire] Kuroiwa Medaka ni Watashi no Kawaii ga Tsuujinai - 08 [720p][139D96D5].mp4
│       │   └── [animegrimoire] Kuroiwa Medaka ni Watashi no Kawaii ga Tsuujinai - 09 [720p][16396C2D].mp4
│       ├── Kusuriya no Hitorigoto
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 25 [720p][DB12C57E].mp4
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 26 [720p][419F07B2].mp4
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 27 [720p][B3272E83].mp4
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 28 [720p][909A226A].mp4
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 29 [720p][E0F6A8B1].mp4
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 30 [720p][F820383B].mp4
│       │   ├── [animegrimoire] Kusuriya no Hitorigoto - 31 [720p][10D15C33].mp4
│       │   └── [animegrimoire] Kusuriya no Hitorigoto - 32 [720p][14452EAB].mp4
│       ├── Magic Maker - Isekai Mahou no Tsukurikata
│       │   ├── [animegrimoire] Magic Maker - Isekai Mahou no Tsukurikata - 01 [720p][550E859B].mp4
│       │   ├── [animegrimoire] Magic Maker - Isekai Mahou no Tsukurikata - 02 [720p][23392950].mp4
│       │   ├── [animegrimoire] Magic Maker - Isekai Mahou no Tsukurikata - 03 [720p][62E782A8].mp4
│       │   ├── [animegrimoire] Magic Maker - Isekai Mahou no Tsukurikata - 04 [720p][A69C2BD1].mp4
│       │   ├── [animegrimoire] Magic Maker - Isekai Mahou no Tsukurikata - 05 [720p][D5582170].mp4
│       │   ├── [animegrimoire] Magic Maker - Isekai Mahou no Tsukurikata - 06 [720p][24B14C5F].mp4
│       │   ├── [animegrimoire] Magic Maker - Isekai Mahou no Tsukurikata - 07 [720p][A6FEAA0E].mp4
│       │   ├── [animegrimoire] Magic Maker - Isekai Mahou no Tsukurikata - 08 [720p][8E7D721B].mp4
│       │   └── [animegrimoire] Magic Maker - Isekai Mahou no Tsukurikata - 09 [720p][439BB553].mp4
│       ├── Mahoutsukai Precure!! Mirai Days
│       │   ├── [animegrimoire] Mahoutsukai Precure!! Mirai Days - 01 [720p][43C8C55A].mp4
│       │   ├── [animegrimoire] Mahoutsukai Precure!! Mirai Days - 02 [720p][DFBBA33F].mp4
│       │   ├── [animegrimoire] Mahoutsukai Precure!! Mirai Days - 03 [720p][89219D5B].mp4
│       │   ├── [animegrimoire] Mahoutsukai Precure!! Mirai Days - 04 [720p][AE6907E8].mp4
│       │   ├── [animegrimoire] Mahoutsukai Precure!! Mirai Days - 05 [720p][B5151698].mp4
│       │   ├── [animegrimoire] Mahoutsukai Precure!! Mirai Days - 06 [720p][414AB8A].mp4
│       │   ├── [animegrimoire] Mahoutsukai Precure!! Mirai Days - 07 [720p][C91F0B22].mp4
│       │   └── [animegrimoire] Mahoutsukai Precure!! Mirai Days - 08 [720p][35FAB642].mp4
│       ├── Momentary Lily
│       │   ├── [animegrimoire] Momentary Lily - 01 [720p][9914E963].mp4
│       │   ├── [animegrimoire] Momentary Lily - 02 [720p][9A010B8].mp4
│       │   ├── [animegrimoire] Momentary Lily - 03 [720p][72BB404F].mp4
│       │   ├── [animegrimoire] Momentary Lily - 04 [720p][80C095CF].mp4
│       │   ├── [animegrimoire] Momentary Lily - 05 [720p][B4BBD0DD].mp4
│       │   ├── [animegrimoire] Momentary Lily - 06 [720p][F08FF918].mp4
│       │   ├── [animegrimoire] Momentary Lily - 07 [720p][6BF6F605].mp4
│       │   ├── [animegrimoire] Momentary Lily - 08 [720p][3392A45E].mp4
│       │   └── [animegrimoire] Momentary Lily - 10 [720p][666F9093].mp4
│       ├── NEET Kunoichi to Nazeka Dousei Hajimemashita
│       │   ├── [animegrimoire] NEET Kunoichi to Nazeka Dousei Hajimemashita - 01 [720p][E661308].mp4
│       │   ├── [animegrimoire] NEET Kunoichi to Nazeka Dousei Hajimemashita - 02 [720p][F74BA760].mp4
│       │   ├── [animegrimoire] NEET Kunoichi to Nazeka Dousei Hajimemashita - 03 [720p][E1B95F08].mp4
│       │   ├── [animegrimoire] NEET Kunoichi to Nazeka Dousei Hajimemashita - 04 [720p][1BF11DF].mp4
│       │   ├── [animegrimoire] NEET Kunoichi to Nazeka Dousei Hajimemashita - 05 [720p][F4170B14].mp4
│       │   ├── [animegrimoire] NEET Kunoichi to Nazeka Dousei Hajimemashita - 06 [720p][BFFCEFDA].mp4
│       │   ├── [animegrimoire] NEET Kunoichi to Nazeka Dousei Hajimemashita - 07 [720p][6BE0F305].mp4
│       │   ├── [animegrimoire] NEET Kunoichi to Nazeka Dousei Hajimemashita - 08 [720p][9F83C7D7].mp4
│       │   └── [animegrimoire] NEET Kunoichi to Nazeka Dousei Hajimemashita - 09 [720p][F3F6871F].mp4
│       ├── Nihon e Youkoso Elf-san
│       │   ├── [animegrimoire] Nihon e Youkoso Elf-san - 01 [720p][5E88B529].mp4
│       │   ├── [animegrimoire] Nihon e Youkoso Elf-san - 02 [720p][6B08F8C4].mp4
│       │   ├── [animegrimoire] Nihon e Youkoso Elf-san - 03 [720p][58BCAE0].mp4
│       │   ├── [animegrimoire] Nihon e Youkoso Elf-san - 04 [720p][33E224F0].mp4
│       │   ├── [animegrimoire] Nihon e Youkoso Elf-san - 05 [720p][B4B622D9].mp4
│       │   ├── [animegrimoire] Nihon e Youkoso Elf-san - 06 [720p][10507463].mp4
│       │   ├── [animegrimoire] Nihon e Youkoso Elf-san - 07 [720p][1C225BC6].mp4
│       │   └── [animegrimoire] Nihon e Youkoso Elf-san - 08 [720p][AF711B53].mp4
│       ├── Re Zero kara Hajimeru Isekai Seikatsu
│       │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 59 [720p][4366B656].mp4
│       │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 60 [720p][D84EBD8D].mp4
│       │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 61 [720p][ACC03EB2].mp4
│       │   ├── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 62 [720p][C99A348C].mp4
│       │   └── [animegrimoire] Re Zero kara Hajimeru Isekai Seikatsu - 63 [720p][D0506379].mp4
│       ├── Sorairo Utility
│       │   ├── [animegrimoire] Sorairo Utility - 01 [720p][C643EDEF].mp4
│       │   ├── [animegrimoire] Sorairo Utility - 02 [720p][D74854B1].mp4
│       │   ├── [animegrimoire] Sorairo Utility - 03 [720p][3EAD3335].mp4
│       │   ├── [animegrimoire] Sorairo Utility - 04 [720p][B44985D8].mp4
│       │   ├── [animegrimoire] Sorairo Utility - 05 [720p][37FCA9B2].mp4
│       │   ├── [animegrimoire] Sorairo Utility - 06 [720p][F46BA9D4].mp4
│       │   ├── [animegrimoire] Sorairo Utility - 07 [720p][23AF1AD3].mp4
│       │   ├── [animegrimoire] Sorairo Utility - 08 [720p][45386D3F].mp4
│       │   └── [animegrimoire] Sorairo Utility - 09 [720p][342F47EB].mp4
│       └── Wonderful Precure!
│           ├── [animegrimoire] Wonderful Precure! - 47 [720p][3A95A35B].mp4
│           ├── [animegrimoire] Wonderful Precure! - 48 [720p][6977E099].mp4
│           ├── [animegrimoire] Wonderful Precure! - 49 [720p][6F883352].mp4
│           └── [animegrimoire] Wonderful Precure! - 50 [720p][2AC0CFAB].mp4
└── Off-Season
    ├── Blue Archive the Animation
    │   ├── [animegrimoire] Blue Archive the Animation - 01v2 [720p][C7C3B3A6].mp4
    │   ├── [animegrimoire] Blue Archive the Animation - 02v2 [720p][8F889BCA].mp4
    │   ├── [animegrimoire] Blue Archive the Animation - 03v2 [720p][6175DD22].mp4
    │   ├── [animegrimoire] Blue Archive the Animation - 04v2 [720p][C0F7587E].mp4
    │   ├── [animegrimoire] Blue Archive the Animation - 05v2 [720p][C65EBF0A].mp4
    │   ├── [animegrimoire] Blue Archive the Animation - 06v2 [720p][BD21B44E].mp4
    │   ├── [animegrimoire] Blue Archive the Animation - 07v2 [720p][414068BC].mp4
    │   ├── [animegrimoire] Blue Archive the Animation - 08v2 [720p][F62AB6CC].mp4
    │   ├── [animegrimoire] Blue Archive the Animation - 09v2 [720p][2949D261].mp4
    │   ├── [animegrimoire] Blue Archive the Animation - 10v2 [720p][F4CDF0B7].mp4
    │   ├── [animegrimoire] Blue Archive the Animation - 11v2 [720p][322581F8].mp4
    │   └── [animegrimoire] Blue Archive the Animation - 12v2 [720p][8A296F5A].mp4
    ├── Boku no Kokoro no Yabai Yatsu
    │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 12.5 [720p][532BC2B2].mp4
    │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 13 [720p][98C0838A].mp4
    │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 14 [720p][FDD545AE].mp4
    │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 15 [720p][4B80A917].mp4
    │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 16 [720p][5E3A9DAF].mp4
    │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 17 [720p][8053AB00].mp4
    │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 18 [720p][F1D06DD5].mp4
    │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 19 [720p][C9267D6].mp4
    │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 20 [720p][50042065].mp4
    │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 21 [720p][88C7BD35].mp4
    │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 22 [720p][52A51A48].mp4
    │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 23 [720p][327B30F4].mp4
    │   ├── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 24 [720p][1CD9403C].mp4
    │   └── [animegrimoire] Boku no Kokoro no Yabai Yatsu - 25 [720p][D6F6418A].mp4
    ├── Hananoi-kun to Koi no Yamai
    │   ├── [animegrimoire] Hananoi-kun to Koi no Yamai - 01 [720p][69F7B97C].mp4
    │   ├── [animegrimoire] Hananoi-kun to Koi no Yamai - 02 [720p][F025610D].mp4
    │   ├── [animegrimoire] Hananoi-kun to Koi no Yamai - 03 [720p][DECCD229].mp4
    │   ├── [animegrimoire] Hananoi-kun to Koi no Yamai - 04 [720p][2A3528D4].mp4
    │   ├── [animegrimoire] Hananoi-kun to Koi no Yamai - 05 [720p][2D4E2F8D].mp4
    │   ├── [animegrimoire] Hananoi-kun to Koi no Yamai - 06 [720p][D9F14247].mp4
    │   ├── [animegrimoire] Hananoi-kun to Koi no Yamai - 07 [720p][8287E332].mp4
    │   ├── [animegrimoire] Hananoi-kun to Koi no Yamai - 08 [720p][75AC9801].mp4
    │   ├── [animegrimoire] Hananoi-kun to Koi no Yamai - 09 [720p][425AB948].mp4
    │   ├── [animegrimoire] Hananoi-kun to Koi no Yamai - 10 [720p][4649A2A5].mp4
    │   └── [animegrimoire] Hananoi-kun to Koi no Yamai - 12 [720p][4091D349].mp4
    ├── Inugami-san to Nekoyama-san
    │   ├── [animegrimoire] Inugami-san to Nekoyama-san - 01 [720p][A1C96619].mp4
    │   ├── [animegrimoire] Inugami-san to Nekoyama-san - 02 [720p][86E3E234].mp4
    │   ├── [animegrimoire] Inugami-san to Nekoyama-san - 03 [720p][D69E377A].mp4
    │   ├── [animegrimoire] Inugami-san to Nekoyama-san - 04 [720p][1E924772].mp4
    │   ├── [animegrimoire] Inugami-san to Nekoyama-san - 05 [720p][A6D21084].mp4
    │   ├── [animegrimoire] Inugami-san to Nekoyama-san - 06 [720p][26C1C02E].mp4
    │   ├── [animegrimoire] Inugami-san to Nekoyama-san - 07 [720p][87844B16].mp4
    │   ├── [animegrimoire] Inugami-san to Nekoyama-san - 08 [720p][6AE577A7].mp4
    │   ├── [animegrimoire] Inugami-san to Nekoyama-san - 09 [720p][62516353].mp4
    │   ├── [animegrimoire] Inugami-san to Nekoyama-san - 10 [720p][B61BE585].mp4
    │   ├── [animegrimoire] Inugami-san to Nekoyama-san - 11 [720p][EF1AE2B].mp4
    │   ├── [animegrimoire] Inugami-san to Nekoyama-san - 12 [720p][314D2B70].mp4
    │   └── [animegrimoire] Inugami-san to Nekoyama-san - 13 [720p][67E60A7C].mp4
    ├── Mob kara Hajimaru Tansaku Eiyuutan
    │   ├── [animegrimoire] Mob kara Hajimaru Tansaku Eiyuutan - 01 [720p][825C42DF].mp4
    │   ├── [animegrimoire] Mob kara Hajimaru Tansaku Eiyuutan - 02 [720p][48E7EB0].mp4
    │   ├── [animegrimoire] Mob kara Hajimaru Tansaku Eiyuutan - 03 [720p][27DBBB5].mp4
    │   ├── [animegrimoire] Mob kara Hajimaru Tansaku Eiyuutan - 04 [720p][D6447160].mp4
    │   ├── [animegrimoire] Mob kara Hajimaru Tansaku Eiyuutan - 05 [720p][B894AE9E].mp4
    │   ├── [animegrimoire] Mob kara Hajimaru Tansaku Eiyuutan - 06 [720p][76E936B6].mp4
    │   ├── [animegrimoire] Mob kara Hajimaru Tansaku Eiyuutan - 07 [720p][3728CB71].mp4
    │   ├── [animegrimoire] Mob kara Hajimaru Tansaku Eiyuutan - 08 [720p][20774FCE].mp4
    │   ├── [animegrimoire] Mob kara Hajimaru Tansaku Eiyuutan - 09 [720p][AF4B9157].mp4
    │   ├── [animegrimoire] Mob kara Hajimaru Tansaku Eiyuutan - 10 [720p][E65E1B2D].mp4
    │   ├── [animegrimoire] Mob kara Hajimaru Tansaku Eiyuutan - 11 [720p][9BE86A2E].mp4
    │   └── [animegrimoire] Mob kara Hajimaru Tansaku Eiyuutan - 12 [720p][B6A0F7EC].mp4
    ├── Odekake Kozame
    │   ├── [animegrimoire] Odekake Kozame - 00 [720p][CD86111B].mp4
    │   ├── [animegrimoire] Odekake Kozame - 01 [720p][709A97E1].mp4
    │   ├── [animegrimoire] Odekake Kozame - 02 [720p][493A2273].mp4
    │   ├── [animegrimoire] Odekake Kozame - 03 [720p][A131ADEB].mp4
    │   ├── [animegrimoire] Odekake Kozame - 04 [720p][F4BCCD9B].mp4
    │   ├── [animegrimoire] Odekake Kozame - 05 [720p][23319CC2].mp4
    │   ├── [animegrimoire] Odekake Kozame - 06 [720p][8666A6E2].mp4
    │   ├── [animegrimoire] Odekake Kozame - 07 [720p][2B906E64].mp4
    │   ├── [animegrimoire] Odekake Kozame - 08.5 [720p][DEEF175C].mp4
    │   ├── [animegrimoire] Odekake Kozame - 08 [720p][61CEDF42].mp4
    │   ├── [animegrimoire] Odekake Kozame - 09 [720p][3DE009D5].mp4
    │   ├── [animegrimoire] Odekake Kozame - 10 [720p][A6392520].mp4
    │   ├── [animegrimoire] Odekake Kozame - 11 [720p][13F715BC].mp4
    │   ├── [animegrimoire] Odekake Kozame - 12 [720p][E44D4617].mp4
    │   ├── [animegrimoire] Odekake Kozame - 13 [720p][C2283211].mp4
    │   ├── [animegrimoire] Odekake Kozame - 14 [720p][E3870D61].mp4
    │   ├── [animegrimoire] Odekake Kozame - 15 [720p][9026257C].mp4
    │   ├── [animegrimoire] Odekake Kozame - 16 [720p][D9FE07CF].mp4
    │   ├── [animegrimoire] Odekake Kozame - 17 [720p][865AB0FB].mp4
    │   ├── [animegrimoire] Odekake Kozame - 18 [720p][2174DFA8].mp4
    │   ├── [animegrimoire] Odekake Kozame - 19 [720p][9F0BD0BF].mp4
    │   ├── [animegrimoire] Odekake Kozame - 20 [720p][38D14D78].mp4
    │   ├── [animegrimoire] Odekake Kozame - 21 [720p][ADD7D036].mp4
    │   ├── [animegrimoire] Odekake Kozame - 22 [720p][6F71F265].mp4
    │   ├── [animegrimoire] Odekake Kozame - 23 [720p][E6746D29].mp4
    │   ├── [animegrimoire] Odekake Kozame - 24 [720p][5807DAFB].mp4
    │   ├── [animegrimoire] Odekake Kozame - 25 [720p][DDB05F].mp4
    │   ├── [animegrimoire] Odekake Kozame - 26 [720p][89404A85].mp4
    │   ├── [animegrimoire] Odekake Kozame - 27.5 [720p][7A89C52F].mp4
    │   ├── [animegrimoire] Odekake Kozame - 27 [720p][C6B2476C].mp4
    │   ├── [animegrimoire] Odekake Kozame - 29 [720p][6E62F720].mp4
    │   ├── [animegrimoire] Odekake Kozame - 30 [720p][6FF8EFD3].mp4
    │   ├── [animegrimoire] Odekake Kozame - 31 [720p][1565681C].mp4
    │   ├── [animegrimoire] Odekake Kozame - 33 [720p][A31E8CBD].mp4
    │   ├── [animegrimoire] Odekake Kozame - 34 [720p][8CD271E].mp4
    │   ├── [animegrimoire] Odekake Kozame - 35 [720p][3A0BCC5B].mp4
    │   ├── [animegrimoire] Odekake Kozame - 36 [720p][C6096884].mp4
    │   ├── [animegrimoire] Odekake Kozame - 37 [720p][94A8E232].mp4
    │   ├── [animegrimoire] Odekake Kozame - 38 [720p][4A614E22].mp4
    │   ├── [animegrimoire] Odekake Kozame - 39 [720p][D710611E].mp4
    │   ├── [animegrimoire] Odekake Kozame - 40 [720p][31284E40].mp4
    │   ├── [animegrimoire] Odekake Kozame - 41 [720p][5D2C3542].mp4
    │   ├── [animegrimoire] Odekake Kozame - 42 [720p][FDAFB4FC].mp4
    │   ├── [animegrimoire] Odekake Kozame - 43 [720p][DA626508].mp4
    │   ├── [animegrimoire] Odekake Kozame - 44 [720p][484B998C].mp4
    │   ├── [animegrimoire] Odekake Kozame - 45 [720p][E95397E8].mp4
    │   ├── [animegrimoire] Odekake Kozame - 46 [720p][45239795].mp4
    │   ├── [animegrimoire] Odekake Kozame - 47 [720p][71B34C48].mp4
    │   ├── [animegrimoire] Odekake Kozame - 48 [720p][F443CDB3].mp4
    │   ├── [animegrimoire] Odekake Kozame - 49 [720p][F490EE36].mp4
    │   ├── [animegrimoire] Odekake Kozame - 50 [720p][B3479030].mp4
    │   ├── [animegrimoire] Odekake Kozame - 51 [720p][30A71B22].mp4
    │   ├── [animegrimoire] Odekake Kozame - 52 [720p][7A46674].mp4
    │   ├── [animegrimoire] Odekake Kozame - 53 [720p][197E9EC0].mp4
    │   ├── [animegrimoire] Odekake Kozame - 54 [720p][35D1179F].mp4
    │   ├── [animegrimoire] Odekake Kozame - 55 [720p][60050F08].mp4
    │   ├── [animegrimoire] Odekake Kozame - 56 [720p][D29D0528].mp4
    │   ├── [animegrimoire] Odekake Kozame - 57 [720p][7AF08E5C].mp4
    │   ├── [animegrimoire] Odekake Kozame - 58 [720p][5A1CF6EC].mp4
    │   ├── [animegrimoire] Odekake Kozame - 59 [720p][44BF39D4].mp4
    │   └── [animegrimoire] Odekake Kozame - 60 [720p][D8DC4E3].mp4
    ├── Tensei Kizoku, Kantei Skill de Nariagaru
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 01 [720p][C2635A75].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 02 [720p][E8D74C06].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 03 [720p][748E41E6].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 04 [720p][2CFE4DC5].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 05 [720p][A05F3AE8].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 06 [720p][380692AA].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 07 [720p][F6CC6D25].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 08 [720p][AC9A7DE1].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 09 [720p][333B9827].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 10 [720p][62DEF434].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 11 [720p][31CA1F57].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 12 [720p][2140EAAC].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 14 [720p][F0936EB5].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 15 [720p][9128EFCA].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 16 [720p][E2355F96].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 17 [720p][EBAB93C7].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 18 [720p][CA2825DA].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 19 [720p][FDA47941].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 20 [720p][E9818198].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 21 [720p][956B3EB5].mp4
    │   ├── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 22 [720p][D85643DA].mp4
    │   └── [animegrimoire] Tensei Kizoku, Kantei Skill de Nariagaru - 23 [720p][6985392F].mp4
    ├── Uramichi Oniisan
    │   ├── [animegrimoire] Uramichi Oniisan - 01v2 [720p][6A49D740].mp4
    │   ├── [animegrimoire] Uramichi Oniisan - 02v2 [720p][D2CD4009].mp4
    │   ├── [animegrimoire] Uramichi Oniisan - 03v2 [720p][6F786500].mp4
    │   ├── [animegrimoire] Uramichi Oniisan - 04 [720p][D34A3ECB].mp4
    │   ├── [animegrimoire] Uramichi Oniisan - 05v2 [720p][DF3C0989].mp4
    │   ├── [animegrimoire] Uramichi Oniisan - 06v2 [720p][4B86B379].mp4
    │   ├── [animegrimoire] Uramichi Oniisan - 07v2 [720p][FD58BEA].mp4
    │   ├── [animegrimoire] Uramichi Oniisan - 08v2 [720p][E5E18EFF].mp4
    │   ├── [animegrimoire] Uramichi Oniisan - 09v2 [720p][116163C2].mp4
    │   ├── [animegrimoire] Uramichi Oniisan - 10v2 [720p][418D615].mp4
    │   ├── [animegrimoire] Uramichi Oniisan - 11v2 [720p][1F1C81E2].mp4
    │   ├── [animegrimoire] Uramichi Oniisan - 12v2 [720p][DF289994].mp4
    │   └── [animegrimoire] Uramichi Oniisan - 13 [720p][806FDFCD].mp4
    └── Yuru Camp
        ├── [animegrimoire] Yuru Camp - 01 [720p][990F9507].mp4
        ├── [animegrimoire] Yuru Camp - 02 [720p][E0D9D152].mp4
        ├── [animegrimoire] Yuru Camp - 03 [720p][2B882E80].mp4
        ├── [animegrimoire] Yuru Camp - 04 [720p][66D11D17].mp4
        ├── [animegrimoire] Yuru Camp - 05 [720p][B31AC4AA].mp4
        ├── [animegrimoire] Yuru Camp - 06 [720p][F24C469E].mp4
        ├── [animegrimoire] Yuru Camp - 07 [720p][743F5F42].mp4
        ├── [animegrimoire] Yuru Camp - 08 [720p][68EEF2BD].mp4
        ├── [animegrimoire] Yuru Camp - 09 [720p][B3C5E87F].mp4
        ├── [animegrimoire] Yuru Camp - 10 [720p][805EC1B3].mp4
        ├── [animegrimoire] Yuru Camp - 11 [720p][2F34A06F].mp4
        └── [animegrimoire] Yuru Camp - 12 [720p][78466F1D].mp4


637 directories, 6174 files
```