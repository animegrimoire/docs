# Simple Guide to use Windows encoder

## 1. Extract file

![Extract downloaded file](https://i.ibb.co/Yt5jB4r/001.png "Extract downloaded file")

## 2. Put source file (*.mkv) to `animegrimoire-encoder\source\` folder

![Place source file](https://i.ibb.co/3Wtc4B2/002.png "Place source file")

## 3. Start `launch_encoder.bat`

![Double-click to launch](https://i.ibb.co/PGS251z/003.png "Double-click to launch")

## 4. Wait for encoder to process files

![The encoding process might take some time, depending on your hardware resources.](https://i.ibb.co/TYMQG2m/004.png "The encoding process might take some time, depending on your hardware resources.")

## 5. Encoding finished, check encoded file in `animegrimoire-encoder\encoded_mp4\` folder

![Finished](https://i.ibb.co/YXXBmCV/005.png "Finished")
