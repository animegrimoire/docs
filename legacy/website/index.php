<?php
/*(Trigger 1: Create sqlite db)
avi@rhel8:~/sqlite$ sqlite3 animegrimoire.db
SQLite version 3.27.2 2019-02-25 16:06:06
Enter ".help" for usage hints.
sqlite> CREATE TABLE eventlog (
   ...> id INTEGER PRIMARY KEY NOT NULL,
   ...> file_name TEXT NOT NULL,
   ...> size TEXT NOT NULL,
   ...> time TEXT NOT NULL,
   ...> event TEXT NOT NULL,
   ...> machine_id TEXT NOT NULL
   ...> );
   ...> .exit
(Trigger 2: Insert Data)
sqlite> INSERT INTO eventlog values({'$id','$file_name','$size','$time','$event','$machine_id'});
(Trigger 3: Build PHP)*/
$pdo = new PDO('sqlite:animegrimoire.db');
$stm = $pdo->query("SELECT * FROM eventlog ORDER BY id;");
$rows = $stm->fetchAll(PDO::FETCH_NUM);
echo '<!DOCTYPE html>';
echo '<html lang="en-US">';
echo '<head>';
echo '<meta charset="UTF-8" />';
echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
echo '<title>Animegrimoire EventLogs Database</title>';
echo '<link rel="shortcut icon" type="image/jpg" href="favicon.ico"/>';
echo '<link rel="icon" href="favicon.ico" type="image/x-icon">';
echo '<meta name="author" content="Aurora Team" />';
echo '<meta name="contact" content="abuse@animegrimoire.moe" />';
echo '<meta name="copyright" content="animegrimoire" />';
echo '<meta name="description" content="Animegrimoire Event Log Database"/>';
echo '<meta name="keywords" content="animegrimoire logs, encoded episode"/>';
echo '<style type="text/css">.tg {border-collapse:collapse;border-spacing:0;}.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:12px; overflow:hidden;padding:5px 5px;word-break:normal;}.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:12px; font-weight:normal;overflow:hidden;padding:5px 5px;word-break:normal;}.tg .tg-amwm{font-weight:bold;text-align:center;vertical-align:top}.tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}.tg .tg-0lax{text-align:left;vertical-align:top}</style>';
echo '</head>';
echo '<table class="tg">';
echo '<tbody>';
echo '<tr>';
echo '<td class="tg-amwm">ID</td>';
echo '<td class="tg-amwm">File Name</td>';
echo '<td class="tg-amwm">Size (bytes)</td>';
echo '<td class="tg-amwm">Time</td>';
echo '<td class="tg-amwm">Event</td>';
echo '<td class="tg-amwm">Machine ID</td>';
echo '</tr>';
foreach ($rows as $row)
{
    echo '<tr>';
    echo "<td class=tg-0lax>$row[0]</td>";
    echo "<td class=tg-0lax>$row[1]</td>";
    echo "<td class=tg-0lax>$row[2]</td>";
    echo "<td class=tg-0lax>$row[3]</td>";
    echo "<td class=tg-0lax>$row[4]</td>";
    echo "<td class=tg-0lax>$row[5]</td>";
    echo '</tr>';
}
echo '</tbody>';
echo '</table>';
echo '</html>';
?>

