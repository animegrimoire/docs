@echo off
color 0a
:: LIBRARY POSITIONS
set ffmpeg=.\lib\ffmpeg
set preset=.\lib\x264_animegrimoire.json
set HandBrakeCLI=.\lib\HandBrakeCLI
set rename=.\lib\etc\renamer\arenc.exe
setlocal enabledelayedexpansion
TITLE Animegrimoire Encoder
:: ADD DIRECTORY FOR OUTPUT
if not exist .\temp mkdir .\temp
if not exist .\fonts mkdir .\fonts
if not exist .\watermarked mkdir .\watermarked
if not exist .\encoded_mp4 mkdir .\encoded_mp4
if not exist .\finished mkdir .\finished

:: Prepare Files
%rename% -e .\lib\etc\renamer\sanitize.conf -t files -p .\source -o filename -m rename

:: Font Handler
for /f "delims=|" %%f in ('dir /b .\source\') do %ffmpeg% -nostats -loglevel 0 -dump_attachment:t "" -i ".\source\%%~nxf" -y
@powershell -Command "Get-ChildItem -Recurse -Include '*.*tf','*.*tc','*.*TF','*.*TC' | Move-Item -Force -Destination .\fonts"
@powershell -NoProfile -ExecutionPolicy Bypass -Command "& '.\lib\installfont.ps1'"

:: Launch Encoder
for /f "delims=|" %%f in ('dir /b .\source\') do start /wait .\lib\animegrimoire.bat ".\source\%%~nxf"

exit
