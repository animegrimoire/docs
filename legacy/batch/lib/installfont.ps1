# Set variable
$fontsPath     =".\fonts"
$objShell      = New-Object -ComObject Shell.Application
$objFolder     = $objShell.Namespace(0x14)
$localSysPath  = "$Env:USERPROFILE\AppData\Local\Microsoft\Windows\Fonts"
$localSysFonts = Get-ChildItem -Path $localSysPath -Recurse -File -Name | ForEach-Object -Process {[System.IO.Path]::GetFileNameWithoutExtension($_)}
# Install font
ForEach ($font in (dir $fontsPath -Recurse -Include '*.*tf','*.*tc','*.*TF','*.*TC')){
    if ($localSysFonts -like $font.BaseName) {
        Write-Output "Skip: ${font} already exists in ${localSysPath}"
    }
	else {
        Write-Output "Installing: Font ${font}"
        $objFolder.CopyHere($font.FullName)
        $firstInstall = $true
    }
}
