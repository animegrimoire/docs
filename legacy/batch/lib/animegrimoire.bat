@echo off
color 0a
:: VARIABLE NAME
set filename=%~n1
set fullpath=%~f1
set underscoreFilename=%filename: =_%
set underscoreFilename=%underscoreFilename:[=%
set underscoreFilename=%underscoreFilename:]=%
set subtitle=%underscoreFilename%_subtitle
set subtitleTemp=%underscoreFilename%_temp_subtitle
set videoNoSubtitle=%underscoreFilename%_no_sub
TITLE Encoding: %~n1

:: Make sure only one instance of HandBrakeCLI running, otherwise ask user to confirm job termination
echo Checking another HandBrakeCLI process..
@powershell -Command "Get-Process -Name HandBrakeCLI -ErrorAction SilentlyContinue | Stop-Process -Confirm -PassThru"

:: watermark
set watermarkStyle=Style: Watermark,Worstveld Sling,20,^^^&H00FFFFFF,^^^&H00FFFFFF,^^^&H00FFFFFF,^^^&H00FFFFFF,0,0,0,0,100,100,0,0,1,0,0,9,0,5,0,1
set watermarkText=Dialogue: 0,0:00:00.00,0:00:04.00,Watermark,,0000,0000,0000,,animegrimoire.moe
:: FOR SEARCHING LINE IN TEXT
set line1find=Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, Encoding
set line2find=Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text

:: extract subtitle
%ffmpeg% -nostats -loglevel 0 -i "%fullpath%" -map 0:s ".\temp\%subtitle%.ass" -y

:: demux file, remove original subtitle
%ffmpeg% -nostats -loglevel 0 -i "%fullpath%" -map 0 -map 0:s -codec copy ".\temp\%videoNoSubtitle%.mkv" -y

:: add watermark, using powershell
@powershell -Command "get-content ".\temp\%subtitle%.ass" | %%{$_ -replace \"%line1find%\",\"%line1find%`r`n%watermarkStyle%\"}" >> ".\temp\%subtitleTemp%01.ass"
@powershell -Command "get-content ".\temp\%subtitleTemp%01.ass" | %%{$_ -replace \"%line2find%\",\"%line2find%`r`n%watermarkText%\"}" >> ".\temp\%subtitleTemp%02.ass"

:: send back watermark to mkv
%ffmpeg% -nostats -loglevel 0 -i ".\temp\%videoNoSubtitle%.mkv"  -i ".\temp\%subtitleTemp%02.ass" -c:v copy -c:a copy -c:s copy -map 0:0 -map 0:1 -map 1:0 -metadata:s:a:0 language=jpn -metadata:s:s:0 language=eng ".\watermarked\%watermarkedFilename%.mkv" -y

:: encode file
%HandBrakeCLI% --preset-import-file %preset% -Z "x264_Animegrimoire" -i ".\watermarked\%watermarkedFilename%.mkv" -o ".\encoded_mp4\%filename%.mp4"

:: cleaning up
del .\temp\%subtitle%.ass
del .\temp\%subtitleTemp%01.ass
del .\temp\%subtitleTemp%02.ass
del .\temp\%videoNoSubtitle%.mkv
del .\watermarked\%watermarkedFilename%.mkv
del /Q .\fonts\*
move "%~f1" .\finished\

exit
