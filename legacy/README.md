# Legacy Documentation

The following documentation is considered legacy, meaning it has either been surpassed or replaced. These documents are provided for archival purposes.
