import os  
import os.path  
from tabulate import tabulate
from datetime import date  

season_order = ['Winter', 'Spring', 'Summer', 'Fall']

rows = []

for root, _, files in os.walk("/home/Archive"):  
    year = root.split("/")[-3]
    season = root.split("/")[-2]
    if year.isdigit():
        for file in files:
            if os.path.splitext(file)[1] in ['.mp4', '.mkv']:
                size = os.path.getsize(os.path.join(root, file)) / 1048576  
                size = round(size, 2)
                season_year = season + " " + year  
                rows.append([season_year, size, file])

rows.sort(key=lambda x: (int(x[0].split(" ")[1]), season_order.index(x[0].split(" ")[0])))

total_size = round(sum(row[1] for row in rows) / 1024, 2)
total_files = len(rows)
total_folders = len(set(row[0] for row in rows))
summary = f"## {total_size}GB, {total_files} Files, {total_folders} Folders. ({date.today().strftime('%d-%m-%Y')})"

with open("Grimoire_Archive.md", "w") as f:  
    f.write("# The Grimoire Archive\n\n")
    f.write(summary + "\n\n")
    f.write(tabulate(rows, headers=["Season", "Size (MB)", "File"], tablefmt="pipe"))
