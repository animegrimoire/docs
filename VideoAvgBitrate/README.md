# Comparison of VideoAvgBitrate Output

At line 93 of the [x264_Animegrimoire.json](https://gitlab.com/animegrimoire/animegrimoire/-/blob/master/config/x264_Animegrimoire.json?ref_type=heads#L93) file, you will find the following bitrate control settings:

```json
            "VideoAvgBitrate": 640,
```

Currently, our project utilizes a bitrate of 640kbps. You can compare the results to the following:

| VideoAvgBitrate | 512kbps | 550kbps | 640kbps | 769kbps | 1024kbps | 2048kbps | 3072kbps | 4096kbps | original-source |
|-----------------|---------|---------|---------|---------|-----------|-----------|-----------|-----------|-----------------|
| Frame 360       | ![img](img/encoded-VideoAvgBitrate_512KBps-360.png) | ![img](img/encoded-VideoAvgBitrate_550KBps-360.png) | ![img](img/encoded-VideoAvgBitrate_640KBps-360.png) | ![img](img/encoded-VideoAvgBitrate_768KBps-360.png) | ![img](img/encoded-VideoAvgBitrate_1024KBps-360.png) | ![img](img/encoded-VideoAvgBitrate_2048KBps-360.png) | ![img](img/encoded-VideoAvgBitrate_3072KBps-360.png) | ![img](img/encoded-VideoAvgBitrate_4096KBps-360.png) | ![img](img/source-original-360.png) |
| Frame 720       | ![img](img/encoded-VideoAvgBitrate_512KBps-720.png) | ![img](img/encoded-VideoAvgBitrate_550KBps-720.png) | ![img](img/encoded-VideoAvgBitrate_640KBps-720.png) | ![img](img/encoded-VideoAvgBitrate_768KBps-720.png) | ![img](img/encoded-VideoAvgBitrate_1024KBps-720.png) | ![img](img/encoded-VideoAvgBitrate_2048KBps-720.png) | ![img](img/encoded-VideoAvgBitrate_3072KBps-720.png) | ![img](img/encoded-VideoAvgBitrate_4096KBps-720.png) | ![img](img/source-original-720.png) |
| Frame 1080      | ![img](img/encoded-VideoAvgBitrate_512KBps-1080.png) | ![img](img/encoded-VideoAvgBitrate_550KBps-1080.png) | ![img](img/encoded-VideoAvgBitrate_640KBps-1080.png) | ![img](img/encoded-VideoAvgBitrate_768KBps-1080.png) | ![img](img/encoded-VideoAvgBitrate_1024KBps-1080.png) | ![img](img/encoded-VideoAvgBitrate_2048KBps-1080.png) | ![img](img/encoded-VideoAvgBitrate_3072KBps-1080.png) | ![img](img/encoded-VideoAvgBitrate_4096KBps-1080.png) | ![img](img/source-original-1080.png) |
| Frame 1437      | ![img](img/encoded-VideoAvgBitrate_512KBps-1437.png) | ![img](img/encoded-VideoAvgBitrate_550KBps-1437.png) | ![img](img/encoded-VideoAvgBitrate_640KBps-1437.png) | ![img](img/encoded-VideoAvgBitrate_768KBps-1437.png) | ![img](img/encoded-VideoAvgBitrate_1024KBps-1437.png) | ![img](img/encoded-VideoAvgBitrate_2048KBps-1437.png) | ![img](img/encoded-VideoAvgBitrate_3072KBps-1437.png) | ![img](img/encoded-VideoAvgBitrate_4096KBps-1437.png) | ![img](img/source-original-1437.png) |
| file result     | [Link](files/encoded-VideoAvgBitrate_512KBps.mp4) | [Link](files/encoded-VideoAvgBitrate_550KBps.mp4) | [Link](files/encoded-VideoAvgBitrate_640KBps.mp4) | [Link](files/encoded-VideoAvgBitrate_768KBps.mp4) | [Link](files/encoded-VideoAvgBitrate_1024KBps.mp4) | [Link](files/encoded-VideoAvgBitrate_2048KBps.mp4) | [Link](files/encoded-VideoAvgBitrate_3072KBps.mp4) | [Link](files/encoded-VideoAvgBitrate_4096KBps.mp4) | [Link](files/source-original.mkv) |
